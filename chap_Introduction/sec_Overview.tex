\section{From seismic acquisition to interpretation}
Seismic experiments are in common use in the oil and gas industry for the subsurface imaging and reservoir management of a reservoir. Seismic waves can travel over long distances and are thus well suited to the study of the geologic structures involved in oil production, located at a depth of a few kilometres in the subsurface. Seismic exploration uses active seismic experiments, meaning the source at the origin of wave propagation is artificially triggered, contrary to passive seismic considered in seismology for the study of natural earthquakes. We review the main steps of active seismic experiments in this section. The reader is referred to \textcite{sheriff_exploration_2006,yilmaz_seismic_2001} for more details.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Seismic data acquisition}
Seismic surveys can take place in land or marine environments. In the usual land acquisition, the source is a vibrating truck and an array of receivers called geophones measures the motion of particles. They are evenly placed at the surface along one or several lines (\cref{fig:surface_acquisition}).
The experiment is run many times with different source and receiver positions. Under the approximation of single scattered energy, a point of the subsurface is in this way illuminated through many different angles. The presence of a drilled borehole can also provide a different acquisition geometry called \gls{vsp} with sources at the surface and receivers in the well (\cref{fig:acq_vsp}). In the case of cross-well acquisition, sources and receivers are located in two different wells. In marine acquisition, the source is an air-gun and receivers called hydrophones sensitive to the pressure are distributed along several streamers towed by a specific vessel. Alternatively receivers can be located at the sea floor in a display called \gls{obc} (\cref{fig:acq_obc}).
%-------------------------------------
\begin{figure}[!htbp]
%%% 
  \begin{subfigure}[b]{\linewidth}
    \includegraphics[scale=0.6]{Images/Seismic_Acquisition}
    \caption{surface acquisition (taken from Danish Energy Agency).}
    \label{fig:surface_acquisition}
  \end{subfigure}
  %%%
  \begin{subfigure}[b]{0.49\linewidth}
    \includegraphics[]{Figures/acquisition_vsp}
    \caption{VSP acquisition.\label{fig:acq_vsp}}
  \end{subfigure}\hfill%
  %%% 
  \begin{subfigure}[b]{0.49\linewidth}
    \includegraphics[]{Figures/acquisition_obc}
    \caption{\gls{obc} acquisition.\label{fig:acq_obc}}
  \end{subfigure}
  %%% 
\caption{Acquisition geometries for different types of seismic surveys.}
\label{fig:other_acquisitions}
\end{figure}
%-------------------------------------

The data recorded by a receiver is a function of time and is usually called a \emph{trace}. The collection of traces recorded during an experiment is represented on a panel called \emph{shot gather} with the time on the vertical axis, and the distance between source and receiver (also called offset) on the horizontal axis (\cref{fig:data_example}).
%-----------------------------------
\begin{figure}[!htbp]
%\includegraphics[scale=0.5]{Images/data_record_robein}
\includegraphics[scale=0.5]{Images/anandakrishan_2}
\caption{An example of a 2D seismic data recorded in marine acquisition
 %(from \textcite{robein_seismic_2010})
 \parencite[from][]{anandakrishnan_influence_1998}.
 }
\label{fig:data_example}
\end{figure}
%-----------------------------------

A shot gather records the Earth's response to the source excitation.
Many events corresponding to different kinds of waves can be observed. We can distinguish between pressure waves (P-waves), where particles move parallel to the wave propagation direction, and shear waves (S-waves), where the particle motion is orthogonal to the wave propagation \parencite{aki_quantitative_2002}. Besides, energy travels as surface and body waves. Surface waves are more energetic and provide information about the near-surface \parencite{socco_surface-wave_2004,perez_solano_imagerie_2013}. They are used in seismology but commonly considered as noise in seismic exploration which uses body waves. The latter can be classified according to their path in the subsurface:
\begin{itemize}
	\item transmitted waves, such as direct and diving waves travel between the source and the receiver without being reflected;	
	\item (primary-)reflected and diffracted waves are generated at discontinuities of the Earth with strong impedance contrast;
	\item refracted waves travel along these interfaces;
	\item multiple reflected waves are caused by strong reflectors such as the sea surface or the water bottom. Upgoing energy is sent back to the subsurface and can reflect a few times on the subsurface structures before being recorded.
\end{itemize}

\subsection{Physical modelling}
For the determination of the subsurface parameters, we first need to define a physical law representative for the wave propagation observed on data recordings. It is mathematically formalised through a partial differential equation. The most general visco-elastic wave equation involves the density, the attenuation of P and S waves and the 21 elastic coefficient of the stiffness tensor relating the stress tensor to the strain tensor. Simplified physics is usually assumed. If the subsurface is considered isotropic, the stiffness tensor reduces to the Lamé parameters \(\lambda\) and \(\mu\). The simplest approximation is the isotropic acoustic case. The Earth is assimilated to a fluid parametrised by the P-wave velocity \(V_P\) and the density \(\rho\). Then only P-waves are considered and reflected events are caused by rapid variations of the acoustic impedance \(I_P= \rho V_P\). Note that the model parameters are function of the spatial coordinates: even for the constant density acoustic case, a large number of parameters should be specified for solving the wave equation.

The simulation of wave propagation is usually performed with a finite-difference resolution of the wave-equation \parencite{virieux_p-sv_1986,levander_fourthorder_1988,operto_3d_2007}, or with finite element schemes \parencite[see][for a review]{virieux_review_2011}. Modelling can also rely on a high-frequency approximation such as ray theory \parencite{cerveny_seismic_2005}. The result of the forward problem is synthetic data calculated for all subsurface points, in particular at the receiver positions.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Preprocessing}
Earth's parameters cannot be directly inverted from seismic measurements. Usually, preprocessing is needed before subsequent imaging steps. The first processing stage consists of selecting the wavefield in the data that will be used for the analysis and of attenuating the noise inherent to every practical experiment \parencite{yilmaz_seismic_2001}. Many seismic imaging procedures rely on primary reflection data only and consider other events like transmitted waves or multiples as coherent noise. These should be removed from the data before further analysis. In particular, the removal of coherent noise such as multiples has been an intense research topic \parencite{verschuur_seismic_2013} and will be reviewed in \cref{sec:multiple_removal}.

However, with the improvement of seismic imaging algorithms and computation capabilities, recent developments attempt to use all the information available in the data, like multiples or transmitted events, and more generally the full wavefield. In particular, the purpose of this study is to investigate the use of multiple reflections as signal rather than noise.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Definition of an objective function}
%The purpose of the inverse problem is to find a model of the Earth consistent with observed data.
The accuracy of model parameters used during modelling can be assessed by comparing observed data and calculated data. For a quantitative estimation, a scalar \emph{objective} or \emph{cost function} is defined on the space of admissible models. It is designed such that the best model is a global minimiser or maximiser of the function. It can be defined directly in the data-domain, measuring for example the differences between observed and calculated data in the least-squares sense \parencite{tarantola_inversion_1984}. Alternative formulations in the image domain detailed in \cref{sec:mva} rely on focusing or coherency criteria defined on reconstructed images of the Earth \parencite{alyahya_velocity_1989,symes_migration_2008}. A simple example of image-domain strategy will be given at the end of this section.

The purpose of the inverse problem is to find a set of parameters minimising the objective function. In practice, it is an ill-posed problem, meaning that several models can explain perfectly the data \parencite{tarantola_inverse_2005}, due to an imperfect illumination of the subsurface or to coupling between parameters. This issue can be mitigated by adding a regularisation term to the objective function, usually enforcing the smoothness of the recovered model, or its consistency with a priori information about the subsurface \parencite{asnaashari_regularized_2013}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Solving the inverse problem}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\minisec{Optimisation strategy}
The objective of the inverse problem is to determine a set of model parameters which minimise the objective function through an optimisation procedure. The most general approach involves global optimisation methods \parencite{sen_global_2013} which requires only the ability to compute the value of the objective function. Examples of such methods are simulated annealing \parencite{kirkpatrick_optimization_1983,ingber_very_1989}, genetic algorithms \parencite{holland_adaptation_1975} or particle swarm optimisation \parencite{kennedy_particle_1995}. The drawback of these strategies is their computational cost, as numerous evaluation of the objective function are required.

Local optimisation methods \parencite{nocedal_numerical_2006} are a less expensive alternative.
%Starting from an initial model, the gradient of the objective function is used to determine a \emph{descent direction}, such that the value of the objective function will decrease at the next iteration.
The gradient of the objective function is used to iteratively update an initial guess of the model, such that the value of the objective function decreases with iterations.
Local optimisation requires the ability of computing the gradient of the objective function with respect to the model parameters. The adjoint-state method \parencite{plessix_review_2006,chavent_nonlinear_2009} provides a computationally efficient way of performing this derivation.

In practice the objective function is not necessarily convex because of the non-linear relationship between data and model parameters: gradient-based method may converge to a local minima. Therefore an other requirement for local optimisation is the knowledge of an initial model sufficiently close to the true model. Inversion strategies considering first the low-frequency content of the data and progressively incorporating higher-frequencies help mitigating the non-linear behaviour of the objective function and relax the requirement of an accurate initial model \parencite{bunks_multiscale_1995}. Strategies to define convex objective functions will be detailed in \cref{sec:Intro_Inverse_Problem,sec:mva}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\minisec{Multiparameter inversion}
Most applications of inversion techniques consider a mono-parameter description of wave propagation with the pressure-wave velocity \(V_P\). Moving to a multi-parameter inversion is challenging \parencite{operto_guided_2013}. As pointed out before, due to limited acquisition with sources and receivers at the surface, different classes of parameters are coupled, meaning that they have a similar impact on seismic data. This issue is known as \emph{cross-talk} or \emph{trade-off} between parameter.
%Moreover the number of degrees of freedom and the ill-posedness of the inversion is increased as several classes of parameters are added.
To overcome this difficulty, a suitable parametrisation is needed. In the acoustic example, the parameter couples can be \(\paren{V_P,\rho}\) or \(\paren{V_P,I_P}\) depending of the acquisition \parencite{virieux_overview_2009,zhou_full_2015,zhou_velocity_2016}.
%A hierarchichal strategy inverting successively for different classes of parameters helps to stabilise the inversion. In the elastic case, one may resolve first for \(V_P\) and \(I_P\), then \(V_S\) and  \(I_S\) and finally for the density \parencite{tarantola_strategy_1986}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\minisec{Scale separation}
In the following we consider a monoparameter inversion with the P-wave velocity model, noted \(c\vecx\) from now on, as unknown. \(\vec{x}=\paren{x,y,z}\) or \(\paren{x,z}\) denotes the spatial coordinates. The resolution of the velocity model image that can be recovered is limited by the frequency band of the data (typically \SIrange[range-units=single]{5}{70}{\hertz}) and the data acquisition setup. The model resolution is better described with spatial frequencies. As illustrated by the well-known sketch of \textcite{claerbout_imaging_1985} (\cref{fig:claerbout_spatial_frequencies}), two separate ranges of spatial frequencies of the model can be reconstructed from seismic data \parencite{jannane_wavelengths_1989}, leading to a scale separation of the velocity model (\cref{fig:velocity_decomp_marmousi}):
\begin{itemize}
	\item the smooth slowly-varying component of the model (\cref{fig:Marmousi_c0}), called the \emph{background velocity model}, or \emph{macro-model}, controlling the kinematics of wave-propagation;
	\item rapid changes in the components of the model, responsible for the reflections, referred to as \emph{reflectivity} (\cref{fig:Marmousi_deltac}). This part can be physically interpreted as the Earth discontinuities.
\end{itemize}
%--------------------------------------------------
\begin{figure}[!htbp]
\includegraphics[scale=0.4]{Images/claerbout_1985}
\caption{Spatial frequencies that can be resolved from seismic data \parencite[from][]{claerbout_imaging_1985}.}
\label{fig:claerbout_spatial_frequencies}
\end{figure}
%--------------------------------------------------
%-----------------------------------
\begin{figure}[!htbp]
  \begin{subfigure}[t]{0.32\linewidth}
    \includegraphics[width=\linewidth]{Images/Marmousi_c}
    \caption{Velocity model \(c\paren{\vec{x}}\).}
    \label{fig:Marmousi_c}
  \end{subfigure}\hfill%
  %%%
  \begin{subfigure}[t]{0.32\linewidth}
    \includegraphics[width=\linewidth]{Images/Marmousi_c0}
    \caption{Background model \(c_0\paren{\vec{x}}\).}
    \label{fig:Marmousi_c0}
  \end{subfigure}\hfill%
  %%% 
  \begin{subfigure}[t]{0.32\linewidth}
    \includegraphics[width=\linewidth]{Images/Marmousi_deltac}
    \caption{Model perturbation \(\delta c\paren{\vec{x}}\).}
    \label{fig:Marmousi_deltac}
  \end{subfigure}
  %%%
%\includegraphics[scale=0.5]{Images/Scale_Separation_Marmousi}
\caption{Illustration of the usual scale separation for the Marmousi model \parencite{brougois_marmousi_1990}. The complete velocity model \(c\paren{ \vec{x}}\) is decomposed into a smooth background part \(c_0\paren{\vec{x}}\) and a perturbation \(\delta c\paren{\vec{x}}\) of the background model \(c_0\)  \parencite[adapted from][]{billette_estimation_1998}.}
\label{fig:velocity_decomp_marmousi}
\end{figure}
%-----------------------------------

Note that since the publication of Claerbout's book, the frequency gap has been progressively filled by improvement of seismic source design allowing to record lower frequencies in the data and the progress of imaging techniques allowing to recover a more detailed background velocity model \parencite{nichols_resolution_2012,lambare_recent_2014}. However, the traditional scale separation remains the theoretical basis of many seismic imaging methods.

The scale separation can be written explicitly
\begin{equation}
	c\vecx=c_0\vecx+\delta c\vecx\text{,}
\end{equation}
where \(c_0\) is the background velocity model and \(\delta c\) is the reflectivity.
If \(\abs{\delta c} \ll \abs{c_0}\), the reflectivity can be seen as a perturbation of the background velocity model. Under the \emph{Born approximation}, the relationship between primary reflection data and model perturbation \(\delta c\) is linear. Data still depend non-linearly on the background velocity model \(c_0\), though.

The large and short-scale structures of the velocity model can be inverted simultaneously without scale separation, as in \gls{fwi} presented in \cref{sec:fwi}. Alternatively, they can be recovered separately.
\emph{Migration} algorithms are designed to convert events recorded in time into a reflectivity map of the model perturbation \(\delta c\) function of depth. Under the Born approximation, the determination of  \(\delta c\) is a linear inverse problem. Performing this conversion requires the prior knowledge of the background velocity model. The determination of the long-scale structure of the velocity model is performed with \emph{tomographic} approaches, either in the data-domain \parencite{bishop_tomographic_1985} or in the image-domain \parencite{symes_migration_2008}. Migration and tomography strategies will be reviewed in \cref{sec:migration,sec:tomographic_methods,sec:mva}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Postprocessing}
Finally the quantitative knowledge of physical properties provided by the inversion is used to determine a model of the subsurface at the reservoir scale.
Empirical relationship between \(V_P\) and \(V_S\) may give indications about the presence of hydrocarbon \parencite{tatham_vp/vs_1976} and the permeability and porosity of rock materials \parencite{domenico_rock_1984}.
This information are used to manage the oil production of the reservoir.\par
\vspace{\baselineskip}

This thesis focuses on the resolution of the inverse problem in the isotropic acoustic approximation of the wave equation. We study a method called \gls{mva}, an image-domain technique aiming at recovering the background velocity model. The purpose of the thesis is to investigate the possible inclusion of multiple reflections in this approach, which traditionally considers only primary reflections. The principle and issues of \gls{mva} will be detailed in \cref{sec:mva}. We now review standard time-domain strategies for the resolution of the inverse problem.