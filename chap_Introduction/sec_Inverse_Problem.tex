\section{Time-domain methods for the resolution of the inverse problem}\label{sec:Intro_Inverse_Problem}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
We detail in this section time-domain methods addressing the resolution of the inverse problem in the isotropic acoustic approximation of the wave equation, meaning that the unknown is the pressure wave velocity field \(c\paren{\vec{x}}\). Image-domain methods will be described in \cref{sec:mva}. We first present the \gls{fwi} strategy, which consider the full recorded traces to recover both the large and short scales of \(c\paren{\vec{x}}\). Then alternative strategies relying on the scale separation assumption (\cref{fig:velocity_decomp_marmousi}) are detailed. We shall distinguish between methods dedicated to the recovery of the short-scale structures \(\delta c\paren{\vec{x}}\) (\cref{sec:migration}) and those aiming at retrieving the background velocity model \(c_0\paren{\vec{x}}\) (\cref{sec:tomographic_methods}).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{\glsentrylong{fwi}}\label{sec:fwi}
\minisec{A data-fitting procedure}
\glsfirst{fwi} \parencite{virieux_overview_2009,fichtner_full_2011} is an iterative procedure considering the complete recorded seismic traces. The associated objective function measures the least-squares misfit between recorded data and simulated data.
\begin{equation}\label{eq:ObjFct_FWI}
	J\ped{FWI}\croch{c}=\frac{1}{2}\norm[\big]{P\ap{calc}\croch{c}-\Pobs}^2_2 \text{.}
\end{equation}
All type of waves are included in the modelling of the data (direct, diving, reflected, multiply scattered waves). First introduced by \textcite{tarantola_inversion_1984}, the method has gained in popularity with the increase of computer power. In theory different model parameters (velocity, density, attenuation, anisotropy) can be resolved, requiring a modelling engine able to reproduce the physics of wave-propagation as accurately as possible \parencite{warner_which_2012}. However most applications use the acoustic approximation of the wave-equation and are interested in retrieving only the pressure-wave velocity model of the Earth because of the computational cost of elastic modelling and of the challenges of multi-parameter estimation \parencite{operto_guided_2013}.
%
%Note that the objective function \(J\ped{FWI}\) is formally similar to the objective function of \cref{eq:ObjFct_Migr_Intro}, but this time we want to invert for the full velocity field \(c\), meaning the corresponding inverse problem is not linear any more. The inversion is initiated with a smooth initial guess which is iteratively updated by computing the gradient of the objective function.

\minisec{Resolution analysis}
As \gls{fwi} tries to explain the complete data set, it should provide high-resolution images of the subsurface, with both the long and short-scale structures of the velocity model. The contribution of different kinds of waves in the construction of the velocity model can be analysed with the following relationship 
 \parencite{devaney_filtered_1982,miller_new_1987},
\begin{equation}
	\vec{k}=\frac{2\omega}{c_0}\cos\paren[\bigg]{\frac{\theta}{2}}\vec{n}\text{,}
\end{equation}
linking the spatial frequency vector or wavenumber \(\vec{k}\) at point \(\vec{x}\) to the diffraction angle \(\theta\) associated to a source-receiver pair, \(\vec{n}\) being the normalisation of vector \(\vec{k}\) (\cref{fig:resolution_diffrac_angle}).
In the shallow part of the subsurface, large diffraction angles obtained for diving waves allow to recover the small wavenumbers, \ie the large-scale structure of the velocity model. Because of the limited surface offset range, the deeper part of the subsurface is investigated mainly by reflected waves with a small diffraction angle, and only the high-frequency part of the velocity model is recovered. In the deeper part of the subsurface, \gls{fwi} thus behaves like a non-linear least-squares migration algorithm \parencite{mora_elastic_1988,mora_inversion_1989}. Methods extracting the information about the long-scale structure contained in reflected events will be presented in \cref{sec:tomographic_methods}.
%------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/Wavenumber_diffraction}
\caption{Relationship between the wavenumbers \(\vec{k_s}\) and \(\vec{k_r}\) and the opening angle \(\theta\) at the image point \(\vec{x}\).}
\label{fig:resolution_diffrac_angle}
\end{figure}
%------------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\minisec{The \emph{cycle-skipping} issue}
It is known that the \gls{fwi} objective function~\eqref{eq:ObjFct_FWI} suffers from local minima because of the non-linear relationship between model and data \parencite{gauthier_twodimensional_1986}. This problem is called \emph{cycle-skipping} and imposes in theory the use of global optimisation procedures \parencite{tognarelli_two-grid_2015,galuzzi_stochastic_2016}. These methods are computationally expensive as they require many evaluations of the objective function. Gradient-based methods are less expensive, but they need a starting model close to the solution to avoid converging to a local minimum. More precisely the phase mismatch between data computed with the initial and true model should be less than half the shortest wavelength contained in the data.

To overcome this difficulty, a hierarchical strategy consists of inverting first the low-frequency content of the data and then progressively incorporating higher frequencies, as the basin of attraction of the misfit function is inversely proportional to the central frequency of the data \parencite{bunks_multiscale_1995,pratt_two-dimensional_1996,sirgue_efficient_2004}.
The presence of large offsets recording in the surveys allows also to better constrain the background model with transmitted waves
\parencite{shipp_two-dimensional_2002}.
This hierarchical strategy has been successfully applied on real data sets in marine \parencite{sirgue_3d_2009,vigh_3d_2009} and land \parencite{plessix_application_2010} environments.
%The depth that can be imaged without cycle-skipping is linked to the wavelength considered in the inversion. Then the minimum frequency that should be recorded in the data is given by the ratio between the P-wave velocity and the maximum depth that should be imaged. For example, with a velocity of \SI{2000}{\meter\per\second} and an investigation depth of \SI{2}{\kilo\meter}, the frequency content of the data should start at \SI{1}{\hertz}.
In typical acquisition geometries, for a target at a few kilometres depth the frequency content of the data should start around \SI{1}{\hertz} to avoid cycle skipping.
If low-frequency data or large offsets are not available, other strategies have to be designed to avoid falling in a local minimum.

Alternative techniques modify the definition of the objective function to enlarge the basin of attraction around the correct velocity model.
The usual \(\ell_2\)-norm can be replaced by a more convenient distance. \textcite{metivier_measuring_2016} use an optimal transport distance to measure the misfit between seismograms; the basin of attraction of the objective function is extended at the cost of a new optimisation problem dedicated to the calculation of the distance.
Other strategies transform the signals of both observed and calculated data to a more convenient domain before subtraction.  
\textcite{shin_waveform_2008,shin_waveform_2009} have studied the use of the Laplace and Laplace-Fourier domain to perform the inversion and showed that an accurate smooth model could be built in these domains.
In the Normalised Integration Method \parencite{donno_estimating_2013}, the transformation consists of integrating the square of the signals and can be interpreted as a measure of the accumulation of energy along the trace. The objective function compares monotone increasing signals and is more convex; however the processing of noise for this method should be investigated.
Other methods consider the envelope of seismic data \parencite{wu_seismic_2014,chi_full_2014} to work on less oscillatory signals.
All these transforms allow to mitigate the cycle-skipping issue but produce velocity models with a lower resolution than conventional \gls{fwi}. However they can be considered as a first inversion step dedicated to the building of an accurate starting model for \gls{fwi} \parencite{tejero_comparative_2015}, or for migration algorithms used to recover the short-scale structure of the velocity model.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Linearised waveform inversion}\label{sec:migration}
The purpose of migration techniques is to recover a map of Earth discontinuities corresponding to a perturbation of the velocity model. These methods are based on the classic scale separation and assume the knowledge of an estimate of the macromodel. They were historically designed for primary reflections only. Extension to multiple reflections will be presented in \cref{sec:multiple_using}.

The first migration techniques used geometrical construction \parencite{bleistein_mathematics_2001}. In a simple geologic context, an event appearing at time \(t\) in a trace corresponding to a source at position \(\vec{s}\) and a receiver at position \(\vec{r}\) is due to a diffracting point situated at a location \(\vec{x}\) such that the sum of traveltimes from  \(\vec{s}\) to \(\vec{x}\) and from \(\vec{x}\) to \(\vec{r}\) is equal to \(t\). All the subsurface points satisfying this criterion define an isochrone curve; in the case of an homogeneous velocity model, it is an ellipse with focus points at \(\vec{s}\) and \(\vec{r}\). Repeating the process for all sources and all receivers, the summation of ellipses interferes constructively along the reflectors and destructively anywhere else \parencite{bleistein_mathematics_2001}.

Later the construction of a reflectivity model was reformulated by considering the propagation of complete wavefields and \textcite{claerbout_toward_1971} introduced the concept of \emph{Imaging Condition}. Interfaces are defined as the location where a downgoing incident wavefield coincides in time with an upgoing reflected wavefield. In the case of primary reflections in surface acquisition, the downgoing wavefield is the source wavefield and the upgoing wavefield is the reflected wavefield recorded by the receivers. This leads to a three-step procedure:
\begin{itemize}
	\item propagation of an estimation of the source wavelet to determine the source wavefield \(S\paren{s,\vec{x},t}\) in the medium at all subsurface points and for all times;
	\item backpropagation of the data recorded at receiver positions at all subsurface points and for all times to determine the receiver wavefield \(R\paren{s,\vec{x},t}\);
	\item application of an imaging condition to these wavefields \(S\) and \(R\) to determine an image of the subsurface. Many formulations exist for this imaging condition, the most commonly used is the cross-correlation at zero-lag of the source and receiver wavefields.
\end{itemize}
The procedure is repeated for each shot point, yielding as many migrated sections as shot points in the survey. The obtained images can be stacked to increase the signal to noise ratio. However, the similarities and discrepancies between these different images can also be used as information. This is the basic principle of \gls{mva} techniques presented in \Cref{sec:mva}.

This migration algorithm has been recognised as the first iteration of the least-squares data-fitting inverse problem  \parencite{lailly_seismic_1983,tarantola_inversion_1984}. Under the Born approximation, this inverse problem is linearised. Assuming a fixed estimate of the background velocity model \(c_0\), it consists of determining the model perturbation \(\delta c\) which best reproduces recorded data. The associated scalar misfit function is
\begin{equation}\label{eq:ObjFct_Migr_Intro}
	J_0\croch{c_0,\delta c}=\frac{1}{2}\norm[\big]{P\croch{\delta c}-\Pobs}^2\text{,}
\end{equation}
where \(\Pobs\) stands for recorded data and \(P\croch{\delta c}\) represents the Born-modelling of data with the model perturbation \(\delta c\).
As in the case of \gls{fwi} \pcref{eq:ObjFct_FWI}, this objective function measures the misfit between observed data and calculated data, except that here only the short-scale part of the velocity model is updated, and the associated inverse problem is linear.

The migration procedure detailed above actually computes the gradient of the objective function \eqref{eq:ObjFct_Migr_Intro} in \(\delta c=0\),
 \begin{equation}\label{eq:migration_non_extended}
\pderiv{J_0}{\delta c}\croch[\big]{c_0,\delta c=0}\paren{\vec{x}}=\frac{2}{c_0^3\paren{\vec{x}}}\int_s\int_t \pderiv[2]{}{t}S\croch{c_0}\paren{s,\vec{x},t} R\croch{c_0}\paren{s,\vec{x},t} \diff t \diff s\text{,}
\end{equation}
where \(S\) and \(R\) are the source and receiver wavefield, whose values depend on the background velocity \(c_0\vecx\). Note that the definition of the misfit function \eqref{eq:ObjFct_Migr_Intro} contains an implicit summation over the sources. Defining the reflectivity as the gradient of this function produces a \emph{post-stack} image.

The migration operator is actually the adjoint of the Born modelling operator and provides only a qualitative image of the subsurface: the position of the reflectors is kinematically consistent with the assumed velocity model and the recorded traveltimes. However the method does not provide a correct estimation of their amplitude. To obtain a \emph{quantitative} estimate of the reflectivity model, the minimisation problem presented above should be solved completely. This \emph{true-amplitude migration} can be achieved with an approximate inverse of the Hessian of the objective function \parencite{beylkin_imaging_1985,lambare_iterative_1992,%
plessix_frequency-domain_2004,kiyashchenko_modified_2007}. Alternatively an iterative procedure using gradient-based methods can be defined to minimise the misfit function \parencite{nemeth_leastsquares_1999,ostmo_finite-difference_2002}.
In \cref{chap:iter_migr}, an iterative migration procedure will be presented in the context of second-order Born modelling.

The result of the migration process depends strongly on the velocity model used to compute the source and receiver wavefields. With an incorrect velocity model, events may be positioned at the wrong depth and may not be well focused. In the following section, time-domain methods to construct the background velocity model are reviewed.


\subsection{Tomographic methods: retrieving the large-scale structure of the velocity model}\label{sec:tomographic_methods}
\minisec{Traveltime tomography}
Traveltime tomography techniques are developed with the ray theory, based on the high-frequency asymptotic approximation of the wave equation \parencite{cerveny_seismic_2005}. Wave propagation in the subsurface is described by rays with propagation laws similar to those used in optics. In this formalism, the Green's functions describing the wave propagation can be decomposed into three terms, one accounting for the traveltime, one for the amplitude and one for the source signature. In tomographic methods, events in the data are characterised by their traveltime only. Their amplitude and the finite-frequency nature of the data are not considered, contrary to \gls{fwi}. The objective function of the associated inverse problem measures the difference between traveltimes picked on a selection of events in the data with traveltimes computed with an estimation of the velocity model \(c\):
\begin{equation}
	J\ped{TT}\croch{c_0}=\frac{1}{2}\norm{\tau\croch{c_0}-\tau\ap{obs}}^2\text{.}
\end{equation}
The velocity model is updated iteratively to minimise the traveltime differences. The method requires a first processing stage to pick events, as well as a modelling engine relying on the resolution of the Eikonal equation to compute traveltime maps corresponding to a velocity model \parencite{vidale_finite-difference_1988,podvin_finite_1991,noble_accurate_2014}. Depending on the acquisition, different events can be picked.

The first arrivals are mostly used in seismology. They consists of direct and diving waves and are easier to pick than reflections. In active seismic experiments, they are well suited when the acquisition geometry emphasises transmission effects, for example in surface acquisition with large offsets, cross-well acquisition or with buried receivers and sources at the surface \parencite{vi_nhu_ba_detection_2014}. In the case of surface acquisition without large offsets, the depth penetration of these waves restricts the area where the background velocity can be recovered. Information about the deeper part of the model is contained in reflected waves.

Reflected events can be picked as well. In this case the model is parametrised both by a velocity model and a geometric description of the reflectors. Reflection tomography was historically developed in the time domain \parencite{bishop_tomographic_1985,farra_non-linear_1988}. Each reflection in the data is associated to a reflector. An issue is that picking reflected events along a wide range of offset is a difficult task, especially with complex geology and noisy data \parencite{lailly_smooth_1996}. To improve the signal to noise ratio, the data can be stacked if the model is not too complex. Alternatively the analysis can be transposed to the migrated domain \parencite{stork_reflection_1992}, where interfaces are easier to pick. Reflectors are picked on migrated sections, then the corresponding migrated events are modelled (or demigrated) to be compared to the observed traveltimes  \parencite{jacobs_sequential_1992,grau_geophysical_1993}. Note that with short-offsets only, there is an ambiguity between the velocity and the depth of reflectors \parencite{williamson_tomographic_1990}. Reflection tomography is more generally known to be an ill-posed inverse problem requiring external constraints. These constraints can be existing well data \parencite{le_stunff_taking_1998}, or information about the subsurface provided by other imaging methods, gravimetric or electromagnetic for example \parencite{lines_cooperative_1988}.

Other alternatives to the picking of continuous reflected events are slope tomography methods, and in particular stereotomography \parencite{billette_velocity_1998,lambare_stereotomography_2008,guillaume_dip-constrained_2013}. In addition to traveltimes, the slope of locally coherent events are picked on common shot and common receiver panels. The inverted velocity model should then explain both traveltimes and slopes of picked events. In this method there is no need to describe the model with a set of continuous layer interfaces, only the dip of local events is needed. Then the density of picked events is higher. As for traveltime tomography, local picking can be performed in the image domain \parencite{chauris_migration_2002}. Then kinematic invariants such as traveltimes and slopes can be recomputed in the time domain.

Traveltime tomography has become a standard in the oil and gas industry for velocity model building since the late nineties \parencite{woodward_decade_2008}. Although  recent publications have focused mainly about \gls{fwi}, progresses have been made to better constrain the inversion, include structural information like sharp velocity contrasts and make the method more and more automatic \parencite{lambare_recent_2014}. Improvement of both the method and the recording devices have increased the resolution of velocity model constructed by traveltime tomography \parencite{nichols_resolution_2012}. We now review extension of traveltime tomography that go beyond the high-frequency approximation.

\minisec{\glsentrylong{wett}}
\gls{wett} is an extension of traveltime tomography which takes into account the finite-frequency nature of seismic data. \textcite{luo_waveequation_1991} proposed to cross-correlate the traces of first-arrival events in observed and calculated data instead of subtracting their traveltime. Then they minimise the time-lag maximising the cross-correlation.
	\textcite{van_leeuwen_correlation-based_2010} indicate that the traveltime error might not coincide with the time-lag maximising the cross-correlation because of errors in the source wavelet used for calculated data. They propose to increase the robustness of the approach with an objective function penalising non-zero time-lag.
	Compared to \gls{fwi}, \gls{wett} yields velocity models with lower resolution but is less prone to cycle-skipping and can be used to build a starting model for \gls{fwi} \parencite{wang_integrated_2014}. The resolution of the method can be increased by replacing correlation by deconvolution \parencite{luo_deconvolution-based_2011}.
	A related strategy is \gls{awi} \parencite{warner_adaptive_2014,warner_adaptive_2015} in which the coefficients of a Wiener filter applied to calculated data are determined at each iteration to match observed data. The velocity model is then updated so that the coefficients of the filter amount to a simple zero time lag, which corresponds to the case where calculated data perfectly reproduce observed data.
	%For example, \gls{mbtt} \parencite{chavent_determination_1995,plessix_automatic_1995} is a transposition of the semblance principle. Using an initial estimation of the background velocity model, a migrated section is derived and used to regenerate calculated data which are compared to the observations. As the same smooth velocity model is used during migration and modelling, there is no phase shift between predicted data and observed data, but the amplitude difference carries information about the stack power in the migrated section. The method shares similarities with the \gls{rwi} approach \parencite{chavent_reflection_2015}.
	
 %As in \gls{dwi} the time domain may allow an easier inclusion of multiple reflections, but with a very wrong estimate of the velocity events corresponding to different can correlate and thus mislead the focusing criterion.
	
\minisec{\glsentrylong{rwi}}
\gls{rwi} \parencite{xu_inversion_2012,zhou_fundamental_2012,%
brossier_velocity_2015} is an approach similar to \gls{fwi} developed to extract information about the macromodel from reflected events. It is inspired by the \gls{mbtt} procedure \parencite{chavent_determination_1995,%
plessix_automatic_1995}. It either assumes a scale separation between low and high frequency content of the velocity model \parencite{xu_inversion_2012} or relies  on a parametrisation with both the velocity and the impedance to naturally facilitate the scale separation \parencite{zhou_velocity_2016}.
An initial estimate of the macromodel is used to construct a migrated section. Then reflected events are modelled from the migrated section and the background velocity model, and compared to observed reflections in an objective function similar to \gls{fwi}. With this strategy, transmission wavepaths are constructed between the reflectors and the surface as well as between the reflectors and the receivers, allowing to recover the long-scale part of the velocity model in areas of the subsurface not reached by diving waves.
\textcite{zhou_full_2015,alkhalifah_natural_2016} proposed strategies to combine the information extracted from both \gls{fwi} and \gls{rwi} about the macromodel.

An alternative strategy using the scale separation is the \gls{dwi} approach \parencite{chauris_differential_2013}. A migrated section is computed from one shot gather and used to calculate synthetic data for the next shot. The difference with the corresponding observed data is used as information about the errors contained in the macromodel.	

	Finally \textcite{van_leeuwen_velocity_2008} propose to correlate traces of observed and simulated data separated by a spatial shift and with a temporal lag in order to build coherency panels function of the space and time shifts. The correct velocity model corresponds to focusing of energy at zero spatial and temporal delay. The criteria assessing the quality of the velocity model in this approach as well as in \gls{dwi} are inspired by image-domain methods presented in the next section.



