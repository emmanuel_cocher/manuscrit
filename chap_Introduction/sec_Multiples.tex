\section{Multiple reflections}\label{sec:Intro_Multiples}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Physics of multiples}
As defined by \textcite{verschuur_seismic_2013}, primaries are waves that undergo only one upward reflection, whereas multiple reflections are characterised by at least one downward reflection. Because of spherical divergence and of the loss of energy occurring at each reflection, multiples effectively visible in the data are associated to strong reflectors. This is typically the case in marine acquisition where the interface between the air and the sea acts as a mirror. The water bottom is also a strong reflector. An other example is the chalk layer in the North Sea located between high-velocity layers, creating two high impedance contrasts which are strong multiples generators \parencite{reinier_building_2012}.

A first classification of multiples refers to the location where the shallowest downward reflection occurs. If it is the surface, the multiples are called \emph{surface-related multiples} (\cref{fig:kind_surface}). Note that the upgoing multiple reflection can be reflected at the surface again, leading to higher-order surface multiples (\cref{fig:kind_surface2}). In the following, a first-order surface-multiple refers to the multiple event undergoing a single downward reflection at the surface, and a second-order surface-multiple as the corresponding event for two downward reflections, \etc When the shallowest downward reflection occurs at an interface between two layers in the subsurface, the term \emph{internal multiple} is used (\cref{fig:kind_internal}). Similarly, several order of internal multiples can be defined. 

In the case of marine acquisition, the reverberations occurring in the water column are called \emph{water-layer multiples} (\cref{fig:kind_water_layer}). Multiples undergoing at least one reflection below the water bottom and a reverberation in the water-layer are referred to as \emph{peg-leg multiples} (\cref{fig:kind_pegleg}).
In practical marine acquisition, both source and receivers are located a few meters below the sea surface. Then one part of the energy emitted by the source goes directly to the subsurface, while an other part reflects at the sea surface, leading to a surface multiple called \emph{source ghost}. The same phenomenon occurs at the receiver side and is called \emph{receiver ghost}. These kinds of multiples are also observed in the case of \gls{obc} acquisition (\cref{fig:kind_Ghost_OBC}).
Naturally more complex paths in the subsurface which do not fit in the previous categories are possible (\cref{fig:kind_complex}).
%------------------------------------
\begin{figure}[!htbp]
  %%%
  \begin{subfigure}[b]{0.5\linewidth}
    \includegraphics[]{Figures/Multiples_surface}
    \caption{First-order surface multiple}\label{fig:kind_surface}
  \end{subfigure}%
  %%%
  \begin{subfigure}[b]{0.5\linewidth}
    \includegraphics[]{Figures/Multiples_surface2}
    \caption{Second-order surface multiple}\label{fig:kind_surface2}
  \end{subfigure}
  %%%
  \begin{subfigure}[b]{0.5\linewidth}
    \includegraphics[]{Figures/Multiples_internal}
    \caption{Internal multiples}\label{fig:kind_internal}
  \end{subfigure}%
  %%%
  \begin{subfigure}[b]{0.5\linewidth}
    \includegraphics[]{Figures/Multiples_water_layer}
    \caption{Water-layer multiple}\label{fig:kind_water_layer}
  \end{subfigure}
  %%%  
  \begin{subfigure}[b]{0.5\linewidth}
    \includegraphics[]{Figures/Multiples_peg_leg}
    \caption{Peg-leg multiple}\label{fig:kind_pegleg}
  \end{subfigure}%
  %%%  
  \begin{subfigure}[b]{0.5\linewidth}
    \includegraphics[]{Figures/Multiples_Ghost_OBC}
    \caption{Receiver ghost in \gls{obc} acquisition}\label{fig:kind_Ghost_OBC}
  \end{subfigure}
  %%%
  \begin{subfigure}[b]{0.5\linewidth}
    \includegraphics[]{Figures/Multiples_complex}
    \caption{A more complex multiple}\label{fig:kind_complex}
  \end{subfigure}
  %%%
  \caption{Different kinds of multiple reflections \parencite[similar to][8-9]{verschuur_seismic_2013}. The black point and the white triangle represent source and receiver positions, respectively.}
  \label{fig:kinds_multiples}
\end{figure}
%------------------------------------

One can also distinguish between \emph{long-period} and \emph{short-period} multiples. Long-period multiples (\cref{fig:kind_long}) appear in the data record as separate events from their relative primaries. Visually the same succession of events is repeated at regular intervals of time (\cref{fig:mult_real_dataset}). This is the case with water-layer reverberations recorded in a deep-water environment. Oppositely, short-period multiples (\cref{fig:kind_short}) are produced by reverberation within small layers such as chalk. These reverberations overlap with one another and with the original primary, resulting in a single event with a different wavelet signature in the recorded data. 
%------------------------------------
\begin{figure}[!htbp]
  %%%
  \begin{subfigure}[b]{0.5\linewidth}
    \includegraphics[]{Figures/Multiples_long_period}
    \caption{Long-period multiple}\label{fig:kind_long}
  \end{subfigure}%
  %%%  
  \begin{subfigure}[b]{0.5\linewidth}
    \includegraphics[]{Figures/Multiples_short_period}
    \caption{Short-period multiple}\label{fig:kind_short}
  \end{subfigure}
  %%%
  \caption{Long-period and short-period multiples \parencite[see also][10]{verschuur_seismic_2013}.}
  \label{fig:kinds_multiples_short}
\end{figure}
%------------------------------------
\begin{figure}[!htbp]
	\includegraphics[scale=0.5]{Images/marine_CMP_Trad2003}
	\caption{(a) A real marine data set containing both primaries and multiples is decomposed into (b) primaries only and (c) multiples only \parencite[from][]{trad_latest_2003}.}
	\label{fig:mult_real_dataset}
\end{figure}
%------------------------------------

Many imaging procedures designed for primary reflections require multiples to be removed from data. On the contrary, some methods presented in \cref{sec:multiple_using} include them as complementary information. In any case, one needs the ability to model multiple reflections. Standard multiple modelling techniques are now reviewed.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Modelling of multiples}\label{sec:multiple_modelling}
Surface-related multiples can be included in modelling by replacing the usual absorbing boundary condition at the surface by a free-surface.
Alternatively, multiples can be predicted using primary reflections. First-order surface related-multiples can indeed be considered as the (primary-)response of the Earth to an areal source made of the primary reflections reaching the surface.
Then first-order multiples can be obtained by re-injecting the primaries into a primaries modelling code. If the model of the first layer is known (for example the water layer in marine acquisition), multiples can be predicted by adding a fictive roundtrip to the data through this layer with wavefield extrapolation \parencite{wiggins_attenuation_1988,wiggins_multiple_1999}. Then primaries are transformed in first-order multiples, first-order multiples in second-order multiples, and so on. The surface reflection coefficient should be taken into account (the coefficient \num{-1} is the simplest approximation).

A data-driven alternative requiring no model of the subsurface is possible. In this approach, a first-order surface-multiple is obtained by autoconvolution of the primary reflection. The prediction is exact except for the source wavelet which is present twice in the multiple due to the autocorrelation. A shape correction has then to be applied. Similarly the second-order surface-multiple can be obtained by convolution of the primary with the first-order multiple, and so on. In the 1D case and with a Dirac source, the total response including all order of surface-multiples can be described as an infinite series:
\begin{equation}
	d(t)=\widehat{P_0}(t)
	+\widehat{P_0}(t)\ast \widehat{P_0}(t)
	+\widehat{P_0}(t)\ast \widehat{P_0}(t)\ast \widehat{P_0}(t)
	+\cdots
\end{equation}
where \(\ast\) represent the convolution product and \(\widehat{P_0}(t)\) the primary impulsive response of the Earth. For the 2D and 3D case, a summation over the reflection point at the surface has to be added. More generally, the multiples contained in a data set can be generated by convolving the total data set with the impulse primary response \parencite{verschuur_adaptive_1992,%
weglein_inversescattering_1997,%
dragoset_perspective_2010}
\begin{equation}
	M\paren{\vec{x}_s,\vec{x}_r,t}=\sum_{\vec{x}_k} \widehat{P_0}\paren{\vec{x}_k,\vec{x}_r,t} \ast d\paren{\vec{x}_s,\vec{x}_k,t}\text{,}
\end{equation}
where \(\vec{x}_k\) is the reflection point at the surface.




The interaction of internal multiples with the subsurface can be described by the Lippmann series \parencite{lippmann_rearrangement_1956,ten_kroode_prediction_2002}, originally used in quantum mechanics. The Born approximation used to model primaries is actually the first-order approximation of the series. Alternatively the Bremmer series can be considered \parencite{bremmer_w.k.b._1951,de_hoop_generalization_1996}. \textcite{berkhout_review_2014} describes a \gls{fwmod} strategy which takes as input a reflectivity model and one-way propagation operators from one depth-level to the following based on the velocity model. The modelling is performed recursively: in the first roundtrip, the downgoing wavefield emitted by the source and the primary reflections are computed. The latter interact with the reflectivity model to generate both internal and surface first-order multiples in the second roundtrip. The process is repeated for higher-order multiples and stopped when their amplitude is too weak.
%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%
\subsection{Removal of multiples}\label{sec:multiple_removal}
%\subsubsection{Problems posed by multiples}
Multiples are sometimes easy to recognise in the data, for example long-period multiple appear as a repetition of the primaries pattern. The dip of primaries and multiples events arriving at the same time in the data can also be inconsistent. In more complicated cases, multiples may easily be mistaken for primaries. Moreover, usual migration algorithms are designed to account for primary reflections only. Hence multiples falsely interpreted as primaries can lead to spurious events in the final migrated image and incorrect interpretation of the geology (\cref{fig:mult_spurious_savaguitton}).
%-----------------------------------
\begin{figure}[!htbp]
\includegraphics[scale=0.5]{Images/sava_guitton_pluto}
\caption{Synthetic example of classic migration performed on data contaminated with multiples. WBM is a spurious events caused by a Water Bottom Multiple while M refers to surface related multiples repeating the bottom structure of the salt dome \parencite[from][]{sava_multiple_2005}.}
\label{fig:mult_spurious_savaguitton}
\end{figure}
%-----------------------------------


Numerous strategies to remove multiple reflections from seismic data have been designed. We review briefly the main families here and explain why multiple attenuation remains a challenging issue. The reader is referred to \textcite{verschuur_seismic_2013} for an extensive review.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\minisec{Radon transform}
The first family of multiple removal takes advantage of the fact that a multiple and a primary arriving at the same time in the data have not \enquote{seen} the same velocities in the Earth. With the hypothesis of a velocity increasing with depth, a multiple travels in shallower and slower layers of the subsurface. Then primaries and multiples do not exhibit the same move-out on Common Mid Point (CMP) gathers. Using a NMO-correction with the correct velocity, primaries are flattened but multiples are not. A strategy using these move-out discrepancies transform the CMP gathers to a space where primaries and multiples are easily separable. Multiples are muted and the inverse transform is performed to output CMP gathers without primaries.
The double Fourier transform mapping CMP gathers to the \(f\)-\(k\) domain \parencite{ryu_decomposition_1982} is an example of such a transformation, but the most popular choice is the parabolic \parencite{hampson_inverse_1986,kabir_toward_1999} or hyperbolic \parencite{foster_suppression_1992} Radon transform. The method assumes that primaries and multiples in CMP gathers (with or without NMO-correction) can be described by different parabolic or hyperbolic functions. Then in the Radon domain, they should appear as focused events. After muting of the multiples, the primaries are reconstructed with the inverse Radon transform. In practice, artefacts appear in the Radon domain and primaries and multiples are not well separated, leading to inaccurate multiple attenuation. A remedy consists of redefining this strategy as an inversion aiming at finding the model in the Radon domain that best fits the original data after inverse Radon transform. A sparsity constraint in the Radon domain is added to ensure primaries and multiples are easily separable \parencite{sacchi_highresolution_1995,trad_latest_2003}. However the method still has difficulties in case of complex geology when primaries and multiples do not exhibit enough move-out difference or when they cannot be described by parabolic or hyperbolic functions.
To better account for the propagation effect in complex media, the same analysis can be transposed to the image-domain where the move-out discrepancies are analysed in \glspl{cig} function of the surface offset \parencite{duquet_filtering_1999} or the scattering angle \parencite{sava_multiple_2005}. The velocity model is assumed to be relatively accurate, so that primaries are flat in the angle-domain \glspl{cig}, contrary to multiples. Multiples are then easily recognisable and a similar strategy using the Radon transform is used to discriminate between flat and curved events. As in the data-domain the possible overlap of primaries and multiples at zero-angle may harm the attenuation of multiples. The main hypothesis in this approach is the knowledge of a reliable velocity model.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\minisec{Adaptive subtraction}
The second family of multiple removal techniques is a two-step procedure. First the data set is used to compute a prediction of the multiples following one of the method presented in \cref{sec:multiple_modelling}. Then the prediction is subtracted from the data to yield the primary estimation.

This second step is difficult because the prediction of multiples is in practice never perfect. In wavefield extrapolation methods, inaccurate prediction may be caused by an incorrect model of the layer in which the multiple reflection is simulated. Multiples predicted by convolution of the data with the primary response contain the source wavelet twice. Moreover, predictions based on a 2D-model assumption do not take into account the 3D-propagation effects.
Therefore there may be inaccuracies in the phase, amplitude, and wavelet of predicted events \parencite{abma_comparisons_2005} and simple subtraction of the prediction to the original data does not yield a good estimation of the primaries.
The prediction has to be accommodated to the recorded multiples in a process called \emph{adaptive subtraction}, which consists of applying a filter to the multiples before subtraction. The filter is determined by minimising the energy of the differences between the original data and the filtered multiples in a least-squares sense.
If primaries and multiples are overlapping in the data domain, minimising the energy can result in distortion of the primaries and/or residual multiple in the final result \parencite{nekut_minimum_1998}. Alternatives consider replacing the least-squares criterion by the \(\ell_1\)-norm \parencite{guitton_adaptive_2004}, among other possibilities \parencite{batany_adaptive_2016}. The subtraction can also be performed in a domain where primaries and multiples are less likely to interfere such as the curvelet domain, Radon domain, or frequency domain \parencite{sacchi_highresolution_1995,donno_curvelet-based_2010,batany_signal_2016}.

A very-well known strategy based on multiple prediction and adaptive subtraction is the \gls{srme} method \parencite{verschuur_adaptive_1992}. It is based on the observation that a surface-multiple can be decomposed in several primary reflections connected by a reflection point at the surface. If the source and receiver coverage of the acquisition is dense enough, these primaries are contained in the data and can be used to predict the multiples.
\gls{srme} is implemented as an iterative method aiming at improving an initial estimation of the primaries \parencite{berkhout_estimation_1997}. This estimation is convolved with the data to produce a prediction of the multiples. Then an adaptive subtraction is performed to remove the estimated multiples from the total data, yielding an improved estimation of the primaries. The first estimate of the primaries can be the output of another multiple elimination technique or simply the complete data. This process converges very fast in practice and only a few iterations are needed \parencite{berkhout_estimation_1997}.
One major advantage of this method is that it is fully data-driven and requires no model of the subsurface.
Conversely, the limitations of the method are related to the data acquisition. A requirement is that all the primaries composing the multiples should be recorded in the data. This may not be the case in typical marine acquisition, where the first offsets near the source are missing for practical reasons. This represents also an issue for the extension to 3D marine acquisition where the direction perpendicular to the streamers is sampled with only a few lines. In practice, these limitations require a method to reconstruct the missing primaries, for example by interpolation of the data \parencite{kabir_restoration_1995}.

\gls{epsi} \parencite{van_groenestijn_estimating_2009} addresses the issue of inaccurate multiple prediction issue by recasting the prediction and the adaptive subtraction into a single inversion procedure. An objective function compares observed data to predicted primaries and predicted multiples. The corresponding iterative process should update the estimation of the primaries as well as the source wavelet. \gls{epsi} provides enhancements compared to \gls{srme} but is computationally more expensive.

Note that \gls{srme} is designed to deal with surface-related multiples only. A direct adaptation to internal multiples would consist of wavefield extrapolation to calculate the data that would be recorded at fictive sources and receivers located on the boundary generating the internal multiples. The process should be repeated for each layer and has the disadvantage of requiring an accurate velocity model to process the wavefield extrapolation. \textcite{jakubowicz_wave_1998} proposes an alternative formulation in which the redatuming uses the primary reflections contained in the data, discarding the need for a subsurface model. Alternatively an inverse scattering series can be used to predict all internal multiples in a fully data-driven manner \parencite{weglein_inversescattering_1997,ten_kroode_prediction_2002}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Using multiples as valuable information}\label{sec:multiple_using}
A lot of effort has been put to develop multiple removal techniques because migration and velocity analysis techniques were designed for primary reflections only. However in the last decade, a different approach considering multiples as signal rather than noise has gained popularity. Multiples have travelled at least twice through the subsurface and therefore contain a lot of valuable information that may not be present in primary reflections. For example they may travel in area of the subsurface not illuminated by primaries like beneath salt domes (\cref{fig:multiples_salt_illumination}).
Moreover a surface multiple may originate from a virtual source not present in the survey, for example when short-offsets are missing in the acquisition, thus providing illumination with smaller angle reflections, different wavenumbers for imaging and better vertical resolution than primaries (\cref{fig:couverture_prim_mult}). 
In particular the cross line of a 3D marine acquisition is coarsely sampled and the so-called \emph{acquisition footprint} is still visible in the data. \textcite{long_mitigation_2013} show that this artefacts can be mitigated with the use of multiple reflections. A correct use of multiple reflections could then possibly relax the need of dense source and receiver coverage at the surface. The sensitivity of multiples to the velocity model has been studied by some authors to detect small velocity changes in time-lapse experiments \parencite{snieder_elastic_2002,verschuur_using_2014}.%Similarly the use of multiples to reconstruct missing offset in the data has been studied by. %Finally \textcite{snieder_elastic_2002} shows that multiply scattered wavefield is sensible to small changes in velocity measured in time-lapse acquisition.
%----------------------------------------
\begin{figure}[!htbp]
\includegraphics[scale=0.4]{Images/liu_salt_1}\par
\includegraphics[scale=0.4]{Images/liu_salt_2}
\caption{Example of conventional migration using primaries only (top) and using primaries and multiples (bottom) on the Sigsbee2B synthetic data set. Note the extended illumination provided by multiples below the salt dome in the areas marked with the white ellipses \parencite[from][]{liu_reverse_2011}.}
\label{fig:multiples_salt_illumination}
\end{figure}
%----------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/couverture_prim_mult}
\caption{Illumination of a flat reflector by primary reflections (blue, solid) and first-order surface-related multiples (red, dotted). Multiples allow a wider coverage of the reflector with smaller scattering angles.}
\label{fig:couverture_prim_mult}
\end{figure}
%----------------------------------------

\subsection{Imaging with multiples}
A simple way of using multiple reflections consists of transforming them into pseudo-primaries which can be imaged with classic migration algorithms. The transform can be achieved with seismic interferometry \parencite{schuster_interferometric/daylight_2004}, which states that the response recorded at a point~B to a fictive source located in~A can be obtained by cross-correlating the passive measurements made in~A and~B. When A and~B are two receivers located at the surface, the cross-correlation of the traces recorded in A and B results in the primary reflection linking the two points \parencite{sheng_migrating_2001,shan_source-receiver_2003,%
schuster_interferometric/daylight_2004,%
jiang_migration_2005,jiang_migration_2007}. Alternatively \textcite{berkhout_transformation_2003,berkhout_imaging_2006} deconvolve the data with an estimation of the primaries (obtained with \gls{srme} for example) to transform first-order surface multiple into pseudo-primaries, second-order surface multiple into pseudo-first-order surface multiple, and so on... Then a new application of the \gls{srme} method isolates the pseudo-primaries from higher-order multiples and a classic migration algorithm can be applied.

Conversely the usual migration algorithms can be adapted to multiple reflections. A first attempt was made by \textcite{reiter_imaging_1991} who used Kirchhoff migration to separately image primary reflections as well as ghost reflection against the surface in \gls{obc} data. Their approach assumes that multiples and primaries are separated in time in the data and is thus restricted to deep-water acquisition. More recently this constraint was released by the development of sensors able to separate up and downgoing energy, thus discriminating between upgoing primaries and downgoing receiver ghosts \parencite{muijs_prestack_2007,whitmore_imaging_2010}.

The usual imaging condition for primary reflection can be transposed to multiple reflections. It still consists of correlating a source and a receiver wavefield, except that the source wavefield is the forward propagation of the entire data set and the receiver wavefield the backward propagation of the same data set without primary reflections \parencite{berkhout_multiple_1994,%
guitton_shot-profile_2002,liu_extended_2014}. Compared to the usual primary imaging condition, the source wavefield does not originate at a single point but is an areal shot with  virtual sources located at every receiver position. An advantage of this method is that there is no need to estimate the shape of the source wavelet as the new pseudo-source is directly the data recording. With this methodology, two images can be constructed, one from the migration of primaries, and one from the migration of multiples. 

However, a correct separation of the primaries and the multiples is required and the final image is contaminated with \emph{cross-talk} artefacts (\cref{fig:crosstalk_liu}). These spurious events are caused by downgoing energy interfering with an unrelated upgoing energy, for example a primary with a second-order surface multiple. To avoid these artefacts, each order of multiple should be isolated to produce a separate migrated image.
%-------------------------------------------
\begin{figure}[!htbp]
\includegraphics[scale=0.4]{Images/crosstalk.png}
\caption{Example of cross-talk artefacts. The exact velocity model is displayed on the left panel. The right panel shows the image reconstructed from the migration of multiple reflections. C and D correspond to the true reflectors. All other events including~A and~B are cross-talk artefacts \parencite[from][]{liu_reverse_2011}.}
\label{fig:crosstalk_liu}
\end{figure}
%-------------------------------------------

As a remedy to the \emph{cross-talk} artefacts, the use of the deconvolution imaging condition instead of the cross-correlation has been studied \parencite{muijs_prestack_2007,whitmore_imaging_2010}, however this method has limited success in complex media \parencite{poole_deconvolution_2010,tu_limitations_2013}. More recent studies use a least-squares inversion approach based on fitting the data reconstructed with the estimated reflectivity model and observed data \parencite{brown_least-squares_2005,verschuur_seismic_2011,%
wong_imaging_2014,zhang_least-squares_2014,tu_fast_2015}. When modelling data with reflectivity image contaminated with cross-talk artefact, extra-reflections will appear in synthetic data. These will be back-projected in the reflectivity update to attenuate the cross-talk artefacts.

If internal multiples are properly modelled, they can be included in the inversion procedure. This is the approach of the \gls{fwm} technique \parencite{berkhout_combining_2012,berkhout_review_2014-2,soni_full-wavefield_2014} where the modelling is performed with the Full Wavefield Modelling procedure presented in \cref{sec:multiple_modelling}. The amplitude differences between modelled and observed data is converted into reflectivity update. %To handle internal multiples, a first estimate of the reflectivity computed with a conventionnal method can be used to model first-order internal multiples and compare them to observed data \parencite{malcolm_seismic_2009,fleury_increasing_2013}.

More recently, new imaging techniques have been derived from the resolution of the Marchenko equation \parencite{wapenaar_marchenko_2014}. As in reverse-time migration, Marchenko imaging consists of correlating a downgoing and an upgoing wavefield at each point of the subsurface. However in reverse-time migration, the downgoing wavefield is approximated by direct propagation in a non-reflective media of the source wavelet to the image point, and the upgoing wavefield is obtained in the same way by backpropagation of the residuals. In Marchenko imaging, all the internal multiples reflections encountered by the source and receiver wavefield are accounted for, so that the wavefields correlated at each subsurface location correctly include internal multiples \parencite{behura_autofocus_2014}. As a consequence, Marchenko imaging does not generate cross-talk. These wavefields are obtained from surface recordings and an estimation of the direct arrival recorded at each subsurface location by iteratively solving the Marchenko equation \parencite{rose_single-sided_2002,wapenaar_proposal_2011}. Note that a velocity model is needed for the direct arrival estimation. \textcite{singh_marchenko_2015} extend this procedure to use simultaneously primaries, internal multiples and surface multiples.