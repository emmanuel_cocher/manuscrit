\chapter{Analytic expression for the MVA gradient after direct inversion in 1D}\label{app:grad_analytic}
This appendix is related to \cref{sec:direct_inversion} and details the derivation of the analytic expression of the background velocity update obtained after direct inversion in the 1D case \pcref{eq:Ginv,eq:Qinv_Green,eq:Qinv_explicit} (we do not detail the case of the adjoint \pcref{eq:Gadj,eq:Qadj_Green,eq:Qadj_explicit}, which is very similar). The purpose is to obtain an analytic expression providing results similar to those obtained with the adjoint-state method and wave-equations operators (\cref{app:inversion}). Note that the derivation does not assume a homogeneous background velocity model.

We recall the expression of the objective function,
\begin{equation}\label{eq:app_J1}
	J_1\ap{inv}\croch{c_0}=\frac{1}{2}\int_z a^2\vecz c_0^{2\beta}\vecz \xiinv^2\vecz \diff z\text{,}
\end{equation}
where \(\xiinv\) is obtained by application of the approximate inverse \(\Fdag\) to observed data \pcref{eq:Fdag_1D_Green},
\begin{equation}
	\xiinv\vecz=\croch[\big]{\Fdag\Pobs}\vecz
			   =-4\pderiv{}{z}\int_\omega \frac{1}{\iom^*} \waveletDcnv^*\vecom \croch[\bigg]{\pderiv{}{z_s} G_0^*\paren{s,z,\omega}}^2 P\vecom \diff \omega\text{,}\label{eq:app_Fdag_Green}
\end{equation}
In \cref{sec:direct_inversion}, we have derived the following expression for the gradient \(\Ginv\) of \(J_1\ap{inv}\) with respect to \(c_0\),
\begin{equation}
	\Ginv\paren{y}=
	\beta c_0^{2\beta-1}\vecy a^2\vecy \xiinv^2\vecy
	+ \int_z \Qinv\paren{y,z}\etainv\vecz \diff z
\end{equation}
where \(\etainv\) is obtained as the partial derivative of \(J_1\) with respect to \(\xiinv\),
\begin{equation}
 \etainv\vecz=c_0^{2\beta}\vecz a^2\vecz \xiinv\vecz\text{,}
\end{equation}
and \(\Qinv:\Esp\mapsto\Msp\) is defined as
\begin{equation}
	\Qinv\paren{y,z}=\pderiv{\croch[\big]{\Fdag\Pobs}\vecz}{c_0\vecy}\text{.}
\end{equation}
The purpose of this appendix is the derivation of the expression of \(\Qinv\) given in \cref{eq:Qinv_Green}.


%-------------------------
%-------------------------
%-------------------------
\section{Derivation of \texorpdfstring{\(\Qinv\)}{operator Qinv}}
The derivation starts from \cref{eq:app_Fdag_Green}, hence we need an expression for the derivatives of the Green's function \(G_0\szom\) with respect to \(s\) and \(z\), and with respect to the value of \(c_0\) at depth \(y\). These expressions are given by,
\begin{subequations}
\begin{empheq}[left=\empheqlbrace]{align}
	\pderiv{G_0\szom}{c_0\vecy}&=\label{eq:app_Green_derivative}
		\frac{2\iomsqr}{c_0^3\vecy}G_0\paren{s,y,\omega}G_0\paren{y,z,\omega}\text{,}\\
	\pderiv{G_0\szom}{z_s}&=\label{eq:app_Green_derivative_s}
		-\frac{\iom}{c_0\paren{s}}G_0\szom\text{,}\\
	\pderiv{G_0\szom}{z}&=\label{eq:app_Green_derivative_z}
	\frac{\iom}{c_0\vecz}G_0\szom\text{.}
\end{empheq}
\end{subequations}
The derivative with respect to \(c_0\vecy\)~\eqref{eq:app_Green_derivative} is given by the first-order Born approximation, reviewed in \cref{sec:Born_first_order}: the perturbation \(\delta G_0\paren{s,z,\omega}\) of a reference Green's function \(G_0\paren{s,z,\omega}\) due to a perturbation \(\deltac\) of the reference background velocity model \(c_0\) obeys \pcref{eq:we_P1}
\begin{equation}
	\paren[\bigg]{\frac{\iomsqr}{c_0^2}-\Delta}\delta G_0\paren{s,z,\omega}=\frac{2\deltac\vecz}{c_0^3\vecz}\iomsqr G_0\paren{s,z,\omega}\text{,}
\end{equation}
and \(\delta G_0\paren{s,z,\omega}\) can be expressed as \pcref{eq:def_P1_nonextended},
\begin{equation}
	\delta G_0\paren{s,z,\omega}=\int_x \iomsqr G_0\paren{s,x,\omega}\frac{2\deltac\vecxx}{c_0^3\vecxx}G_0\paren{y,z,\omega} \diff x\text{.}
\end{equation}
\Cref{eq:app_Green_derivative} is obtained as the derivative of this expression with respect to \(\delta c\).

We now detail the important steps in the derivation of \(\Qinv\). Starting from the expression of \(\Fdag\Pobs\) given in \cref{eq:app_Fdag_Green}, we compute the following quantity,
\begin{subequations}
\begin{align}
	&\pderiv{}{c_0\vecy}\croch[\bigg]{\pscal[\big]{\etainv}{\Fdag\Pobs}_\Esp}\\
	=&-\pderiv{}{c_0\vecy}\accol[\bigg]{4\pscal[\bigg]{\pderiv{}{z}\etainv}{
		\int_\omega\frac{1}{\ioma}\waveletDcnv^*\vecom\croch[\bigg]{\pderiv{}{z_s}G_0^*\szom}^2\Pobs\vecom\diff\omega
		}_\Esp}\text{,}\\
\shortintertext{where we have explicitly separated the term \(\partial\etainv/\partial z\) as is done in the calculation with the adjoint-state method \pcref{eqn:deflambda01d}. Then, using \cref{eq:app_Green_derivative_s},}
	=&-4\pscal[\bigg]{\pderiv{}{z}\etainv}{\pderiv{}{c_0\vecy}
		\int_\omega\frac{\iom}{c_0^2\paren{s}}\waveletDcnv^*\vecom\croch[\big]{G_0^*\szom}^2\Pobs\vecom\diff\omega
		}_\Esp\\
	=&16\pscal[\bigg]{\etainv}{\pderiv{}{z}
		\int_\omega\frac{\iom^3}{c_0^2\paren{s}c_0^3\paren{y}}\waveletDcnv^*\vecom G_0^*\szom G_0^*\syom G_0^*\yzom\Pobs\vecom\diff\omega
		}_\Esp,
\end{align}
\end{subequations}
where we have used \cref{eq:app_Green_derivative}. Eventually we need to compute the derivative with respect to \(z\) of the product of Green's functions inside the integral, whose value depends on the relative position of \(y\) and \(z\),
\begin{equation}
	\pderiv{}{z}\croch[\big]{G_0^*\paren{s,z,\omega}G_0^*\paren{y,z,\omega}}
		=\begin{dcases*}
			2\ioma/c_0\vecz & if \(y\leq z\)\text{,}\\
			0     & if \(y\geq z\)\text{,}
		\end{dcases*}
\end{equation}
%\begin{equation}
%	\pderiv{}{z}\croch[\big]{G_0^*\paren{s,z,\omega}G_0^*\paren{z,x,\omega}}
%		=\iom\croch[\big]{p_z\paren{s,z}+p_z\paren{z,x}}G_0^*\paren{s,z,\omega}G_0^*\paren{z,x,\omega}\text{,}
%\end{equation}
%with \(p_z=\partial\tauini{s}{z}/\partial z\). The zero entries in \(\Qinv\) originate from the term in bracket which cancel when \(x\) is below \(z\):
%\begin{equation}
%	p_z\paren{s,z}+p_z\paren{z,x}=
%	\begin{dcases*}
%		2/c_0 & if \(x\leq z\)\text{,}\\
%		0     & otherwise\text{,}
%	\end{dcases*}
%\end{equation}
leading to the expression given in \cref{eq:Qinv_Green},
\begin{equation}\label{eq:app_Qinv_Green}
	\Qinv\paren{y,z}=%
	\begin{dcases*}
		\frac{32}{c_0^2\paren{s}c_0^3\paren{y}c_0\vecz}\int_\omega \iom^4\waveletDcnv^*\vecom G_0^*\paren{s,z} G_0^*\paren{s,y}G_0^*\paren{y,z} \diff \omega
		& if \(y<z\), \\
		0 & if \(y\geq z\),
	  \end{dcases*}
\end{equation}
where the dependence of the Green's function to \(\omega\) have been omitted.

%-------------------------
%-------------------------
%-------------------------
\section{High-frequency expression}
For the numerical applications in \cref{sec:1D_chap5}, we have assumed an homogeneous background velocity model \((c_0\vecz=c_0\)) and replaced the value of the Green's function \(G_0\) in \cref{eq:app_Fdag_Green,eq:app_Qinv_Green} by its asymptotic expression in a 1D homogeneous medium (\(c_0\vecz=c_0\)),
\begin{equation}\label{eq:app_def_Green_1D}
	G_0\paren{s,z,\omega}=\frac{c_0}{2\ioma}\eu^{-\ioma \tauini{s}{z}}\text{.}
\end{equation}
leading to the expressions given in \cref{eq:LinOps_explicit_inv,eq:Qinv_explicit}
\begin{align}
	\xiinv\vecz &=\label{eq:app_Fdag_explicit}
		\frac{2}{c_0}\int_\omega \waveletDcnv^*\vecom \eu^{2\ioma\tau\paren{s,z}} P\vecom \diff \omega\text{,}\\
	\Qinv\paren{y,z} &=\label{eq:app_Qinv_explicit}
		\begin{dcases*}
			-\frac{4}{c_0^3}\int_\omega \iom \waveletDcnv^*\vecom\eu^{\iom\paren[\big]{\tauini{s}{z}+\tauini{s}{y}+\tauini{y}{z}}}\Pobs\vecom \diff \omega& if \(y<z\),\\
			0 & otherwise.
	\end{dcases*}
\end{align}
We check the validity of \cref{eq:app_Qinv_Green} on a numerical example by comparing the gradient obtained with this expression with the gradient obtained by finite differences and slightly perturbed model (\cref{eq:verif_green}). The latter consists of computing the value of \(J_1\) \pcref{eq:app_J1} for velocity models with small velocity perturbations at position \(y\) \(c\vecy=c_0\vecy\pm\delta c_0\). The computation is repeated for each depth \(y\). Note that in this approach, \(\xiinv\) is not obtained through the asymptotic expression~\eqref{eq:app_Fdag_explicit}, but by solving wave-equations as described in appendix~\ref{sec:Fdag}.
%--------------------------------------------
\begin{figure}[!htbp]
	\includegraphics{Figures/compar_grad}
	\caption{Comparison of the reflectivity (left) and background velocity update (right) obtained with the analytic expressions \pcref{eq:app_Fdag_explicit,eq:app_Qinv_explicit} (green dashed line) with the results obtained by solving wave-equations with a finite differences code (red) as described in \cref{app:gradient}. A third value for the gradient (blue) is obtained by computing the value of \(\xiinv\) with wave-equations propagation for slightly perturbed velocity model. For this example, we took \(\beta=3/2\) and use the same reflectivity and background velocity model as in \cref{sec:1D_chap5}, that is homogeneous exact and initial velocity models (\SIlist{3000;2500}{\meter\per\second}) and an exact reflectivity model made of a single reflector at \SI{400}{\meter} depth.}
	\label{eq:verif_green}
\end{figure}
%--------------------------------------------

It is actually important to note that \cref{eq:app_Qinv_explicit} has been obtained by replacing the value of the Green's functions by their high-frequency approximation \pcref{eq:app_def_Green_1D} after the calculation of the derivative with respect to \(c_0\).
One would obtain a different expression by applying the derivative with respect to \(c_0\) to a high-frequency approximation of \(\Fdag\Pobs\) such as \cref{eq:app_Fdag_explicit}. In particular, operator \(\Qinv\) would not be upper triangular any more.

To illustrate this point, we consider the derivative of the Green's function \(G\szom\) with respect to \(c_0\vecy\). Applying the derivative first leads to \cref{eq:app_Green_derivative} and then replacing the Green's function gives
\begin{equation}
	\pderiv{G_0\szom}{c_0\vecy}=\frac{2}{c_0}\eu^{-\iom\paren{\tauini{s}{y}+\tauini{y}{z}}}\text{.}
\end{equation}
On the other hand, computing the derivative of the high-frequency approximation~\eqref{eq:app_Green_derivative} with respect to \(c_0\vecy\) leads to
\begin{equation}
	\pderiv{G_0\szom}{c_0\vecy}=
	\begin{dcases*}
		\frac{2}{c_0}\eu^{-\iom\paren{\tauini{s}{y}+\tauini{y}{z}}} & if \(y<z\),\\
		0 & if \(y\geq z\)\text{,}
	\end{dcases*}
\end{equation}
as the derivative of the traveltime \(\tauini{s}{z}\) with respect to \(c_0\vecy\) reads
\begin{equation}
	\pderiv{\tauini{s}{z}}{c_0\vecy}=
		\begin{dcases*}
			-\frac{1}{c_0^2\vecy} & if \(y<z\)\\
			0 & if \(y\geq z\)\text{.}
		\end{dcases*}
\end{equation}
The main difference is that the high-frequency approximation of the Green's function between \(s\) and \(z\) is insensitive to a perturbation below \(z\), whereas the general Green's function may be affected by a perturbation of the velocity model below \(z\) as a diffracting point for example (\cref{fig:Green_1D}, right).
 %-------------------------
\begin{figure}[!htbp]
\includegraphics[scale=1]{Figures/1D_drawing}
\caption{Perturbation of the Green's function \(G_0\szom\) giving the response in \(z\) to a source in \(s\), when the perturbation is located above \(z\) (left) and below \(z\) (right).}
\label{fig:Green_1D}
\end{figure}