\section{Synthetic examples}\label{sec:examples_chap4}
In this section, the behaviour of preconditioned iterative migration is illustrated on simple examples similar to the ones presented in \cref{chap:iter_migr,chap:IMVA}.

We first consider the case of primary reflections only. Note that in this case, iterative migration is not necessarily required as direct inversion (\cref{sec:article_Herve}) already provides a satisfactory reflectivity image. The primary-only case is studied here to test the efficiency of the preconditioner to accelerate the convergence of iterative migration.
Then we consider a case with first-order surface-related multiples to determine if the preconditioner also improves the convergence rate of migration although it has not been designed for multiples.
Finally, we investigate the efficiency of the preconditioner for the resolution of the adjoint problem with primaries only. We also compare the background velocity updates obtained with direct and iterative inversions.

\subsection{Preconditioned iterative migration with primaries only}\label{sec:examples_chap4_prim_migr}
In this first example, we consider primary reflections only and a simple model with a single reflector located at \SI{300}{\meter} depth in a homogeneous velocity model (\SI{3000}{\meter\per\second}).
% There is actually no need to define an iterative procedure for this case as the pseudo-inverse \(\Fdag\Pobs\) already provides a sufficiently good result for subsequent \gls{mva}. However, as iterations are still needed in the case of multiples, we first examine the behaviour of the preconditioned iterative procedure in this simple case.
%---------------------------------------------------------------------
\minisec{First iterate of the preconditioned iterative scheme}
We first compare the results obtained with the inverse formula derived in \cref{sec:article_Herve} to the results of a single iteration of preconditioned iterative migration. The corresponding reflectivity images are defined as \(\Fdag\Pobs\) and \(\Fdag\FdagT\Fadj\Pobs=\Fdag\paren{F\Fdag}^T\Pobs\), respectively. Note that the approximate inverse has been designed such that \(\paren{F\Fdag}\) is close to the identity operator in the observed data space. Then we expect its transpose \(\paren{F\Fdag}^T\) to be close to identity as well, and thus the first iterate should be close to the result of direct inversion.

Before looking at the reflectivity images, we compare the result of application of the operators \(\paren{F\Fdag}\) and \(\paren{F\Fdag}^T\) to observed data in a too low, correct and too high velocity model (\cref{fig:chap4_prim_testQualInv_Residuals}). All results are expected to be very close to observed data. In practice, both operators correctly reproduce input data and residuals are mainly located at large surface offsets. However the transposed version \(\paren{F\Fdag}^T\) yields low-frequency artefacts before the primary event, especially for a too high velocity model. These artefacts might have undesirable effects on \glspl{cig}.

We now consider the application of the approximate inverse to three data sets: \begin{enumerate*}[label=(\arabic*)]
	\item observed data,
	\item result of application of \(\paren{F\Fdag}\) to observed data and
	\item result of application of \(\paren{F\Fdag}^T\) to observed data
\end{enumerate*} (\cref{fig:chap4_prim_testQualInv_CIGs}). The first case corresponds to direct inversion and the third one to the first iterate of preconditioned iterative migration, while the second case is considered only for comparison. The \glspl{cig} obtained in the second case are closer to the result of direct inversion than the first iterate of the preconditioned scheme. In particular the low frequency artefacts observed on reconstructed data result in low frequency energy above the reflector in the case of a too high-velocity. This effect is less visible for a correct and a too low initial velocity. Nonetheless, these \glspl{cig} show much improvement compared to the adjoint \(\Fadj\Pobs\), first iterate of the non-preconditioned scheme. In particular, migration artefacts are greatly attenuated.
%The introduction of the preconditioner results in energy localised around the reflector because of the source deconvolution.  Migration artefacts are also greatly attenuated. The result is actually very close to the inverse. The main difference is residual energy above the reflector, which is nonetheless much weaker than in the non-preconditioned case. The same conclusions hold for a correct and too high migration velocity (\cref{fig:chap4_prim_prcnd_3_CIGs}).

Eventually we plot the value of the normalised \gls{mva} objective function obtained in homogeneous background velocity models after one iteration with and without preconditioning, and the one obtained after direct inversion (\cref{fig:chap4_prim_plotJV_one_iter}). With the inverse, the function is perfectly centred at the correct velocity model. Because of residual energy above the reflector, the minimum after one preconditioned iteration is not at the correct velocity model, but it is still more satisfactory than in the case of a single iteration without preconditioning.

This suggests that a single iteration of preconditioned iterative migration is not as efficient as direct inversion, and that additional iterations are required. This is actually not an issue as the preconditioned iterative scheme is not meant to be used in this primary-only case but in the more complex case of multiples for which iterations are required anyway to attenuate cross-talk artefacts.
%------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/chap4_prim_testQualInv/chap4_prim_testQualInv_Residuals}
\caption{Result of the application of \(\paren{F\Fdag}\) (first column) and \(\paren{F\Fdag}^T\) (third column) to observed data \(\Pobs\). The corresponding residuals are plotted in the second and fourth column, respectively. All plots share the same colour scale. Observed data are computed in a homogeneous velocity model (\(c_0\ap{ex}=\SI{3000}{\meter\per\second}\)) with a single flat reflector. The velocity model used to compute \(F\) and \(\Fdag\) is too low (top, \(c_0=\SI{2500}{\meter\per\second}\)), correct (middle, \(c_0=\SI{3000}{\meter\per\second}\)), and too high (bottom, \(c_0=\SI{3500}{\meter\per\second}\)). The last two columns show traces extracted at positions indicated by dashed lines on the fourth column.}
\label{fig:chap4_prim_testQualInv_Residuals}
\end{figure}
%------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/chap4_prim_testQualInv/chap4_prim_testQualInv_CIGs}
\caption[CIGs obtained by application of the pseudo-inverse operator to the results of \cref{fig:chap4_prim_testQualInv_Residuals}]{Common Images Gathers obtained in a too low (\SI{2500}{\meter\per\second}, top), correct (\SI{3000}{\meter\per\second}, middle), and too high (\SI{3500}{\meter\per\second} center) background velocity model. \glspl{cig} are computed by of application of the pseudo-inverse \(\Fdag\) to observed data (1st column), to observed data premultiplied by \(\paren{F\Fdag}\) (2nd column) and to observed data premultiplied by \(\paren{F\Fdag}^T\) (3rd column). We also consider the application of the adjoint operator \(\Fadj\) to observed data (4th column). The \glspl{cig} of the first and third columns correspond to the results obtained by direct inversion as in \cref{sec:article_Herve}, and to the first iterate of the preconditioned iterative scheme described in \cref{sec:weights_iterative}, respectively. The \glspl{cig} of the second and fourth columns are displayed for comparison.}
\label{fig:chap4_prim_testQualInv_CIGs}
\end{figure}
%------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/chap4_prim_plotJV_one_iter/chap4_prim_plotJV_one_iter}
\caption{Value of the normalised objective function of \gls{mva} obtained in homogeneous velocity models for the approximate inverse and after one iteration with and without preconditioning. The correct velocity is \SI{3000}{\meter\per\second}. The oscillation in the dashed blue curve is due to low frequency energy appearing above the reflector in CIGs especially for too high velocities (third column in \cref{fig:chap4_prim_testQualInv_CIGs}).}
\label{fig:chap4_prim_plotJV_one_iter}
\end{figure}
%------------------------------------------
%(\cref{fig:chap4_prim_3meths_xi}) in a too low velocity model (\SI{2500}{\meter\per\second}). Note that the first iterate in this case is not the inverse \(\Fdag \Pobs\). It actually reads \(\Fdag\FdagT \Fadj \Pobs\), meaning that it is the result of the application of the preconditioner \(\Fdag\FdagT:\Esp\mapsto\Esp\) to the usual gradient \(F^T\Pobs\).
%------------------------------------------
%\begin{figure}[!htbp]
%\includegraphics{Figures/chap4_prim_3meths_xi/chap4_prim_3meths_xi}
%\caption{Result of iterative migration after one iteration of classic migration \(F^T\Pobs\) (top), one iteration of preconditioned migration \(\Fdag\FdagT F^T \Pobs\) (middle), and inverse of the Born modelling operator \(\Fdag\Pobs\).}
%\label{fig:chap4_prim_3meths_xi}
%\end{figure}
%%------------------------------------------
%\begin{figure}[!htbp]
%\includegraphics{Figures/chap4_prim_inv_migr_prcnd_3_CIGs/chap4_prim_inv_migr_prcnd_3_CIGs}
%\caption{\glspl{cig} obtained after one iteration of classical migration (top), one iteration of preconditioned iterative migration (middle) and with the inverse (bottom). The migration velocity is too low (\SI{2500}{\meter\per\second}, left), correct (\SI{3000}{\meter\per\second}, center) and too high (\SI{3500}{\meter\per\second}, right).}
%\label{fig:chap4_prim_inv_migr_prcnd_3_CIGs}
%\end{figure}
%------------------------------------------

\minisec{Results after a few iterations}
We now perform ten preconditioned iterations on the same model (\cref{fig:chap4_prim_prcnd_3_CIGs}). The main effects of iterations on \glspl{cig} are additional deconvolution, strengthening of energy at large values of \(h\) (in the case of an incorrect velocity model) and attenuation of residual energy above the reflector.
We plot the value of the objective function across iterations obtained with and without preconditioning (\cref{fig:chap4_prim_obj_fct}). We consider the data misfit, as well as the norm of the residuals associated with the linear system~\eqref{eq:lin_system_chap4}: \(\norm[\big]{\paren[\big]{\Fadj F+\lreg I}\xi-\Fadj\Pobs}\), which is actually the value of the gradient of \(J_0\) with respect to \(\xi\). As expected, the preconditioned strategy is faster at the first iteration, but both versions reach the same level of data misfit after a few iterations. As in \textcite{hou_approximate_2015}, we consider a third possibility: iterative migration is performed without preconditioning but with the approximate inverse as initial guess \(\Fdag\Pobs\). In this case very few progress is made at the first iteration because the approximate inverse is already a very good solution to the minimisation problem. Note that we cannot consider the preconditioned iterative minimisation with the approximate inverse as initial value because this would require the ability to compute \(\paren{\Fdag}^{-1}\).

Eventually we compute the value of the normalised \gls{mva} objective function for several homogeneous background velocity models (\cref{fig:chap4_prim_Jv}) after several iterations of the preconditioned scheme. We saw that the minimum of the function does not correspond to the correct value of \(c_0\) after a single iteration (\cref{fig:chap4_prim_plotJV_one_iter}). This is progressively corrected and after five iterations, the objective function of \gls{mva} is stable and minimum for the correct velocity.
%------------------------------------------
%\begin{figure}[!htbp]
%\includegraphics{Figures/chap4_prim_xi_iter/chap4_prim_xi_iter}
%\caption{Results of iterative migration with preconditioning in a too slow velocity model (\SI{2500}{\meter\per\second}). A section at \(h=\SI{0}{\meter}\) (left), a \gls{cig} at \(x=\SI{810}{\meter}\) (center) and the same \gls{cig} multiplied by \(\abs{h}\) are shown after \numlist{1;2;5;10} iterations (from top to bottom).}
%\label{fig:chap4_prim_xi_iter}
%\end{figure}
%------------------------------------------
\begin{figure}[!htbp]
%\begin{minipage}[t]{0.59\linewidth}
	%\centering
	\includegraphics{Figures/chap4_prim_prcnd_3_CIGs/chap4_prim_prcnd_3_CIGs}
	\caption{\glspl{cig} obtained after one (top) and ten (bottom) iterations of preconditioned iterative migration in a too low (\SI{2500}{\meter\per\second}, left), correct (\SI{3000}{\meter\per\second}, centre) and too high (\SI{3500}{\meter\per\second}, right) velocity model.}
	\label{fig:chap4_prim_prcnd_3_CIGs}
%\end{minipage}\hfill%
%\begin{minipage}[t]{0.39\linewidth}
	%\centering	
%\end{minipage}
\end{figure}
%------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/chap4_prim_obj_fct/chap4_prim_obj_fct}
\caption[Decrease of the migration objective function with and without preconditioning]{Results of iterative migration in a too slow velocity model (\SI{2500}{\meter\per\second}) with primaries only. We plot the relative data misfit (left) and the relative normal residual associated with the linear system \eqref{eq:lin_system_chap4}.
% (c) Conjugate Gradient objective function; (d) \gls{mva} objective function. This four cost functions
Red and blue curves correspond to the case of a zero initial guess (\(\xi\itn{1}=0\)) with classical and preconditioned iterative migration, respectively. The green curve corresponds to non-preconditioned migration initiated with the approximate inverse \(\xi\itn{1}=\Fdag \Pobs\). The dashed line corresponds to the value obtained for the approximate inverse \(\Fdag \Pobs\).}
\label{fig:chap4_prim_obj_fct}
\end{figure}
%------------------------------------------
\begin{figure}[!htbp]
	\includegraphics{Figures/chap4_prim_Jv/chap4_prim_Jv}
	\caption{Value of the \gls{mva} objective function obtained in homogeneous background velocity models after several iterations with preconditioning. The correct velocity is \SI{3000}{\meter\per\second}.}
	\label{fig:chap4_prim_Jv}
\end{figure}
%------------------------------------------

In summary, we have shown that a single iteration of preconditioned iterative migration yields slightly degraded results compared to direct inversion because \(\paren{F\Fdag}^T\) is close but not equal to the identity. Its application to observed data yield small low-frequency artefacts. However the \glspl{cig} obtain after a single iteration with the preconditioner is free of migration artefacts, contrary to the non-preconditioned case. Besides the preconditioner also improves the convergence rate of iterative migration.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Preconditioned iterative migration with multiples}\label{sec:examples_chap4_mult_migr}
We now discuss an example with first-order surface-related multiples. We consider an exact model with a background velocity increasing with depth, from \SI{2000}{\meter\per\second} at the surface to \SI{3000}{\meter\per\second} at \SI{1000}{\meter} depth (\cref{fig:chap4_mult_modele}). The model is 2D but laterally invariant. The reflectivity model consists of a single reflector located at \SI{475}{\meter} depth. Contrary to the case of homogeneous models considered in the preceding chapters, we expect events in \glspl{cig} corresponding to primaries and multiples (interpreted as primaries) to have different focusing behaviour. In particular, cross-talk artefacts should not be focused for the correct velocity model and should favour too low velocities.

We consider several initial velocity models linearly increasing with depth. All start at the correct velocity at the surface (\SI{2000}{\meter\per\second}), but the velocity at \SI{1000}{\meter} depth ranges from \SI{2200}{\meter\per\second} to \SI{3300}{\meter\per\second} (\cref{fig:chap4_mult_modele}). We first compute \glspl{cig} by applying the approximate inverse formula to observed data (\cref{fig:chap4_mult_CIGs_inv}) and plot the corresponding \gls{mva} objective function (\cref{fig:chap4_mult_Jvinv}). As observed data are computed with the Born approximation, primaries and multiples are available separately and we also plot the value of the objective function obtained for observed data containing primaries only or multiples only with the same range of initial velocity models (\cref{fig:chap4_mult_modele}). In the case of primaries only, the objective function is minimum for the correct velocity, similarly to the results of \cref{sec:examples_chap4_prim_migr}. Cross-talk artefacts linked to multiples focus for a too low velocity model (\cref{fig:chap4_mult_CIGs_inv}, 3rd column) and curve upward for the correct velocity model (\cref{fig:chap4_mult_CIGs_inv}, 4th column). As a consequence, the minimum of the \gls{mva} objective function considering both primaries and multiples is not obtained for the correct velocity model.
%------------------------------------------
\begin{figure}[!htbp]
	\begin{subfigure}[t]{0.49\linewidth}
		\includegraphics{Figures/chap4_mult/chap4_mult_modele}
		\caption[Background velocity models]{Exact background velocity model (red, solid) and some of the initial models considered (black, dashed). The reflectivity model consists of a single reflector localised at \(z=\SI{475}{\meter}\).}
		\label{fig:chap4_mult_modele}
	\end{subfigure}\hfill%
  	\begin{subfigure}[t]{0.49\linewidth}
		\includegraphics{Figures/chap4_mult/chap4_mult_Jvinv}
		\caption[Normalised MVA objective function]{Normalised \gls{mva} objective function. Each function has been mapped linearly to the interval \(\croch{0,1}\). The correct model corresponds to a velocity of \(\SI{3000}{\meter\per\second}\) at \SI{1000}{\meter} depth.}
		\label{fig:chap4_mult_Jvinv}
	\end{subfigure}
	\begin{subfigure}[b]{\linewidth}
		\includegraphics{Figures/chap4_mult/chap4_mult_CIGs_inv}
		\caption[Common Image Gathers]{Central \glspl{cig} \(\xi\ap{inv}=\Fdag\Pobs\) obtained for different background velocity models (top) and application of the annihilator to these \glspl{cig} \(A\xi\ap{inv}/\norm{\xi\ap{inv}}_\Esp\) (bottom). The plots of the bottom row have the same colour scale.}
		\label{fig:chap4_mult_CIGs_inv}
	\end{subfigure}
\caption{Result of application of the approximate inverse \(\Fdag\) to observed data containing both primaries and first-order surface-related multiples with several initial background velocity model increasing with depth.}
\label{fig:chap4_mult_inv}
\end{figure}
%------------------------------------------

We now perform ten iteration of iterative migration without and with preconditioning using the highest initial velocity model considered in \cref{fig:chap4_mult_inv}. As in the primaries-only example, we also consider the case of iterative migration without preconditioning starting with the approximate inverse \(\Fdag\Pobs\) as initial guess. To help removing residual energy at large offset, we add regularisation with the Huber norm \parencite{guitton_robust_2003}. In the first iterations, the migration objective function decreases faster with preconditioning and with the strategy initialised with the approximate inverse (\cref{fig:chap4_mult_J}), but the data misfit is similar for the three cases after a few iterations. The final \glspl{cig} obtained with the three strategies are very similar (\cref{fig:chap4_mult_CIGs_iter}), but the best attenuation of cross-talk artefacts is obtained in the preconditioned case. This is the optimisation strategy used in the following of the study.

Another practical advantage of the preconditioned strategy is related with the determination of an optimal step length. A general issue of linesearch techniques is the choice of an initial guess \(\alpha_0\) initiating the algorithm. Using the preconditioner, the linesearch strategy of \textcite{more_line_1994} returns step lengths in the interval \(\croch{\num{0.1},\num{1}}\) at each iteration. Hence \(\alpha_0=1\) is always a good initial guess. Besides we can even directly set \(\alpha=1\) for the first iteration and save the computational cost of a linesearch. The first gradient is very close to the approximate inverse, hence setting \(\alpha=1\) yield a reflectivity model \(\xi\itn{2}\) very well explaining primaries, so that residuals are mostly due to multiples.

Finally we consider the same range of initial velocity models as in \cref{fig:chap4_mult_Jvinv} and we compute the value of the \gls{mva} objective function obtained after several iterations of preconditioned iterative migration (\cref{fig:chap4_mult_Jviter}). After a single iteration, we obtain a result similar to the direct inversion case with a minimum obtained for a too low velocity model. With iterations cross-talk artefacts are attenuated and the minimum is progressively shifted to the correct velocity model.
%------------------------------------------
\begin{figure}[!htbp]
\begin{minipage}[b]{0.49\linewidth}
	\includegraphics{Figures/chap4_mult/chap4_mult_CIGs_iter}
	\caption{\glspl{cig} obtained after ten iterations of migration without (left) and with (centre) preconditioning starting with \(\xi\itn{1}=0\), and ten iterations of migration without preconditioning starting with \(\xi\itn{1}=\Fdag\Pobs\) (right).}
	\label{fig:chap4_mult_CIGs_iter}
\end{minipage}\hfill%
\begin{minipage}[b]{0.49\linewidth}
	\includegraphics{Figures/chap4_mult/chap4_mult_J}
	\caption{Decrease of the migration objective function using three optimisation strategies, corresponding to the \glspl{cig} \cref{fig:chap4_mult_inv}.}
	\label{fig:chap4_mult_J}
	\includegraphics{Figures/chap4_mult/chap4_mult_Jviter}
	\caption{Normalised \gls{mva} objective function obtained after several iterations of preconditioned iterative migration. Each function has been mapped linearly to the interval \(\croch{0,1}\). The correct model correspond to a velocity of \SI{3000}{\meter\per\second} at \SI{1000}{\meter} depth.}
	\label{fig:chap4_mult_Jviter}
\end{minipage}
\end{figure}
%------------------------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Preconditioned resolution of the adjoint problem and associated gradient}\label{sec:examples_chap4_prim_adj}
Eventually we consider the computation of the gradient of \(J_1\) with method B as described in \cref{sec:methodB}. We go back to the primaries-only example of \cref{sec:examples_chap4_prim_migr} and test if the preconditioner accelerates the convergence rate of the adjoint problem. We will show that the results are not fully satisfactory; hence we defer to \cref{chap:RMVA} the numerical applications in the case of multiples  (\cref{sec:chap5_ex_mult}).


We assume that the direct problem has been solved in seven iterations, meaning that the final reflectivity is \(\xi\itn{8}\). The adjoint problem then consists of finding the solution \(\eta\in\Esp\) of
\begin{equation}\label{eq:adj_prob_chap4_ex}
	H \eta = b\text{,}
\end{equation}
where \(H\) is the Hessian matrix of \(J_0\), which is independent of \(\xi\) in the linear case, and \(b=A^T A \xi\itn{8}\) is the image residual. \(A\) is the annihilator consisting of a multiplication by the subsurface offset and a power of the background velocity model,
\begin{equation}
	\croch{A\xi}\xh=c_0^\beta\vecx\abs{h}\xi\xh\text{.}
\end{equation}
We choose \(\beta=3/2\), the value resulting in smooth gradients free of artefacts around the reflector in the case of direct inversion (\cref{sec:article_Herve}).
The adjoint problem is solved with ten iterations of the conjugate gradient algorithm with and without preconditioner. We compare the convergence rate obtained with both strategies. Contrary to the direct problem, there is no norm in the observed data-space \(\Dobs\) associated with the resolution of \cref{eq:adj_prob_chap4_ex} which can be used as a convergence criteria. Here the conjugate gradient algorithm actually minimises
\begin{equation}\label{eq:norm_cg_chap4}
	\phi\croch{\eta}=\frac{1}{2}\pscal{\eta}{H\eta}_\Esp - \pscal{b}{\eta}_\Esp\text{.}
\end{equation}
A difficulty with this objective function is that we do not know the value obtained for the ideal solution of the linear problem. Therefore we will also look at the relative normal residuals of the linear system~\eqref{eq:adj_prob_chap4_ex}, defined as
\begin{equation}
	J\ap{Normal~Res.}\croch{\xi}=\frac{\norm{H\xi-b}_\Esp}{\norm{b}_\Esp}\text{,}
\end{equation}
which can be interpreted as the normalised norm of the gradient of \(\phi\) \pcref{eq:norm_cg_chap4}.
Note that contrary to \(\phi\), there is no guarantee that the value of the normal residual decreases at each iteration.

\minisec{Small regularisation}
We first consider a case with small regularisation, as in \cref{sec:examples_chap4_prim_migr}. Using the same right-hand side term \(b\), that is the same value for \(\xi\itn{8}\), we solve the adjoint problem in ten iterations, without and with preconditioning  (\cref{fig:chap4_prim_JNadj_sansReg}). We obtain a much lower convergence rate than for the direct problem, even with the preconditioner. The normal residual associated with the last value \(\eta\itnB{8}{11}\) is just a little lower than for the initial value \(\eta\itnB{8}{1}\). At the last iteration, the adjoint variable \(\eta\itnB{8}{11}\) is very oscillating (\cref{fig:chap4_prim_Heta_sansReg}), and residuals of the linear system are located mainly near the surface, that is away from the reflector position. This is related to the observations made in \cref{chap:IMVA}: weak energy at large values of \(h\), which does not bear relevant kinematic information, has large influence on the resolution of the adjoint problem. Besides, note that the zero value at \(h=0\) in \(b\) is not recovered in \(H\eta\itnB{8}{11}\).
%------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/chap4_JNadj/pb_adjoint_JN_sansReg}
\caption{Convergence of the adjoint problem in the case of small regularisation.}
\label{fig:chap4_prim_JNadj_sansReg}
\end{figure}
%------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/chap4_prim_Heta/chap4_prim_Heta_sansReg}
\caption{Residuals related to the linear adjoint problem \eqref{eq:adj_prob_chap4_ex} after ten iterations. We show the central \gls{cig} of the adjoint variable \(\eta\) (1st column) obtained after ten iterations without preconditioning (top) and with preconditioning (bottom). Then we compare \(H\eta\) (2nd column) with the right-hand side of the adjoint problem (3rd column) and compare this two vectors. On each line, the three most right plots share the same colour scale.}
\label{fig:chap4_prim_Heta_sansReg}
\end{figure}
%------------------------------------------

An undesirable consequence of the slow resolution of the adjoint problem is that the value of the associated gradient \(G\itnB{7}{M}\) does not converge to a stable final value (\cref{fig:chap4_prim_gradients_sansReg,fig:chap4_prim_gradients_cut_sansReg}). The sign of the gradient may even change across iterations (for example \(G\itnB{7}{4}\) in the preconditioned case). Without preconditioning, the gradients exhibit vertical spurious oscillations very similar to those observed in the preceding chapter. When the adjoint problem is solved with the preconditioner we do not observe these artefacts. However the final value is quite different from the one obtained after direct inversion, which is much smoother and homogeneous (\cref{fig:chap4_prim_gVfinal_sansReg}). More investigation is needed to understand these results (\cref{chap:RMVA}). In the following we examine if a stronger regularisation helps to overcome these difficulties.
%------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/chap4_prim_gradients/10gradcompar_sansReg}
\caption{Gradients obtained, in the case of small regularisation, after seven iteration of preconditioned iterative migration for the successive values of the adjoint variable (from top to bottom). The left and right columns correspond to the resolution of the adjoint problem without and with preconditioner, respectively. The gradient is not really stable from one iteration to another.}% Using the notations of \cref{chap:IMVA}, the gradients correspond to \(G\itnB{8}{M+1}\) with \(M=1,...,10\).}
\label{fig:chap4_prim_gradients_sansReg}
\end{figure}
%------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/chap4_prim_gradients/10grad_cut_sansReg}
\caption{Central trace (\(x=\SI{810}{\meter}\)) of the gradients shown in \cref{fig:chap4_prim_gradients_sansReg}. The value of the gradient above the reflector oscillates from one iteration to another.}
\label{fig:chap4_prim_gradients_cut_sansReg}
\end{figure}
%------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/chap4_prim_gradients/gVfinal_sansReg}
\caption{Gradients obtained by direct inversion (top), and after iterative inversion (middle without preconditioning and bottom with preconditioning) with small regularisation (\(N=7\) and \(M=10\), corresponding to the bottom row of \cref{fig:chap4_prim_gradients_cut_sansReg}). The right column shows a trace extracted at the middle position (corresponding to the dashed orange lines in \cref{fig:chap4_prim_gradients_cut_sansReg} for the iterative cases).}
\label{fig:chap4_prim_gVfinal_sansReg}
\end{figure}
%------------------------------------------

\minisec{Stronger regularisation}
As in \cref{chap:IMVA}, we consider increasing the value of the regularisation coefficient \(\lreg\). The convergence of the adjoint problem is improved but still much slower than for the direct problem (\cref{fig:chap4_prim_JNadj_avecReg}). The preconditioner does not clearly improve the convergence rate. This can be explained by the fact that the operator \(\FdagT\paren{\Fadj F +\lreg I}\Fdag\) is theoretically close to the identity only if the regularisation weight is not too high. The final value \(\eta\itnB{8}{11}\) is still very oscillating (\cref{fig:chap4_prim_Heta_avecReg}). Although the residuals of the linear system are weaker in the preconditioned case, the zero value at \(h=0\) in \(b\) is not recovered in \(H\eta\itn{8}\) and there are still strong residuals away from the reflector.
%------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/chap4_JNadj/pb_adjoint_JN_avecReg}
\caption{Convergence of the adjoint problem in the case of stronger regularisation.}
\label{fig:chap4_prim_JNadj_avecReg}
\end{figure}
%------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/chap4_prim_Heta/chap4_prim_Heta_avecReg}
\caption{Same as \cref{fig:chap4_prim_Heta_sansReg} in the case of stronger regularisation.}
\label{fig:chap4_prim_Heta_avecReg}
\end{figure}
%------------------------------------------

With stronger regularisation, the spurious oscillations appearing when no preconditioning is applied (\cref{fig:chap4_prim_gradients_sansReg}) are greatly attenuated (\cref{fig:chap4_prim_gradients_avecReg}), as already noticed in \cref{chap:IMVA}. With the preconditioner, the gradients are smoother and more homogeneous (\cref{fig:chap4_prim_gradients_avecReg,fig:chap4_prim_gradients_cut_avecReg}), but do not converge to a stable solution. Besides they are still quite different from the gradient obtained by direct inversion (\cref{fig:chap4_prim_gVfinal_avecReg}). In particular there are still residual oscillations around the reflector position.
%------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/chap4_prim_gradients/10gradcompar_avecReg}
\caption{Same as \cref{fig:chap4_prim_JNadj_sansReg}, but with a stronger regularisation.}
\label{fig:chap4_prim_gradients_avecReg}
\end{figure}
%------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/chap4_prim_gradients/10grad_cut_avecReg}
\caption{Central trace (\(x=\SI{810}{\meter}\)) of the gradients shown in \cref{fig:chap4_prim_gradients_avecReg}.}
\label{fig:chap4_prim_gradients_cut_avecReg}
\end{figure}
%------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/chap4_prim_gradients/gVfinal_avecReg}
\caption{Same as \cref{fig:chap4_prim_gVfinal_avecReg} with stronger regularisation.}
\label{fig:chap4_prim_gVfinal_avecReg}
\end{figure}
%------------------------------------------

As a conclusion, we note that although direct inversion and iterative migration yield very similar reflectivity images, the associated gradients are quite different. In particular, the use of the preconditioner does not relax the need for strong regularisation to obtain smooth velocity updates. An other issue associated with the iterative case is the difficult resolution of the adjoint problem, and the instability of the associated gradient. Further investigation is needed to understand this unwanted behaviour and design a more robust strategy. This is the purpose of \cref{chap:RMVA}.

