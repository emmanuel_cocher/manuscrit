\section{Introduction of migration weights in the iterative migration process}\label{sec:weights_iterative}
In \cref{sec:article_Herve}, we have introduced an approximate inverse \(\Fdag\) of the extended Born-modelling operator \(F\). Applying this pseudo-inverse to observed data produces \glspl{cig} free of migration artefacts, leading to homogeneous gradients of the associated \gls{mva} objective function. In this section we explain how \(\Fdag\) can be introduced in the iterative migration process. This modification is expected to accelerate the convergence speed of iterative migration, but not necessarily to stabilise the MVA gradient of the nested optimisation process. 

The weighted iterative scheme defined here is different from the one proposed by \textcite{hou_accelerating_2016}. Their strategy is based on the approximate inverse formula of \textcite{hou_approximate_2015}, which can be expressed as a modification of the usual adjoint with two weighting operators:
\begin{subequations}
\begin{equation}
	\Fdag =\Wmod \Fadj \Wdata\text{.}
\end{equation}
Then \textcite{hou_accelerating_2016} propose a modified iterative scheme using the standard linear conjugate gradient algorithm with new definitions for the norms in the extended model and data spaces,
\begin{align}
	\pscal{\xi}{\chi}_{\Wmod}&=\pscal{\xi}{\Wmod\chi}_\Esp\text{,}\\
	\pscal{u}{v}_{\Wdata}&=\pscal{u}{\Wdata v}_{\Dobs}\text{,}
\end{align}
\end{subequations}
with the requirement that \(\Wmod\) and \(\Wdata\) are positive definite operators.

The inversion formula derived in \cref{sec:article_Herve} cannot be decomposed in this way, so another strategy has to be defined. %In \textcite{lameloise_improving_2014}, a new objective function is defined
%\begin{equation}
	%J_0=\frac{1}{2}\norm[\big]{Q\paren{\vec{x}_s,\vec{x}_r,\vec{x}}\paren{P-\Pobs}}^2_\Dobs \text{,}
%\end{equation}
%where the filter \(Q\) introduces the migration weights at the image point \(\vec{x}\) such that the Hessian of the modified misfit function is diagonal. However \(Q\) cannot be interpreted as a preconditioner as it depends on the image point \(\vec{x}\). \textcite{lameloise_improving_2014} actually performs a single iteration.
Here we propose to use the approximate inverse \(\Fdag\) as a right preconditioner. Note that the use of an amplitude-preserving migration operator as a preconditioner has already been studied in other contexts. \textcite{sevink_three-dimensional_1996} use it for iterative migration, but do not consider an extended model space. \textcite{metivier_combining_2015} use the Beylkin migration operator as a left preconditioner in \gls{fwi} and show that it allows to accelerate the recovery of the short-scale structure of the velocity model.

In the following, we detail a preconditioned iterative scheme based on the inverse defined in \cref{sec:article_Herve}, first in the general non-linear case, then in the linear-case, corresponding to primaries modelled with the first-order Born approximation.

%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%
\subsection{Preconditioned iterative migration: non-linear case}
In the general non-linear case, the migration objective function \(J_0:\Esp\mapsto\mathbb{R}\) is defined as (the implicit dependence of \(J_0\) and \(P\) on \(c_0\) is omitted in this section and in \cref{sec:precond_linear} for the sake of clarity),
\begin{equation}\label{eq:defJ0_nonlin_chap4}
	J_0\croch{\xi}=\frac{1}{2}\norm[\big]{M P\croch{\xi}-\Pobs}^2_\Dobs+\phi\croch{\xi}\text{,}
\end{equation}
where operator \(M:\Dsp\mapsto\Dobs\) selects the value of calculated data at receiver positions.
Preconditioning may be seen as a change of variables. Let us define a new variable \(\Pc{\xi}\in\Dobs\) such as
\begin{equation}
	\xi=\Fdag\Pc{\xi}\text{,}
\end{equation}
where \(\Fdag\) is the pseudo-inverse of the extended Born modelling operator \(F\), as defined in \cref{sec:article_Herve}. Replacing \(\xi\) by \(\Fdag\Pc{\xi}\) in \cref{eq:defJ0_nonlin_chap4} leads to a new objective function \(\Pc{J_0}:\Dobs\mapsto\mathbb{R}\),
\begin{equation}\label{eq:defJ0_nonlin_pc_chap4}
	\Pc{J_0}\croch[\Big]{\Pc{\xi}}=\frac{1}{2}\norm[\Big]{M P\croch[\Big]{\Fdag\Pc{\xi}}-\Pobs}^2_\Dobs+\phi\croch[\Big]{\Fdag\Pc{\xi}}\text{,}
\end{equation}
with
\begin{subequations}
\begin{empheq}[left=\empheqlbrace]{align}
	\Pc{J_0}\croch[\Big]{\Pc{\xi}}&=J_0\croch{\xi}\text{,}\label{eq:precond_equiv_J}\\
	\pderiv{\Pc{J_0}}{\Pc{\xi}}\croch[\Big]{\Pc{\xi}}&=\FdagT \pderiv{J_0}{\xi}\croch{\xi}\label{eq:precond_equiv_grad}
\end{empheq}
\end{subequations}
The preconditioned iterative migration scheme now consists of finding the value of \(\Pc{\xi}\) in the data space minimising \(\Pc{J_0}\). The corresponding value in the image space is a minimiser of \(J_0\) and can be computed as \(\xi=\Fdag\Pc{\xi}\). The inclusion of the preconditioner in the minimisation algorithm is easy. The algorithm is initiated with \(\xi\itn{1}=0\) in \(\Esp\), so that the initial value in \(\Dobs\) is \(\Pc{\xi}\itn{1}=0\). Then at each iteration, two steps are added to the classic algorithm, so that the value of \(\Pc{\xi}\itn{n+1}\) is updated from \(\Pc{\xi}\itn{n}\) in four steps:
\begin{enumerate}[label=(\roman*)]
	\item \label{item:precond_step1}given \(\Pc{\xi}\itn{n}\in\Dobs\), compute the corresponding value \(\xi\itn{n}\in\Esp\) according to \(\xi\itn{n}=\Fdag\Pc{\xi}\itn{n}\);
	\item compute the value and gradient of the original objective function: \(J_0\croch{\xi\itn{n}}\) and \(g\itn{n}=\pderivinline{J_0}{\xi}\croch{\xi\itn{n}}\in\Esp\);
	\item \label{item:precond_step3} using \cref{eq:precond_equiv_grad,eq:precond_equiv_grad}, go back to the \(\Dobs\)-space  to determine the value of the new objective function \(\Pc{J_0}\), and its gradient \(\Pc{g}\itn{n}\in\Dobs\) with respect to \(\Pc{\xi}\);
	\item determine a descent direction \(\Pc{d}\itn{n}\in\Dobs\) and a step length \(\alpha\itn{n}\in\mathbb{R}\) to update \(\Pc{\xi}\),
		\begin{equation}
			\Pc{\xi}\itn{n+1}=\Pc{\xi}\itn{n}+\alpha\itn{n}\Pc{d}\itn{n}\text{,}
		\end{equation}
		and go back to step~\ref{item:precond_step1}.
\end{enumerate}
This optimisation algorithm is the same as the one described in \cref{chap:iter_migr}, except that it is performed in the observed data space instead of the extended model space. Only two additional calculations are needed: one to go from \(\Dobs\) to \(\Esp\) (step~\ref{item:precond_step1}) where the usual gradient is computed, and one to go back to \(\Dobs\) (step~\ref{item:precond_step3}).
This operation requires the ability to compute the adjoint \(\FdagT\) of the approximate inverse \(\Fdag\). The calculation of this operator is detailed in \cref{app:Fdag_adjoint}. It can be interpreted as a weighted modelling favouring small reflection angles.
Note that if the algorithm was initiated with a non-zero value \(\xi\itn{1}\), we would need to compute the inverse \(\paren{\Fdag}^{-1}\) of \(\Fdag\) to compute the corresponding value in the \(\Dobs\)-space. Starting with \(\xi\itn{1}=0\), this operator does not need to be computed.

The main computational cost remains the computation of the usual gradient in \(\Esp\) (resolution of seven wave-equations, as described in \cref{tab:cost_grad}). With preconditioning, four additional wave-equations have to be solved (\cref{tab:cost_precond}), two for the application of \(\Fdag\) to \(\Pc{\xi}\itn{n}\) and two for the application of \(\FdagT\) to \(\partial J_0/\partial \xi \croch{\xi\itn{n}}\) (\cref{app:Fdag_adjoint}).
%---------------------------
\begin{table}[!htbp]
\begin{tabular}{ccc}
 \toprule
 \(\Fdag\)  & \(\phantom{\Esp}\Dobs\mapsto\Esp\phantom{\Dobs}\) & 2 \\ \midrule
 \(\FdagT\) & \(\phantom{\Dobs}\Esp\mapsto\Dobs\phantom{\Esp}\) & 2 \\ \midrule
 \(\FdagT \Fdag\) & \(\Esp\mapsto\Esp\) & 3 \\ \bottomrule
\end{tabular}
\caption{Number of wave-equations to be solved to compute the inverse map \(\Fdag\), its adjoint \(\FdagT\) and the composition of the two operators. The number indicated here should be multiplied by the number of source positions considered in the acquisition.}
\label{tab:cost_precond}
\end{table}
%---------------------------

In the non-linear case of multiples, the efficiency of the preconditioned strategy will be demonstrated on a numerical example (\cref{sec:examples_chap4_mult_migr}). We now consider the special linear case of primaries only, where we explain theoretically why we expect the choice of \(\Fdag\) as preconditioner to accelerate the convergence.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Preconditioned iterative migration: linear case}\label{sec:precond_linear}
We now consider the linear case, where the modelling is described by the linear extended Born modelling operator \(F:\Esp\mapsto\Dobs\) and the regularisation is the \(\ell_2\)-norm. In this specific case, the implementation simplifies and the benefits of the preconditioner are easier to understand.

The classic \pcref{eq:defJ0_nonlin_chap4} and preconditioned \pcref{eq:defJ0_nonlin_pc_chap4} versions of the objective function read
\begin{subequations}
\begin{empheq}[left=\empheqlbrace]{align}
	J_0\croch{\xi}&=
		\frac{1}{2}\norm[\big]{F\xi-\Pobs}^2_\Dobs
		+\lreg\frac{1}{2}\norm[\big]{\xi}^2_\Esp\text{,}\\
	\Pc{J_0}\croch[\Big]{\Pc{\xi}}&=
		\frac{1}{2}\norm[\big]{F\Fdag\Pc{\xi}-\Pobs}^2_\Dobs
		+\lreg\frac{1}{2}\norm[\big]{\Fdag\Pc{\xi}}^2_\Esp\text{.}
\end{empheq}
\end{subequations}
Deriving these expressions with respect to \(\xi\) and \(\Pc{\xi}\) respectively lead to two linear systems
\begin{subequations}
\begin{empheq}[left=\empheqlbrace]{align}
	\paren[\big]{\Fadj F + \lreg I}\xi&=\Fadj \Pobs\text{,}\label{eq:lin_system_chap4}\\
	\FdagT\paren[\big]{\Fadj F + \lreg I}\Fdag\Pc{\xi}&=\FdagT \Fadj \Pobs\text{.}\label{eq:lin_system_pc_chap4}
\end{empheq}
\end{subequations}
%The operator \(\FdagT\paren[\big]{\Fadj F + \lreg I}\Fdag\) is positive semi-definite by construction, and
The linear system \pcref{eq:lin_system_pc_chap4} can be solved with a classic conjugate-gradient (CG) algorithm \parencite[112]{nocedal_numerical_2006}. Note that this system is defined in the \(\Dobs\)-space, contrary to the classic linear system \pcref{eq:lin_system_chap4}.

As \(\Fdag\) is a pseudo inverse of \(F\), the operator \(\FdagT \Fadj F \Fdag=\paren{F\Fdag}^T\paren{F\Fdag}\) should be close to the identity operator. Then, provided that the regularisation weight \(\lreg\) is relatively small, the resolution of this new linear system should be much faster and should require less iterations.

In practice, the resolution of the preconditioned system~\eqref{eq:lin_system_pc_chap4} with the classic CG-algorithm is equivalent to the resolution of the usual system~\eqref{eq:lin_system_chap4} with the preconditioned CG-algorithm  \parencite[119]{nocedal_numerical_2006} and the preconditioner \(\Fdag\FdagT:\Esp\mapsto\Esp\). This has the advantage of fewer modifications in the implementation, in particular the resolution is still performed in the \(\Esp\)-space. Compared to the usual CG-algorithm, the additional computational cost is the product at each iteration of \(\Fdag\FdagT\) with a vector of \(\Esp\), requiring three additional wave-equations to be solved (\cref{tab:cost_precond}).

\subsection{Preconditioned resolution of the linear adjoint problem}
We now consider the computation of the gradient of the \gls{mva} objective function studied in \cref{chap:IMVA}. Using method A, introducing the preconditioner would change the equations to be solved and would add new contributions to the total gradient, as \(\Fdag\) and \(\FdagT\) depend on \(c_0\). In the following, we consider only method B where the final reflectivity is defined as the exact solution of the linear system \(\pderiv{J_0}{\xi}\paren{\xi^\infty}=0\). Then introducing the preconditioner does not change the computation of the gradient, but only allows to find an approximate solution of this linear system in a reduced number of iterations.

Moreover, the preconditioner can also be used to accelerate the resolution of the adjoint system, which is a linear problem
\begin{equation}
	\croch[\bigg]{\pderiv[2]{J_0}{\xi}\croch[\big]{c_0,\xi\itn{N+1}}}\eta = A^T A \xi\itn{N+1}\text{,}
\end{equation}
where the adjoint variable is noted \(\eta\) from now on. In the linear case, this system is very similar to the direct problem \pcref{eq:lin_system_chap4},
\begin{equation}
	\paren[\big]{\Fadj F + \lreg I}\eta = A^T A \xi\itn{N+1}\text{.}
\end{equation}

Then the preconditioned CG-algorithm with \(\Fdag\FdagT\) as preconditioner can be used to solve the adjoint problem, both in the linear and non-linear cases. In the linear case at least, the preconditioner should in theory accelerate the resolution of the adjoint problem. This will be tested in \cref{sec:examples_chap4_prim_adj}.

In summary, preconditioning does not change the computation of the gradient of the \gls{mva} objective function with method B \pcref{eq:GradBapprox}. The calculation only uses the last iterates \(\xi\itn{N+1}\) and \(\eta\itnB{N+1}{M+1}\) of the direct and adjoint problems. Preconditioning only provides a more efficient way to find a solution to these problems in fewer iterations.