\section{Robustness of Iterative Migration Velocity Analysis}\label{sec:dense}
This section presents and discusses results obtained when a different modelling tool is used for the modelling of synthetic observed data and for inversion.
In tests~A, B, and C (\crefrange{sec:app_lowfreq}{sec:app_drho}), reflections are due to a dense model perturbation (velocity model perturbation or density model perturbation). Before presenting the results of these tests, we show the velocity updates obtained in the reference case where the same modelling tool is used to compute observed data and calculated data.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Reference case}\label{sec:app_reference}
In the reference case, the reflectivity model is laterally invariant with rapidly varying values between \SIlist{100;500}{\meter} depth, and zero values above and below (\cref{fig:dense_model}, right). Using a Ricker wavelet with maximum frequency \SI{40}{\hertz}, we obtain observed data with several events overlapping with one another (\cref{fig:dense_Pobs}).
The first gradient of the \gls{mva} objective function is expected to be negative above \(z=\SI{500}{\meter}\) and zero below.
%--------------------------------------------------------
\begin{figure}[!htbp]
\begin{minipage}[t]{0.49\linewidth}
	\centering
	\includegraphics{Figures_dense/modele_dense_xi}
	\caption{Exact and initial background velocity model (left) and dense exact reflectivity model (right).}
	\label{fig:dense_model}
\end{minipage}\hfill%
\begin{minipage}[t]{0.49\linewidth}
	\centering
	\includegraphics{Figures_dense/Pobs_velocity_prim_mult}
	\caption{Observed data computed in the model of \cref{fig:dense_model} under the first-order Born approximation (left) and a second-order Born approximation (right).}
	\label{fig:dense_Pobs}
\end{minipage}
\end{figure}
%---------------------------------------------------------

We first compute the gradients obtained after direct inversion. In the primaries only case, all events in \glspl{cig} have a downward curvature (\cref{fig:dense_ref_InvPrim}). As expected, the gradient is negative above the deeper reflector and zero below. When direct inversion is applied to both primaries and first-order surface multiples, additional cross-talk artefacts appear above and below the deeper reflector with an upward curvature (\cref{fig:dense_ref_InvMult}). As a consequence positive values appear on the gradient below the deeper reflector as well as strong edge effects.

In the case of primaries only, the combination of iterative migration and filtering with \(\Fdag F\) leads to \glspl{cig} very similar to those obtained by direct inversion (\cref{fig:dense_ref_Prim}). The associated gradient has small non-zero values below the reflector but is quite close to the result shown in \cref{fig:dense_ref_InvPrim}. Finally in the case of multiples, iterative migration succeeds in removing cross-talks artefacts resulting in filtered \glspl{cig} very similar to the previous case (\cref{fig:dense_ref_Mult10}). The associated gradient has a consistent negative value above the deeper reflector and spurious positive values below are attenuated compared to \cref{fig:dense_ref_InvMult}. These results are satisfactory, and smoothing the gradients yields consistent background velocity updates for subsequent inversion (\cref{fig:dense_ref_smooth_prim,fig:dense_ref_smooth_mult}). Note also that the \enquote{truncated gradients} (\cref{fig:dense_ref_smooth_prim,fig:dense_ref_smooth_mult}) are very similar to the gradient of \cref{fig:dense_ref_InvPrim}.
%---------------------------------------------------------
\begin{figure}[!htbp]
\captionsetup[subfigure]{justification=raggedright,singlelinecheck=false}
\captionsetup{singlelinecheck=false}
%-----
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:dense_ref_InvPrim}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}%\centering
	\includegraphics{Figures_dense/CIG_gV_InvPrim}
\end{subfigure}
%-----
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:dense_ref_InvMult}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}%\centering
	\includegraphics{Figures_dense/CIG_gV_InvMult}
\end{subfigure}
%-----
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:dense_ref_Prim}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}%\centering
	\includegraphics{Figures_dense/CIG_gV_Prim}
\end{subfigure}
%-----
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:dense_ref_Mult10}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}%\centering
	\raggedright
	\includegraphics{Figures_dense/CIG_gV_Mult10}
\end{subfigure}
%-----
\caption[CIGs and gradient obtained in the reference case]{%
Gradient and \glspl{cig} obtained with the model of \cref{fig:dense_model} with different inversion strategies with and without multiples:%
\begin{enumerate}[nosep,label=(\alph*)]
	\item direct inversion on primaries only;
	\item direct inversion on primaries and multiples;
	\item iterative migration on primaries only (\(N=7\) and \(M=10\));
	\item iterative migration on primaries and multiples (\(N=10\) and \(M=8\)).
\end{enumerate}%
}
\label{fig:dense_ref}
\end{figure}
%----------------------------------------------------------
\begin{figure}[!htbp]
\captionsetup[subfigure]{justification=raggedright,singlelinecheck=false}
\captionsetup{singlelinecheck=false}
\begin{subfigure}{0pt}\addtocounter{subfigure}{4}\end{subfigure}%
%-----
%\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:dense_ref_smooth_prim}}%
%\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}%\centering
\begin{subfigure}[t]{1.0\linewidth}
	\includegraphics{Figures_dense/CIG_gV_Smooth_Prim}
	\phantomsubcaption\label{fig:dense_ref_smooth_prim}%
	\phantomsubcaption\label{fig:dense_ref_TG_prim}%
\end{subfigure}
%-----
%\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:dense_ref_smooth_mult}}%
%\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}%\centering
\begin{subfigure}[t]{1.0\linewidth}
	\includegraphics{Figures_dense/CIG_gV_Smooth_Mult}
	\phantomsubcaption\label{fig:dense_ref_smooth_mult}%
	\phantomsubcaption\label{fig:dense_ref_TG_mult}%
\end{subfigure}
%-----
\caption{Velocity updates obtained by smoothing the total gradient obtained after iterative inversion (left) and by keeping only the contribution due to the pseudo-inverse (right), in the case of primaries (top), and with both primaries and multiples (bottom).}
\label{fig:dense_ref_smooth}
\end{figure}
%----------------------------------------------------------

In summary we obtain results very similar to the case of a single reflector studied in \cref{sec:chap5_ex_mult}. However, the gradients presented here are less homogeneous. This can be interpreted as a superposition of the contributions due to the different events in the \glspl{cig}, although the gradient obtained with all the events in the \glspl{cig} is not formally a linear combination of the gradient obtained from \glspl{cig} containing single events. We also observe strong edge effects for all gradients. As already mentioned in \cref{chap:RMVA} (see also \cref{fig:2D_prim_gV_extended}), we are mainly interested in the central part of the gradient. With a larger model and the same acquisition parameters, the edge effects would remain similar while the central part would be extended. In these examples we have considered relatively small models to limit the computational cost. We now begin the series of tests summarised in \cref{tab:tests_chap6} and focus on the central part of the gradients.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\subsection{Truncation of late multiples}\label{sec:app_truncation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Test A -- Sensibility to the lack of low frequencies in observed data}\label{sec:app_lowfreq}
In this first test, we use two different source wavelets for observations and inversion. Low frequencies up to \SI{5}{\hertz} are removed from the Ricker wavelet for observations (\cref{fig:src_badsrc2}). The original Ricker wavelet is used during inversion.
%---------------------------------------------------------
\begin{figure}[!htbp]
	\includegraphics{Figures_dense/src_badsrc2}
	\caption[Source wavelet used to generate observed data in test A.]{Source wavelet used to generate observed data in test A. It is obtained by setting to zero low frequencies in the original Ricker.}
	\label{fig:src_badsrc2}
\end{figure}
%---------------------------------------------------------

In the eight cases (\cref{fig:dense_badsrc2,fig:dense_badsrc2_smooth}), we obtain results very similar to the reference case (\cref{fig:dense_ref,fig:dense_ref_smooth}). The main difference lies in the shape of events in \glspl{cig} modified in a similar way as the source wavelet is, but this has almost no influence on the shape of the gradient. We conclude that the behaviour of the \gls{mva} techniques analysed in the preceding chapters with the standard Ricker wavelet should not be altered by the lack of low-frequencies inherent to real data acquisition. This is a different behaviour than for \gls{fwi} \parencite{sirgue_efficient_2004,virieux_overview_2009}. To our knowledge, the importance of source wavelet estimation has not been studied in the \gls{mva} literature yet.
%---------------------------------------------------------
\begin{figure}[!htbp]
\captionsetup[subfigure]{justification=raggedright,singlelinecheck=false}
\captionsetup{singlelinecheck=false}
%-----
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:dense_badsrc2_InvPrim}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}\centering
	\includegraphics{Figures_dense/CIG_gV_InvPrim_badsrc2}
\end{subfigure}
%-----
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:dense_badsrc2_InvMult}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}\centering
	\includegraphics{Figures_dense/CIG_gV_InvMult_badsrc2}
\end{subfigure}
%-----
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:dense_badsrc2_Prim}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}\centering
	\includegraphics{Figures_dense/CIG_gV_Prim_badsrc2}
\end{subfigure}
%-----
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:dense_badsrc2_Mult10}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}\centering
	\raggedright
	\includegraphics{Figures_dense/CIG_gV_Mult10_badsrc2}
\end{subfigure}
%-----
\caption[CIGs and gradient obtained for test A]{Same as \cref{fig:dense_ref} when observed data are generated with the source wavelet shown in \cref{fig:src_badsrc2} (test A).}
\label{fig:dense_badsrc2}
\end{figure}
%---------------------------------------------------------
%---------------------------------------------------------
\begin{figure}[!htbp]
\captionsetup[subfigure]{justification=raggedright,singlelinecheck=false}
%\captionsetup{singlelinecheck=false}
%-----
%\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:dense_badsrc2_smooth_prim}}%
%\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}%\centering
\begin{subfigure}[t]{\linewidth}
	\includegraphics{Figures_dense/CIG_gV_Smooth_Prim_badsrc2}
	\phantomsubcaption\label{fig:dense_badsrc2_smooth_prim}%
	\phantomsubcaption\label{fig:dense_badsrc2_TG_prim}%
\end{subfigure}
%-----
%\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:dense_badsrc2_smooth_mult}}%
%\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}%\centering
\begin{subfigure}[t]{\linewidth}
	\includegraphics{Figures_dense/CIG_gV_Smooth_Mult_badsrc2}
	\phantomsubcaption\label{fig:dense_badsrc2_smooth_mult}%
	\phantomsubcaption\label{fig:dense_badsrc2_TG_mult}%
\end{subfigure}
%-----
%
\caption{Same as \cref{fig:dense_ref_smooth} for the gradients presented in \cref{fig:dense_badsrc2} (test A).}
\label{fig:dense_badsrc2_smooth}
\end{figure}
%---------------------------------------------------------


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Test B -- Sensibility to inaccuracies of source estimation}\label{sec:app_source}
In test B, observed data are generated with the same reflectivity model as in the reference, but with a different source wavelet, defined as the derivative of the original Ricker wavelet (\cref{fig:src_badsrc}). Inversion is still performed with the standard Ricker.
%------------------------------------------
\begin{figure}[!htbp]
	\includegraphics{Figures_dense/src_badsrc}
	\caption[Source wavelet used to generate observed data in test B.]{Source wavelet used to generate observed data in test B, obtained as the time derivative of the original Ricker wavelet.}
	\label{fig:src_badsrc}
\end{figure}
%------------------------------------------

The result of direct inversion is very similar to the reference case when applied to primaries only (\cref{fig:dense_ref_InvPrim,fig:dense_badsrc_InvPrim}).
However multiples have a larger influence than in the reference case (\cref{fig:dense_ref_InvMult,fig:dense_badsrc_InvMult}): cross-talk artefacts in \glspl{cig} have larger amplitudes compared to events due to primaries and the associated positive values in the gradient spread over a larger area.
This may be related to the stretching of events in \glspl{cig}, decreasing with depth (\cref{fig:dense_badsrc_stretch}), so that deeper events have lower frequencies. Applying a time derivative to the source wavelet in observed data changes the frequency content of \glspl{cig}, and strengthens deeper events. Note that this is only a partial explanation as the approximate inverse should correct for the stretch.
%---------------------------------------------------------
\begin{figure}[!htbp]
\captionsetup[subfigure]{justification=raggedright,singlelinecheck=false}
\captionsetup{singlelinecheck=false}
%-----
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:dense_badsrc_InvPrim}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}\centering
	\includegraphics{Figures_dense/CIG_gV_InvPrim_badsrc}
\end{subfigure}
%-----
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:dense_badsrc_InvMult}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}\centering
	\includegraphics{Figures_dense/CIG_gV_InvMult_badsrc}
\end{subfigure}
%-----
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:dense_badsrc_Prim}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}\centering
	\includegraphics{Figures_dense/CIG_gV_Prim_badsrc}
\end{subfigure}
%-----
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:dense_badsrc_Mult10}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}\centering
	\raggedright
	\includegraphics{Figures_dense/CIG_gV_Mult10_badsrc}
\end{subfigure}
%-----
\caption[CIGs and gradient obtained for test A]{Same as \cref{fig:dense_ref} when observed data are generated with the source wavelet shown in \cref{fig:src_badsrc} (test B).}
\label{fig:dense_badsrc}
\end{figure}
%---------------------------------------------------------
\begin{figure}[!htbp]
\begin{minipage}[c]{0.49\textwidth}%
%\mbox{}\\[-\baselineskip]%
	\caption{Stretching factor \(c_0/\cos\theta\), with \(\theta\) the half opening angle at the image point, obtained for different depths and the maximum surface offset value \(h\ped{surface}=\SI{700}{\meter}\) in the initial velocity model of \cref{fig:dense_model}.}
	\label{fig:dense_badsrc_stretch}
\end{minipage}\hfill%
\begin{minipage}[c]{0.49\textwidth}%
%\mbox{}\\[-\baselineskip]%
	\includegraphics{Figures_dense/stretching.pdf}
\end{minipage}
\end{figure}
%---------------------------------------------------------

Iterative inversion on primaries only results in \glspl{cig} very similar to the direct inversion case. The gradient is however altered by positive values around \SI{100}{\meter} depth, which are not attenuated by smoothing (\cref{fig:dense_badsrc_smooth_prim}). Note that the \enquote{truncated gradient} (\cref{fig:dense_badsrc_TG_prim}) is remarkably close to the result of direct inversion (\cref{fig:dense_badsrc_InvPrim}). In the case of multiples, iterative migration fails to explain both primaries and multiples correctly (\cref{fig:dense_badsrc_residuals,fig:dense_badsrc_J0}). Some multiple reflections are still misinterpreted as primaries after ten iterations (\cref{fig:dense_badsrc_residuals}, bottom row, between \SIlist{0.45;0.6}{\second}). Actually, the errors in the shape of the source are converted into a different reflector shape compared to the reference case. Primaries and multiples interact once and twice with reflectors respectively, hence changing the shape of the reflector can compensate for the source wavelet in the case of primaries, but this result in a wrong phase for predicted multiples. At the first iteration, the reflectivity is optimised for primary reflection, then little improvement is made in the following iterations for both primaries and multiples (\cref{fig:dense_badsrc_J0}, bottom). This results in poor final data residuals and cross-talk artefacts not being attenuated in the final \glspl{cig} (\cref{fig:dense_badsrc_Mult10}).
This has unwanted consequences on the convergence of the adjoint problem (\cref{fig:dense_JNadj}, green curve), which may not be positive definite in the case of multiples as discussed in \cref{sec:chap5_ex_mult}. A negative curvature is encountered after eight iterations and we stop the conjugate gradient iterations. Although the norm of the objective function associated with the CG-algorithm  still decreases, the relative normal residuals increases from iteration 5, which is not satisfactory. A larger regularisation weight may mitigate this issue, but at the expense of a possible higher final data misfit.
The gradient computed after eight iterations of resolution of the adjoint problem looks consistent (\cref{fig:dense_badsrc_Mult10}) but its value and signs are actually not stable from one iteration to another. Note however that the \enquote{truncated gradient} (\cref{fig:dense_badsrc_TG_mult}) which does not depend on the number of adjoint iterations, shows much improvement compared to \cref{fig:dense_badsrc_InvMult} and therefore stands as a reliable alternative.
%---------------------------------------------------------
\begin{figure}[!htbp]
\begin{subfigure}{0pt}\addtocounter{subfigure}{4}\end{subfigure}%
\begin{subfigure}[t]{\linewidth}
	\includegraphics{Figures_dense/CIG_gV_Smooth_Prim_badsrc}
	\phantomsubcaption\label{fig:dense_badsrc_smooth_prim}%
	\phantomsubcaption\label{fig:dense_badsrc_TG_prim}%
\end{subfigure}
\begin{subfigure}[t]{\linewidth}
	\includegraphics{Figures_dense/CIG_gV_Smooth_Mult_badsrc}
	\phantomsubcaption\label{fig:dense_badsrc_smooth_mult}%
	\phantomsubcaption\label{fig:dense_badsrc_TG_mult}%
\end{subfigure}
\caption{Same as \cref{fig:dense_ref_smooth} for the gradients presented in \cref{fig:dense_badsrc} (test B).}
\label{fig:dense_badsrc_smooth}
\end{figure}
%---------------------------------------------------------
\begin{figure}[!htbp]
\begin{minipage}[t]{0.49\textwidth}\centering
	\includegraphics{Figures_dense/traces_Res_badsrc}
	\caption{Observed data (containing both primaries and multiples) and calculated data obtained after ten iterations of iterative migration in the reference case (top, \cref{fig:dense_ref_Mult10}), and in test B (bottom, \cref{fig:dense_badsrc_Mult10}).}
	\label{fig:dense_badsrc_residuals}
\end{minipage}\hfill%
\begin{minipage}[t]{0.49\textwidth}\centering
	\includegraphics{Figures_dense/JM_badsrc}
	\caption{Migration objective function (red) obtained in the reference case (top) and in test B (bottom). We also compute the data misfit of primaries alone and multiples alone (blue and green, respectively). Note that the red and blue curves in the reference case are almost superimposed.}
	\label{fig:dense_badsrc_J0}
\end{minipage}
\end{figure}
%---------------------------------------------------------
\begin{figure}[!htbp]
	\includegraphics{Figures_dense/JNadj_Prim_ref_ABC}
	\caption{Norm associated with the conjugate gradient algorithm (top) and relative normal residuals (bottom) obtained across iterations for the resolution of the adjoint problem in the reference case as well as in the tests~A, B, C and~D both in the case of primaries only (left), and in the case of primaries and multiples (right). The five case shown here correspond to different linear systems and cannot be directly compared. Note that the relative normal residuals are actually the norm of the gradient of the CG-objective  and may not decrease from one iteration to the following.}
	\label{fig:dense_JNadj}
\end{figure}
%---------------------------------------------------------

This test indicates that the estimation of the source wavelet is a key point for the accuracy of iterative migration. A poor estimate prevents the attenuation of cross-talk artefacts and leads to inconsistent background velocity updates.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Test C -- Density perturbation}\label{sec:app_drho}
%\minisec{Parametrisation \(c\) and \(\rho\)}
We now consider a new exact model for the modelling of observed data. The exact (and initial) background velocity model are the same as those used in the reference case (\cref{fig:dense_model}, left) but here with a zero velocity perturbation. Instead we assume that reflections originate only from a variable density model \(\rho\vecx\), modifying the wave-equation~\eqref{eq:wave_equation} into
\begin{equation}
\frac{\iomsqr}{c^2}P - \rho \nabla\cdot\paren[\bigg]{\frac{1}{\rho}\nabla P}=\wavelet\vecom\deltaxs\text{.}
\end{equation}
For this test, we will decompose the density model \(\rho\vecx\) into \(\rho\vecx=\rho_0\vecx + \delta\rho\vecx\) and model data under a second-order Born approximation. Similar to the velocity perturbation case (\cref{sec:Born_first_order}), we decompose \(P\) into \(P=P_0+\delta P\) and obtain
\begin{align}
\frac{\iomsqr}{c^2}\paren{P_0+\delta P} - \paren{\rho_0+\delta \rho} \nabla\cdot\croch[\bigg]{\frac{1}{\rho_0+\delta \rho}\nabla \paren{P_0+\delta P}}
	&=\wavelet\vecom\deltaxs\text{,}\\
\frac{\iomsqr}{c^2}\paren{P_0+\delta P} - \paren{\rho_0+\delta \rho} \nabla\cdot\croch[\bigg]{\paren[\bigg]{\frac{1}{\rho_0}-\frac{\delta \rho}{\rho_0^2}}\nabla \paren{P_0+\delta P}}
	&=\wavelet\vecom\deltaxs\text{,}
\end{align}
leading to
%\begin{align}
%\frac{\iomsqr}{c^2}\delta P - \rho_0 \nabla\cdot\croch[\bigg]{\frac{1}{\rho_0}\nabla\delta P}
	%&=-\rho_0 \nabla\cdot \paren[\bigg]{\frac{\delta \rho}{\rho_0^2}\nabla P_0} + \delta\rho\nabla\cdot\paren[\bigg]{\frac{1}{\rho_0}\nabla P_0}\text{,}\\
\begin{equation}
	\frac{\iomsqr}{c^2}\delta P - \rho_0 \nabla\cdot\croch[\bigg]{\frac{1}{\rho_0}\nabla\delta P}
	=-\rho_0\nabla\paren[\bigg]{\frac{\delta\rho}{\rho_0^2}}\cdot\nabla P_0+\delta\rho\nabla\paren[\bigg]{\frac{1}{\rho_0}}\cdot\nabla{P_0}\text{,}
\end{equation}
	%\shortintertext{which can be written with the dimensionless reflectivity coefficient \(r=\delta\rho/\paren{2\rho_0}\),}
%\frac{\iomsqr}{c^2}\delta P - \rho_0 \nabla\cdot\croch[\bigg]{\frac{1}{\rho_0}\nabla\delta P}
	%&=2\rho_0\croch[\bigg]{r\nabla\paren[\bigg]{\frac{1}{\rho_0}}
	                     %-\nabla\paren[\bigg]{\frac{r}{\rho_0}}}\cdot\nabla P_0\text{.}
%\end{align}
%\end{subequations}
Assuming a homogeneous background density model \(\rho_0\vecx=\rho_0\), this expression simplifies into
\begin{equation}
\frac{\iomsqr}{c^2}\delta P - \Lapl\delta P
=-\frac{1}{\rho_0}\nabla\delta \rho\cdot\nabla P_0
%=-2\nabla\delta r\cdot\nabla P_0\text{.}
\end{equation}
In test C, we set \(\rho_0=1\), and we consider a laterally invariant density perturbation with the same shape as the velocity perturbation defined in the reference case (\cref{fig:dense_model}, right). The magnitude of the perturbation is determined such that the amplitude of reflection data at zero surface offset is the same as in the reference case (\cref{fig:dense_drho_Pobs}). Moving from a velocity to a density perturbation does not change the kinematics of primary and multiples reflections. The main difference is that amplitudes are lower for wide opening angles, corresponding to large surface offsets of early primary reflections due to the shallower reflectors (\cref{fig:dense_drho_Pobs}, right).
%---------------------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures_dense/Pobs_velocity_density_mult}
\caption{Observed data obtained with a perturbation of the velocity model (1st column) and with a perturbation of the density model (2nd column). Both plots use the same colour scale. Traces extracted at the position indicated by the dashed lines are shown on the two most right panels.}
\label{fig:dense_drho_Pobs}
\end{figure}
%---------------------------------------------------------

This difference of amplitudes has a direct impact on \glspl{cig} obtained by direct inversion. Compared to the reference case (\cref{fig:dense_ref_InvPrim}), the shallower reflectors have much lower amplitudes at large value of the subsurface-offset, so that only the defocusing of the deeper reflectors is interpreted in the velocity analysis (\cref{fig:dense_drho_InvPrim}). The gradient looks more homogeneous but this is due only to the attenuation of shallower reflectors. The same effect appears when multiples are added to observed data. Due to smaller opening angles than primaries, their amplitudes in observed data remain similar to the reference case. As a consequence they have higher influence on velocity analysis: the amplitude of positive energy below the deeper reflector is relatively stronger than the above negative values due to primaries (\cref{fig:dense_drho_InvMult}).
%---------------------------------------------------------
\begin{figure}[!htbp]
\captionsetup[subfigure]{justification=raggedright,singlelinecheck=false}
\captionsetup{singlelinecheck=false}
%-----
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:dense_drho_InvPrim}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}\centering
	\includegraphics{Figures_dense/CIG_gV_InvPrim_drho}
\end{subfigure}
%-----
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:dense_drho_InvMult}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}\centering
	\includegraphics{Figures_dense/CIG_gV_InvMult_drho}
\end{subfigure}
%-----
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:dense_drho_Prim}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}\centering
	\includegraphics{Figures_dense/CIG_gV_Prim_drho}
\end{subfigure}
%-----
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:dense_drho_Mult10}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}\centering
	\raggedright
	\includegraphics{Figures_dense/CIG_gV_Mult10_drho}
\end{subfigure}
%-----
\caption[CIGs and gradient obtained for test A]{Same as \cref{fig:dense_ref} when observed data are generated with a density perturbation instead of a velocity perturbation (test C).}
\label{fig:dense_drho}
\end{figure}
%---------------------------------------------------------

After iterative migration and filtering, we obtain \glspl{cig} very similar to the result shown in \cref{fig:dense_drho_InvPrim}, both in the case of primaries only and in the case of primaries and multiples (\cref{fig:dense_drho_Prim,fig:dense_drho_Mult10}). The final reflectivity correctly explains both primaries and multiples (\cref{fig:dense_drho_residuals}) although reflections are modelled with a velocity perturbation for the determination of the optimal model perturbation. Compared to the reference case (\cref{fig:dense_badsrc_residuals,fig:dense_badsrc_J0}, top), the final misfit is slightly higher for multiples (\cref{fig:dense_drho_J0}), but this does not prevent cross-talk artefacts from being attenuated. The associated gradients are very close to the one obtained in \cref{fig:dense_drho_InvPrim}, especially after smoothing (\cref{fig:dense_drho_smooth_prim,fig:dense_drho_smooth_mult}). Again, the truncated gradients (\cref{fig:dense_drho_TG_prim,fig:dense_drho_TG_mult}) are remarkably close to the velocity update obtained in the direct inversion case, meaning that the reflectivity image obtained after iterative migration allows to faithfully reproduce the kinematics of primary reflections.
%---------------------------------------------------------
\begin{figure}[!htbp]
\begin{minipage}[t]{0.49\textwidth}\centering
	\includegraphics{Figures_dense/traces_Res_drho}
	\caption{Observed data and calculated data obtained after ten iterations of iterative migration for two values of the surface offset, \(h=\SI{0}{\meter}\) (top) and \(h=\SI{417.6}{\meter}\) (bottom) (test~C).}
	\label{fig:dense_drho_residuals}
\end{minipage}\hfill%
\begin{minipage}[t]{0.49\textwidth}\centering
	\includegraphics{Figures_dense/JM_drho}
	\caption{Migration objective function (red) obtained when observed data are modelled with a density model perturbation instead of a velocity model perturbation (test~C).}
	\label{fig:dense_drho_J0}
\end{minipage}
\end{figure}
%---------------------------------------------------------
\begin{figure}[!htbp]
\begin{subfigure}{0pt}\addtocounter{subfigure}{4}\end{subfigure}%
\begin{subfigure}[t]{\linewidth}
	\includegraphics{Figures_dense/CIG_gV_Smooth_Prim_drho}
	\phantomsubcaption\label{fig:dense_drho_smooth_prim}%
	\phantomsubcaption\label{fig:dense_drho_TG_prim}%
\end{subfigure}
\begin{subfigure}[t]{\linewidth}
	\includegraphics{Figures_dense/CIG_gV_Smooth_Mult_drho}
	\phantomsubcaption\label{fig:dense_drho_smooth_mult}%
	\phantomsubcaption\label{fig:dense_drho_TG_mult}%
\end{subfigure}
\caption{Same as \cref{fig:dense_ref_smooth} for the gradients presented in \cref{fig:dense_drho} (test C).}
\label{fig:dense_drho_smooth}
\end{figure}
%---------------------------------------------------------

We conclude from test~C that our approach correctly extracts the kinematic information contained in reflections originating from density perturbations. Iterative migration succeeds in finding a velocity perturbation model explaining correctly both primaries and multiples reflections. The amplitude difference, especially for wide opening angles, between reflections originating from density and velocity perturbation is compensated by a modification of the value of the reflectivity model at large values of \(h\). The \gls{mva} gradient is still consistent, the difference being that a smaller weight is given in the background velocity update to reflections originating from shallow reflectors.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\clearpage
%\minisec{Parametrisation \(c\) and \(\IP\)}
%We parametrise the model with the acoustic impedance \(\IP=\rho c\) instead of \(\rho\), transforming the wave equation into
%\begin{equation}
%\frac{\iomsqr}{c^2}P - \frac{\IP}{c} \nabla\cdot\paren[\bigg]{\frac{c}{\IP}\nabla P}=\wavelet\vecom\deltaxs\text{.}
%\end{equation}
%We consider a perturbation of the acoustic impedance \(\IP=\IPZ+\delta \IP\),
%\begin{subequations}
%\begin{align}
%\frac{\iomsqr}{c^2}\paren{P_0+\delta P} - \frac{\IPZ+\delta \IP}{c} \nabla\cdot\croch[\bigg]{\frac{c}{\IPZ+\delta\IP}\nabla \paren{P_0+\delta P}}
%	&=\wavelet\vecom\deltaxs\\
%\frac{\iomsqr}{c^2}\paren{P_0+\delta P} - \frac{\IPZ+\delta \IP}{c} \nabla\cdot\croch[\bigg]{\paren[\bigg]{\frac{c}{\IPZ}-\frac{c\delta\IP}{\IPZ^2}}\nabla \paren{P_0+\delta P}}
%	&=\wavelet\vecom\deltaxs
%\end{align}
%leading to
%\begin{align}
%\frac{\iomsqr}{c^2}\delta P - \frac{\IPZ}{c} \nabla\cdot\croch[\bigg]{\frac{c}{\IPZ}\nabla\delta P}
%	&=-\frac{\IPZ}{c} \nabla\cdot \paren[\bigg]{\frac{c\delta\IP}{\IPZ^2}\nabla P_0} + \frac{\delta\IP}{c}\nabla\cdot\paren[\bigg]{\frac{c}{\IPZ}\nabla P_0}\\
%	&=-\frac{\IPZ}{c}\nabla\paren[\bigg]{\frac{c\delta\IP}{\IPZ^2}}\cdot\nabla P_0-\frac{\delta\IP}{\IPZ}\Lapl P_0+\frac{\delta\IP}{c}\nabla\cdot\paren[\bigg]{\frac{c}{\IPZ}\nabla P_0}\text{.}
%\end{align}
%\end{subequations}
%Assuming a homogenoeus background impedence model \(\IPZ\vecx=\IPZ\),
%\begin{subequations}
%\begin{align}
%\frac{\iomsqr}{c^2}\delta P - \frac{1}{c}\nabla\cdot\paren[\big]{c\nabla\delta P}
%	&=-\frac{1}{c\IPZ}\nabla\paren{c\delta\IP}\cdot\nabla P_0+\frac{\delta\IP}{\IPZ}\croch[\bigg]{-\Lapl P_0+\frac{1}{c}\nabla\cdot\paren[\big]{c\nabla P_0}}\text{,}\\
%	&=-\frac{1}{\IPZ}\nabla\paren{\delta\IP}\cdot\nabla P_0\text{,}
%\end{align}
%\end{subequations}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\minisec{Parametrisation \(\IP\) and \(\rho\)}
%We parametrise the model with the acoustic impedance \(\IP\) and the density \(\rho\), leading to
%\begin{equation}
%\frac{\iomsqr\rho^2}{\IP^2}P - \rho \nabla\cdot\paren[\bigg]{\frac{1}{\rho}\nabla P}=\wavelet\vecom\deltaxs\text{.}
%\end{equation}
%We consider a perturbation of the density \(\rho=\rho+\delta \rho\),
%\begin{subequations}
%\begin{align}
%\frac{\iomsqr}{\IP^2}\paren{\rho_0+\delta\rho}^2\paren{P_0+\delta P} - \paren{\rho_0+\delta \rho} \nabla\cdot\croch[\bigg]{\frac{1}{\rho_0+\delta \rho}\nabla \paren{P_0+\delta P}}
%	&=\wavelet\vecom\deltaxs\\
%\frac{\iomsqr}{c^2}\paren{\rho_0^2+2\rho_0\delta\rho}\paren{P_0+\delta P} - \paren{\rho_0+\delta \rho} \nabla\cdot\croch[\bigg]{\paren[\bigg]{\frac{1}{\rho_0}-\frac{\delta \rho}{\rho_0^2}}\nabla \paren{P_0+\delta P}}
%	&=\wavelet\vecom\deltaxs
%\end{align}
%leading to
%\begin{equation}
%\frac{\iomsqr\rho_0^2}{\IP^2}\delta P - \rho_0 \nabla\cdot\croch[\bigg]{\frac{1}{\rho_0}\nabla\delta P}
%	=-\rho_0 \nabla\cdot \paren[\bigg]{\frac{\delta \rho}{\rho_0^2}\nabla P_0} + \delta\rho\nabla\cdot\paren[\bigg]{\frac{1}{\rho_0}\nabla P_0}-\frac{2\iomsqr\rho_0\delta\rho}{\IP^2}P_0\text{.}
%\end{equation}
%\end{subequations}
%Assuming a homogenoeus background density model \(\rho_0\vecx=\rho_0\),
%\begin{equation}
%\frac{\iomsqr\rho_0^2}{\IP^2}\delta P - \Lapl\delta P=-\frac{1}{\rho_0}\nabla\delta\rho\cdot\nabla P_0-\frac{2\iomsqr\rho_0\delta\rho}{\IP^2}P_0\text{.}
%\end{equation}
%\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
