\section{Introduction}
%In this chapter, we concentrate on a synthetic, yet more realistic example. We consider a case where an interface with a strong contrast generates strong multiples which may overlap and hide weakest primary reflections. The methods developed in the previous chapter are applied to this case.
The aim of the thesis was to provide a more robust \gls{mva} procedure based on iterative migration, able to deal with multiple reflections. In \crefrange{chap:iter_migr}{chap:RMVA}, we have progressively built a strategy, whose final formulation can be summarised in four steps:
\begin{itemize}
%\begin{enumerate}[label=(\alph)]
	\item preconditioned iterative migration to determine the reflectivity section \(\xi\itn{N+1}\) best explaining observed data. The preconditioner is a pseudo-inverse \(\Fdag\) of the extended modelling operator \(F\). It has been derived for the case of primaries only (\cref{chap:WIMVA}) and is similar to the one proposed by \textcite{hou_approximate_2015};
	\item application of the filter \(K=\Fdag F\) to \(\xi\itn{N+1}\) to remove unwanted energy at large values of \(h\) that does not have any meaning in terms of wave propagation kinematics. The filtered reflectivity \(\zeta=K\xi\itn{N+1}\) is used as input of the usual \gls{mva} objective function
	\begin{equation}\label{eq:defJ1_chap6}
		J_1\croch{c_0}=\frac{1}{2}\norm[\big]{A\zeta\croch{c_0}}^2_\Esp\text{;}
	\end{equation}
	\item computation of the image residual \(K^TA^TA\zeta\) and resolution of a linear adjoint problem (even in the case of multiples) for the adjoint variable \(\eta\)
	\begin{equation}
		\croch[\bigg]{\pderiv[2]{J_0}{\xi}\croch[\big]{c_0,\xi\itn{N+1}}}\eta=K^TA^TAK\xi\itn{N+1}\text{;}
	\end{equation}
	\item computation of the approximate gradient \(G\itnB{N}{M}\), using the last iterate \(\xi\itn{N+1}\) and the last iterate \(\eta\itnB{N+1}{M+1}\), as
		\begin{equation}
			G\itnB{N}{M}=\croch[\bigg]{\pderivsec{J_0}{\xi}{c_0}\croch[\big]{c_0,\xi\itn{N+1}}}\eta\itnB{N+1}{M+1}\text{.}
		\end{equation}
\end{itemize}

We have also introduced in \cref{sec:TG} an alternative strategy for the derivation of a background velocity update after iterative migration. It consists of using the extended reflectivity image resulting from iterative migration to recompute primary reflection data. Then the macro-model update is obtained with the direct inversion strategy applied to this new data set free of multiples, as if it was observed data. The background velocity update obtained with this method is only one of the contribution to the gradient of the objective function~\eqref{eq:defJ1_chap6} with respect to \(c_0\), and is therefore referred to as \enquote{\emph{truncated gradient}} in this chapter.
%In the case of primaries only, we have shown that all other contributions should vanish if \(\xk\itn{N+1}\) and \(\eta\itnB{N+1}{M+1}\) are satisfactory solutions of the direct and adjoint problems.
The advantage of this strategy is that there is no need to solve the adjoint problem any more, so that the computational expense of the method is considerably reduced. Furthermore the direct inversion strategy has been shown to provide smooth consistent gradients in a wide variety of cases (\cref{sec:article_Herve_2D_applications}). Therefore we expect the \emph{truncated gradient} to benefit of its favourable behaviour also in the case of multiples.


Currently the iterative migration strategy has been applied only on observed data modelled with a second-order Born approximation, that is with the same modelling tool as the one used in inversion. This is referred to as the \enquote{inverse crime} \parencite{wirgin_inverse_2004} in inverse problems literature. The purpose of this chapter is first to evaluate the robustness of the approach when synthetic observed data are obtained with a different modelling code. This may allow us to determine possible limitations of our approach in the perspective of real data applications (not considered in this thesis). The second objective is to go beyond the computation of the first gradient and run several non-linear iterations to update the background model. Finally we examine the ability of our \gls{mva} strategy to benefit from the additional information contained in first-order surface-related multiples.

In the first part of this chapter (\cref{sec:dense}), we run four tests, named~A, B, C and~D, summarised in \cref{tab:tests_chap6}. In tests~A and~B, we model observed data with the constant density acoustic wave-equation and a \emph{modified source wavelet}, the Ricker wavelet still being used for inversion. In tests~C and~D, observed data are modelled with a \emph{variable density} acoustic propagation code, first under the Born approximation, then with a full finite-difference modelling and a \emph{free-surface condition}. For each test, we compute six background velocity updates using different strategies (the letters in the list below correspond to the labels of subfigures in \cref{sec:dense}):
\begin{enumerate}[label=(\alph*),noitemsep]
	\item \label{item:inv_prim} direct inversion on primary reflection data only;
	\item \label{item:inv_mult} direct inversion on data containing both primaries and first-order surface-related multiples;
	\item \label{item:iter_prim} iterative inversion on primary reflection data only;
	\item \label{item:iter_mult} iterative inversion on data containing both primaries and first-order surface-related multiples; 
	\item \label{item:smooth_prim} smoothed version of the gradient obtained in~\ref{item:iter_prim} after application of a gaussian blur;
	\item \label{item:TG_prim} \enquote{truncated gradient} obtained in the case of primaries, as described above;
	\item \label{item:smooth_mult} same as~\ref{item:smooth_prim} in the case of multiples (\ie smoothed version of~\ref{item:iter_mult});
	\item \label{item:TG_mult} same as~\ref{item:TG_prim} in the case of multiples.
\end{enumerate}
The result of direct inversion \ref{item:inv_prim} on primaries only is used as reference. The case \ref{item:inv_mult} is considered to assess how multiples misinterpreted as primaries alter the result of \ref{item:inv_prim}. Then we compare the result of iterative inversion on multiples \ref{item:iter_mult} with \ref{item:inv_prim} to assess the robustness of our approach.
To determine if potential failures of iterative inversion in the presence of multiples are due to multiples or to the iterative strategy, we also compute the gradient obtained by iterative inversion in the case of primaries only \ref{item:iter_prim}. Smoothing is commonly applied before updating the velocity model, therefore we show a smoothed version (\ref{item:smooth_prim} and~\ref{item:smooth_mult}) of the gradient obtained after iterative inversion \ref{item:iter_mult}.
%!-------------------------------------
\begin{table}[!htbp]
\begin{tabular}{cccC{3.5cm}C{5.5cm}}
\toprule
test & section & figures & source wavelet & modelling tool for observed data \\ \toprule
 ref.& \ref{sec:app_reference} & \ref{fig:dense_ref} and \ref{fig:dense_ref_smooth}
 	 & Ricker wavelet& 2nd-order Born approximation \\ \midrule
 A   & \ref{sec:app_lowfreq} & \ref{fig:dense_badsrc2} and \ref{fig:dense_badsrc2_smooth}
     & \cellcolor{lightgray} Ricker wavelet without low frequencies & 2nd-order Born approximation \\ \midrule
 B   & \ref{sec:app_source} & \ref{fig:dense_badsrc} and \ref{fig:dense_badsrc_smooth}
     & \cellcolor{lightgray} time derivative of Ricker wavelet & 2nd-order Born approximation \\ \midrule
 C   & \ref{sec:app_drho} & \ref{fig:dense_drho} and \ref{fig:dense_drho_smooth}
     & Ricker wavelet & \cellcolor{lightgray}2nd-order Born approximation\newline density perturbation instead of velocity perturbation \\ \midrule
 D   & \ref{sec:blocky} & \ref{fig:blocky_fd} and \ref{fig:blocky_fd_smooth}
     & Ricker wavelet & \cellcolor{lightgray}finite difference and free-surface condition with variable density \\\bottomrule
 %E   & \ref{sec:blocky_born} & \ref{fig:blocky_born} and \ref{fig:blocky_born_smooth}
    % & Ricker wavelet & \cellcolor{lightgray}2nd-order Born approximation\newline velocity and density perturbation \\ \bottomrule
\end{tabular}
\caption{Series of tests performed in \cref{chap:applications}. The coloured cells correspond to the modification introduced in the modelling code for observed data compared to the modelling code used during inversion.}
\label{tab:tests_chap6}
\end{table}
%-------------------------------------

The same exact (in principle unknown) velocity model is used for tests~A, B and~C. Those tests are compared to a reference case where the same code is used for the modelling of observations and for inversion. The model is \SI{1800}{\meter} large and \SI{700}{\meter} deep, laterally invariant and discretised on a \(\SI{4.8}{\meter}\times\SI{4.8}{\meter}\)~grid. The background velocity model is increasing with depth from  \SI{2000}{\meter\per\second} at the surface to approximately \SI{2700}{\meter\per\second} at \SI{700}{\meter} depth (\cref{fig:dense_model}, left). The initial velocity model is similar to the exact model except that velocities are underestimated (from \SI{2000}{\meter\per\second} at the surface to \SI{2400}{\meter\per\second} at the maximum depth). Sources are located every 6 grid points at the surface with receivers at each grid point on the surface within \(\SI{\pm 700}{\meter}\) around the source. The model used in test~D is different and will be presented in \cref{sec:blocky}.

For all tests~A, B, C and~D, we define an annihilator similar to the one used at the end of \cref{chap:RMVA}, 
\begin{equation}
	A\xi\paren{z,x,h}=\abs{h}\cdot z \cdot c_0^\beta\paren{z,x}\xi\paren{z,x,h}\text{.}
\end{equation}
It consists of a multiplication by the absolute value of the subsurface offset to penalise defocused energy as well as a power \(\beta=-3/2\) of the background velocity model which has been shown in \cref{chap:WIMVA} to attenuate spurious oscillations appearing in the \gls{mva} gradient around reflectors' positions. To emphasise the influence of deeper reflectors which may be hidden by cross-talk artefacts, we add a multiplication by the depth \(z\) in the definition of the annihilator.

In the second part of this chapter (\cref{sec:blocky_iterations}), a blocky velocity model and a variable density model both with lateral variations are defined to generate observed data under a second-order Born approximation. We run several outer iterations on the background velocity model and compare the results obtained by direct inversion on primary reflection data only and the \enquote{truncated gradient} strategy applied on data containing both primaries and first-order surface multiples.

Finally we study in section~\cref{sec:hole} an example with an incomplete acquisition where multiples may bring additional information compared to primaries. We want to test the ability of iterative \gls{mva} to use this information.