\subsection{Test D -- Observed data modelled without the Born approximation}\label{sec:blocky}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\subsection{Observed data computed without the Born approximation}\label{sec:blocky_nonborn}
Finally we test the ability of our approach to deal with observed data modelled without the Born approximation. We use an exact velocity model different from tests~A, B and C but with the same dimensions and we keep the same acquisition parameters. The velocity increases with depth, similar to the previous case, but instead of a dense reflectivity, we add two layers with homogeneous velocity (\SI{2600}{\meter\per\second}) between \SIlist{200;400}{\meter} depth and \SIlist{550;600}{\meter} depth; the density model is variable with interfaces located at the same positions as the velocity discontinuities (\cref{fig:blocky_model_born}, red curves). Both models are laterally invariant and the velocity perturbations are not proportional to the density perturbations.
%----------------------------------------
%\begin{figure}[!htb]
%\includegraphics{Figures_blocky/modele_fd}
%\caption{Velocity and density model.}
%\label{fig:blocky_model}
%\end{figure}
%----------------------------------------
\begin{figure}[!htb]
\includegraphics{Figures_blocky/modele_born}
\caption{Exact discontinuous velocity and density 1D profiles used to model observed data (red, first and third columns). The blue curves correspond to smoothed velocity and density models, and associated velocity and density perturbations used to model data with second-order Born approximation. The dashed green line is the initial velocity model used to compute the gradient (test D).}
\label{fig:blocky_model_born}
\end{figure}
%----------------------------------------

Observed data are modelled with a finite-difference variable density acoustic propagation code. In the case of primaries only, \glspl{pml} are implemented on each edge of the model while a free-surface condition is used to model multiples (\cref{fig:blocky_Pobs}, 1st column). The main effect compared to second-order Born approximation are the following:
\begin{itemize}[noitemsep]
	\item an angle dependent reflectivity coefficient at the free surface;
	\item ghost effects at the source and receiver positions;
	\item all orders of multiples are modelled, as well as internal multiples.
\end{itemize}
For comparison, we smooth the velocity and density models and define associated velocity and density perturbations (\cref{fig:blocky_model_born}, blue curves) to model data under a second-order Born approximation (\cref{fig:blocky_Pobs}, 2nd column). To take ghosts effects into account, we use a scaled second-order time derivative of the Ricker wavelet in the case of multiples (\cref{fig:src_free-surface}).

In the primaries only example, both data sets are very similar. Amplitudes are overestimated by the Born approximation, but with consistent kinematics and a similar amplitude ratio at zero and large offsets (\cref{fig:blocky_Pobs}, top row). In the case of multiples, the kinematics is not as well reproduced by the Born approximation as in the previous case, but remains consistent. However the amplitudes are overestimated with much larger discrepancies at large offsets than at zero offsets. The origin of these errors is the use of a second-order time derivative to model ghost reflections, valid at zero-offset only.
%----------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures_blocky/Pobs_blocky}
\caption{Observed data computed with a finite-difference modelling code and a free-surface condition in the multiple case (left column, corresponding to the red 1D profiles of \cref{fig:blocky_model_born}). We compare this data set with data modelled under the second-order Born approximation (2nd column, corresponding to the blue 1D profiles of \cref{fig:blocky_model_born}) (test D).}
\label{fig:blocky_Pobs}
\end{figure}
%----------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures_blocky/src_ghost}
\caption{Ricker source wavelet with maximum frequency of \SI{40}{\hertz} used to model observed data with a finite difference scheme (black). A second-order time derivative of this wavelet is used to take source and receiver ghost effects into account when modelling data under the second-order Born approximation (test D).}
\label{fig:src_free-surface}
\end{figure}
%---------------------------------------------------------

We now compute the background velocity updates using the observed data sets obtained without the Born approximation (\cref{fig:blocky_Pobs}, 1st column). For calculated data, the density model is assumed homogeneous and the initial velocity model is increasing with depth (\cref{fig:blocky_model_born}, dashed green curve).
Direct inversion on primaries data yields \glspl{cig} with downward curved events only, which is consistent with the initial too low velocity model (\cref{fig:blocky_fd_InvPrim}). The central part of the associated gradient is smooth and homogeneous with negative values as expected.
In the multiple case, we use the modified source wavelet introduced in the preceding paragraph to model ghost reflections for the inversion (\cref{fig:src_free-surface}). Cross-talk artefacts with an upward curvature are superimposed on the events related to primaries, resulting in spurious positive values in the gradient (\cref{fig:blocky_fd_InvMult}).

Iterative inversion in the primaries only case yields \glspl{cig} similar to direct inversion (\cref{fig:blocky_fd_Prim}). The gradient is negative but exhibits oscillations around reflector positions, which can be nonetheless attenuated by smoothing (\cref{fig:blocky_fd_smooth_prim}). Again, the \enquote{truncated gradient} (\cref{fig:blocky_fd_TG_prim}) is very close to the result shown in \cref{fig:blocky_fd_InvPrim}.
This contrasts with the mixed results obtained with multiples. Cross-talk artefacts are still visible on \glspl{cig} (\cref{fig:blocky_fd_Mult10}), although final data residuals are very small (\cref{fig:traces_testD,fig:JM_testD}). Our interpretation of this apparent contradiction is that the final reflectivity contains events with downward curvature corresponding to the true reflectors which correctly explain primary reflections. However the amplitudes of multiples modelled from these reflectors do not match those in observed data, because of the inaccuracy of both the second-order Born approximation and the modelling of ghosts used here. The difference is explained by residual cross-talk artefacts, which are nonetheless smaller than in \cref{fig:blocky_fd_InvMult}.
As a consequence, spurious positive values are still visible in the gradient (\cref{fig:blocky_fd_Mult10}). They are not attenuated after smoothing (\cref{fig:blocky_fd_smooth_mult}). Note also that the convergence of the adjoint problem is slower than in the previous cases (\cref{fig:dense_JNadj}, orange curve). As in the previous example, we observe that the truncated gradient provides more consistent velocity updates and seems more insensitive to residual cross-talk artefacts in \glspl{cig} (\cref{fig:blocky_fd_TG_mult}).
%---------------------------------------------------------
\begin{figure}[!htbp]
\captionsetup[subfigure]{justification=raggedright,singlelinecheck=false}
\captionsetup{singlelinecheck=false}
%-----
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:blocky_fd_InvPrim}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}\centering
	\includegraphics{Figures_blocky/CIG_gV_blocky_InvPrim}
\end{subfigure}
%-----
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:blocky_fd_InvMult}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}\centering
	\includegraphics{Figures_blocky/CIG_gV_blocky_InvMult}
\end{subfigure}
%-----
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:blocky_fd_Prim}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}\centering
	\includegraphics{Figures_blocky/CIG_gV_Prim}
\end{subfigure}
%-----
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:blocky_fd_Mult10}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}\centering
	\raggedright
	\includegraphics{Figures_blocky/CIG_gV_Mult}
\end{subfigure}
%-----
\caption[CIGs and gradient obtained for test D]{Same as \cref{fig:dense_ref} when observed data are generated with a finite-difference modelling code without Born approximation (test D).}
\label{fig:blocky_fd}
\end{figure}
%---------------------------------------------------------
%---------------------------------------------------------
\begin{figure}[!htbp]
\begin{subfigure}{0pt}\addtocounter{subfigure}{4}\end{subfigure}%
\begin{subfigure}[t]{1.0\linewidth}
	\includegraphics{Figures_blocky/CIG_gV_Smooth_Prim}%
	\phantomsubcaption\label{fig:blocky_fd_smooth_prim}%
	\phantomsubcaption\label{fig:blocky_fd_TG_prim}%
\end{subfigure}
\begin{subfigure}[t]{1.0\linewidth}
	\includegraphics{Figures_blocky/CIG_gV_Smooth_Mult}%
	\phantomsubcaption\label{fig:blocky_fd_smooth_mult}%
	\phantomsubcaption\label{fig:blocky_fd_TG_mult}%
\end{subfigure}
\caption{Same as \cref{fig:dense_ref_smooth} for the gradients presented in \cref{fig:blocky_fd} (test D).}
\label{fig:blocky_fd_smooth}
\end{figure}
%---------------------------------------------------------
\begin{figure}[!htbp]
\begin{minipage}[t]{0.49\linewidth}\centering
	\includegraphics{Figures_blocky/traces_testD}
	\caption{Residuals obtained at zero offset (left) and large offset (right) after 10 iterations of iterative migration (test D).}
	\label{fig:traces_testD}
\end{minipage}\hfill%
\begin{minipage}[t]{0.49\linewidth}
	\includegraphics{Figures_blocky/JM_blocky}\centering
	\caption{Value of the data misfit across iterations (test D).}
	\label{fig:JM_testD}
\end{minipage}
\end{figure}
%---------------------------------------------------------

%To confirm this hypothesis, we artificially modify the observed data set and multiply each trace of the acquisition by a coefficient such that the amplitude of the first primary event matches the one obtained under a second-order Born approximation in the smoothed version of the model (\cref{fig:blocky_Pobs}, bottom row, 2nd and 3rd columns). Then we apply the iterative migration process on this new data set (\cref{fig:blocky_fd_Mult10_modif,fig:blocky_fd_Mult10_modif_Smooth}). Final \glspl{cig} exhibit defocused energy with events curved both upward and downward, very similar to the result of direct inversion (\cref{fig:blocky_fd_Mult10}), meaning that the final reflectivity model is consistent with observed primaries but cannot explain multiples. Residual positive values around \SI{500}{\meter} depth still alter the gradient, although they do not seem to interfere in the \enquote{truncated gradient} (\cref{fig:blocky_fd_Mult10_modif_Smooth}, right).
%%---------------------------------------------------------
%\begin{figure}[!htbp]
%\includegraphics{Figures_blocky/CIG_gV_blocky_modif_Mult}
%\caption{Same as \cref{fig:blocky_fd_Mult10} for the modified data set shown in \cref{fig:blocky_Pobs} (3rd column, bottom).}
%\label{fig:blocky_fd_Mult10_modif}
%\end{figure}
%%---------------------------------------------------------
%\begin{figure}[!htbp]
%\includegraphics{Figures_blocky/CIG_gV_Smooth_Mult_modif}
%\caption{Same as \cref{fig:blocky_fd_smooth_mult} for the gradient shown in \cref{fig:blocky_fd_Mult10_modif}.}
%\label{fig:blocky_fd_Mult10_modif_Smooth}
%\end{figure}
%%---------------------------------------------------------

We conclude from test D that iterative migration has difficulty in presence of a free-surface, because source and receiver ghosts are not properly modelled.
We used a second-order time derivative of the source wavelet to account for this effect, but this approximation is valid for zero reflection angles only.
A better modelling tool should be investigated to deal with this issue.
As for the preceding examples, the best result is obtained here with the \enquote{truncated gradient} approach.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\subsection{Observed data computed with the Born approximation}\label{sec:blocky_born}
%%---------------------------------------------------------
%\begin{figure}[!htbp]
%\captionsetup[subfigure]{justification=raggedright,singlelinecheck=false}
%\captionsetup{singlelinecheck=false}
%%
%\caption[CIGs and gradient obtained for test E]{Same as \cref{fig:dense_ref} when observed data are generated with second-order Born approximation with both density and velocity perturbations (test E).}
%\label{fig:blocky_born}
%\end{figure}
%%---------------------------------------------------------
%
%%---------------------------------------------------------
%\begin{figure}[!htbp]
%\captionsetup[subfigure]{justification=raggedright,singlelinecheck=false}
%%\captionsetup{singlelinecheck=false}
%\begin{subfigure}{0pt}\addtocounter{subfigure}{4}\end{subfigure}%
%%-----
%\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:blocky_born_smooth_prim}}%
%\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}%\centering
%	%\includegraphics{Figures_blocky/CIG_gV_Smooth_Prim_born}
%\end{subfigure}
%%-----
%\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:blocky_born_smooth_mult}}%
%\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}%\centering
%	%\includegraphics{Figures_blocky/CIG_gV_Smooth_Mult_born}
%\end{subfigure}
%%-----
%%
%\caption{Same as \cref{fig:dense_ref_smooth} for the gradients presented in \cref{fig:blocky_drho} (test C).}
%\label{fig:blocky_born_smooth}
%\end{figure}
%%---------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Iterations on the background velocity model}\label{sec:blocky_iterations}
We now consider a 2D model with lateral variations and iterate on the background velocity model.
The model is \SI{2500}{\meter} large and is made of three 1D velocity profiles similar to the previous example with two sub-vertical faults and a low-velocity layer between \SIlist{400;550}{\meter} depth in the central part of the model (\cref{fig:viter_exact_model}, top). We also consider a variable density model with interfaces located at the same positions as in the velocity model, but with different contrast values.

We have seen in the preceding section that free-surface remains an issue as we cannot model properly ghost reflections at wide angles. Therefore we consider here a simpler case and model observed data under a second-order Born approximation. For that purpose we use smoothed versions of the velocity and density models (\cref{fig:viter_exact_model}, bottom). From now on, \enquote{exact velocity model} refers to this smoothed version. Although the forward modelling code for observations and inversion both use the second-order Born approximation, we are not in the position of an \enquote{inverse crime}. Observed data are indeed modelled with variable density and both velocity and density perturbations, the density perturbations being stronger. For inversion, we assume a constant density model (\(\rho_0=1\)) and only model reflections due to velocity perturbations.
%---------------------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures_inversion/exact_model}
\caption{Exact velocity and density model (top), and their respective smoothed versions (bottom) used to generate observed data with a second-order Born approximation.}
\label{fig:viter_exact_model}
\end{figure}
%---------------------------------------------------------


Starting with an homogeneous (\SI{2000}{\meter\per\second}) background velocity model as initial guess, the inversion aims at minimising the following \gls{mva} objective function
\begin{equation}\label{eq:J1_iterations}
	J_1\croch{c_0}=\frac{\norm[\big]{h\cdot z \cdot c_0^\beta \cdot \xi\croch{c_0}\paren{z,x,h}}^2_\Esp}
	{\norm[\big]{z\cdot c_0^\beta\cdot\xi\croch{c_0}\paren{z,x,h}}^2_\Esp}\text{.}
\end{equation}
To prevent convergence towards background velocity models minimising the energy in \glspl{cig} instead of minimising only defocused energy, we consider an objective function normalised by the energy of the reflectivity image without multiplication by \(h\) \parencite{chauris_two-dimensional_2001}.
As the shallower interface is a strong reflector, a multiplication by \(z\) is introduced in the annihilator, so that deeper reflectors have enough influence on the velocity model reconstruction. Besides, as velocities are underestimated in the initial velocity model, reflectors are migrated to shallower depths, and should be shifted toward deeper positions across iterations, which conflicts with the \(z\)-multiplication in \glspl{cig}. Therefore a multiplication by \(z\) is also introduced in the normalisation term of the objective function~\eqref{eq:J1_iterations}.

We first use the direct inversion strategy on primaries only and perform 20 iterations of non-linear conjugate gradient with the Polak-Ribière formula (\cref{fig:viter_inv_prim_vel,fig:viter_inv_prim_CIG}). To accelerate the convergence, we assume that the velocity in the first layer is known and set to zero the gradient below \SI{100}{\meter} depth. We also introduce a preconditioner consisting of a  multiplication by the depth \(z^\alpha\) with \(\alpha=\num{1}\) in the applications. Finally we smooth the  resulting velocity update with a gaussian blur, with a stronger smoothing in the \(x\)-direction (\(\sigma_x=\SI{140}{\meter}\) and \(\sigma_z=\SI{90}{\meter}\)). These values are progressively decreased every four iterations to recover more detailed structure in the last iterations, as shown in \cref{tab:smoothing}.
%---------------------------------------------------------
\begin{table}[!htbp]
\begin{tabular}{ccc}
	\toprule
	iterations       & \(\sigma_z\)    & \(\sigma_x\) \\ \midrule
	\numrange{1}{4}  & \(\sigma_{z0}=\SI{90}{\meter}\) & \(\sigma_{x0}=\SI{140}{\meter}\) \\ \midrule
	\numrange{5}{8}  & \(0.75\cdot\sigma_{z0}\) & \(0.75\cdot\sigma_{x0}\) \\ \midrule
	\numrange{9}{12}  & \(0.5\cdot\sigma_{z0}\) & \(0.5\cdot\sigma_{x0}\) \\ \midrule
	\numrange{13}{20}  & \(0.25\cdot\sigma_{z0}\) & \(0.25\cdot\sigma_{x0}\) \\
	\bottomrule
\end{tabular}
\caption{Parameters of the gaussian blur used to smooth the gradient at each iteration on the background velocity model.}
\label{tab:smoothing}
\end{table}
%---------------------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures_inversion/viter_inv_prim_vel}
\caption{Result of iterations on the background velocity model with the direct inversion strategy on data containing primaries only. The colour bar ranges from \SIrange{1950}{2750}{\meter\per\second} and is similar to the one used in the left column of \cref{fig:viter_exact_model}.}
\label{fig:viter_inv_prim_vel}
\end{figure}
%---------------------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures_inversion/viter_inv_prim_CIG}
\caption{Reflectivity image obtained by direct inversion in the background velocity models shown in \cref{fig:viter_inv_prim_vel}. The bottom row displays the results obtained in the exact velocity model. The same colour scale is used for all sections at \(h=0\) (left column). All \glspl{cig} are also plotted with the same colour scale, which has been truncated for a better representation of deeper reflectors.}
\label{fig:viter_inv_prim_CIG}
\end{figure}
%---------------------------------------------------------

The most energetic event in the initial \glspl{cig} is the continuous interface located at \SI{2000}{\meter} depth (\cref{fig:viter_inv_prim_CIG}, top row). The associated defocused energy drives the first velocity updates and the shallower velocity contrast is recovered after a couple of iterations (\cref{fig:viter_inv_prim_vel}). Subsequent iterations address the focusing of events related to the low velocity anomaly at larger depths, progressively appearing between iterations \numlist{5;15}.
After \num{9} iterations, the value of the objective function has decreased to the value obtained in the exact model (\cref{fig:Jv_3inv}, left). Smaller values are reached by further attenuating defocused energy but this does not necessarily result in more relevant information on the background velocity model. In particular, velocity updates after iteration \num{15} are mostly located around the faults to the detriment of the low velocity layers on the left and right sides of the faults (around \(z=\SI{400}{\meter}\) and \(z=\SI{500}{\meter}\), respectively) which are not recovered in the final model.
Finally note that \glspl{cig} are well focused after 5 iterations and similar to those obtained in the exact model (\cref{fig:viter_inv_prim_CIG}, 4th and bottom row). Few modifications are visible in subsequent iterations compared to the evolution of the background velocity model.
%---------------------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures_inversion/Jv_3inv}
\caption{Value of the \gls{mva} objective function~\eqref{eq:J1_iterations} across iterations (blue, solid). The dashed black line represents the value obtained in the exact background velocity model. Three different inversions strategies are shown: direct inversion on primaries only (left, corresponding to \cref{fig:viter_inv_prim_vel}), direct inversion on primaries and multiples (centre, corresponding to \cref{fig:viter_inv_mult_vel}), and iterative inversion on primaries and multiples with application of the filter \(\Fdag F\) and the \enquote{truncated gradient} as background velocity update (right, final result shown in \cref{fig:v_iter_TG_vel}, bottom right).}
\label{fig:Jv_3inv}
\end{figure}
%---------------------------------------------------------

The same example is run with first-order surface multiples in observed data with identical smoothing parameters (\cref{fig:viter_inv_mult_vel,fig:viter_inv_mult_CIG}). We use direct inversion for the definition of the extended image to evaluate the impact of multiples on the reconstruction of the velocity model.
As already discussed in \cref{sec:examples_chap4_mult_migr}, the associated \gls{mva} objective function is not minimal for the correct velocity model due to the presence of multiples. Here, a smaller value is reached after a single iteration (\cref{fig:Jv_3inv}, middle), indicating that inversion converges to a compromise model which should focus both cross-talks artefacts and events corresponding to true reflectors.
The velocity models obtained in the first iterations have a shape similar to the primaries only case (\cref{fig:viter_inv_prim_vel,fig:viter_inv_mult_vel}), but after a few iterations, events with an upward curvature (around \(z=\SI{400}{\meter}\) depth at iteration 5 for example, fourth row in \cref{fig:viter_inv_mult_CIG}) misguide background velocity updates and prevent a proper focusing of the event corresponding to the shallowest interface. Consequently, the value of the velocity at this interface is not correctly recovered, and neither is the low velocity anomaly. 
%---------------------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures_inversion/viter_inv_mult_vel}
\caption{Same as \cref{fig:viter_inv_prim_vel} with both primaries and first-order surface multiple in observed data.}
\label{fig:viter_inv_mult_vel}
\end{figure}
%---------------------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures_inversion/viter_inv_mult_CIG}
\caption{Same as \cref{fig:viter_inv_prim_CIG} in the case of multiples for the background velocity models of \cref{fig:viter_inv_mult_vel}.}
\label{fig:viter_inv_mult_CIG}
\end{figure}

%---------------------------------------------------------

Finally we replace direct inversion by iterative migration, still with both primaries and first-order surface multiples in observed data. We perform \num{5} inner iterations on the reflectivity model for each outer iteration and compute a background velocity update with the \enquote{truncated gradient} strategy: primaries are remodelled from the final reflectivity model and the direct inversion strategy applied to this new data set to obtain a background velocity update.
We begin by comparing the reflectivity sections obtained in the initial-velocity model (\cref{fig:v_iter_TG_CIG}). The cross-talk artefacts appearing after direct inversion the complete data set (around \(z=\SI{400}{\meter}\)) due to the multiple reflecting twice at the shallower interface are greatly attenuated after five iterations. %Residual energy in still present between at \SIlist{550;650}{\meter} depth, but these events have minor influence of the background velocity update.
Although the gradient obtained with direct inversion on primaries and multiples is quite similar to the primaries only case, we obtain a much closer velocity update with the \enquote{truncated strategy} (\cref{fig:v_iter_TG_vel}, left column). Note that the largest values in the gradient are due to the faults, but consistent homogeneous updates are obtained on the left and right sides of the fault. The objective function decreases at the same pace as in the primary only case, and reaches a slightly lower value than the one obtained in the correct velocity model. Finally,  the background velocity model recovered after fifteen iterations is very similar to the result of inversion obtained in the primaries only case (\cref{fig:v_iter_TG_vel}, right column), showing that multiples have been correctly included in the process.
%---------------------------------------------------------
\begin{figure}[!htbp]
	\begin{subfigure}{1.0\linewidth}
		\includegraphics{Figures_inversion/v_iter_TG_CIG}
		\caption{Migrated images in the initial velocity model, using direct inversion on primaries only (top), direct inversion on primaries and multiple (middle), and iterative migration and filtering on primaries and multiples (bottom).}
		\label{fig:v_iter_TG_CIG}
	\end{subfigure}
	\begin{subfigure}{1.0\linewidth}
		\includegraphics{Figures_inversion/v_iter_TG_vel}
		\caption{First gradient of the inversion (left), corresponding to the migrated images of \subref{fig:v_iter_TG_CIG}, and background velocity model recovered after \num{15} iterations.}
		\label{fig:v_iter_TG_vel}
	\end{subfigure}
	\caption{Results of 20 iterations on the background velocity model using the iterative migration strategy with observed data containing both primaries and first-order surface multiples, and comparison with the results obtained with direct inversion. In the iterative case, the \enquote{truncated gradient} strategy is used to compute the background velocity update.}
	\label{fig:v_iter_TG}
\end{figure}
%---------------------------------------------------------

We conclude from this example that the truncated gradient approach, consisting of applying the direct inversion to primaries only recomputed from the final result of iterative migration, is an efficient strategy to deal with surface-related multiples, which otherwise prevent the inversion to converge to a reliable background velocity model. The inversion should in theory also be performed with the complete gradient, but this is more computationally expensive as the adjoint problem has to be solved and as more inner iterations are needed in practice to obtain consistent gradients.
%Besides we do not expect this strategy to provide better results.
The truncated gradient is not formally the gradient of an objective function but efficiently deals with multiples at a reasonable computational cost.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Incomplete acquisition}\label{sec:hole}
Finally we consider an example with an incomplete acquisition to test the ability of our strategy to benefit from the additional information contained in multiples. For this example, only the first \gls{mva} gradient is computed (we do not loop over the background velocity model).
We consider a \SI{700}{\meter} deep and \SI{3750}{\meter} large model, discretised on a \(\num{4.8}\times\SI{4.8}{\meter}\) grid, with sources every six grid points from \(x=\SI{0}{\meter}\) to \(x=\SI{1500}{\meter}\) and from \(x=\SI{2250}{\meter}\) to \(x=\SI{3750}{\meter}\) (\cref{fig:hole_model}). Receivers are located at each grid point at the surface within \SI{\pm 700}{\meter} around each source. Unlike sources, receivers are not removed from the central part of the model.
Iterative migration is performed with this acquisition setting. After 20 iterations, data are re-computed for sources located every four grid points, including the central part. Using the truncated gradient strategy, velocity analysis is performed on this new data set with the direct inversion strategy (\cref{sec:article_Herve}).
%---------------------------------------------------------
\begin{figure}[!htbp]
	\includegraphics{Figures_inversion/hole_model}
	\caption{Laterally invariant velocity model considered in \cref{sec:hole}. It is constructed by extending the central part of the 2D velocity model shown in \cref{fig:viter_exact_model}. The horizontal black arrows indicate sources lateral position. There are no sources between \(x=\SI{1500}{\meter}\) and \(x=\SI{2250}{\meter\per\second}\).}
	\label{fig:hole_model}
\end{figure}
%---------------------------------------------------------

We want to compare this background velocity update with the result obtained using the direct inversion strategy on the original data set. We consider a pure 1D velocity and density model (\cref{fig:hole_model}) built by extending the central part of the exact model shown in \cref{fig:viter_exact_model}. Hence the difference between the results of direct and iterative inversion are not due to 2D effects. The initial background velocity model is homogeneous (\SI{2000}{\meter\per\second}), so that we expect a negative gradient.

With this acquisition setting, no zero-offset trace is recorded in the central part of the model. The width of the acquisition hole (\SI{750}{\meter}) is only a little larger than the maximum surface offset (\SI{700}{\meter}). However tapers are applied to smooth the discontinuity in source and receivers positions and to attenuate edge effects. Hence sources located near the acquisition hole have little influence and the area not illuminated by primaries and direct inversion is not negligible (\cref{fig:hole_grad}, top). Consequently there is no update of the background velocity in the central part of the model using the direct inversion strategy.
%---------------------------------------------------------
\begin{figure}[!htbp]
	\includegraphics{Figures_inversion/hole_gradient}
	\caption{Section at \(h=0\) of the reflectivity model (left) and MVA gradient (right) obtained in a homogeneous background velocity model (\SI{2000}{\meter\per\second}) with three different strategies: direct inversion on primaries only (top), and truncated gradient computed after 20 iterations on \(J_0\) with primaries only in both observed and calculated data (middle), and with primaries and multiples in both observed and calculated data (bottom). In the iterative case, the reflectivity section displayed here is the result of application of the filter \(\Fdag F\) to the final reflectivity model computed by iterative migration.}
	\label{fig:hole_grad}
\end{figure}
%---------------------------------------------------------

Twenty iterations are performed on the reflectivity section in the primary only case, as well as in the case of first-order surface multiples (\cref{fig:hole_grad,fig:hole_JM}). In the primary only case, iterative migration provides a reflectivity model with a smaller hole in the central part of the model compared to direct inversion (\cref{fig:hole_grad}, middle). The area with non-zero background velocity update is also reduced. In the case of multiples, we obtain a very similar reflectivity image (\cref{fig:hole_grad}, bottom). Although the multiples illuminate the central part of the model, we do not succeed in recovering a continuous reflector. Some cross-talk artefact are even still visible (around \(z=\SI{400}{\meter}\) and \(x=\SI{1600}{\meter}\) for example). As a consequence the MVA gradient does not exhibit improvement compared to the primary only case and there is even a stronger artefact with a wrong sign around \(x=\SI{1600}{\meter}\) and \(x=\SI{2200}{\meter}\). A better result might be obtained by performing much more iterations, but the associated computational cost would not be affordable.
%---------------------------------------------------------
\begin{figure}[!htbp]
	\includegraphics{Figures_inversion/hole_JM}
	\caption{Relative data misfit across iterations when performing iterative migration in a homogeneous velocity model (\SI{2000}{\meter\per\second}) with primary only (left, corresponding to \cref{fig:hole_grad}, middle) and primaries and first-order surface multiples (left, corresponding to \cref{fig:hole_grad}, right).}
	\label{fig:hole_JM}
\end{figure}
%---------------------------------------------------------

We conclude from this example, that iterative migration allows to extract more information on the edges of the model compared to direct inversion, even in the case of primaries only. However, the additional information contained in first-order surface multiples is difficult to recover as it is located on the edges of the acquisition where strong edge effects alter the gradient. Also the tapers applied to the sources and receivers on the edges of the acquisition reduce the weight given to multiples containing additional information.
