\chapter{Product of the Hessian of the MVA objective function with a vector of the \texorpdfstring{\(\Msp\)}{M}-space}\label{app:HessVec_Vel}
In this appendix we consider the case of non-linear iterations on the background velocity model \(c_0\) to determine a minimum of the \gls{mva} objective function \(J_1\croch{c_0}\). In the following calculations, we consider only the case of direct inversion of primary reflections, the reflectivity image \(\xi\) being defined by the application of operator \(\Fdag\) to observed data \(\Pobs\). We keep a very general notation for the expression of \(J_1\),
\begin{equation}
	J_1\croch{c_0}=\fjmva\croch{c_0,\xi}\text{\qquad with \qquad} \xi=\Fdag\Pobs\text{,}
\end{equation}
so that further expressions are applicable to the standard definition \(J_1\croch{c_0}=\norm{A\xi}^2_\Esp\), to a normalised version \(J_1\croch{c_0}=\norm{A\xi}^2_\Esp/\norm{\xi}^2_\Esp\) and to similar versions with the introduction of a power of the background velocity model \(c_0^\beta\) as proposed in \cref{sec:article_Herve}.

In Newton optimisation methods (appendix~\ref{sec:descent_direction}), the descent direction \(d\in\Msp\) is defined as the solution of the following linear system, called Newton equation,
\begin{equation}\label{eq:Newton_equations_appD}
	\croch[\bigg]{\pderiv[2]{J_1}{c_0}\croch[\big]{c_0\itn{n}}}d=-\pderiv{J_1}{c_0}\croch[\big]{c_0\itn{n}}\text{,}
\end{equation}
where \(c_0\itn{n}\) is the value of the background velocity model at the \(n\)th non-linear iteration. Most application of \gls{mva} do not solve this equation and use a quasi-Newton such as \(l\)-BFGS. In \textcite{liu_inversion_2014,shen_horizontal_2015}, \(d\) is defined as the opposite of the gradient element-wise divided by the product of the Hessian with the unit vector of the \(\Msp\)-space. One could also use the truncated-Newton method, already studied in the framework of Full Waveform Inversion by \textcite{metivier_full_2013}. It consists of iteratively solving \cref{eq:Newton_equations_appD} with the linear conjugate-gradient algorithm, without explicitly computing the full Hessian matrix. Then several products of the Hessian matrix with a vector \(V\in\Msp\) are required. An efficient procedure to compute this product should then be defined.

We propose here a method based on the adjoint-state method \parencite{plessix_review_2006}, very similar to the one used by \textcite{metivier_full_2013} for \gls{fwi} and to the method presented in \cref{sec:Hess_vec_chap3} to compute the Hessian-vector product in the case of the migration objective function \(c_0\).
Given a vector \(V\) of \(\Msp\), we define the scalar objective function \(\Gamma\croch{c_0,V}\) as
\begin{equation}
	\Gamma\croch{c_0,V}=\pscal[\bigg]{\pderiv{J_1}{c_0}}{V}_\Msp\text{,}
\end{equation}
such that the derivative of \(\Gamma\) with respect to \(c_0\) gives the desired matrix-vector product,
\begin{equation}
	\pderiv{\Gamma}{c_0}\croch{c_0,V}=%
		\croch[\bigg]{\pderiv[2]{J_1}{c_0}\croch{c_0}}V\text{.}
\end{equation}

Noting \(G\) the gradient of \(J_1\) with respect to \(c_0\), we define the Lagrangian by defining constraints on the state equations solved to determine the gradient with the adjoint variables \(\gamma\in\Msp\), \(\paren{\Xi_0,\Xi,E,E_0}\in\Esp\) and \(\paren{\Pi_0,L_1,N_1,M_0}\in\Dsp\),
\begin{equation}
\begin{split}
	\Lagr{\Gamma}=\pscal[\big]{G}{V}_\Msp
		&-\pscal[\bigg]{\gamma}{G-\pderiv{\fjmva}{c_0}\croch{c_0,\xi}-\Cxi{\mu_0}{P_0}-\Cxi{\nu_1}{\lambda_1}-\fmod\croch{c_0,\eta,\xi_0}}_\Msp\\
		&-\pscal[\big]{\Pi_0}{\Wop P_0-\Wg{S}}_\Dsp\\
		&-\pscal[\big]{L_1}{\WopA \lambda_1-\Wr M^T\Pobs}_\Dsp\\
		&-\pscal[\big]{\Xi_0}{\xi_0-\WQxi{P_0}{\lambda_1}}_\Dsp\\
		&-\pscal[\big]{\Xi}{\xi-\Wmod \xi_0}_\Esp\\
		&-\pscal[\bigg]{E}{\eta-\pderiv{\fjmva}{\xi}\croch{c_0,\xi}}_\Esp\\
		&-\pscal[\big]{E_0}{\eta_0-\Wmod^*\eta}_\Esp\\
		&-\pscal[\big]{N_1}{\Wop \nu_1 - \WKxi{P_0}{\eta_0}}_\Dsp\\
		&-\pscal[\big]{M_0}{\WopA \mu_0 - \WKKxi{\lambda_1}{\eta_0}}_\Dsp\text{,}
\end{split}
\end{equation}
where \(C\), \(\ovl{Q}\), \(\ovl{K^-}\), \(\ovl{K^+}\), \(\Wmod\), \(\Wmod^*\), \(\Wr\) and \(\ovl{S}\) are defined in \cref{eq:def_WKxi,eq:def_WKKxi,eq:def_Cxi,eq:def_WQxi,%
eq:def_Wmod,eq:def_WmodAdj,eq:def_Wr,eq:def_Sdcnv}, and \(\fmod:\paren{\Msp\times\Esp\times\Esp}\mapsto\Msp\) is defined as
\begin{equation}
	\fmod\croch{c_0,\eta,\xi_0}\vecx=k\alpha c_0^{\alpha-1}\vecx\int_h \eta\xh\pderiv{\xi_0}{z}\xh\diff h\text{,}
\end{equation}
with the value of  \(k\) and \(\alpha\) defined in \cref{eq:def_Wmod}.
Deriving the Lagrangian with respect to the state variable \(\mu_0\), \(\nu_1\), \(\eta_0\), \(\eta\), \(\xi\), \(\xi_0\), \(\lambda_1\), \(P_0\) yields the following sets of adjoint equations
\begin{subequations}\label[pluralequation]{eqs:HessVecVel_adj1}
\begin{empheq}[left=\empheqlbrace]{align}
	\Wop M_0   &=\Bxi{P_0}{V}\\
	\WopA N_1  &=\Bxi{\lambda_1}{V}\\
	E_0        &=\WQxi{P_0}{N_1}+\WQxi{M_0}{\lambda_1}\\
	E          &=\Wmod E_0
	            +\croch[\bigg]{\pderiv{\fmod}{\eta}\croch{c_0,\eta,\xi_0}}V%\\
\end{empheq}
\end{subequations}
\begin{subequations}\label{eqs:HessVecVel_adj2}
\begin{empheq}[left=\empheqlbrace]{align}
	\Xi        &=\croch[\bigg]{\pderiv[2]{\fjmva}{\xi}\croch{c_0,\xi}}E
	            +\croch[\bigg]{\pderivsec{\fjmva}{c_0}{\xi}\croch{c_0,\xi}}V\\
	\Xi_0      &=\Wmod^*E_0+\croch[\bigg]{\pderiv{\fmod}{\xi_0}\croch{c_0,\eta,\xi_0}}V\\
	\Wop L_1   &=\Bxi{\nu_1}{V}+\WKxi{P_0}{\Xi_0}+\WKxi{M_0}{\eta_0}\\
	\WopA\Pi_0 &=\Bxi{\mu_0}{V}+\WKKxi{\lambda_1}{\Xi_0}+\WKKxi{N_1}{\eta_0}\text{,}
\end{empheq}
\end{subequations}
where operator \(B:\Dsp\times\Msp\mapsto\Dsp\) is defined for \(u\in\Dsp\) and \(V\in\Msp\) as
\begin{equation}
	\Bxi{u}{V}\sxom=2\iomsqr\frac{V\vecx}{c_0^3\vecx}u\sxom\text{.}
\end{equation}
Eventually the desired matrix-vector product is obtained as the gradient of \(\Gamma\) with respect to \(c_0\) and reads
\begin{equation}
\begin{split}
	\croch[\bigg]{\pderiv[2]{J_1}{c_0}\croch{c_0}}V
		=&\pderiv{\Gamma}{c_0}\\
		=&\Cxi{\Pi_0}{P_0}+\Cxi{L_1}{\lambda_1}+\Cxi{N_1}{\nu_1}+\Cxi{M_0}{\mu_0}\\
		&-\frac{3V}{c_0}\paren[\big]{\Cxi{\mu_0}{P_0}+\Cxi{\nu_1}{\lambda_1}}\\
		&+\croch[\bigg]{\pderiv[2]{\fjmva}{c_0}\croch{c_0,\xi}}V
		+\croch[\bigg]{\pderiv{\fmod}{c_0}\croch{c_0,\eta,\xi_0}}V
		+\croch[\bigg]{\pderivsec{\fjmva}{\xi}{c_0}\croch{c_0,\xi}}E\text{.}
\end{split}
\end{equation}

We have divided the adjoint equations into two sets. In the first group of equation, a vector \(E\in\Esp\) is determined through computations very similar to those used to determine \(\xi\).
The second set of equations looks like the one used to determine the gradient \(\partial J_1/\partial c_0\).
Although only four \enquote{new} wavefields have to be computed \(\paren{\Pi_0,L_1,M_0,N_1}\), the approach is quite expensive because 4D wavefields arrays are too large to be kept in memory and need to be recomputed, first to determine the value of \(E\), then to compute the remaining contributions of the matrix-vector product.
%----------------------------------------------
\begin{table}[!htb]
\begin{tabular}{lcl}
\toprule
compute \(\xi=\Fdag\Pobs\) and evaluate \(J_1\croch{c_0}\) & 2 & \(P_0\), \(\lambda_1\)\\\midrule
given \(\xi\), compute \(\displaystyle\pderiv{J_1}{c_0}\croch{c_0}\) & 4 & \(P_0\), \(\lambda_1\), \(\nu_1\), \(\mu_0\)\\\midrule
given  \(\xi\) and \(\displaystyle\pderiv{J_1}{c_0}\croch{c_0}\), compute \(\displaystyle\croch[\bigg]{\pderiv{J_1}{c_0}\croch{c_0}}V\) & 12 & \(2\paren[\big]{P_0, \lambda_1, M_0, N_1}\), \(\nu_1\), \(\mu_0\), \(L_1\), \(\Pi_0\) \\
\bottomrule
\end{tabular}
\caption{Number of wave-equations to be solved to compute the value of the \gls{mva} objective function in the direct inversion case, its gradient and the product of its Hessian with a vector of \(V\in\Msp\). The number indicated here should be multiplied by the number of source positions considered in the acquisition.}
\end{table}
%----------------------------------------------