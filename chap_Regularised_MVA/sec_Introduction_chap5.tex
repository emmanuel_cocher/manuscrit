\section{Introduction}
In this chapter, we analyse on a simple 1D case the behaviour of iterative migration velocity analysis. The advantage of this simple setting is the possibility to explicitly build modelling and migration operator and to run a large number of iterations. First we summarise the results obtained in 2D in the preceding chapters.
\begin{itemize}
	\item In \cref{chap:iter_migr}, iterative migration has been introduced and we have shown that migration artefacts as well as cross-talk artefacts appearing on \glspl{cig} at the first iteration are progressively attenuated (\cref{fig:prim_ex1_3_CIGs});
	\item In \cref{chap:IMVA}, we compared two methods yielding an approximate gradient of the \gls{mva} objective function after iterative migration. In the first approach, we derived the exact gradient of an approximation of the ideal objective function. In the second one, we compute an approximate gradient of this ideal objective function. The second approach has a simpler implementation. The gradient is obtained in two steps. First an adjoint variable in the extended model space has to be computed as the solution of a linear problem, even in the presence of multiples.
	 Then the gradient is computed using the last iterate of the direct and adjoint problems. Numerical evidence show that regularisation has to be introduced in iterative migration to ensure the smoothness of the gradient (\cref{fig:VelWithReg,fig:VelWithoutReg}) and to prevent the apparition of spurious oscillations;
	\item In \cref{chap:WIMVA}, we have introduced an approximate inverse of the extended Born modelling operator. It is derived under the high frequency approximation, but involves wave-equations operators only. For data containing primaries only, the application of this pseudo-inverse to observed data yields \glspl{cig} free of migration artefacts. The gradient of the associated \gls{mva} objective function is smooth and free of oscillations around the reflectors, provided that a specific power of the background velocity is introduced in the annihilator. We showed how this approximate inverse can be used as a preconditioner to speed up the resolution of iterative migration (\cref{fig:chap4_prim_obj_fct}), needed in the presence of multiples.
\end{itemize}
However several difficulties remain, investigated in this chapter:
\begin{itemize}
	\item If the modelling is linear, the direct and adjoint problems are very similar. They consist of the same system with two different source terms,
	\begin{subequations}
	\begin{align}
		H\xi&=a\text{\quad for the direct problem,} \\
		H\eta&=b\text{\quad for the adjoint problem,}
	\end{align}
	\end{subequations}
	where \(H\) is the Hessian matrix of the migration objective function. However the resolution of the adjoint problem requires in practice more iterations than the direct problem. Using the approximate inverse operator as a preconditioner, a single iteration already provides a very good solution for the direct problem, but this is not the case for the adjoint problem (\cref{fig:chap4_prim_JNadj_sansReg});
	\item Although direct and iterative inversion result in very similar reflectivity images, they lead to quite different background velocity updates (\cref{fig:chap4_prim_gVfinal_sansReg}). In the iterative case, the gradient still has oscillations around the reflector and modifying the \gls{mva} objective function with a power of the background velocity does not allow to remove these oscillations. 
\end{itemize}

In order to gain a better understanding of these issues and to propose solutions addressing them, we study here a 1D-case defined in the same fashion as in \cref{sec:article_Herve}. Although the 1D-case cannot reproduce all the features of 2D subsurface-offset extended migration, its reduced dimensionality allows to explicitly build and display modelling and inverse operators as matrices, and to considerably reduce computation time, so that performing tens or hundreds of iterations for solving both the direct and adjoint problems is affordable. For example, it becomes possible to study the distribution of eigenvalues of the normal operator.

In this analysis we consider only the linear case of primaries. We have shown that direct inversion works already well in this case; the objective here is to understand why the iterative case is unstable and to propose a robust solution to this issue. At the end of the chapter, we discuss on a 2D numerical example how the conclusions can be extended to the case of multiples. We begin by presenting the 1D \gls{mva} approach. After a brief review of direct inversion with the approximate inverse operator, we detail the computation of the gradient in the case of iterative migration. A simple numerical example illustrates that the 1D and 2D cases have a similar behaviour. Eventually we propose a modification of the iterative \gls{mva} procedure to alleviate the difficulties listed above.

In the second part of the chapter, this new strategy is applied on 2D examples, first on primaries only, then on an example with primaries and first-order surface multiples. The objective is to determine if the new approach defined in 1D exhibits the same benefits in the 2D case.


