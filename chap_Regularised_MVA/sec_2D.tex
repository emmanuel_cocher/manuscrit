\section{2D-Examples}
In this section, we apply the new approach consisting of introducing a filter in the objective function of \gls{mva}:
\begin{equation}\label{eq:chap5_J1_filter}
	J_1\paren{c_0}=\frac{1}{2}\norm[\big]{A\croch{c_0}K\croch{c_0}\xi\itn{N+1}\croch{c_0}}^2_\Esp\text{.}
\end{equation}
We will consider only the filter \(K=\Fdag F\) based on the approximate inverse \(\Fdag\). From now on, the reflectivity is defined as a squared slowness perturbation (\(\xi=2\delta c/{c_0^3}\)). In \cref{eq:chap5_J1_filter}, the annihilator consists of a multiplication by \(\abs{h}\) and also includes a multiplication by a power of the background velocity model \(c_0^\beta\), with \(\beta=3/2\).

In \cref{sec:chap5_ex_prim}, we consider primaries only and an example similar to the one of \cref{chap:WIMVA}. The objective is to determine if the introduction of the filter \(K\) has the same benefits as in the pure 1D case. We compare the results obtained with and without filtering final \glspl{cig} and  expect the filter to improve the convergence rate of the adjoint problem and to yield gradients similar to those obtained after direct inversion.

Then we move on to an example with primaries and first-order surface-related multiples (\cref{sec:chap5_ex_mult}). This case was not considered in the 1D-analysis of the preceding section. We want to determine if the new approach extend to the case of multiples and yield coherent velocity updates.


\subsection{Primaries only}\label{sec:chap5_ex_prim}
We first consider an example with primaries only similar to the one presented in \cref{sec:examples_chap4_prim_adj}, to compare two approaches, without and with the filter \(K=\Fdag F\). A single flat reflector is located at \SI{300}{\meter} depth in a too low homogeneous velocity model (\SI{2500}{\meter\per\second}). The exact background velocity model is homogeneous too (\SI{3000}{\meter\per\second}). The model is \SI{1620}{\meter} large and \SI{450}{\meter} deep and is discretised on a \(\SI{6}{\meter}\times\SI{6}{\meter}\) grid. Sources are located at each point of the surface with receiver also at every grid point within a maximum surface offset of \(\SI{\pm 540}{\meter}\).

We perform seven iterations to solve the direct problem with preconditioning and small \(\ell_2\) regularisation, leading to \(\xi\itn{8}\). After application of \(K=\Fdag F\) to this final reflectivity, we obtain a \gls{cig} very close to the one obtained by direct inversion (\cref{fig:2D_prim_CIGs}). In particular, spurious oscillations located above and below the reflector have been greatly attenuated. In \(\xi\itn{8}\), we observe around \(z=\SI{350}{\meter}\) spurious events with an opposite curvature from the main event. We do not have a formal explanation for these events which may not be due to edge effects as PML and tapers are implemented on each edge of the model. Note that they are greatly attenuated by the filter.
%--------------------------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/2D_prim/2D_CIGs_new}
\caption{Central \gls{cig} (top) obtained after sever iteration of preconditioned iterative migration (left). After application of \(\Fdag F\), we obtain a \gls{cig} (middle) very close to the result of direct inversion (right). The bottom row display the same \glspl{cig} multiplied by the absolute value of the subsurface offset. The same colour scale is used in each row.}
\label{fig:2D_prim_CIGs}
\end{figure}
%--------------------------------------------------------------


We now consider the adjoint problem. With the filter, the right-hand side
term reads \(K^TA^TAK\xi\itn{8}\) instead of \(A^TA\xi\itn{8}\). As in 1D, this new vector can be expressed as \(\Fadj Q\), result of application of the adjoint to a data set \(Q=\FdagT A^TAK\xi\itn{8}\in\Dobs\).
As a consequence it is similar to the right-hand side term of the direct problem \(\Fdag\Pobs\) with an event curved upward looking like usual migration artefacts in addition to the downward event corresponding to the true reflector (\cref{fig:2D_prim_Heta}, third column).
The adjoint problem is solved in 10 iterations, with and without preconditioning for comparison. With the application of the filter, the adjoint problem is easier to solve and converge much faster, contrary to the original case (\cref{fig:2D_prim_JNadj}).
Moreover, the preconditioner accelerates the convergence rate, which is also satisfactory. Note however that the convergence rate is still slower than for the direct problem.
The last iterates \(\eta\itnB{8}{11}\) in the two cases are both very oscillating (\cref{fig:2D_prim_Heta}, first column), but in the new approach, the residuals of the linear system are much smaller (\cref{fig:2D_prim_Heta}, fourth column).




We now consider the sequence of gradients of \(J_1\) with respect to \(c_0\) associated to the successive values of adjoint variables obtained without and with the filter when the adjoint problem is solved with preconditioning.
With the filter, this sequence converges to a stable gradient and the final value is reached after five iterations (\cref{fig:2D_prim_10grad,fig:2D_prim_10grad_cut}). Moreover, the final gradient exhibit small oscillations, but is very close to the one obtained by direct inversion (\cref{fig:2D_prim_gVfinal}). This result is very similar to the one obtained in 1D (\cref{fig:KFdagF_gradient}).
%--------------------------------------------------------------
\begin{figure}[!htb]
\includegraphics{Figures/2D_prim/2D_Heta}
\caption{Central \gls{cig} of the last iterate \(\eta\itnB{8}{11}\) obtained in the resolution of the adjoint problem (1st column) and application of the Hessian \(H\) to \(\eta\itnB{8}{11}\) (2nd column). We also display the right-hand side term \(b\itn{8}\) of the linear system (3rd column) and the corresponding residual (4th column). We consider the case where no filter is introduced in the \gls{mva} objective function (top, similar to \cref{fig:chap4_prim_Heta_sansReg}), and the case with a filter \(K=\Fdag F\) based on the approximate inverse (bottom). The three most right plots of each line share the same colour scale.}
\label{fig:2D_prim_Heta}
\end{figure}
%--------------------------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/2D_prim/2D_JNadj_Xi1}
\caption{Convergence rate of the adjoint problem without (top) and with (bottom) application of the filter \(\Fdag F\). The left column shows the decrease of the norm associated with the linear conjugate gradient algorithm, the right column shows the norm of residuals of the linear system.}
\label{fig:2D_prim_JNadj}
\end{figure}
%--------------------------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/2D_prim/2D_10grad_Xi1}
\caption{Gradient \(G\itnB{7}{M}\) obtained with \(\xi\itn{8}\) and with the successive adjoint variables \(\eta\itnB{8}{M+1}\) without (left) and with (right) application of the filter \(K=\Fdag F\). The left column is close to the right column of \cref{fig:chap4_prim_gradients_sansReg} where a very similar model was considered.}
\label{fig:2D_prim_10grad}
\end{figure}
%--------------------------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/2D_prim/2D_10grad_cut_Xi1}
\caption{Section of the gradients at the middle \(x\)-position obtained with the final reflectivity \(\xi\itn{8}\) and the successive values of the adjoint variables \(\eta\itnB{8}{M+1}\), without (left) and with (right) the filter \(K=\Fdag F\).}
\label{fig:2D_prim_10grad_cut}
\end{figure}
%--------------------------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/2D_prim/2D_gVfinal_Xi1}
\caption{Gradient obtained after direct inversion (top), and after iterative inversion without (middle) and with (bottom) filter. For the iterative case, seven and ten iterations are performed to solve the direct and adjoint problems respectively. The right column displays a section of the gradient at the middle position \(x=\SI{810}{\meter}\). The blue dashed line is the gradient obtained after direct inversion (top) and is displayed for comparison.  The middle row is close to the bottom row of \cref{fig:chap4_prim_gVfinal_sansReg} where a very similar model was considered.}
\label{fig:2D_prim_gVfinal}
\end{figure}

As in the 1D case, the gradient can be decomposed into several parts. But contrary to the 1D case where we identified ten contributions \pcref{eq:gVconv_decomp_filter}, the reflectivity has been defined here as a square slowness perturbation \(\xi=2\delta c /c_0^3\). With this parametrisation, the contributions \(G_4\), \(G_5\), \(G_6\) and \(G_9\) in \cref{eq:gVconv_decomp_filter} are zero. The remaining contributions are \(G_7+G_\beta\), \(G_2+G_8\) and \(G_1+G_3\) (\cref{fig:2D_prim_gVdecomp}, right column, top, middle and bottom plots respectively). As \(\xi\itn{8}\) and \(\eta\itnB{8}{11}\) are good solutions of the direct and adjoint problems respectively, the second and third contributions are small with respect to the first one. The latter originates from the operator \(\Fdag\) in the filter and has a physical meaning. It is the gradient that would be obtained by applying the direct inversion strategy to primary data re-computed with first-order Born modelling from the final reflectivity \(F\xi\itn{8}\). As this data set is very close to observed data, this contribution is very close to the gradient obtained with the direct inversion approach (\cref{fig:2D_prim_gVfinal}, top). Although this contribution is not the gradient of an objective function, we discuss in \cref{sec:TG} an alternative strategy using it as background velocity update.
%--------------------------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/2D_prim/2D_gVdecomp}
\caption{Total gradient obtained with the filter (top left, similar to the bottom left plot in \cref{fig:2D_prim_gVfinal}. The gradient is decomposed into three part (right column). Using the notations of \cref{eqs:gVconv_decomp_filter}, the first is due to the approximate inverse (\(G_7+G_\beta\), top), the second should be zero if both direct and adjoint problems are perfectly solved (\(G_2+G_7\), middle) and the third should be zero if the direct problem is perfectly solved and the data residuals zero (\(G_1+G_3\), bottom).}
\label{fig:2D_prim_gVdecomp}
\end{figure}
%--------------------------------------------------------------

Finally we comment on the edge effects visible on the left and right side of the gradient (\cref{fig:2D_prim_gV_extended}, bottom left). Their extension ({\(x=\)\SIrange{0}{550}{\meter}} and {\(x=\)\SIrange{1000}{1620}{\meter}}) is approximately equal to the value of the maximum surface offset (\SI{540}{\meter}, here). Note that in the reflectivity section, the inhomogeneous part only is half of the maximum surface offset (\cref{fig:2D_prim_gV_extended}, top left). If we keep the same value for the maximum surface offset and extend the model laterally, the homogeneous central part is extended, but not the part altered by edge effects (\cref{fig:2D_prim_gV_extended}, right). Therefore, in this example as well as in the following, we focus on the smoothness and coherency of the gradient in the central part. The attenuation of these artefacts is not specifically addressed in this thesis. We observe that tapers on source and on receivers positions, properly included in the gradient computation, help mitigating this undesirable effect. This issue deserves nonetheless further investigation.
%--------------------------------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/2D_prim/2D_xi_gV_extended}
\caption{Section of the reflectivity \(\xi\itn{8}\) at \(h=\SI{0}{\meter}\) (top) and gradient \(G\itnB{7}{10}\) (bottom) obtained with \(x\ped{max}=\SI{1620}{\meter}\) (left, same gradient as \cref{fig:2D_prim_gVfinal} bottom left), and with \(x\ped{max}=\SI{2430}{\meter}\). Extending the model laterally for a similar value of the maximum surface offset results in a larger homogeneous central part, the edge effects remaining similar.}
\label{fig:2D_prim_gV_extended}
\end{figure}
%--------------------------------------------------------------


In summary, the introduction of the filter has a similar effect in the pure 1D and the 2D cases. It improves the convergence rate of the adjoint problem and the associated gradient is close to the result obtained after direct inversion. Also, the introduction of the filter relax the requirement of sufficiently strong regularisation illustrated in the examples of \cref{chap:IMVA,chap:WIMVA} where no filter was considered.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Primaries and Multiples}\label{sec:chap5_ex_mult}
We want to test this new approach on a case with first-order surface-related multiples. We consider an example similar to the one studied in \cref{sec:examples_chap4_mult_migr}. The exact model is laterally invariant with a single reflector located at \SI{300}{\meter} depth and a background velocity model increasing with depth (\cref{fig:2D_mult_model}), leading to two events in observed data (\cref{fig:2D_mult_Pobs}). The initial background velocity model is also increasing with depth, with the correct velocity at the surface but with a too low gradient. In all the following examples, we add a multiplication by \(z\) in the definition of the annihilator to emphasise the impact of multiples. This weight is taken into account in the derivation of the gradient.
%--------------------------------------------------------------
\begin{figure}[htb]
\begin{minipage}[t]{0.49\linewidth}
	\centering
	\includegraphics{Figures/2D_mult/modele_gradmult}
	\caption{Exact (red) and initial (blue) background velocity model. The reflectivity model consists of a single reflector located at \SI{300}{\meter} depth.}
	\label{fig:2D_mult_model}
\end{minipage}\hfill%
\begin{minipage}[t]{0.49\linewidth}
	\centering
	\includegraphics{Figures/2D_mult/Pobs_gradmult}
	\caption{Observed data obtained for a source located at the middle position on the surface in the exact model of \cref{fig:2D_mult_model}.}
	\label{fig:2D_mult_Pobs}
\end{minipage}
\end{figure}
%--------------------------------------------------------------


For reference, we first apply the direct inversion strategy. If we remove multiples from observed data, we obtain a single event in \glspl{cig}, with a downward curve and the gradient is negative and homogeneous above the reflector, which is consistent with the too low initial velocity model (\cref{fig:2D_mult_InvPrim}). With both primaries and multiples in observed data, a new event corresponding to the multiple interpreted as a primary appears around \SI{600}{\meter} depth (\cref{fig:2D_mult_InvMult}). The actual multiple travelled twice in the exact model between the surface and the exact reflector position. It is interpreted as a primary travelling in the initial velocity model between the surface and a depth roughly twice the one of the exact reflector. Hence, this \enquote{imaginary} primary has \enquote{seen} a higher velocity than the true multiple. As a consequence, part of the energy of the cross-talk artefact is defocused with an upward curvature and the gradient has positive contribution above the reflector
%--------------------------------------------------------------
\begin{figure}[!htb]
%\captionsetup[subfigure]{aboveskip=-2pt,belowskip=-1pt}
\captionsetup[subfigure]{singlelinecheck=false}
%-----
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:2D_mult_InvPrim}}%
\begin{subfigure}[valign=t]{0.75\linewidth}
	\includegraphics{Figures/2D_mult/CIG_gV_InvPrim}
\end{subfigure}
%-----
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:2D_mult_InvMult}}%
\begin{subfigure}[valign=t]{0.75\linewidth}
	\includegraphics{Figures/2D_mult/CIG_gV_InvMult}	
\end{subfigure}
%-----
\caption{Gradient and \glspl{cig} obtained in the initial model shown in \cref{fig:2D_mult_model}, after direct inversion with observed data containing \subref{fig:2D_mult_InvPrim} primaries only or \subref{fig:2D_mult_InvMult} both primaries and multiples.}
\label{fig:2D_mult_gV_inv}
\end{figure}
%--------------------------------------------------------------


We now consider iterative inversion with the filter \(\Fdag F\) introduced in the definition of the \gls{mva} objective function \pcref{eq:chap5_J1_filter}. First, we study the case of observed data containing only the primary reflection (\cref{fig:2D_mult_gV_iter_prim}). We obtain a gradient very similar to the case of direct inversion (\cref{fig:2D_mult_InvPrim}), which indicates that the new approach works well in non-homogeneous models as well.
%--------------------------------------------------------------
\begin{figure}[!htbp]
	\includegraphics{Figures/2D_mult/CIG_gV_Prim}
	\caption{Gradient and \glspl{cig} obtained in the initial model shown in \cref{fig:2D_mult_model}, after iterative inversion with observed data containing primaries only. The direct and adjoint problems are solved in \(N=\num{7}\) and \(M=\num{10}\) iterations respectively, and with small \(\ell_2\) regularisation.}
\label{fig:2D_mult_gV_iter_prim}
\end{figure}
%--------------------------------------------------------------

We now consider the full data set with the primary reflection and the first-order surface multiple and use preconditioned non-linear optimisation as in \cref{sec:examples_chap4_mult_migr}. The linear adjoint problem reads in the general case
\begin{equation}
	\croch[\bigg]{\pderiv[2]{J_0}{\xi}\croch[\big]{c_0,\xi\itn{N+1}}}\eta=A^TK^TKA^T\xi\itn{N+1}\text{,}
\end{equation}
the difference with the linear case being that the Hessian matrix depends on the final value of \(\xi\). This operator reads
\begin{equation}
\begin{split}
	\pderiv[2]{J_0}{\xi}=&\pderiv[2]{P_3}{\xi}\paren{P_1+P_3-\Pobs}+\paren[\bigg]{\pderiv{P_1}{\xi}}^T\paren[\bigg]{\pderiv{P_1}{\xi}}+2\paren[\bigg]{\pderiv{P_1}{\xi}}^T\paren[\bigg]{\pderiv{P_3}{\xi}}+\paren[\bigg]{\pderiv{P_3}{\xi}}^T\paren[\bigg]{\pderiv{P_3}{\xi}}\\
	&+\lreg\pderiv[2]{\phi}{\xi}\text{,}
	\end{split}
\end{equation}
where we have omitted the dependency to \(\xi\itn{N+1}\) and operator \(M\) for readability. Because of the first term in this expression, the Hessian may not be positive definite in case of too strong residuals. This is an issue as the conjugate gradient algorithm is designed for positive definite matrix only. This problem should not appear if iterative migration has converged, meaning that residuals are small, or if enough regularisation is added. An additional safeguard consists of exiting the algorithm as soon as a negative curvature is encountered \parencite{metivier_full_2013}.

Using small \(\ell_2\) regularisation and ten iterations to solve the adjoint problem, we first compare the gradient obtained after \numlist{1;10} iterations of migration (\cref{fig:2D_mult_iter_mult_01D,fig:2D_mult_iter_mult_10D}). In the first case, residuals are still strong and a negative curvature has been encountered at the 10th iteration. Therefore the final gradient is actually \(G\itnB{10}{9}\). Because of the cross-talk artefact in the \gls{cig}, the gradient is non-zero below the reflector. After ten iterations, the artefact is weaker and is further attenuated by the application of the filter. However, the gradient is not homogeneous and exhibits strong oscillations around the reflector. 
To obtain a more satisfactory result, we first increase the number of iterations for the resolution of the direct and adjoint problems (\num{30} and \num{20}, respectively), but the gradient is still far from being homogeneous (\cref{fig:2D_mult_iter_mult_30D}). We notice that even after several iterations and application of the filter, the annihilator still strengthen residual energy at large values of \(h\) below the reflector (\cref{fig:2D_mult_iter_mult_30D}, 4th column). This suggests that these areas may not be well constrained. As a remedy, we add a Huber norm to the migration objective function \(J_0\) with a bigger weight on these parts of the \glspl{cig}:
\begin{equation}
	\phi\croch{\xi}=\int_{\vec{x}}\int_h \hub\paren[\big]{h\cdot z\cdot \xi\xh} \diff h \diff \vec{x}\text{,}
\end{equation}
and we use again 10 iterations to solve the direct and adjoint problems (\cref{fig:2D_mult_iter_mult_10DReg}). Although the final \glspl{cig} look very similar to the one obtained in the previous case, this modification greatly improves the gradient, which is homogeneous with the correct sign above the reflector and very weak below.

%Note however the convergence of the adjoint problem is slower than in the case of primaries only (\cref{fig:2d_mult_iter_mult_JN})
%--------------------------------------------------------------
\begin{figure}[!htbp]
%\captionsetup[subfigure]{aboveskip=-2pt,belowskip=-1pt}
\captionsetup[subfigure]{justification=raggedright,singlelinecheck=false}
\captionsetup{singlelinecheck=false}
%-----
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:2D_mult_iter_mult_01D}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}\centering
	\includegraphics{Figures/2D_mult/CIG_gV_Mult01}
\end{subfigure}
%------
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:2D_mult_iter_mult_10D}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}
	\includegraphics{Figures/2D_mult/CIG_gV_Mult10}
\end{subfigure}
%------
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:2D_mult_iter_mult_30D}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}
	\includegraphics{Figures/2D_mult/CIG_gV_Mult30}
\end{subfigure}
%------
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:2D_mult_iter_mult_10DReg}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}
	\includegraphics{Figures/2D_mult/CIG_gV_MultReg}
\end{subfigure}
%------
\caption[Gradient and CIGs obtained in the initial model shown in \cref{fig:2D_mult_model}, after iterative inversion with multiples]{Gradient and \glspl{cig} obtained after iterative inversion with observed data containing both primaries and multiples. Different optimisation strategies are considered, regarding regularisation and the number \(N\) and \(M\) of iterations used to solve the direct and adjoint problems:%
\begin{enumerate}[nosep,label=(\alph*)]
	\item small \(\ell_2\) regularisation, \(N=\num{1}\), \(M=\num{9}\);
	\item small \(\ell_2\) regularisation, \(N=\num{10}\), \(M=\num{10}\);
	\item small \(\ell_2\) regularisation, \(N=\num{30}\), \(M=\num{20}\);
	\item stronger Huber regularisation, \(N=\num{10}\), \(M=\num{10}\).
\end{enumerate}}
\label{fig:2D_mult_iter_mult}
\end{figure}
%--------------------------------------------------------------
%\begin{figure}
%	\includegraphics{Figures/2D_mult/JNadj_Mult_compar}
%	\caption{Convergence of the adjoint problem in the four cases presented in \cref{fig:2D_mult_iter_mult}. We consider the objective function associated with the conjugate gradient algorithm (left) and the norm of the residuals of the linear system (right). Note that they correspond to four different linear systems and cannot be compared directly. Therefore the curves of the left plot have been mapped linearly to the interval \(\croch{-1,0}\).}
%	\label{fig:2d_mult_iter_mult_JN}
%\end{figure}
%%--------------------------------------------------------------

As in the 1D case, we decompose the gradient into three parts (\cref{fig:2D_mult_iter_mult_decomp}). The first one (\cref{fig:2D_mult_iter_mult_decomp}, 1st column) is due to the operator \(\Fdag\) in the definition of \(J_1\) \pcref{eq:chap5_J1_filter}. As already mentioned in the linear case, this is the gradient that would be obtained by applying the direct inversion approach to observed primaries re-computed from the final reflectivity free of cross-talk artefacts. As a consequence this contribution is very similar to the one obtained by applying direct inversion to primary only (\cref{fig:2D_mult_InvPrim}), provided that the final reflectivity correctly explains observed data.
The second contribution (\cref{fig:2D_mult_iter_mult_decomp}, 2nd column) is directly related to data residuals, and its share in the final gradient decreases as far as the number of iterations increases (\cref{fig:2D_mult_iter_mult_01D_decomp,fig:2D_mult_iter_mult_10D_decomp,fig:2D_mult_iter_mult_30D_decomp}). With regularisation, the final data residuals may be degraded, and this contribution may be not negligible any more (although it is still quite small in the example of \cref{fig:2D_mult_iter_mult_10DReg_decomp}).
Eventually we notice that the third part (\cref{fig:2D_mult_iter_mult_decomp}, 3rd column) is relatively strong in the four cases and does not necessarily add a coherent contribution to the gradient. Contrary to the linear case, it does not vanish when the direct and adjoint problems are perfectly solved.
%--------------------------------------------------------------
\begin{figure}[!htbp]
%\captionsetup[subfigure]{aboveskip=-2pt,belowskip=-1pt}
\captionsetup[subfigure]{justification=raggedright,singlelinecheck=false}
\captionsetup{singlelinecheck=false}
%-----
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:2D_mult_iter_mult_01D_decomp}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}\centering
	\includegraphics{Figures/2D_mult/gV_decomp_Mult01}
\end{subfigure}
%------
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:2D_mult_iter_mult_10D_decomp}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}
	\includegraphics{Figures/2D_mult/gV_decomp_Mult10}
\end{subfigure}
%------
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:2D_mult_iter_mult_30D_decomp}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}
	\includegraphics{Figures/2D_mult/gV_decomp_Mult30}
\end{subfigure}
%------
\adjustbox{minipage=1.3em,valign=t}{\subcaption{}\label{fig:2D_mult_iter_mult_10DReg_decomp}}%
\begin{subfigure}[valign=t]{\dimexpr\linewidth-1.3em\relax}
	\includegraphics{Figures/2D_mult/gV_decomp_MultReg}
\end{subfigure}
%------
\caption[Decomposition of the gradients of \cref{2D_mult_iter_mult}]{Decomposition of the gradients of \cref{fig:2D_mult_iter_mult} into three parts, similar to the one presented in \cref{fig:2D_prim_gVdecomp} in the case of primaries only. The first contribution (left) can be interpreted as the gradient that would be obtained by applying the direct inversion strategy to recalculated primaries remodelled with the last reflectivity image of the iterative migration processing. The second contribution (middle) is directly related to data residuals and should be zero if iterative migration has converged. The remaining contributions (right) are difficult to interpret and are not necessarily zero if convergence is reached for both the direct and adjoint problems.}
\label{fig:2D_mult_iter_mult_decomp}
%----------------------------------
\end{figure}

We conclude from this example that the new strategy consisting of adding a filter in the definition of the \gls{mva} objective function is also efficient in the case of data containing first-order surface multiples. The filter attenuates unwanted energy at large values of \(h\) as in the primaries only case and helps attenuating residual energy of cross-talk artefacts. Note however that contrary to the primaries only case, regularisation remains essential to iterative migration for a proper attenuation of cross-talk artefacts and consistent background velocity updates.

\subsection{Alternative strategy}\label{sec:TG}
Both in the primaries only case and in the presence of multiples, we have isolated a contribution to the total gradient due to the operator \(\Fdag\) in the filter \(K\) and showed that it is very close to the result of direct inversion applied on primaries only (\cref{fig:2D_prim_gVdecomp}, top right and \cref{fig:2D_mult_iter_mult_decomp}, 1st column). We propose as an alternative strategy to use this contribution only to update the background model. In this new approach, the \gls{mva} objective function to be minimised still reads
\begin{equation}\label{eq:J1_TG}
	J_1\croch{c_0}=\frac{1}{2}\norm[\big]{A\croch{c_0}\Fdag\croch{c_0} F\croch{c_0} \xi\itn{N+1}\croch{c_0}}^2_\Esp\text{.}
\end{equation}
We briefly rederive an expression for the total gradient of the objective function~\eqref{eq:J1_TG} with respect to \(c_0\) and identify the contribution which is considered in the new strategy. We define the following Lagrangian,
\begin{equation}\label{eq:J1_TG_Lagr}
	\Lagr{J_1}=\frac{1}{2}\norm[\big]{A\zeta}_\Esp^2
		-\pscal[\big]{\gamma}{\zeta-\Fdag P\ap{calc}}_\Esp%\\
		-\pscal[\big]{Q}{P\ap{calc}-F\xi\itn{N+1}}_\Dsp%\\
		-\pscal[\bigg]{\eta}{\pderiv{J_0}{\xi}\croch[\big]{c_0,\xi\itn{N+1}}}_\Esp
\end{equation}
The value of the adjoint variables \(\gamma\), \(Q\) and \(\eta\) are obtained by zeroing the partial derivatives of the Lagrangian~\eqref{eq:J1_TG_Lagr} with respect to the state variables \(\zeta\), \(P\ap{calc}\) and \(\xi\itn{N+1}\),
\begin{subequations}
\begin{empheq}[left=\empheqlbrace]{align}
	\gamma &=A^TA\zeta\itn{N+1}\text{,}\\
	Q &=\FdagT\gamma\text{,} \\
	\croch[\bigg]{\pderiv[2]{J_0}{\xi}\croch[\big]{c_0,\xi\itn{N+1}}}\eta&=\Fadj Q\text{,}\label{eq:adjprob_TG}
\end{empheq}
\end{subequations}
and the total gradient of the objective function~\eqref{eq:J1_TG} with respect to \(c_0\) is obtained as the partial derivative of the Lagrangian~\eqref{eq:J1_TG_Lagr} with respect to \(c_0\),
\begin{multline}\label{eq:J1_TG_GradTotal}
	\pderiv{J_1}{c_0}=
	\overbrace{%
	\pderiv{}{c_0}\croch[\bigg]{\frac{\norm[\big]{A\zeta}^2_\Esp}{2}}
	+\pderiv{}{c_0}\croch[\bigg]{\pscal[\big]{\gamma}{\Fdag P\ap{calc}}_\Esp}%
	}^{\text{\enquote{truncated gradient}}}
	+\pderiv{}{c_0}\croch[\bigg]{\pscal[\big]{Q}{F\xi\itn{N+1}}_\Dsp}\\
	+\croch[\bigg]{\pderivsec{J_0}{\xi}{c_0}\croch[\big]{c_0,\xi\itn{N+1}}}\eta\text{.}
%\end{split}
\end{multline}
In the new approach, the background velocity update is defined as the sum of the first two contributions in~\eqref{eq:J1_TG_GradTotal}, the two remaining contributions being dropped. Therefore this strategy will be referred to as \enquote{\emph{truncated gradient}} in \cref{chap:applications}.

We now explain the meaning of this new approach. We apply iterative migration to a data set containing both primaries and first-order surface multiples to derive an extended reflectivity free of cross-talk artefacts. This extended image is used to re-compute primary reflections under the first-order Born approximation. The value of this wavefield at receiver positions is kinematically consistent with the primaries contained in the original observed data set and defines a new data set contains primaries only. The velocity analysis is performed on this data set as if it were observed data, using the direct inversion strategy presented in \cref{sec:article_Herve}.

The background velocity update used in the \enquote{truncated gradient} strategy is not the gradient of an objective function, but this definition has several advantages. First, it requires less computational effort as there is no need to solve the adjoint problem~\eqref{eq:adjprob_TG} any more. Moreover the need to add proper regularisation and the difficult choice of parameters of the regularisation function is relaxed. Finally note that re-modelled primaries can be computed for source positions not present in the original acquisition, for example in the case of an irregular or incomplete acquisition. In the latter case, multiples may provide extra illumination to reconstruct a more detailed reflectivity model and compute primary reflections not recorded in the original acquisition. Also, the new strategy allows to provide the direct inversion strategy with data acquired with dense source and receiver coverage, which is one of the hypotheses made in the derivation of the approximate inverse \(\Fdag\).

%In addition, as we are computing a velocity update from primary reflections only, we may drop the extra-information in the final reflectivity brought by multiples. Note however that we can use a different acquisition geometry in the computation of \(\Fdag F\) than the one of observed data. In the case of an incomplete acquisition, one can regenerate primaries with a dense source and receiver coverage and thus benefit from the extra-information brought by multiples

%In summary, we have shown that the new objective function penalising \glspl{cig} filtered by \(\Fdag F\) works well when multiples are considered in iterative migration. However, contrary to the primary-only case, regularisation is still essential to obtain a coherent velocity update.