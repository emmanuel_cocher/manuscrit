\chapter{Modelling of free-surface reflection under the Born approximation}\label{app:free_surface}
In this appendix related to \cref{sec:free_surface}, we justify on a 1D case the expression for the operator \(\Ms\) defined in \cref{eq:def_Ms} accounting for the reflection at the free-surface.
Intuitively, we would like to use the Born formula and define a source term made of primaries recorded at the surface multiplied by a constant reflection coefficient \(R=\paren{-1}\), as the free surface is known to act as a mirror \parencite{schuster_basics_2007}. Numerical comparison with a finite-difference modelling with a free-surface condition suggests that an additional coefficient and a time derivative \(2\ioma/c_0\) have to be applied as well. The purpose of this appendix is to explain the reason for this additional term. Actually, the origin of the reflection at the free surface can be described by a constant velocity perturbation above the surface with value \(\paren{-1}\). However, in practice, the model considered for simulation does not extend above the free surface and we consider a reflectivity localised at the surface. This requires a modification of the Born formula.

Let us consider a pure 1D case with a single source and a single receiver both buried and localised at depth \(z_s\). The surface is at a depth \(z_0<z_s\). In the asymptotic approximation, the 1D Green's function reads
\begin{equation}
	G_0\paren{z_s,z,\omega}=\frac{c_0\paren{z}}{2\ioma}\eu^{-\ioma\tau\paren{z_s,z}}\text{,}
\end{equation}
where \(\tau\paren{s,z}\) is the traveltime between the source and the depth \(z\) the surface. 
First we consider a constant reflectivity model above the surface \(\xi\paren{z}=R\cdot H\paren{z_0-z}\) where \(H\) stands for the Heaviside function. Using \cref{eq:Born_modelling_Prim_without_h}, the upgoing incident wave reads
\begin{equation}
	P\ped{up}(z_s,z,\omega)=\frac{c_0\paren{z}}{2\ioma}\eu^{-\ioma \tau\paren{z_s,z}}\wavelet\vecom\text{,}
\end{equation}
and the downgoing reflected wave recorded at the source position reads
\begin{equation}
	P\ped{down}(z_s,z_r=z_s,\omega)=R\wavelet\vecom\int_{z=z_0}^{-\infty} \eu^{2\ioma\tau\paren{z_s,z}} \diff z\text{.}
\end{equation}
We linearise the value of the traveltime around the surface,
\begin{equation}
	\tau\paren{z_s,z}\approx\tau_0+\frac{1}{c_0\paren{z}}\paren{z_s-z_0}\text{,}
\end{equation}
where \(\tau_0=\tau\paren{z_s,z_0}\). This leads to
\begin{equation}\label{eq:Pdown_free_surf_heaviside}
	P\ped{down}(z_s,z_r=z_s,\omega)=R\frac{c_0\paren{z}}{2\ioma}\eu^{-2\ioma\tau_0}\wavelet\text{.}
\end{equation}
The downgoing wavefield has the same shape as the upgoing wavefield, except for the multiplication by the reflection coefficient \(R=\paren{-1}\). The surface acts as a mirror on a upgoing wavefields.

In practice we consider a reflectivity localised at \(z_0\): \(\xi\paren{z}=R\cdot\delta\paren{z-z_0}\). Following the same steps, this leads to
\begin{equation}
	P\ped{down}(z_s,z_r=z_s,\omega)=R\,\eu^{-2\ioma\tau_0}\wavelet\vecom\text{.}
\end{equation}
Compared to, \cref{eq:Pdown_free_surf_heaviside}, a coefficient \(\frac{c_0}{2\ioma}\) is missing in the result. To model the surface reflection properly, the source term of the Born modelling equation~\eqref{eq:def_P1_nonextended} has to be multiplied by \(\frac{c_0}{2\ioma}\).