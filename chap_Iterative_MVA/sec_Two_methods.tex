\section{Computing the gradient of \texorpdfstring{\(J_1\)}{J1}}
We investigate the computation of two different approximations of the gradient with respect to the background velocity model \(c_0\) of the ideal \gls{mva} objective function
\begin{equation}\label{eq:J1_ideal}
	J_1^\infty\croch{c_0}=\frac{1}{2}\norm[\big]{A\xi^\infty\croch{c_0}}_\Esp^2\text{,}
\end{equation}
where \(\xi^\infty\) obeys the first order optimality condition of \(J_0\croch{c_0,\xi}\),
\begin{equation}\label{eq:MethBConditionConvergence}
	\pderiv{J_0}{\xi}\croch{c_0,\xi^\infty}=0\text{.}
\end{equation}
Operator \(A:\Esp\mapsto\Esp\) is called \emph{annihilator}. For more detail on the meaning of this operator, we refer to \cref{sec:mva}. In the numerical applications, the annihilator will consist of a multiplication by the absolute value of the subsurface offset: \(A\xi\paren{\vec{x},h}=\abs{h}\xi\paren{\vec{x},h}\). The subsurface offset is the extra parameter on which the focusing criterion is tested. For a correct model, \(\xi\xh\) should be null for \(h\neq 0\). The annihilator penalises defocused energy arising in an incorrect background velocity model. We keep the notation \(A\) in this chapter for the sake of generality. 

In the following, we first compute the exact gradient of an approximation \(J_1\itn{N}\) of \(J_1^\infty\) obtained when \(N\)~iterations are performed to solve the migration problem. We call this approach method A. In the second approach, called method B, we assume that iterative migration reaches convergence after \(N\)~iterations and use the optimality criterion~\eqref{eq:MethBConditionConvergence} to determine an approximate gradient of \(J_1^\infty\).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Method A: Exact gradient of an approximate objective function}\label{sec:methodA}
In method A, we compute the exact gradient of the following approximation of \(J_1^\infty\), defined as
\begin{equation}
	J_1\itn{N}\croch{c_0}=\frac{1}{2}\norm[\big]{A\xi\itn{N+1}\croch{c_0}}_\Esp^2
\end{equation}
where \(\xi\itn{N+1}\) is obtained after \(N\)~iterations of gradient-based minimisation of the migration objective function \pcref{eq:def_J0} described in the preceding chapter.

A general way of performing this computation is the use of automatic differentiation algorithms \parencite{barth_algorithmic_2008}. Here we employ the adjoint state method \parencite{plessix_review_2006} to obtain an explicit expression allowing the analysis of each contribution to the total gradient. To keep simple expressions, we consider the case where iterative migration is performed with steepest descent and a linesearch procedure \(\lsearch\). Then the state equations for the computations are given by
\begin{subequations}\label{eq:MigrIter}\label[pluralequation]{eqs:MigrIter}
\begin{equation}
\xi\itn{1}=0\label{eq:MigrItera}
\end{equation}
\begin{empheq}[left=\forall n\in\croch{1,N}\quad \empheqlbrace]{align}
	g\itn{n}&=\pderiv{J_0}{\xi}\croch[\big]{c_0,\xi\itn{n}}\label{eq:MigrIterb}\\
	d\itn{n}&=-g\itn{n}\label{eq:DescDirEquations}\\
	\alpha\itn{n}&=\lsearch\paren[\big]{\xi\itn{n},d\itn{n}}\\
	\xi\itn{n+1}&=\xi\itn{n}+\alpha\itn{n}d\itn{n}\text{,}\label{eq:MigrItere}
\end{empheq}
\end{subequations}
where \(g\itn{n}\) is the gradient of the objective function, \(d\itn{n}\) is the descent direction and \(\alpha\itn{n}\) the step length at iteration \(n\).
We associate an adjoint variable to each equation of the iterative process \pcrefrange{eq:MigrIterb}{eq:MigrItere}. These Lagrange multipliers can be interpreted as constraints on the state equations. The associated Lagrangian reads:
\begin{equation}
\begin{split}
	\Lagr{J_1^A}=
		J_1\itn{N}\croch{c_0}
		&-\sum_{n=1}^{N}\pscal[\big]{\eta\itn{n}}{\xi\itn{n+1}-\xi\itn{n}-\alpha\itn{n}d\itn{n}}_\Esp\\ 
		&-\sum_{n=1}^{N}\beta\itn{n}\paren[\big]{\alpha\itn{n}-\lsearch\paren{\xi\itn{n},d\itn{n}}}\\ 
		&-\sum_{n=1}^{N}\pscal[\big]{\delta\itn{n}}{d\itn{n}+g\itn{n}}_\Esp\\
		&-\sum_{n=1}^{N}\pscal[\Big]{\gamma\itn{n}}{g\itn{n}-\pderiv{J_0}{\xi}\croch[\big]{c_0,\xi\itn{n}}}_\Esp
\end{split}
\end{equation}
where the scalars \(\beta\itn{n}\) and the vectors of \(\Esp\) \(\eta\itn{n}\), \(\delta\itn{n}\) and \(\gamma\itn{n}\) are the adjoint variables associated to the state variables \(\alpha\itn{n}\), \(\xi\itn{n}\), \(d\itn{n}\) and \(\gamma\itn{n}\) respectively . Similar to \cref{sec:Grad_J0} for the derivation of the gradient of \(J_0\), the value of the adjoint variables is obtained by zeroing the derivatives of the Lagrangian with respect to the state variables,
\begin{subequations}\label{eq:adjmigrA}\label[pluralequation]{eqs:adjmigrA}
\begin{equation}
\eta\itn{N}=A^TA\xi\itn{N+1}
\end{equation}
\begin{empheq}[left=\forall n \in \croch{N,1}\empheqlbrace]{align}
	\beta\itn{n}&=\pscal[\big]{\eta\itn{n}}{d\itn{n}}_\Esp\label{eq:beta_MethA}\\
	\delta\itn{n}&=
			\alpha\itn{n}\eta\itn{n}
			+\beta\itn{n}\pderiv{\lsearch}{d}\paren[\big]{\xi\itn{n},d\itn{n}}\\
	\gamma\itn{n}&=
			-\delta\itn{n}\label{eq:gammaMethA}\\ 
	\eta\itn{n-1}&=\label{eq:eta_MethA}
			\eta\itn{n}
			+\pderiv[2]{J_0}{\xi}\paren{c_0,\xi\itn{n}}\gamma\itn{n}%\\
			+\beta\itn{n}\pderiv{{\lsearch}}{\xi}\paren[\big]{\xi\itn{n},d\itn{n}}\text{.}
\end{empheq}
\end{subequations}
The gradient of \(J_1\) with respect to \(c_0\) then equals to
\begin{equation}\label{eq:GradA}
	G_A\itn{N}=
	\sum_{n=1}^N\psiop{c_0,\xi\itn{n}}{\gamma\itn{n}}
	+\sum_{n=1}^N \beta\itn{n}\pderiv{\lsearch}{c_0}\paren[\big]{\xi\itn{n},d\itn{n}}\text{.}
\end{equation}

Thus method A is an iterative process initiated at iteration \(N\) with the adjoint source term \(\eta\itn{N}=A^TA\xi\itn{N+1}\) named \emph{image residual}. Then, \crefrange{eq:beta_MethA}{eq:eta_MethA} have to be solved from \(n=N\) to \(n=1\). Method A requires the ability of computing the product of second-order derivatives of \(J_0\) with the successive values of \(\gamma\). An efficient technique to perform this calculation will be presented in \cref{sec:Hess_vec_chap3}. Note that the linesearch function may depend on \(c_0\), hence the second term in the gradient. If we consider a descent method with a fixed step, that is \(\alpha\itn{n}=\alpha_0\), we have \(\beta\itn{n}=0\).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Method B: approximate gradient of the ideal objective function}\label{sec:methodB}
In method B, we derive a simpler expression with the assumption that iterative migration is performed until convergence \parencite{chauris_inversion_2015}. That is, we suppose that the final reflectivity section obeys the optimality condition~\eqref{eq:MethBConditionConvergence} and we consider the ideal \gls{mva} objective function~\eqref{eq:J1_ideal}, built with \(\xi^\infty\) instead of \(\xi\itn{N+1}\). The gradient can again be evaluated via the adjoint state method. The associated Lagrangian is
\begin{equation}
	\Lagr{J_1^B}\croch{c_0,\xi^\infty,\sigma}=J_1^\infty\croch{c_0}-\pscal[\Big]{\sigma}{\pderiv{J_0}{\xi}\croch{c_0,\xi^\infty}}_\Esp\text{.}
\end{equation}
with \(\sigma\) the adjoint state that satisfies;
\begin{equation}\label{eq:adjmigrB}
	\croch[\bigg]{\pderiv[2]{J_0}{\xi}\croch{c_0,\xi^\infty}}\sigma=A^TA\xi^\infty\text{.}
\end{equation}
The ideal solution \(\sigma^\infty\) of this linear problem is the deconvolved version of the image residual \(A^TA\xi^\infty\).
The gradient of \(J_1\) with respect to \(c_0\) then reads
\begin{equation}\label{eq:GradB}
	G_B^\infty=-\psiop{c_0,\xi^\infty}{\sigma^\infty}\text{.}
\end{equation}
This is the optimal gradient obtained when both \cref{eq:MethBConditionConvergence,eq:adjmigrB} are verified. In practice we approximate \(\xi^\infty\) by \(\xi\itn{N+1}\) and we solve iteratively the linear system
\begin{equation}\label{eq:adjmigrBapprox}
	\croch[\bigg]{\pderiv[2]{J_0}{\xi}\croch{c_0,\xi\itn{N+1}}}\sigma=A^TA\xi\itn{N+1}
\end{equation}
with \(M\) iterations. This leads to the approximate gradient of \(J_1\)
\begin{equation}\label{eq:GradBapprox}
	G_B\itnB{N}{M}=-\psiop{c_0,\xi\itn{N+1}}{\sigma\itnB{N+1}{M+1}}\text{.}
\end{equation}
Even if the migration process is non-linear, the adjoint equation~\eqref{eq:adjmigrB} is a linear problem of the type \(H\sigma=\theta\) where \(H\) is the Hessian of \(J_0\) and \(\theta\) is the image residual. %This is actually the Newton equation \eqref{eq:linear_system_newton} presented in \cref{sec:Newton_method} with a different right-hand side term.
If the convergence assumption is satisfied, the residuals are small and the Hessian operator is positive semi-definite. %, as seen in \cref{sec:Hess_J0}.
Hence \cref{eq:adjmigrB} is easily solved with a linear conjugate-gradient algorithm.
Compared to method A, the adjoint equations of method B are solved independently of the optimisation strategy used for iterative migration. The adjoint problem may actually be solved with a different number of iterations, as the velocity update only involves the last value of \(\xi\itn{n}\) and \(\sigma\itn{m}\) regardless of how they are computed.  \Cref{fig:SketchMethA,fig:SketchMethB} illustrate how adjoint variables are computed in both methods and how they are combined with state variables to compute the velocity update. 
%------------------------------------------------
\begin{figure}[!htbp]
\centering
\includegraphics{Figures/MethodesGradientA.pdf}
\caption{Sketch of method A. Thin black arrows show the order of computation of the variables. Thick grey arrows show how they are combined to compute the velocity update}
\label{fig:SketchMethA}
\end{figure}
%------------------------------------------------
\begin{figure}[!htbp]
\centering
\includegraphics{Figures/MethodesGradientB.pdf}
\caption{Same as \cref{fig:SketchMethA} for method B. Note that the numbers \(N\) and \(M\) of iterations performed to solve the direct and adjoint problems are not necessarily the same.}
\label{fig:SketchMethB}
\end{figure}
%------------------------------------------------

We detail the advantages of both methods regarding implementation and memory requirements in \cref{sec:compar_impl}. First we elaborate on the computation of second-order derivatives of \(J_0\).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Computation of the second-order derivatives of \texorpdfstring{\(J_0\)}{J0}}\label{sec:Hess_vec_chap3}
Both methods involve the second-order derivatives of \(J_0\). In method A, the product of \(\pderivinline[2]{J_0}{\xi}\) and \(\pderivsecinline{J_0}{\xi}{c_0}\) with the adjoint variable \(\gamma\) is required \(N\) times. In method B, the resolution of the adjoint system \pcref{eq:adjmigrBapprox} with the linear conjugate gradient requires \(M\) products of the Hessian \(\pderivinline[2]{J_0}{\xi}\) with a vector of \(\Esp\) and the final computation of the gradient \pcref{eq:GradBapprox} requires a single product of \(\pderivsecinline{J_0}{\xi}{c_0}\) with the final value \(\sigma\itnB{N}{M}\).  The contribution of the regularisation is straightforward, hence we focus on the second derivatives of \(\Jres\). Following \textcite{metivier_full_2013}, an efficient procedure to compute their product with a vector of \(\Esp\) is defined with a second-order adjoint-state technique.

We consider a new scalar function \(\Gamma\) and a vector \(\chi\) of \(\Esp\)
\begin{equation}
	\Gamma\croch[\big]{c_0,\xi,\chi}=\pscal[\bigg]{\pderiv{\Jres}{\xi}\croch{c_0,\xi}}{\chi}_\Esp
\end{equation}
such that the gradients of \(\Gamma\) with respect to \(\xi\) and \(c_0\) equal the desired matrix-vector products
\begin{subequations}
	\begin{empheq}[left=\empheqlbrace]{align}
		\pderiv{\Gamma}{\xi}&=\croch[\bigg]{\pderiv[2]{\Jres}{\xi}\croch{c_0,\xi}}\chi\text{,}\\
		\pderiv{\Gamma}{c_0}&=\croch[\bigg]{\pderivsec{\Jres}{\xi}{c_0}\paren{c_0,\xi}}\chi\text{.}
	\end{empheq}
\end{subequations}
We now apply the classic adjoint-state method to compute the gradients of \(\Gamma\). We define a Lagrangian with the adjoint variables \(\mu_i\) and \(\nu_i\) associated to the state variables \(P_i\) and \(\lambda_i\), respectively. The state equations are given by \cref{eq:def_P,eq:def_lambda}. The Lagrangian reads
\begin{equation}
\begin{split}
	\Lagr{\Gamma}\croch{c_0,\xi,\chi,P_i,\lambda_i,\mu_i,\nu_i}=&
	\pscal[\big]{\Qxi{P_0}{\lambda_1}+\Qxi{P_2}{\lambda_3}}{\chi}_\Esp\\
	&-\pscal[\big]{\mu_0}{\Wop P_0-S}_\Dsp\\
	&-\pscal[\big]{\mu_1}{\Wop P_1-\Kxi{P_0}{\xi}}_\Dsp\\
	&-\pscal[\big]{\mu_2}{\Wop P_2-\Ms P_1}_\Dsp\\
	&-\pscal[\big]{\mu_3}{\Wop P_3-\Kxi{P_2}{\xi}}_\Dsp\\
	&-\pscal[\big]{\nu_3}{\WopA \lambda_3- M^T\paren[\big]{M\paren{P_1+P_3}-\Pobs}}_\Dsp\\
	&-\pscal[\big]{\nu_2}{\WopA \lambda_2-\KKxi{\lambda_3}{\xi}}_\Dsp\\
	&-\pscal[\big]{\nu_1}{\WopA \lambda_1- M^T\paren[\big]{M\paren{P_1+P_3}-\Pobs}-\MsA\lambda_2}_\Dsp\text{.}
\end{split}
\end{equation}
We derive \(\Lagr{\Gamma}\) with respect to the state variables \(P_i\) and \(\lambda_i\) to find the adjoint equations:
\begin{subequations}
\begin{empheq}[left=\empheqlbrace]{align}
	\Wop \nu_1&=\Kxi{P_0}{\chi}\\
	\Wop \nu_2&=\Ms\nu_1\\
	\Wop \nu_3&=\Kxi{P_2}{\chi}+\Kxi{\nu_2}{\xi}\\
	\WopA\mu_3&=M^TM\paren{\nu_1+\nu_3}\\
	\WopA\mu_2&=\KKxi{\mu_3}{\xi}+\KKxi{\lambda_3}{\chi}\\
	\WopA\mu_1&=M^TM\paren{\nu_1+\nu_3}+\MsA \mu_2\\
	\WopA\mu_0&=\KKxi{\mu_1}{\xi}+\KKxi{\lambda_1}{\chi}
\end{empheq}
\end{subequations}
The desired matrix-vector products are obtained by derivating \(\Lagr{\Gamma}\) with respect to \(\xi\) and \(c_0\)
\begin{alignat}{2}
	\pderiv{\Gamma}{\xi}\croch{c_0,\xi,\chi}=&&
		\croch[\bigg]{\pderiv[2]{\Jres}{\xi}\croch{c_0,\xi}}\chi
		&=\Qxi{P_0}{\mu_1}+\Qxi{P_2}{\mu_3}+\Qxi{\lambda_3}{\nu_2}\\
	\pderiv{\Gamma}{c_0}\croch{c_0,\xi,\chi}=&&
		\croch[\bigg]{\pderivsec{\Jres}{\xi}{c_0}\croch{c_0,\xi}}\chi
		&=\sum_{i=0}^3\Cxi{P_i}{\mu_i}+\sum_{i=1}^3\Cxi{\lambda_i}{\nu_i}\text{,}
\end{alignat}
where we defined the classical normalised cross-correlation operator \(\Cxi{u}{v}: \Dsp\times\Dsp\mapsto\Msp\) for two vectors \(\paren{u,v}\) of \(\Dsp\) as
\begin{equation}\label{eq:def_Cxi}
	\Cxi{u}{v}\paren{\vec{x}}=
	%\frac{2}{c_0^3\paren{\vec{x}}}\Qxi{u}{v}\paren{\vec{x},h=0}
	\frac{2}{c_0^3\paren{\vec{x}}}\int_s\int_\omega\iomsqr u\sxom v\sxom \diff \omega \diff \vec{s}
\end{equation}

%These gradients are computed using the adjoint-state method with the state equations given by \cref{eq:def_P,eq:def_lambda}. See \cref{app:Hess_vector} for details.
Note that both products can be computed simultaneously. Their calculations are twice as expensive as the computation of the gradient \(\pderivinline{\Jres}{\xi}\) (\cref{tab:cost_hess}). Note that if we calculate only \(\pderivinline[2]{J_0}{\xi}\), some direct and adjoint variables are not needed, leading to less computations. In particular, in the case of primaries only, this operator does not depend on \(\xi\) and is computed in the same way as the gradient of \(J_0\).
%---------------------------
\begin{table}[!htbp]
\begin{tabular}{ccc}
 \toprule
 & primaries only & primaries and multiples  \\ \midrule
 \(J_0\croch{c_0,\xi}\) & 2 & 4 \\ \midrule
 \(\displaystyle\pderiv{J_0}{\xi}\croch{c_0,\xi}\)  & 3 & 7 \\ \midrule
 \(\displaystyle\croch[\bigg]{\pderiv[2]{J_0}{\xi}\croch{c_0,\xi}}\chi\)  & 3 & 11 \\ \midrule
 \(\displaystyle\croch[\bigg]{\pderivsec{J_0}{\xi}{c_0}\croch{c_0,\xi}}\chi\)  & 6 & 14 \\ \bottomrule
\end{tabular}
\caption{Number of wave-equations to be solved to evaluate the value, the gradient of the objective function at a point \(\xi\), and the product of its Hessian with a vector \(\chi\). The number indicated here should be multiplied by the number of source positions considered in the acquisition.}
\label{tab:cost_hess}
\end{table}
%---------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Stability of the gradient}\label{sec:grad_stab_chap3}
Ideally the MVA objective function should be evaluated at \(\xi^\infty\) and its gradient \(G^\infty\) used to update the background velocity model. In practice we perform a limite number of iterations and use \(G_A\itn{N}\) or \(G_B\itnB{N}{M}\). One may want to estimate the decrease in convergence speed made by considering approximate gradients for the minimisation of \(J_1^\infty\) \parencite{friedlander_hybrid_2012} and bound the error defined as the difference between the macro-model recovered after a few outer iterations \(c_0^f\) and the exact macro-model \(c_0^*\). Ideally it should be bound by the error made in the resolution of the inner problem by performing only \(N\) iterations. This analysis is not trivial as the gradient is not linear in \(\xi\) (\(\pderivinline{J_1}{c_0}=\croch{\pderivinline{\xi}{c_0}}^TA^TA\xi\)). Some simple results can be stated, though.
We have shown in \cref{chap:iter_migr} that after an insufficient number of inner-iterations, the approximate objective function of method A is not minimum for the correct macro-model on one hand, and the gradient obtained with method B is very unlikely to lead to an accurate macro-model estimation on the other hand. 
An other issue is that the null space of the MVA objective function \(J_1\) is not empty, meaning that different background velocity models can lead to relatively well-focused energy in \glspl{cig}. As an illustration, we present in \cref{fig:inv_results} some results of iterations on the background velocity models. These results and the approach used to obtain them will be detailed in \cref{chap:WIMVA,chap:applications}.
We display the exact macro-model used to compute observed data and two results obtained after twenty iterations on the macro-model starting with two different initial guesses. These three different macro-models result in \glspl{cig} with very similar focusing property. They differ in particular on the edges of the model where the acquisition setting does not constrain the model well. Larger surface-offset may better constrain these areas. This illustrates the practical difficulty to bound the error between the exact background model and the one recovered after several outer iterations.
%----------------------------------------------
\begin{figure}[!htbp]
	\includegraphics{Figures/inv_results/inv_results}
	\caption{Results of inversion (left, 2nd and 3rd row) obtained with observed data modelled in the exact model shown on the first row (left). These results are presented in more details in \cref{chap:applications}. For each background velocity model, three \glspl{cig} are displayed at the lateral positions \SIlist{500;1500;2000}{\meter} as well as three traces of the velocity models at the same locations. The red curve corresponds to the exact model. The blue and green curves correspond to the final models displayed in the 2nd and 3rd row, respectively. These results are obtained with two different initial models (dashed curves), a homogeneous macro-model and a model with values increasing with depth.}
	\label{fig:inv_results}
\end{figure}
%----------------------------------------------

Instead, we propose here to study the stability of the MVA gradient computed for a fixed background velocity model \(c_0\) and successive values of the reflectivity model computed during iterative migration \parencite{huang_born_2016}. Ideally we would like to be able to bound the error between the ideal gradient \(G^\infty\) and its approximations \(G_A\itn{N}\) and \(G_B\itnB{N}{M}\) by the error on the reflectivity model \(\xi\itn{N+1}-\xi^\infty\). We begin by presenting three preliminary results for which we do not have formal proofs, but which allow to bound the error in the gradient by the error in the resolution of the inner problem. Then we use the numerical results presented in \cref{sec:ex_primaries_chap3} to determine if these hypotheses are reasonable.

\minisec{Lipschitz conditions}\label{sec:lipschitz_justif}
The stability of the MVA gradient across inner-iterations is based on the following Lipschitz conditions,
\begin{subequations}
\begin{align}
	\norm[\bigg]{\pderiv{}{c_0}\paren[\big]{\xi^\infty-\xi\itn{N+1}}}_{\Esp\times\Msp}
		&\leq\Xi\norm[\big]{\xi^\infty-\xi\itn{N+1}}_\Esp\text{,}\label{eq:lipschitz_A}\\
	\norm[\Bigg]{\pderivsec{J_0}{\xi}{c_0}\croch[\big]{c_0,\xi^\infty}-\pderivsec{J_0}{\xi}{c_0}\croch[\big]{c_0,\xi\itn{N+1}}}_{\Esp\times\Msp}
		&\leq \Xi_1 \norm[\big]{\xi^\infty-\xi\itn{N+1}}_\Esp\text{,} \label{eq:lipschitz_B1}\\
	\norm[\Bigg]{\pderiv[2]{J_0}{\xi}\croch[\big]{c_0,\xi^\infty}-\pderiv[2]{J_0}{\xi}\croch[\big]{c_0,\xi\itn{N+1}}}_{\Esp\times\Esp}
		&\leq \Xi_2 \norm[\big]{\xi^\infty-\xi\itn{N+1}}_\Esp\text{,}\label{eq:lipschitz_B2}
\end{align}
\end{subequations}
where \(\Xi\), \(\Xi_1\) and \(\Xi_2\) are three positive constants.

We make two simplifying hypotheses, first a linear modelling \(F:\Esp\mapsto\Dobs\) of the data \(P\croch{c_0,\xi}=F\croch{c_0}\xi\), so that the derivatives of \(J_0\) read
\begin{subequations}
\begin{align}
	\pderiv{J_0}{\xi}\croch{c_0,\xi}&=H\xi-b\text{,}\label{eq:GradJ0_lin_case}\\
	\pderiv[2]{J_0}{\xi}\croch{c_0,\xi}&=H\text{,}
\end{align}
\end{subequations}
with \(b=F^T\Pobs\) and the Hessian \(H=F^TF+\lreg I\). Second, we use a steepest descent direction and a constant step size \(\alpha\), then
\begin{equation}\label{eq:xi_eq_gen}
	H\xi\itn{N+1}=\paren{I-C^N}b\text{,}
\end{equation}
with the operator \(C=I-\alpha H\) supposed to ensure convergence to \(\xi^\infty\), which satisfies
\begin{equation}
	H\xi^\infty=b\text{.}
\end{equation}
Then
\begin{subequations}
\begin{align}
	\pderiv{}{c_0}\paren{\xi\itn{N+1}-\xi^\infty}&=-\pderiv{}{c_0}\paren[\big]{H^{-1}C^Nb}\\
		&=-\paren[\bigg]{\pderiv{H^{-1}}{c_0}C^Nb+NH^{-1}\pderiv{C}{c_0}C^{N-1}b+H^{-1}C^N\pderiv{b}{c_0}}
\end{align}
\end{subequations}
The convergence of this expression is determined by operator \(C^N\) and so is the convergence of the migration process \pcref{eq:xi_eq_gen}, which indicates that the Lipschitz condition \eqref{eq:lipschitz_A} may be acceptable.

In the linear case the Hessian \(H=\pderivinline[2]{J_0}{\xi}\croch{c_0,\xi}\) is independent of \(\xi\), so that condition~\eqref{eq:lipschitz_B2} is trivial. Eventually we use \cref{eq:GradJ0_lin_case} to write
\begin{equation}
	\pderivsec{J_0}{\xi}{c_0}\croch{c_0,\xi\itn{N+1}}-
	\pderivsec{J_0}{\xi}{c_0}\croch{c_0,\xi^\infty}=
	\pderiv{H}{c_0}\paren{\xi\itn{N+1}-\xi^\infty}
	+H\pderiv{}{c_0}\paren{\xi\itn{N+1}-\xi^\infty}\text{,}
\end{equation}
which guarantees condition~\eqref{eq:lipschitz_B1} provided the first Lipschitz condition~\eqref{eq:lipschitz_A} is verified.

\minisec{Stability of method A}
In method A we consider the exact gradient of \(J_1\itn{N}\), then the error can be expressed as
\begin{equation}
	\Delta G_A\itn{N}=G_A\itn{N}-G^\infty=\croch[\bigg]{\pderiv{}{c_0}\xi\itn{N+1}}^TA^TA\xi\itn{N+1}-\croch[\bigg]{\pderiv{}{c_0}\xi^\infty}^TA^TA\xi^\infty\text{.}
\end{equation}
We write \(\xi^\infty=\xi^\infty-\xi\itn{N+1}+\xi\itn{N+1}\), so that
\begin{equation}
	\Delta G_A\itn{N}=\croch[\bigg]{\pderiv{}{c_0}\xi\itn{N+1}}^TA^TA\paren[\big]{\xi\itn{N+1}-\xi^\infty}-\croch[\bigg]{\pderiv{}{c_0}\paren[\big]{\xi^\infty-\xi\itn{N+1}}}^TA^TA\xi^\infty\text{.}
\end{equation}
Using the Lipschitz condition \eqref{eq:lipschitz_A}, it leads to
\begin{equation}
	\norm[\big]{\Delta G_A\itn{N}}_\Msp\leq k_A \norm[\big]{\xi\itn{N+1}-\xi^\infty}_\Esp\text{,}
\end{equation}
with \(k_A\) a scalar constant. Hence, the stability of method A relies only on the hypothesis formulated in \cref{eq:lipschitz_A}.

\minisec{Stability of method B}
For method B, the optimal gradient is given by \eqref{eq:GradB}. In practice we use an approximate gradient: \cref{eq:MethBConditionConvergence} is solved in \(N\) iterations to find \(\xi\itn{N+1}\) and \cref{eq:adjmigrBapprox} is solved in \(M\) iterations to find \(\sigma\itnB{N+1}{M+1}\). The error associated to these approximations reads
\begin{equation}
	\Delta G_B\itnB{N}{M}=G_B\itnB{N}{M}-G^\infty=
	\psiop{c_0,\xi^\infty}{\sigma\itnB{\infty}{\infty}}-
	\psiop{c_0,\xi\itn{N+1}}{\sigma\itnB{N+1}{M+1}}\text{.}
	%\pderivsec{J_0}{\xi}{c_0}\paren{\xi^\infty}\sigma\itnB{\infty}{\infty}-
	%\pderivsec{J_0}{\xi}{c_0}\paren{\xi\itn{N+1}}\sigma\itnB{N+1}{M+1}\text{.}
\end{equation}
Similarly to the previous case, we want to bound the error \(\Delta G_B\itnB{N}{M}\) by the errors made in the resolution of the direct and adjoint problems, that is:
\begin{equation}\label{eq:convergence_B}
	\norm[\big]{\Delta G_B\itnB{N}{M}}_\Msp \leq
	k_\xi\norm[\big]{\xi\itn{N+1}-\xi^\infty}_\Esp+
	k_\sigma\norm[\big]{\sigma\itnB{N+1}{M+1}-\sigma\itnB{N+1}{\infty}}_\Esp\text{,}
\end{equation}
with \(k_\xi\) and \(k_\sigma\) two constants.
Therefore we decompose \(\sigma\itnB{N+1}{M+1}\) into
\begin{equation}
	\sigma\itnB{N+1}{M+1}=\sigma\itnB{N+1}{M+1}-\sigma\itnB{N+1}{\infty}+\sigma\itnB{N+1}{\infty}-\sigma\itnB{\infty}{\infty}+\sigma\itnB{\infty}{\infty}\text{,}
\end{equation}
so that the error is the sum of three contributions
\begin{equation}\label{eq:errorGB_decomp}
\begin{split}
\Delta G_B\itnB{N}{M}=&
	\croch[\Bigg]{\pderivsec{J_0}{\xi}{c_0}\croch[\big]{c_0,\xi^\infty}
	-\pderivsec{J_0}{\xi}{c_0}\croch[\big]{c_0,\xi\itn{N+1}}}
	\sigma\itnB{\infty}{\infty}\\
	&-\pderivsec{J_0}{\xi}{c_0}\croch[\big]{c_0,\xi\itn{N+1}}
	\paren[\big]{\sigma\itnB{N+1}{M+1}
	-\sigma\itnB{N+1}{\infty}}\\
	&-\pderivsec{J_0}{\xi}{c_0}\croch[\big]{c_0,\xi\itn{N+1}}
	\paren[\big]{\sigma\itnB{N+1}{\infty}
	-\sigma\itnB{\infty}{\infty}}\text{.}
\end{split}
\end{equation}

Using \cref{eq:lipschitz_B1}, the first contribution in \cref{eq:errorGB_decomp} may be bounded by \(\norm{\xi^\infty-\xi\itn{N+1}}\). The second contribution can be bounded by the error in the resolution of the adjoint problem \(\norm{\sigma\itnB{N+1}{M+1}-\sigma\itnB{N+1}{\infty}}\). For the third term, we use the fact that \(\sigma\itnB{N+1}{\infty}\) and \(\sigma\itnB{\infty}{\infty}\) are the exact solutions of problems~\eqref{eq:adjmigrBapprox} and \eqref{eq:adjmigrB} respectively, so that
\begin{equation}
	\croch[\bigg]{\pderiv[2]{J_0}{\xi}\croch[\big]{c_0,\xi\itn{N+1}}}{\sigma\itnB{N+1}{\infty}}-
	\croch[\bigg]{\pderiv[2]{J_0}{\xi}\croch[\big]{c_0,\xi^\infty}}{\sigma\itnB{\infty}{\infty}}
	=A^TA\paren[\big]{\xi\itn{N+1}-\xi^\infty}\text{.}
\end{equation}
We write again \(\sigma\itnB{N+1}{\infty}=\sigma\itnB{N+1}{\infty}-\sigma\itnB{\infty}{\infty}+\sigma\itnB{\infty}{\infty}\) to obtain
\begin{equation}\label{eq:Error3}
\begin{split}
	\croch[\bigg]{\pderiv[2]{J_0}{\xi}\croch[\big]{c_0,\xi\itn{N+1}}}\paren[\big]{\sigma\itn{N+1,\infty}-\sigma\itnB{\infty}{\infty}}=&
	\croch[\Bigg]{\pderiv[2]{J_0}{\xi}\croch[\big]{c_0,\xi^\infty}-\pderiv[2]{J_0}{\xi}\croch[\big]{c_0,\xi\itn{N+1}}}\sigma\itnB{\infty}{\infty}\\
	&+A^TA\paren[\big]{\xi\itn{N+1}-\xi^\infty}\text{.}
\end{split}
\end{equation}
Using \cref{eq:lipschitz_B2,eq:Error3}, we can bound the third contribution in \cref{eq:errorGB_decomp} by the error \(\norm{\xi^\infty-\xi\itn{N+1}}\).
In the linear case, \cref{eq:lipschitz_B2} is automatically verified and the validity of \cref{eq:lipschitz_B1} is implied by \cref{eq:lipschitz_A}, as observed above. We conclude that for both methods~A and~B, the validitity of \cref{eq:lipschitz_A} is essential to the stability of the MVA gradient across inner-iterations. The numerical examples of \cref{sec:ex_primaries_chap3} will help us to determine under which conditions this hypothesis is verified.%This allows us to bound the error \(\Delta G_B\itnB{N}{M}\) as in \cref{eq:convergence_B}, meaning that, similarly to method A, the convergence speed of the gradient for method B is determined by the convergence rates of \(\xi\) and \(\sigma\).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Comparison in terms of implementation}\label{sec:compar_impl}
%We have presented in the theory section two methods to compute the gradient of \(J_1\) and we have shown on numerical examples that they yield the same results provided that the convergence assumption holds and that sufficient regularisation is applied.
Methods A and B differ regarding implementation of the adjoint equations. In method A, the adjoint equations depend on the optimisation method chosen for iterative migration, whereas in method B, the adjoint variables are solution of a linear system solved independently of the calculation made for the migration. Method B is thus easier to implement. In addition, the adjoint equations of method A \pcref{eqs:adjmigrA} involve the values of the state variables of migration \(\xi\itn{n}\), \(g\itn{n}\), and \(d\itn{n}\) at each iteration. Hence method A requires storage of \(3N\) \(\Esp\)-vectors. An alternative is to recompute their value when required during the computation of the adjoint variables. But they are needed in the reverse order to the one in which they are computed: we need first \(\xi\itn{N}\), then \(\xi\itn{N-1}\), \etc Recomputation is then very expensive. In comparison, the adjoint variables in method B are computed independently of the successive values of the state variables. Only the last value of \(\xi\itn{n}\) is needed to initiate the resolution, then we only store the variables needed to use the conjugate-gradient algorithm.% We eventually conclude that method B is much simpler to implement.

In both methods the main computational step is the product of the Hessian with a \(\Esp\)-vector. We have presented in \cref{sec:Hess_vec_chap3} an efficient way to perform this calculation based on second-order adjoint-state approach \parencite{metivier_full_2013}.
Note that the wavefields \(P_i\) and \(\lambda_i\) involved in the matrix-vector products have already been computed during the migration step, so that storing them would save a lot of computation time. However, these 4D arrays (\(n_t\times n_z \times n_x \times n_s\)) are too big to be stored and recomputation is necessary. We cannot process data source by source as for example the calculation of \(\xi\) implies a summation over all sources.

In method A, computing the derivative of the optimal step length with respect to the velocity model and the state variables adds extra computation of \(J_0\) derivatives, especially in the non-linear case where the optimal step \(\alpha\itn{n}\) is the result of a sophisticated procedure involving several trial steps; the formulas for this derivatives may not be straightforward as well. Hence, method A is roughly \num{1.5} times more expensive in computation time.

