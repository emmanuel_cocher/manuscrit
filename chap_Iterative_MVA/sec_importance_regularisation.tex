\subsection{Importance of regularisation}\label{sec:importance_regularisation}
To illustrate the importance of the regularisation term in the migration objective function, we run the same example with a 50 times smaller value of \(\lreg\). The corresponding gradients of \(J_1\) (\cref{fig:VelWithoutReg}) have shapes similar to the previous case (\cref{fig:VelWithReg}) for the first iterations but after 10 iterations they are altered with sub-vertical spurious oscillations, emphasised on \cref{fig:VelWithoutReg} with dashed lines for method A. Moreover the gradients obtained for both methods do not look similar, although the value of \(J_0\) seems to reach convergence (\cref{fig:ObjFctWithoutReg_a}).
%------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/prim_without_reg_gV/prim_without_reg_gV}
\caption{Same as \cref{fig:VelWithReg} with a smaller regularisation weight \(\lreg\).}
\label{fig:VelWithoutReg}
\end{figure}
%------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/fct_obj_without_reg/fct_obj_without_reg}
{\phantomsubcaption\label{fig:ObjFctWithoutReg_a}}
{\phantomsubcaption\label{fig:ObjFctWithoutReg_b}}
{\phantomsubcaption\label{fig:ObjFctWithoutReg_c}}
\caption{Same as \cref{fig:ObjFctWithReg} with a smaller regularisation weight \(\lreg\).}
\label{fig:ObjFctWithoutReg}
\end{figure}
%------------------------

Actually, the norm of the penalised \glspl{cig} still increases steadily after 10 iterations (\cref{fig:ObjFctWithoutReg_c}). This norm is not used in the inner loop iterative process, but we expect it to converge to a stable value as does \(\xi\). This result is not satisfactory and indicates that residual energy at large values of \(h\) in the \glspl{cig} (\cref{fig:CIGitersWithoutReg}, right) has a weak impact on the objective function \(J_0\), but is amplified by the multiplication by \(h\) and deeply influences the value of \(J_1\) and the velocity update.
%------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/prim_without_reg_xi/prim_without_reg_xi}
\caption{Same as \cref{fig:CIGitersWithReg} with a smaller regularisation weight \(\lreg\).}
\label{fig:CIGitersWithoutReg}
\end{figure}
%------------------------
A sufficiently strong regularisation term allows to better constrain the reflectivity model at large values of \(h\) and ensures the convergence of the value of the \gls{mva} objective function with iterations, as illustrated by the former example.
Finally, we would like to put these results in perspective with the theoretical study of the gradient stability led in \cref{sec:grad_stab_chap3}. We have shown that the stability of the gradient obtained with both methods in the linear case was ensured by the validity of the Lipschitz condition~\eqref{eq:lipschitz_A}. The results shown in \cref{fig:VelWithReg,fig:VelWithoutReg} suggest that \cref{eq:lipschitz_A} is not valid unless sufficient regularisation is introduced during iterative migration. This observation is complementary to the observations of \textcite{huang_born_2016}, that no theoretical relation between the error on the gradient and the error in the inner iterations can be found. More precisely, referring to \cref{sec:mva}, \cref{fig:grad_instab_fit} suggests that the error increases exponentially.

We now identify the terms in the calculation of the gradient with method A that are responsible for the sub-vertical spurious oscillations. We use an example with smaller dimensions but with the same reflectivity and velocity value. Here, after 8 iterations of migration with steepest descent and optimal step size, we obtain with method A the gradient presented in \cref{fig:VelFinDiff_a}, which is altered with the same kind of artefacts. We control the correct computation of the gradient by also deriving the gradient with a finite difference approach, requiring only to evaluate the objective function. As this is very expensive (proportional to the number of model parameters), we restrict the computation to a single line at depth \(z=\SI{96}{\meter}\) and obtain a similar result (\cref{fig:VelFinDiff_b}). These spurious oscillations are already visible in the adjoint variables (see for example \(\gamma\itn{8}\) in  \cref{fig:migrdemigr_a}) and come from small oscillations located above the reflector in the final migrated image, especially at large offsets. Their energy in \(\xi\itn{N+1}\) is weak compared to the reflector, but they are strengthened in the computation of successive adjoint variables by two mechanisms. The first is the iterative application of the Hessian \(\pderivinline[2]{\Jres}{\xi}\croch{c_0,\xi\itn{n}}\) in the construction of the adjoint variables. In the linear case, it does not depend on \(\xi\itn{n}\) and its application to a vector \(\chi\) of \(\Esp\) is a sequence of a data modelling step using \(\chi\) followed by a migration of this data back to the \(\Esp\)-space. Both steps involve two Green's functions from the source and the receiver position to a point of the subsurface. In a smooth model, the asymptotic amplitude term of the Green's function decays with the square-root of the distance, therefore the Hessian has a dynamic effect of strengthening the shallow events and attenuating deeper ones. As the background velocity used for modelling and migration is the same, the diagonal term of the Hessian has no kinematic effect and the position of events is thus not modified.
We show the effect of the Hessian applied to the adjoint variable \(\gamma\itn{8}\) (\cref{fig:migrdemigr_a}), which contains both horizontal events (emphasised with blue dashed lines) and vertical events (red dashed lines) with the same shape as the artefacts of the velocity update. By nature, these artefacts are always above the interfaces and thus are amplified after application of the Hessian (\cref{fig:migrdemigr_b}), while the deeper horizontal events are attenuated. The regularisation term mitigates the attenuation of deeper events and prevents the apparition of artefacts.
%------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/artefact_fin_diff/artefact_fin_diff}
{\phantomsubcaption\label{fig:VelFinDiff_a}}
{\phantomsubcaption\label{fig:VelFinDiff_b}}
\caption{\subref{fig:VelFinDiff_a} Gradient of \(J_1\) computed using method A after 8 iterations. \subref{fig:VelFinDiff_b} Section of this image at \(z=\SI{96}{\meter}\) (red, solid) compared to the gradient obtained with finite differences (blue, dashed). Both are normalised with the same constant.}
\label{fig:VelFinDiff}
\end{figure}
%------------------------

The second origin of artefacts comes from the linesearch contributions \(\pderivinline{\lsearch}{\xi}\paren{\xi\itn{n},d\itn{n}}\) and \(\pderivinline{\lsearch}{d}\paren{\xi\itn{n},d\itn{n}}\) (\cref{fig:dadd_a}) to the construction of adjoint variables in method A \pcref{eq:adjmigrA}. They depend on the corresponding gradient of migration \(\pderivinline{J_0}{\xi}\croch{c_0,\xi\itn{n}}\), which has non-negligible energy above the reflector in the last iterations (\cref{fig:dadd_b}). The contributions of the linesearch also involve a sequence of modelling and migration steps applied to this gradient, hence, similarly to the application of the Hessian, the spurious events above the reflector are strengthened.
%------------------------
\begin{figure}[!htbp]
\begin{minipage}[t]{0.49\linewidth}
\includegraphics{Figures/artefact_hessian/artefact_hessian}
{\phantomsubcaption\label{fig:migrdemigr_a}}
{\phantomsubcaption\label{fig:migrdemigr_b}}
\caption{Illustration of the effect of migration/demigration on \(\gamma\itn{8}\). \subref{fig:migrdemigr_a} Value of variable \(\gamma\) at iteration \num{8} (section at \(h=0\)). \subref{fig:migrdemigr_b} result of the application of the Hessian on this image (section at \(h=0\)).}
\label{fig:migrdemigr}
\end{minipage}\hfill%
\begin{minipage}[t]{0.49\linewidth}
\includegraphics{Figures/artefact_alpha/artefact_alpha}
{\phantomsubcaption\label{fig:dadd_a}}
{\phantomsubcaption\label{fig:dadd_b}}
\caption{\subref{fig:dadd_a} Contribution of the line search \(\pderiv{\lsearch}{d}\paren{\xi\itn{8},d\itn{8}}\) to the equation of \(\delta\itn{8}\).  \subref{fig:dadd_b} Gradient of \(J_0\) at the end of the iterations \(g\itn{8}=\pderiv{J_0}{\xi}\croch{c_0,\xi\itn{8}}\) (section at \(h=0\)).}
\label{fig:dadd}
\end{minipage}
\end{figure}
%------------------------

%This example illustrates the need for a migration operator preserving the amplitude of events, corresponding to a Hessian closer to the identity operator.
