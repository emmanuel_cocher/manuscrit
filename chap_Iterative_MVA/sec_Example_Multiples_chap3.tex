\subsection{Example with  multiples}
To investigate the ability of iterative migration velocity analysis to handle multiple reflections, we run the same example with first-order surface-related multiples added to observed and calculated data. To take cross-talk events into account, the model is extended to \SI{600}{\meter} depth. Two events are present in observed data: the primary reflection and its associated surface multiple, recorded at approximately twice the time of the primary.
%------------------------
%\begin{figure}[!htbp]
%\includegraphics{Figures/mult_Pobs/mult_Pobs}
%\caption{A shot point at \(x=\SI{1350}{\meter}\) with a primary reflection and a surface-multiple}
%\label{fig:DataMult}
%\end{figure}
%------------------------
To ensure the multiple event has enough influence, the residuals between observed data and calculated data are multiplied by the recording time \(t\) in the definition of \(J_0\). %The migration problem is not linear anymore and is solved with a non-linear conjugate-gradient, using Polak-Ribière approach and a linesearch ensuring the strong Wolfe conditions \parencite{more_line_1994}. 15 iterations are performed (\cref{fig:CIGsmult}).
The data weight is taken into account in the derivation of the gradient of \(J_1\) with respect to the background model.
%------------------------
\begin{figure}[!htbp]
\includegraphics{figures/mult_CIGs/mult_CIGs}
\caption{Results of migration after 1 (top) and 15 (bottom) iterations of conjugate gradient when we consider primaries and first-order surface-related multiples. Section at \(h=0\) (left), CIG at \(x=\SI{1350}{\meter}\) (middle), and the same CIG penalised by \(\abs{h}\) (right). Two event are visible: the true reflector (TR) and the multiple imprint caused by cross-talk (MI)}
\label{fig:CIGsmult}
\end{figure}
%------------------------

In the first iteration, starting from \(\xi=0\), both events in observed data are interpreted as primary reflections. The primary reflection produces the same event as in the primaries-only case, while the multiple event adds an imprint at twice the depth of the true reflector. This artefact creates a new primary in the modelled data at the next iteration, which in turn will produce an update with a negative sign in the new reflectivity update. This way cross-talk artefacts are progressively attenuated.
%The regularisation coefficient is here the result of a compromise, a too weak value would lead to spurious oscillations in the velocity update. However a too strong value prevent cross-talk artefacts to be fully attenuated.

We now compute the gradient of the \gls{mva} objective function after one and fifteen iterations of migration. Compared to the primary only case, a more complex linesearch procedure \(\lsearch\) is used during migration. Thus the derivation of the associated contributions to the exact gradient in method A become much more complex too. Moreover, method B seems less sensitive to vertical spurious oscillations illustrated in \cref{fig:VelWithoutReg}. Therefore we use method B for the computation of the gradient in this multiple example \cref{fig:Velmults}. At the first iteration, the cross-talk artefact adds non-physical energy below the true reflector. After 15 iterations of migration, this artefact is largely attenuated and the gradient looks similar to the one obtained in the case where only primaries were considered.
%------------------------
\begin{figure}[!htbp]
\includegraphics{figures/mult_gradients/mult_gradients}
\caption{Velocity updates obtained with method B after 1 (top) and 15 (bottom) iterative migration of data containing both primaries and first-order surface-related multiples. They correspond to the migrated images of \cref{fig:CIGsmult}. Blue, grey and red represent negative, null and positive values respectively. Each velocity update is plotted with its own colour scale.}
\label{fig:Velmults}
\end{figure}
%------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\minisec{Choice of the regularisation coefficient}
Similar to the case of primaries only, a sufficiently strong regularisation coefficient is required to avoid the apparition of spurious vertical oscillations. However if too much weight is given to the regularisation term, the attenuation of cross-talk artefacts is not as efficient and their imprint is still visible both on the final reflectivity model and the \gls{mva} gradient. As an illustration, we compute the gradient obtained with method B and 15 iterations of iterative migration in three cases corresponding to three values of the regularisation coefficient (\cref{fig:mult_gradients_3reg}). The choice of the regularisation is thus a trade-off between the smoothness of the gradient and the attenuation of artefacts and should in principle be the result of a Pareto curve analysis \parencite{hansen_l-curve_1999}. However such an analysis is quite expensive, and \(\lreg\) is chosen empirically in practice.
%------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/mult_gradients_3reg/mult_gradients_3reg}
\caption{Reflectivity sections at \(h=\SI{0}{\meter}\) (left) and velocity updates (right) obtained for three increasing (from top to bottom) values of the regularisation parameter \(\lreg=\)\numlist{1e-3;2e-3;3e-3}. In this example, positive and negative values correspond to black and white colours, respectively.}
\label{fig:mult_gradients_3reg}
\end{figure}
%------------------------