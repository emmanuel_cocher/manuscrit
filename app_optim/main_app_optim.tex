\chapter{Non-linear local optimisation}\label{app:optim}
In this appendix, we review standard non-linear optimisation methods. For an extensive review, the reader is referred to \textcite{nocedal_numerical_2006}. Non-linear optimisation is needed in \cref{chap:iter_migr} to determine a reflectivity model minimising the data misfit when multiples are included in the calculation of reconstructed data. The objective function of \gls{dso} is also minimised with non-linear optimisation to determine a background velocity model minimising defocused energy in \glspl{cig}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{General form of the algorithm}
Without loss of generality, we use in this appendix the notations of iterative migration, that is we want to find a vector \(\xi\in\Esp\) minimising a scalar objective function \(J_0\paren{\xi}\).
With gradient-based methods, the reflectivity model at iteration \(\paren{n+1}\) is updated from the reflectivity model at iteration \(\paren{n}\) following
\begin{equation}\label{eq:update_formula_app}
	\xi\itn{n+1}=\xi\itn{n}+\alpha\itn{n} d\itn{n}\text{.}
\end{equation}
where \(d\in\Esp\) is called descent direction and the positive scalar \(\alpha\) is called step size. The process is initialised with an initial guess \(\xi\itn{1}=\xi\ap{ini}\), and is stopped when a termination criterion is satisfied, for example when the value of the objective function or the norm of its gradient goes below a given threshold. In the numeric examples shown in this thesis, a maximum number of iterations \(N\) is set, so that the final result is \(\xi\itn{N+1}\).

In local optimisation methods, the value of the objective function should decrease at each iteration, that is \(J_0\paren{\xi\itn{n+1}}<J_0\paren{\xi\itn{n}}\). Then \(d\) and \(\alpha\) have to verify some conditions:
\begin{itemize}
	\item to ensure that there exists a step length \(\alpha\) allowing the decrease of \(J_0\), \(d\) should be a \emph{descent direction}, meaning that it should verify
	\begin{align}\label{eq:descent_condition}
		\pscal[\big]{d\itn{n}}{g\itn{n}}_\Esp &\leq 0\text{,}\\
		\shortintertext{where \(g\) is the gradient of \(J_0\),}
		g\itn{n}&=\pderiv{J_0}{\xi}\paren{\xi\itn{n}}\text{.}
	\end{align}
	The most obvious choice for \(d\) is the opposite of the gradient. Alternative strategies providing faster convergence are presented in \cref{sec:descent_direction}.
	\item If condition~\eqref{eq:descent_condition} is verified, very small values of \(\alpha\) ensure that the value of \(J_0\) at iteration \(\paren{n+1}\) is smaller than its value at iteration \(\paren{n}\). However to avoid performing numerous small steps, we would like to find a value for \(\alpha\itn{n}\) close to the exact minimiser of \(J_0\) along \(\xi\itn{n}+\alpha\itn{n}d\itn{n}\). An optimal \(\alpha\) value \(\alpha\itn{n}\) would lead to 
	\begin{equation}\label{eq:prop_best_alpha}
		\pscal[\big]{d\itn{n}}{g\itn{n+1}}_\Esp=0\text{.}
	\end{equation}
	Determining a good value for \(\alpha\itn{n}\) is actually a one-dimensional optimisation problem. We elaborate on \emph{linesearch} strategies addressing this problem in the next section.
\end{itemize}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Linesearch}\label{sec:linesearch}
The linesearch procedure aims at finding the minimum of \(J_0\) along the direction \(\xi\itn{n}+\alpha\itn{n} d\itn{n}\). In practice a scalar objective function \(\psi:\mathbb{R}\mapsto\mathbb{R}\) is defined for fixed values of \(\xi\itn{n}\) and \(d\itn{n}\) as
\begin{subequations}
\begin{align}
	\psi\paren{\alpha}&=J_0\paren[\big]{\xi\itn{n}+\alpha d\itn{n}}\text{,}\\
	\shortintertext{and its gradient reads}
	\psi'\paren{\alpha}&=\pscal[\bigg]{d\itn{n}}{\pderiv{J_0}{\xi}\paren[\big]{\xi\itn{n}+\alpha d\itn{n}}}_\Esp\text{.}
\end{align}
\end{subequations}
For linear problems, \(\psi\) is quadratic and the exact minimiser can easily be determined. For non-linear problems, the shape of \(\psi\) is more complex and there is not necessarily an analytic formula for the minimiser of \(\psi\). Then an inexact linesearch procedure is used to determine a \enquote{good} minimiser of \(\psi\). The objective is to find a value for \(\alpha\) ensuring the following conditions called \emph{Wolfe conditions} (\cref{fig:wolfe_conditions}):
\begin{subequations}
\begin{itemize}
	\item sufficient decrease condition (also called Armijo condition)
		\begin{equation}
			\psi\paren{\alpha} \leq \psi\paren{0} +c_1\alpha\psi'\paren{0}\text{,}
		\end{equation}
		The scalar coefficient \(c_1\) is positive, hence small values always satisfy this condition.
	\item curvature condition
		\begin{equation}
			\psi'\paren{\alpha} \geq c_2 \psi'\paren{0}\text{,}
		\end{equation}
		with \(c_2\) a positive scalar coefficient. This condition excludes too small values of \(\alpha\) which satisfy the first conditions. In the case of the \emph{strong} Wolfe conditions, a more restrictive curvature condition is chosen
		\begin{equation}
			\abs[\big]{\psi'\paren{\alpha}} \leq c_2 \abs[\big]{\psi'\paren{0}}\text{.}
		\end{equation}
		The two positive coefficients \(c_1\) and \(c_2\) should verify \(0<c_1<c_2<1\) to ensure that there exists a step length satisfying both conditions.
\end{itemize}
\end{subequations}
%--------------------------------------------
\begin{figure}[!htb]
\centering
\includegraphics{Figures/Wolfe}
{\phantomsubcaption\label{fig:wolfe_a}}
{\phantomsubcaption\label{fig:wolfe_b}}
{\phantomsubcaption\label{fig:wolfe_c}}
\caption[Illustration of the strong Wolfe conditions]{The strong Wolfe conditions for a relatively complex scalar function \(\psi\) (blue, solid): \subref{fig:wolfe_a}~sufficient decrease (or Armijo) condition; \subref{fig:wolfe_b}~strong curvature condition; \subref{fig:wolfe_c}~combination of the sufficient decrease condition and the strong curvature condition. The coloured areas show the range of acceptable step length values.}
\label{fig:wolfe_conditions}
\end{figure}
%--------------------------------------------

The values of the coefficient \(c_1\) and \(c_2\) and the choice of the regular or strong curvature conditions depend on the descent direction strategy (detailed in the next section. For non-linear conjugate gradients methods, the strong Wolfe conditions are required and recommended parameters are \(c_1=\num{1e-4}\) and \(c_2=\num{0.1}\). For quasi-Newton methods, the regular curvature condition is sufficient with \(c_2=\num{0.9}\) \parencite{nocedal_numerical_2006}.

Algorithms ensuring the strong Wolfe conditions are relatively expensive because the value and the gradient of the objective function should be calculated at each trial step size. Therefore the choice of successive trial steps should be efficient. Efficient strategies usually consist of two steps. First an interval containing an acceptable step size is determined. Then this interval is reduced iteratively until an acceptable step size is found. For our implementation, we have chosen the strategy of \textcite{more_line_1994} as recommended by \textcite[162]{nocedal_numerical_2006}, using polynomial interpolations to compute the successive step size.%In practice no more than \num{2} or \num{3} trials are necessary to find an acceptable step length.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Descent direction}\label{sec:descent_direction}
We review classic strategies used to determine a descent direction.% A numerical comparison will be presented in \cref{sec:mult_ex3_hole}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\minisec{Steepest Descent}
The steepest descent is the simplest strategy. The descent direction is simply the opposite of the gradient:
\begin{equation}
	d\itn{n}=-g\itn{n}\text{.}
\end{equation}
It obviously satisfies the descent condition~\eqref{eq:descent_condition}. It is a natural choice because it is the direction providing the maximum decrease of \(J_0\) in the neighbourhood of \(\xi\itn{n}\). However other strategies provide faster decrease of the objective function.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\minisec{Non-linear conjugate-gradient}
The conjugate-gradient algorithm originally designed for linear problems can be extended to non-linear optimisation. The descent direction is based on the steepest descent but an additional term takes into account the value of the gradient obtained in the previous iterations,
\begin{equation}
	d\itn{n}=-g\itn{n}+\beta\itn{n}d\itn{n-1}\text{.}
\end{equation}
Many formulas have been proposed for the definition of \(\beta\) \parencite{hager_survey_2006}. The most popular are
\begin{subequations}
\begin{align}
	\beta\ped{FR}\itn{n}&
	=\frac{\pscal[\big]{g\itn{n}}{g\itn{n}}_\Esp}{\pscal[\big]{g\itn{n-1}}{g\itn{n-1}}_\Esp}
	\quad \text{\parencite{fletcher_function_1964}}\\\shortintertext{and}
	\beta\ped{PR}\itn{n}&
	=\frac{\pscal[\big]{g\itn{n}}{g\itn{n}-g\itn{n-1}}_\Esp}{\pscal[\big]{g\itn{n-1}}{g\itn{n-1}}_\Esp}
	\quad \text{\parencite{polak_note_1969}.}
	%\\
	%\beta\ped{HS}\itn{n}&=\frac{\pscal[\big]{g\itn{n}}{g\itn{n}-g\itn{n-1}}_\Esp}{\pscal[\big]{g\itn{n}-g\itn{n-1}}{d\itn{n-1}}_\Esp} \quad \text{\parencite{hestenes_methods_1952}}
\end{align}
\end{subequations}
The Fletcher-Reeves formula is actually the one implemented in the linear version of the algorithm. In the linear case, successive gradients are orthogonal and the Polak-Ribière formula is equivalent to the Fletcher-Reeves one. 

The descent condition~\eqref{eq:descent_condition} reads
\begin{equation}
	\pscal[\big]{d\itn{n}}{g\itn{n}}_\Esp
	=-\norm[\big]{g\itn{n}}^2_\Esp
	+\beta\itn{n}\pscal[\big]{d\itn{n-1}}{g\itn{n}}_\Esp\text{.}
\end{equation}
Then an exact linesearch automatically ensures that \(d\itn{n}\) is a descent direction \pcref{eq:prop_best_alpha}. More generally a step length ensuring the strong Wolfe conditions is sufficient \parencite[122]{nocedal_numerical_2006}.
The Polak-Ribière formula is the one providing the faster convergence in most cases \parencite[131]{nocedal_numerical_2006}. It is the one used in this study.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\minisec{Newton method}\label{sec:Newton_method}
Newton strategies are based on a second-order Taylor expansion of the objective function
\begin{equation}\label{eq:J0_second_order_expansion}
	J_0\paren{\xi\itn{n}+\alpha\itn{n}d\itn{n}}
	=J_0\paren{\xi\itn{n}}
	+\alpha\itn{n}\pscal[\big]{d\itn{n}}{g\itn{n}}_\Esp
	+\frac{1}{2}\paren[\big]{\alpha\itn{n}}^2\pscal[\big]{d\itn{n}}{H\itn{n}d\itn{n}}_\Esp\text{,}
\end{equation}
where we have noted \(H\) the Hessian matrix of \(J_0\),
\begin{equation}\label{eq:def_Hessian_matrix}
	H\itn{n}\paren{\xi\itn{n}}=\pderiv[2]{J_0}{\xi}\paren{\xi\itn{n}}\text{.}
\end{equation}
Deriving \cref{eq:J0_second_order_expansion} with respect to \(d\itn{n}\) leads to the \emph{Newton equation}
\begin{equation}\label{eq:linear_system_newton}
	H\itn{n}d\itn{n}=-g\itn{n}\text{.}
\end{equation}
The Newton direction \(d\itn{n}\) is the solution of this equation, which reads, provided that the Hessian matrix is invertible
\begin{equation}
	d\itn{n}=-\paren[\big]{H\itn{n}}^{-1} g\itn{n}\text{.}
\end{equation}
The descent condition \cref{eq:descent_condition} reads
\begin{equation}
	\pscal{d\itn{n}}{g\itn{n}}_\Esp=-\pscal[\Big]{g\itn{n}}{\paren[\big]{H\itn{n}}^{-1}g\itn{n}}\text{,}
\end{equation}
therefore if \(H\) is positive definite, the Newton direction is a descent direction. If it is not positive definite, it may not be invertible and may not define a descent direction. %The positive definitiveness of the Hessian of \(J_0\) will be examined in \cref{sec:Hess_J0}.

\minisec{Quasi-Newton methods}
The use of a Newton descent requires the computation of the Hessian matrix and its inversion, which are both computer expensive procedures. \emph{Quasi-Newton} methods are designed to alleviate this difficulty. They use an approximation of the inverse of the Hessian \(B\itn{n}\approx \paren[\big]{H\itn{n}}^{-1}\), and the quasi-Newton direction reads
\begin{equation}
	d\itn{n}=-B\itn{n} g\itn{n}\text{.}
\end{equation}
The steepest descent can actually be interpreted as a quasi-Newton method with the Hessian approximated with the identity operator. A popular choice for the definition of \(B\) uses the BFGS formula (named after Broyden, Fletcher, Goldfarb and Shanno) \parencite[136]{nocedal_numerical_2006}. Starting with the identity, the approximation of \(H^{-1}\) is improved iteratively at each iteration
\begin{equation}
	B\itn{n+1}=\paren[\big]{I - \rho\itn{n}s\itn{n}{y\itn{n}}^T}H\itn{n}\paren[\big]{I - \rho\itn{n}y\itn{n}{s\itn{n}}^T}+ \rho\itn{n}s\itn{n}{s\itn{n}}^T\text{,}
\end{equation}
with \(\rho\itn{k}=\frac{1}{\pscal{y\itn{n}}{s\itn{n}}_\Esp}\).
A difficulty is that the huge matrix \(B\itn{n}\) has to be kept in memory.	
This difficulty can be overcome with the \emph{limited-memory} BFGS (l-BFGS) technique \parencite{nocedal_updating_1980}, which computes the value of \(B\itn{n}\) from the values of \(\xi\itn{n}\) and \(g\itn{n}\) at the \(m\) preceding iterations (where \(m\) typically ranges between \numlist{3;20}). With this method, there is no need to compute nor to store \(H\itn{n}\).

An alternative strategy called the truncated Newton method \parencite[168]{nocedal_numerical_2006} aims at resolving the linear system~\eqref{eq:linear_system_newton} iteratively with the classical linear conjugate gradient algorithm. The method needs only an efficient way of computing the application of the Hessian to a vector of \(\Esp\) (matrix-vector product) and there is no storage requirements. See \textcite{metivier_full_2013,metivier_full_2014} for applications of the truncated Newton method in the framework of Full Waveform Inversion.
