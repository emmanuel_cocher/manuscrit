\chapter[head={Computation of the adjoint of \,\(\Fdag\)},tocentry={Computation of the adjoint of the approximate inverse of the extended Born modelling operator}]{Computation of the adjoint of the approximate inverse of the extended Born modelling operator}\label{app:Fdag_adjoint}
In \cref{sec:article_Herve}, we have proposed a formula for an approximate inverse of the extended Born modelling operator \({F:\Esp\mapsto\Dobs}\).
In \cref{sec:weights_iterative}, this approximate inverse, noted \({\Fdag:\Dobs\mapsto\Esp}\), is used as a preconditioner to improve the convergence rate of the iterative migration process. The derivation involves the adjoint of the approximate inverse, noted here \({\FdagT:\Esp\mapsto\Dobs}\). In this appendix, we derive an expression for \(\FdagT\), using the adjoint-state technique. We begin by reviewing the implementation of the approximate inverse \(\Fdag\).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Equations for the forward map}\label{sec:Fdag}
The application of the approximate inverse \(\Fdag\) to a vector \(P\) of \(\Dobs\) produces a vector \(\xi\ap{inv}\) of \(\Esp\). The implementation is very similar to the case of the adjoint \(\Fadj\) described in \cref{sec:Grad_J0}. It consists of computing modified source and receiver wavefields, noted here \(\Wg{P_0}\) and  \(\Wg{\lambda_1}\) respectively, by analogy with the notations of \cref{sec:Grad_J0}. The application of the approximate inverse to \(P\) is obtained by solving the following equations,
\begin{subequations}\label{eq:def_Fdag}\label[pluralequation]{eqs:def_Fdag}
\begin{empheq}[left=\empheqlbrace]{align}
	\Wop \Wg{P_0}&= \Wg{S}\text{,}\\
	\WopA \Wg{\lambda_1}&=\Wr P\text{,}\\
	\xi_0 &=\WQxi{\Wg{P_0}}{\Wg{\lambda_1}}\text{,}\\
	\xiinv&=\Wmod \xi_0\text{,}
\end{empheq}
\end{subequations}
where we defined
\begin{itemize}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\item the modified extended cross-correlation operator \(\WQxi{u}{v}:\Dsp\times\Dsp\mapsto\Esp\), defined for \(\paren{u,v}\in\Dsp\) as
	\begin{equation}\label{eq:def_WQxi}
		\croch[\big]{\WQxi{u}{v}}\xh=
		\int_s\int_\omega\frac{1}{\iom}
		u^*\paren{s,\vec{x}-h,\omega}v\paren{s,\vec{x}+h,\omega}
		\diff \omega \diff s\text{.}
	\end{equation}
	It is similar to the usual operator \(\Qxi{u}{v}\) \pcref{eq:def_Qxi}, except that a first order integration replaces the second order derivative;
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
	\item the weighting operator \(\Wmod:\Esp\mapsto\Esp\), which consists of a derivative with respect to \(z\) and a multiplication by a power \(\alpha\) of \(c_0\),
		\begin{equation}\label{eq:def_Wmod}
			\croch[\big]{\Wmod \xi\xh}\xh = -\frac{k}{c_0^\alpha\vecx}\pderiv{}{z}\croch[\Big]{\xi\xh}\text{,}
		\end{equation}
		If \(\xi\) is defined as a velocity perturbation \(\xi=\delta c/\paren{2c_0}\), then \(k=8\) and \(\alpha=0\); if \(\xi\) is defined as a squared slowness perturbation \(\xi=2\delta c/c_0^3\), then \(k=32\) and \(\alpha=2\).\par
		The vertical derivative in this weight can be interpreted under the high-frequency approximation as a multiplication by the cosine of the half-opening angle and the cosine of the dip angle at the image point (\cref{fig:weights_inverse}),
		\begin{equation}
			\frac{\ioma}{c_0\vecx}\cos\theta\cos\phi\text{;}
		\end{equation}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\item the weighting operator \(\Wr:\Dobs\mapsto\Dsp\), which in practice constructs a dipole source term around the depth \(z_r\) of the receivers. Its practical implementation is
		\begin{equation}\label{eq:def_Wr}
			\croch[\big]{\Wr P\paren{s,r,\omega}}\paren[\big]{s,\vec{x}=\paren{x,z},\omega}=
				\begin{dcases*}
					-\frac{1}{2\Delta z} P\paren{s,r,\omega}& if \(z=z_r-1\),\\
					+\frac{1}{2\Delta z} P\paren{s,r,\omega}& if \(z=z_r+1\),\\
					0& elsewhere,
				\end{dcases*}
		\end{equation}
		with \(\Delta z\) the size of a grid point in the \(z\) direction and \(z_r\) the position of the receivers. It can be interpreted as a multiplication by the cosine of the take-off angle at the receiver position (\cref{fig:weights_inverse}),
		\begin{equation}
			\frac{\ioma}{c_0\paren{r}}\cos\beta_r\text{;}
		\end{equation}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\item Finally, \(\Wg{S}\) is a modified source term, which involves a deconvolved version \(\waveletDcnv\) of the source wavelet \(\wavelet\) and an other weighting operator \(\Ws\) defined in the same way as \(\Wr\) for the source depth \(z_s\)
	\begin{equation}\label{eq:def_Sdcnv}
		\Wg{S}=\Ws\croch[\big]{\deltaxs\waveletDcnv}\text{.}
	\end{equation}
	\(\Ws\) can be interpreted as a multiplication by the cosine of the take-off angle at the source position (\cref{fig:weights_inverse}),
		\begin{equation}
			\frac{\ioma}{c_0\paren{s}}\cos\beta_s\text{.}
		\end{equation}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{itemize}
%------------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/weights_inverse}
\caption{Angles involved in the interpretation of the weights defining the inverse operator~\(\Fdag\).}
\label{fig:weights_inverse}
\end{figure}
%------------------------------------

\section{Equations for the adjoint map}
We want to compute the adjoint \(\FdagT\) of the operator \(\Fdag\). It is applied to a vector \(\chi\) of \(\Esp\) and computes a vector \(Q=\FdagT\chi\) in \(\Dobs\). We use the adjoint-state method and define a scalar objective function
\begin{equation}
	\Gamma\paren{\chi,P}=\pscal{\chi}{\Fdag P}_\Esp\text{,}
\end{equation}
such that
\begin{equation}
	\pderiv{\Gamma}{P}\paren{\chi,P}=\FdagT\chi\text{.}
\end{equation}
The state equations are given by \cref{eqs:def_Fdag}. We note \(\xiinv=\Fdag P\) and define the Lagrangian with the adjoint states \(\eta\), \(\eta_0\), \(\Wg{\mu_0}\) and \(\Wg{\nu_1}\),
\begin{equation}
	\begin{split}
	\Lagr{\Gamma}=\pscal{\chi}{\xiinv}_\Esp
	&-\pscal{\eta}{\xiinv-\Wmod \xi_0}_\Esp\\
	&-\pscal{\eta_0}{\xi_0-\WQxi{\Wg{P_0}}{\Wg{\lambda_1}}}_\Esp\\
	&-\pscal{\Wg{\mu_0}}{\Wop\Wg{P_0}-\Wg{S}}_\Dsp\\
	&-\pscal{\Wg{\nu_1}}{\WopA\Wg{\lambda_1}-\Wr P}_\Dsp
	\end{split}
\end{equation}
Deriving the Lagrangian with respect to the state variables \(\paren{\xiinv,\xi_0,\ovl{P_0},\ovl{\lambda_1}}\) gives the adjoint equations
\begin{subequations}\label{eq:def_FdagT}\label[pluralequation]{eqs:def_FdagT}
\begin{empheq}[left=\empheqlbrace]{align}
	\eta&=\chi\text{,}\\
	\eta_0&=\Wmod^*\eta\text{,}\\
	\WopA\Wg{\mu_0}&=\WKKxi{\Wg{\lambda_1}}{\eta_0}\text{,}\\
	\WopA\Wg{\nu_1}&=\WKxi{\Wg{P_0}}{\eta_0}\text{,}
\end{empheq}
\end{subequations}
and the derivatives of \(\Lagr{\Gamma}\) with respect to \(P\) gives the application of \(\FdagT\) to \(\chi\),
\begin{equation}
	\FdagT\chi=\pderiv{\Lagr{\Gamma}}{P}=\Wr^*\Wg\nu_1\text{.}
\end{equation}
These equations involve the adjoint of the weighting operators defined in the previous section,
\begin{itemize}
	\item the operators \(\WKxi{P}{\xi}\) and \(\WKKxi{P}{\xi}:\Dsp\times\Esp\mapsto\Dsp\), defined for \(P\in\Dsp\) and \(\xi\in\Esp\) as
	\begin{subequations}
	\begin{align}
		\WKxi{P}{\xi}\sxom&=\int_h \frac{1}{\iom} P\paren{s,\vec{x}-2h,\omega}\xi\paren{\vec{x}-h,h} \diff h\text{,} \label{eq:def_WKxi} \\
		\WKKxi{P}{\xi}\sxom&=\int_h \frac{1}{\iom} P\paren{s,\vec{x}+2h,\omega}\xi\paren{\vec{x}+h,h} \diff h\text{.} \label{eq:def_WKKxi}
	\end{align}
	\end{subequations}
	Note that the integration operator is causal in \cref{eq:def_WKxi} and anti-causal in \cref{eq:def_WKKxi} \parencite{claerbout_geophysical_2014}. They are similar to operators \(K^{-}\) and \(K^{+}\) defined in \cref{eq:def_Kxi,eq:def_KKxi}, except that the time integration replaces the second order time derivative;
	\item the adjoint \(\Wmod^* :\Esp\mapsto\Esp\) of \(\Wmod\), defined as
		\begin{equation}\label{eq:def_WmodAdj}
			\croch[\big]{\Wmod^* \xi\xh}\xh = 
			k\pderiv{}{z}\croch[\bigg]{\frac{1}{c_0^\alpha\vecx}\xi\xh}\text{,}
		\end{equation}
	\item and the adjoint \(\Wr^*:\Dsp\mapsto\Dobs\) of \(\Wr\), defined as
		\begin{equation}
			\croch[\big]{\Wr^* P\sxom}\paren{s,r,\omega}=
				\frac{1}{2\Delta z}
				\croch[\big]{P\paren{s,\paren{x,z_r+1},\omega}
				-P\paren{s,\paren{x,z_r-1},\omega}}\text{.}
		\end{equation}
\end{itemize}

Note that only the value of \(\Wg{\nu_1}\) is required and \(\Wg{\mu_0}\) does not need to be computed. If we remove the weighting operators, that is  \(\Wmod=I_\Esp\), \(\Wr^*=M\), \(\Wg{K^{-}}=K^{-}\) and \(\waveletDcnv=\wavelet\), then \cref{eqs:def_FdagT} are equivalent to the usual first-order extended Born modelling. It means that in terms of implementation, \(\FdagT\) and \(F\) have the same structure.
