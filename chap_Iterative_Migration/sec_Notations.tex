\section{Notations}
We consider a surface acquisition and a 2D Earth model where a subsurface point is noted \(\vec{x}=\paren{x,z}\). We define
%---------------------------------------
\begin{itemize}
	\item the \textbf{model space} \(\Msp\) of physical model parametrised by \(\vec{x}\). For example the background velocity model \(c_0\vecx\);
	\item the \textbf{extended model space} \(\Esp\) of variables function of the subsurface point \(\vec{x}\) and the subsurface offset considered strictly horizontal \(\vec{h}=\paren{h,0}\), for example the extended reflectivity \(\xi\xh\);
	\item the \textbf{observed data space} \(\Dobs\) parametrised by the source horizontal coordinate \(s\), the receiver horizontal coordinate \(r\) and the angular frequency \(\omega\) (or time \(t\)) in the frequency domain formulation. Observed data are noted \(\Pobs\srom\). Although we use the time domain for the implementation, equations will be written in the frequency domain in the following to simplify calculations;
	\item the \textbf{data space} \(\Dsp\). We note \(P\croch{c_0,\xi}\sxom\) the wavefield calculated with the estimated reflectivity model \(\xi\). Contrary to \(\Pobs\), \(P\) is known everywhere in the subsurface. We define the operator \(M:\Dsp\mapsto\Dobs\) which, applied to a simulated wavefield \(P\croch{c_0,\xi}\sxom\), selects its values at receivers' locations. Hence \(M P\) can be compared to \(\Pobs\) during iterative migration.
\end{itemize}
%----------------------------------------
\Cref{tab:spaces} summarises the notations used for the different spaces and their size. For more details on the choice of the data and model spaces, the reader is referred to \textcite{blazek_mathematical_2013,symes_seismic_2014}. %In order to reduce the requirements in computation cost and memory, data is actually not calculated everywhere in the subsurface but within a fixed distance around the source that includes at least all the receivers.
Note that with the introduction of the subsurface offset, the extended model \(\Esp\) and the observed data space \(\Dobs\) have the same dimension, which allows to alternate between these two spaces without losing information.
%----------------------------------------
\begin{table}[!htbp]
\centering
\begin{tabular}{rcl} \toprule
space                & notation & size \\ \midrule
model space          & \(\Msp\) & \(n_z \times n_x\)\\ \midrule
extended model space & \(\Esp\) & \(n_z \times n_x \times n_h\) \\ \midrule
observed data space  & \(\Dobs\)& \(n_t \times n_s \times n_r\) \\ \midrule
data space           & \(\Dsp\) & \(n_t \times n_s \times n_z \times n_x\)\\ \bottomrule
\end{tabular}
\caption[The different spaces involved and size of their elements.]{Data and model spaces involved in this study and size of their elements. \(n_z\), \(n_x\) and \(n_h\) are the number of grid points considered in the extended model space. \(n_t\) is the number of time samples. \(n_s\) and \(n_r\) are the number of sources and receivers considered in the acquisition. In the data space, \(n_x\) can be restricted to a zone around the shot gather including at least all the receivers.}
\label{tab:spaces}
\end{table}
%----------------------------------------


The inner product of two vectors \(\xi\) and \(\chi\) of \(\Esp\) is noted
\begin{equation}
	\pscal{\xi}{\chi}_\Esp=\int_\mathbf{x}\int_h \xi\xh\chi\xh \diff h \diff \vec{x}\text{,}
\end{equation}
and the associated norm is \(\norm{\xi}_\Esp=\sqrt{\pscal{\xi}{\xi}_\Esp}\).
%
Similarly we define the inner-product \(\pscal{}{}_\Dsp\) and the norm \(\norm{\,\cdot\,}_\Dsp\) in the data space.
The norm of the data residuals \(R\), defined as the difference between observed data and the value of simulated data at receivers' positions,
\begin{equation}
	R\croch{c_0,\xi}\srom=\croch[\Big]{MP\croch{c_0,\xi}}\srom-\Pobs\srom\text{,}
\end{equation}
is given by
\begin{equation}
\norm{R}_{\Dobs}=\sqrt{\int_s\int_r\int_\omega \abs[\big]{R\paren{s,r,\omega}}^2 \diff \omega \diff r \diff s}\text{.}
\end{equation}
Finally we call \(F^*\) and \(F^{-1}\) the adjoint and inverse operators of an operator \(F\).