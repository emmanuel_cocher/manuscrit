\section{Modelling under the second-order Born approximation}
We detail in this section how primaries and multiples are modelled with a second-order Born approximation in the extended domain. We first review the usual Born approximation for primary reflections in the non-extended case.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{First-order Born approximation}\label{sec:Born_first_order}
In the constant-density acoustic approximation, wavefield propagation obeys the following wave-equation
\begin{equation}\label{eq:wave_equation}
	\frac{\iomsqr}{c\paren{\vec{x}}^2}P\sxom - \Lapl P\sxom = \wavelet\vecom\deltaxs\text{,}
\end{equation}
for a source located at \(\vec{s}=\paren{s,0}\) with a wavelet \(\wavelet\vecom\). The velocity model is noted \(c\paren{\vec{x}}\).

The Born approximation is a linearisation of this wave-equation and relies on the scale separation presented in the introduction. We write
\begin{equation}
	c\paren{\vec{x}}=c_0\paren{\vec{x}}+\deltac\paren{\vec{x}}\text{,}
\end{equation}
where \(c_0\) and \(\delta c\) stands for the background velocity model and the model perturbation respectively. Accordingly we separate the data \(P\) into \(P=P_0+\delta P\). \(P_0\) corresponds to the data modelled in the smooth background \(c_0\) and \(\delta P\) is the perturbation caused by the model perturbation \(\delta c\).

We suppose that the model perturbation is small with respect to the background velocity model, that is \(\delta c \ll c_0\). Neglecting second-order term \(\delta P \delta c\), \(P_0\) and \(\delta P\) are solution of
\begin{subequations}\label{eq:born_wave-equation}
\begin{empheq}[left=\empheqlbrace]{align}
	\Wop P_0\sxom &= \wavelet\vecom \delta\paren{\vec{x}-s}\text{,}\label{eq:we_P0}\\
	\Wop \delta P\sxom &= \iomsqr\xi\vecx P_0\sxom\text{,}\label{eq:we_P1}
\end{empheq}
\end{subequations}
%\mymodif{%
where we have defined
\begin{itemize}
	\item the wave operator \(\Wop\) as
		\begin{equation}
			\Wop=\frac{\iomsqr}{c_0^2\paren{\vec{x}}}-\Lapl\text{.}
		\end{equation}
		Note that  \(\Wop\) is defined with the smooth background velocity \(c_0\paren{\vec{x}}\);
	\item the reflectivity \(\xi\vecx\) as
		\begin{equation}\label{eq:def_xi_nonextended}
			\xi\vecx=\frac{2\delta c\vecx}{c_0^3\vecx}\text{.}
		\end{equation}
		The coefficient \(2/c_0^3\vecx\) is included in the definition of \(\xi\vecx\) to further simplify calculations, although the physical reflectivity property is described by the dimensionless quantity \(\xi\vecx=\delta c\vecx/\paren{2c_0\vecx}\). Note that the reflectivity defined in \cref{eq:def_xi_nonextended} is not extended yet. The case of the extended model will be presented in \cref{sec:introduction_depth_offset}.
\end{itemize}%
%}
The data perturbation \(\delta P\) corresponds to the reflected wave due to the reflectivity \(\xi\). In all the following, it will be noted \(P_1\).

The solutions of \cref{eq:born_wave-equation} involve Green's functions \(G_0\) defined as the solution of the wave-equation with an impulsive source
\begin{equation}
	\Wop G_0\paren{s,\vec{x},\omega}=\delta\paren{\vec{x}-s}\text{.}
\end{equation}
Then one can verify that
\begin{subequations}\label{eq:Born_modelling_Prim_without_h}
\begin{empheq}[left=\empheqlbrace]{align}
	P_0\sxom &=G_0\sxom \wavelet\vecom\text{,}\label{eq:def_P0_nonextended}\\
	P_1\sxom &=\wavelet\vecom \int_y \iomsqr G_0\paren{\vec{s},\vec{y},\omega} \xi\paren{\vec{y}} G_0\paren{\vec{y},\vec{x},\omega} \diff \vec{y}\text{,}\label{eq:def_P1_nonextended}
\end{empheq}
\end{subequations}
are solutions of \cref{eq:we_P0,eq:we_P1}, respectively.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Reflection at the free surface}\label{sec:free_surface}
To consider first-order surface-related multiples, we need to model the reflection of upgoing primaries at the surface. The latter is known to act as a mirror and a first guess is to model multiples by applying the primary modelling workflow to a new areal source made of primaries recorded at the surface multiplied by a constant coefficient \(R=\num{-1}\). Numerical comparison with a finite differences modelling and a free-surface condition suggests that an additional coefficient \(2\ioma/c_0\) should be applied as well. We explain the physical meaning of this coefficient on a pure 1D case in \cref{app:free_surface} and assume that it is valid in the general 2D case, too.

In the following, we note \(P_2\) the wavefield resulting from the reflection of the primaries \(P_1\) at the surface. The associated wave-equation is
\begin{equation}\label{eq:def_P2_nonextended}
	\Wop P_2\paren{\vec{s},\vec{x},\omega} = \Ms P_1\paren{\vec{s},\vec{x},\omega}\text{,}
\end{equation}
where operator \(M_s:\Dsp\mapsto\Dsp\) is defined by
\begin{equation}\label{eq:def_Ms}
	\Ms P_1\paren{\vec{s},\vec{x},\omega} =%
	\paren{-1} \delta\paren{\vec{x}-\vec{x}\ped{surf}}\frac{2\ioma}{c_0\paren{\vec{x}}}%
	P_1\paren{\vec{s},\vec{x},\omega}\text{,}
\end{equation}
and includes the additional reflection coefficient discussed in \cref{app:free_surface}.

We test the accuracy of this approximation, in particular at non-zero offset, with the following numerical example. We consider a Ricker with a maximum frequency of \SI{40}{\hertz} as the source wavelet (\cref{fig:ricker}). The source and the receivers are buried in the subsurface assumed homogeneous with a velocity of \SI{3000}{\meter\per\second} (\cref{fig:born_free_surf_model}).
We first simulate the propagation of the source with a classical second-order accurate space and time finite-difference scheme. Absorbing boundary conditions with \gls{pml} \parencite{berenger_perfectly_1994,komatitsch_unsplit_2007} are implemented on each edge of the model except on the surface where a free-surface condition is implemented with the method of images \parencite{levander_fourthorder_1988,robertsson_numerical_1996} (\cref{fig:born_free_surf_data}, left).
Then we use the same code with \glspl{pml} on each edge of the model to solve \cref{eq:def_P0_nonextended,eq:def_P2_nonextended} (\cref{fig:born_free_surf_data}, middle).
The difference between the two results increases with offset (\cref{fig:born_free_surf_data}, right). The amplitudes accuracy decays with offsets (\cref{fig:born_free_surf_traces}) as the reflection coefficient \(\paren{-1}\) at the surface is set independent of the incident angle, but more importantly the traveltime remains accurate. Thus \cref{eq:def_P2_nonextended} stands as a good approximation to model the surface reflection.
%----------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/ricker/ricker}
\caption{The Ricker wavelet with maximum frequency of \SI{40}{\hertz}.}
\label{fig:ricker}
\end{figure}
%----------------------------
%----------------------------
\begin{figure}[!htbp]
  %%%
  \begin{subfigure}[t]{0.49\linewidth}
    \includegraphics[]{Figures/born_free_surf/born_free_surf_modele}
    \caption{Position of the source (black dot) and the receivers (along the dashed lined).}
    \label{fig:born_free_surf_model}
  \end{subfigure}\hfill%
   \begin{subfigure}[t]{0.49\linewidth}
    \includegraphics[]{Figures/born_free_surf/born_free_surf_traces}
    \caption{Traces extracted at the position indicated by the dashed lines in~\subref{fig:born_free_surf_data}}
    \label{fig:born_free_surf_traces}
  \end{subfigure}
  %%%
   \begin{subfigure}[b]{\linewidth}
    \includegraphics[]{Figures/born_free_surf/born_free_surf}
    \caption{Data recorded at receiver positions when the modelling is performed with finite differences (left) and with the Born approximation (centre). The right panel display their difference. The same colour scale is used for the three plots.}
    \label{fig:born_free_surf_data}
  \end{subfigure}
  %%%
%   \begin{subfigure}[b]{0.99\linewidth}
%    \includegraphics[]{Figures/born_free_surf/born_free_surf_angles_imagesc}
%  \end{subfigure}
%  %%%
%  \begin{subfigure}[b]{0.99\linewidth}
%    \includegraphics[]{Figures/born_free_surf/born_free_surf_angles_correl}
%  \end{subfigure}
%  %%%
\caption{Accuracy of the second-order Born approximation with respect to the free-surface reflection.}% Using buried source and receivers (a), data are modelled first with a free-surface condition (b), and then with the Born approximation (c). Both plots and their difference (d) are represented with the same colorscale. Three traces (e,f,g) are extracted at the positions indicated by the dahsed lines on (b).}
\label{fig:born_free_surf}
\end{figure}
%----------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Second-order Born approximation} 
First-order surface multiples \(P_3\) are modelled with the same equation as primary reflections \(P_1\) \pcref{eq:def_P1_nonextended}, except that the incident wavefield is \(P_2\) instead of \(P_0\). Then, given a background velocity model \(c_0\paren{\vec{x}}\) and a (non-extended) reflectivity model \(\xi\paren{\vec{x}}\), data modelling is performed by solving four wave-equations
%\begin{subequations}\label{eq:modelling_non_ext}
\begin{subequations}\label[pluralequation]{eqs:modelling_non_extended}
\begin{empheq}[left=\empheqlbrace]{align}
	\Wop P_0\sxom &= \wavelet\vecom \deltaxs\text{,}\\
	\Wop P_1\sxom &= \iomsqr\xi\vecx P_0\sxom\text{,}\\
	\Wop P_2\sxom &= \Ms P_1\sxom\\
	\Wop P_3\sxom &= \iomsqr\xi\vecx P_2\sxom\text{,}
\end{empheq}
\end{subequations}
%\end{subequations}
where \(\Ms\) is defined in \cref{eq:def_Ms}.

\minisec{Comparison with a finite-difference modelling and a free-surface condition}
We have tested the validity of the Born approximation with respect to the free surface.
In theory, the Born approximation is valid for small velocity contrasts. We want to study its behaviour in the case of high contrasts which may cause strong multiples.
We compare data modelling with the second-order Born approximation described by \cref{eqs:modelling_non_extended} with a finite-difference modelling and a free-surface condition on four examples.
We consider a background velocity of \SI{1500}{\meter\per\second} and two values for the velocity perturbation: \SIlist{1600;2500}{\meter\per\second}, corresponding to a limited (\(100/\paren{1500+1600}=\num{0.03}\)) and a larger (\(1000/\paren{1500+1600}=\num{0.25}\)) velocity contrast. For each value, we consider two cases
\begin{itemize}[nolistsep]
	\item a horizontal reflective layer with homogeneous velocity below the interface located at \(z_0=\SI{200}{\meter}\) (\cref{fig:born_mult_refl_1600,fig:born_mult_refl_2500}). In this case, the background velocity model used for second-order Born modelling is obtained by smoothing the exact slowness model \(\sigma\vecx\) defined as \(\sigma\vecx=1/c_0\vecx\) and the velocity perturbation is calculated as \(\);
		\item a horizontal \enquote{diffractive interface}, with a velocity perturbation localised at depth \(z_0\) and a homogeneous velocity (\SI{1500}{\meter\per\second}) below and above the interface (\cref{fig:born_mult_difr_1600,fig:born_mult_difr_2500}).
\end{itemize}

The source and the receivers are located near the surface. Ghosts at the source and the receivers sides are created by the free-surface in the finite-difference simulation. This is accounted for in the Born modelling by a time shift in the source wavelet and in the computed data
\begin{subequations}
	\begin{empheq}[left=\empheqlbrace]{align}
		\Omega\ped{ghost}\paren{t}&=\Omega\paren{t}-\Omega\paren[\Big]{t+2\frac{z_s}{c_0}}\text{,}\\
		P\ped{ghost}\paren{s,r,t}&=P\paren{t}-P\paren[\Big]{t+2\frac{z_r}{c_0}}\text{.}
	\end{empheq}
\end{subequations}
Once more, this approximation is valid for vertical wave propagation only.
%----------------------------
\begin{figure}[!htbp]
  %%%
   \begin{subfigure}[b]{\linewidth}
    \includegraphics[]{Figures/born_mult_refl/born_mult_refl_1600}
    \caption{Data recorded at receiver positions when the modelling is performed with finite differences (left) and with a free-surface condition and a second-order Born approximation (centre). The right panel display their difference. The same colour scale is used for the three plots.}
    \label{fig:born_mult_refl_1600_data}
  \end{subfigure}
  \begin{subfigure}[t]{0.39\linewidth}
    \includegraphics[]{Figures/born_mult_refl/born_mult_refl_modeles_1600}
    \caption{1D-models used for finite-differences and Born modelling.}
    \label{fig:born_mult_refl_1600_model}
  \end{subfigure}\hfill%
  \begin{subfigure}[t]{0.60\linewidth}
    \includegraphics[]{Figures/born_mult_refl/born_mult_refl_traces_1600}
    \caption{Traces extracted at the positions indicated by the dashed lines in~\subref{fig:born_mult_refl_1600_data}. The bottom row is a zoom of the top row allowing to compare multiples modelled with both approaches.}
    \label{fig:born_mult_refl_1600_traces}
  \end{subfigure}
  %%%
\caption[Accuracy of the second order Born approximation in the reflective case with a small velocity contrast]{Comparison of finite-differences modelling and free-surface modelling with a second-order Born approximation in the reflective case.}
\label{fig:born_mult_refl_1600}
\end{figure}
%----------------------------
\begin{figure}[!htbp]
  %%%
   \begin{subfigure}[b]{\linewidth}
    \includegraphics[]{Figures/born_mult_refl/born_mult_refl_2500}
    \caption{Data recorded at receiver positions when the modelling is performed with finite differences (left) and with a free-surface condition and a second-order Born approximation (centre). The right panel display their difference. The same colour scale is used for the three plots.}
    \label{fig:born_mult_refl_2500_data}
  \end{subfigure}
  %%%
  \begin{subfigure}[t]{0.39\linewidth}
    \includegraphics[]{Figures/born_mult_refl/born_mult_refl_modeles_2500}
    \caption{1D-models used for finite-differences and Born modelling.}
    \label{fig:born_mult_refl_2500_model}
  \end{subfigure}\hfill%
  \begin{subfigure}[t]{0.60\linewidth}
    \includegraphics[]{Figures/born_mult_refl/born_mult_refl_traces_2500}
    \caption{Traces extracted at the positions indicated by the dashed lines in~\subref{fig:born_mult_refl_2500_data}.}
    \label{fig:born_mult_refl_2500_traces}
  \end{subfigure}
  %%%
\caption[Accuracy of the second order Born approximation in the reflective case with a high velocity contrast]{Same as \cref{fig:born_mult_refl_1600}, but with a stronger velocity contrast.}
\label{fig:born_mult_refl_2500}
\end{figure}
%----------------------------
%----------------------------
\begin{figure}[!htbp]
 %%%
   \begin{subfigure}[b]{\linewidth}
    \includegraphics[]{Figures/born_mult_difr/born_mult_difr_1600}
    \caption{Data recorded at receiver positions when the modelling is performed with finite differences (left) and with a free-surface condition and a second-order Born approximation (centre). The right panel display their difference. The same colour scale is used for the three plots.}
    \label{fig:born_mult_difr_1600_data}
  \end{subfigure}
  %%%
  \begin{subfigure}[t]{0.39\linewidth}
    \includegraphics[]{Figures/born_mult_difr/born_mult_difr_modeles_1600}
    \caption{1D-models used for finite-differences and Born modelling.}
    \label{fig:born_mult_difr_1600_model}
  \end{subfigure}\hfill%
  \begin{subfigure}[t]{0.60\linewidth}
    \includegraphics[]{Figures/born_mult_difr/born_mult_difr_traces_1600}
    \caption{Traces extracted at the positions indicated by the dashed lines in~\subref{fig:born_mult_difr_1600_data}. The bottom row is a zoom of the top row allowing to compare multiples modelled with both approaches.}
    \label{fig:born_mult_difr_1600_traces}
  \end{subfigure}
  %%%
\caption[Accuracy of the second order Born approximation in the diffractive case with a small velocity contrast]{Comparison of finite-differences modelling and free-surface modelling with a second-order Born approximation in the diffractive case.}
\label{fig:born_mult_difr_1600}
\end{figure}
%----------------------------
\begin{figure}[!htbp]
 %%%
   \begin{subfigure}[b]{\linewidth}
    \includegraphics[]{Figures/born_mult_difr/born_mult_difr_2500}
    \caption{Data recorded at receiver positions when the modelling is performed with finite differences (left) and with a free-surface condition and a second-order Born approximation (centre). The right panel display their difference. The same colour scale is used for the three plots.}
    \label{fig:born_mult_difr_2500_data}
  \end{subfigure}
  %%%
  \begin{subfigure}[t]{0.39\linewidth}
    \includegraphics[]{Figures/born_mult_difr/born_mult_difr_modeles_2500}
    \caption{1D-models used for finite-differences and Born modelling.}
    \label{fig:born_mult_difr_2500_model}
  \end{subfigure}\hfill%
  \begin{subfigure}[t]{0.60\linewidth}
    \includegraphics[]{Figures/born_mult_difr/born_mult_difr_traces_2500}
    \caption{Traces extracted at the positions indicated by the dashed lines in~\subref{fig:born_mult_difr_2500_data}.}
    \label{fig:born_mult_difr_2500_traces}
  \end{subfigure}
  %%%
\caption[Accuracy of the second order Born approximation in the diffractive case with a high velocity contrast]{Same as \cref{fig:born_mult_difr_1600}, but with a stronger velocity contrast.}
\label{fig:born_mult_difr_2500}
\end{figure}
%----------------------------

Both in the reflection and diffraction cases, multiples are accurately modelled when the velocity contrast is small. In the diffraction case, the kinematics of multiples is respected even with a high velocity contrast, although the amplitudes are overestimated (\cref{fig:born_mult_difr_2500}). In the reflection case with a higher velocity contrast , the Born approximation does not model the kinematics of primaries as accurately, and it is worse in the case of multiples (\cref{fig:born_mult_refl_2500}).
This is due to the propagation in a smooth model that modifies the velocities compared to the propagation in a blocky velocity model. Note however that the time shift between the two data sets is very similar at zero and far offset. Besides, there is not a unique choice for the definition of the smooth model. It is obtained in these examples by smoothing the blocky slowness model, but other definitions may lead to better results, at least at zero offset. In practice, there is no need to smooth an exact model: the \gls{mva} strategy aims actually at determining both a background velocity model and a reflectivity model allowing to reproduce observed data with a second-order Born approximation.

Eventually, we note that reflection multiples (\cref{fig:born_mult_refl_1600,fig:born_mult_refl_2500}) appear as a scaled version of the primaries with an opposite wavelet, contrary to diffraction multiples (\cref{fig:born_mult_difr_1600,fig:born_mult_difr_2500}) which have a different wavelet from the primaries.

%In the context of background velocity estimation with \gls{mva} techniques, the kinematics of wave propagation should be accurately modelled. We conclude form these examples that the second-order Born approximation may not be valid for high-velocity contrasts (\(>\SI{1000}{\meter\per\second}\)). This may limit the scope of real cases which may be considered with our approach, as multiples are then weaker than their respective primaries. Nonetheless, in \cref{chap:applications}, we consider a case where the multiples interfere with primaries caused by weak reflectors, which may still be a valid case.

\minisec{Non-linearity of multiples with respect to the reflectivity model}
Finally, we illustrate the linear dependence of primaries and non-linear dependence of multiples to the reflectivity model in the second-order Born approximation with a simple example (\cref{fig:linerity_prim}). We consider two different reflectivity models \(\xi_A\) and \(\xi_B\) consisting of a single reflector located at \SI{200}{\meter} depth for model~A and \SI{300}{\meter} for model~B. A single shot is fired from the middle point in the surface. The source wavelet is a Ricker with a maximum frequency of \SI{40}{\hertz} (\cref{fig:ricker}). We compute the response recorded by receivers at the surface, respectively \(P_1\croch{\xi_A}\) and \(P_1\croch{\xi_B}\) (\cref{fig:linerity_prim}, first and second columns). Then we compare the response obtained with the linear combination of models \(\xi_C=\xi_A+2\xi_B\) (\cref{fig:linerity_prim}, third column) with the linear combination of the individual data sets \(P_1\croch{\xi_A}+2P_1\croch{\xi_B}\) (\cref{fig:linerity_prim}, fourth column). In the case of primaries only (\cref{fig:linerity_prim}, middle row) these data sets are similar, which confirms the linear relationship between velocity perturbation and primary reflections obtained under the first-order Born-approximation. In the case of multiples two remaining events are visible, the multiple which reflects twice on the deeper reflector and the one which reflects once at each reflector. As expected, the relationship between data and model perturbation is thus non linear under the second-order Born approximation.
%----------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/Lin_Prim_Mult/Lin_Prim_Mult}
\caption[Illustration of the linearity of primaries and non-linearity of multiples to the model perturbation]{Illustration of the linearity of primaries and non-linearity of multiples to the model perturbation. Two reflectivity models \(\xi_A\) and \(\xi_B\) and their combination \(\xi_C\) are considered (top row). Data are modelled in the case of primary reflections only (middle row) and in the case of primaries and first-order surface multiples (bottom row). The most right column shows the difference between data modelled in \(\xi_C\) and the combination of the data sets computed with \(\xi_A\) and \(\xi_B\).}
\label{fig:linerity_prim}
\end{figure}
%----------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Introduction of the horizontal subsurface offset}\label{sec:introduction_depth_offset}
We now extend the second-order Born modelling procedure to the extended domain.
The space of physical models \(\Msp\) is extended with the subsurface offset \(\vec{h}\), considered strictly horizontal \(\vec{h}=\paren{h,0}\). The extended reflectivity \(\xi\xh\) now depends on this extra parameter and lives in \(\Esp\), and is defined as
\begin{equation}
	\xi\xh=\frac{2\delta c\xh}{c_0^3\paren{\vec{x}}}\text{.}
\end{equation}

The modelling of primary reflections now includes a spatial delay at depth as illustrated on \cref{fig:subsurface_offset_primaries}. \Cref{eq:def_P1_nonextended} defining \(P_1\) is then transformed into
\begin{equation}\label{eq:P1_ext_long}
	P_1\sxom=\iomsqr\wavelet\vecom \int_\vec{y}\int_h G_0\paren{\vec{s},\vec{y}-h,\omega} \xi\paren{\vec{y},h} G_0\paren{\vec{y}+h,\vec{x},\omega} \diff h\diff \vec{y}\text{,}
\end{equation}
with a new integral over \(h\). In the case of first-order surface-related multiples, several definitions are possible (\cref{fig:subsurface_offset_multiples}). We consider the case where a spatial delay is introduced at each reflection point (\cref{fig:subsurface_offset_multiples_a}), as it is a more general case and leads to similar formulations for the definition of \(P_1\) and \(P_3\). The modelling of first-order surface-related multiples now reads
\begin{multline}\label{eq:P3_ext_long}
	P_3\paren{s,\vec{x},\omega}=
	\iomsqr 
	\int_\vec{z}\int_k G_0\paren{m,\vec{z}-k,\omega}\xi\paren{\vec{z},k}\\
	G_0\paren{\vec{z}+k,\vec{x},\omega} \diff k \diff \vec{z}\int_{m\in\partial\Omega} \paren{-1}\frac{2\ioma}{c_0\paren{m}} P_1\paren{s,m,\omega} \diff m\text{,}
\end{multline}
where \(m\) is a point located at the surface \(\partial\Omega\). As before, the coefficient \(2\ioma/c_0\vecx\) is introduced to properly simulate the reflection at the free-surface.
%-------------------------------------
\begin{figure}[!htbp]
\begin{minipage}[t]{0.33\textwidth}
	\centering
	%\begin{subfigure}[t]{0.32\linewidth}
	\includegraphics{Figures/Offset_Prof_prim}
	\caption{Modelling of primary reflections with the horizontal subsurface\-/offset \(h\) defined as a spatial delay at depth.}
	\label{fig:subsurface_offset_primaries}
\end{minipage}\hfill%
\begin{minipage}[t]{0.65\textwidth}
	%\captionsetup[subfigure]{belowskip=-1pt}
	\centering
	%\end{subfigure}\hfill%
	\begin{subfigure}[b]{0.49\linewidth}
		\includegraphics{Figures/Offset_Prof_mult}
		\caption{}
		\label{fig:subsurface_offset_multiples_a}
	\end{subfigure}\hfill%
	\begin{subfigure}[b]{0.49\linewidth}
		\includegraphics{Figures/Offset_Prof_mult_b}
		\caption{}
		\label{fig:subsurface_offset_multiples_b}
	\end{subfigure}
	\caption{Two possible schemes for the modelling of first\-/order surface\-/related multiples with the horizontal subsurface\-/offset.  A spatial delay is introduced either at each subsurface point \subref{fig:subsurface_offset_multiples_a} or at a single image point \subref{fig:subsurface_offset_multiples_b}.}
	\label{fig:subsurface_offset_multiples}
\end{minipage}
\end{figure}
%-------------------------------------


The implementation of \cref{eq:P1_ext_long,eq:P3_ext_long} in their current formulation is not straightforward. Applying the wave-operator \(\Wop\) to this formula allows to derive a more convenient modelling scheme:
\begin{subequations}\label{eq:def_P_temp}
\begin{empheq}[left=\empheqlbrace]{align}
	\Wop P_0\sxom &= K_{P_0}\sxom=\wavelet\vecom\deltaxs\text{,}\\
	\Wop P_1\sxom &= K_{P_1}\sxom=\int_h\iomsqr P_0\paren{s,\vec{x}-2h,\omega}\xi\paren{\vec{x}-h,h}\diff h\text{,}\\
	\Wop P_2\sxom &= K_{P_2}\sxom=\Ms P_1\sxom\\%\paren{-1}\iom\int_{m\in\partial\Omega}P_1\paren{s,m,\omega}\delta\paren{x-m}\diff m\text{,}\\
	\Wop P_3\sxom &= K_{P_3}\sxom=\int_h\iomsqr P_2\paren{s,\vec{x}-2h,\omega}\xi\paren{\vec{x}-h,h}\diff h\text{,}
\end{empheq}
\end{subequations}
which involves the usual wave equation with modified source terms \(K_{P_1}\) and \(K_{P_3}\) that include the integral over the subsurface offset. Thus the introduction of the subsurface offset does not change the wave-equation to be solved, but only the source terms \(K_{P_1}\) and \(K_{P_3}\). Note that there is no additional loop over \(h\), except in the source terms. The number of wave-equations to be solved remains the same.

To simplify further expressions, we introduce the vector \(S\in\Dsp\) which is zero everywhere in space except at the location of the seismic source: \(S\sxom=\wavelet\vecom\deltaxs\). We also define the bilinear operator \(\Kxi{u}{\chi}:\Dsp\times\Esp\mapsto\Dsp\) for a vector \(u\) of \(\Dsp\) and a vector \(\chi\) of \(\Esp\) as
\begin{equation}\label{eq:def_Kxi}
	\Kxi{u}{\chi}\paren{s,\vec{x},\omega}=
	\int_h\iomsqr u\paren{s,\vec{x}-2h,\omega}\chi\paren{\vec{x}-h,h}\diff h\text{.}
\end{equation}
%We also defines the linear operator \(\Ms:\Dsp\mapsto\Dsp\) which selects the value of the primary wavefield at the surface to construct the source term in the equation defining \(P_2\) with the surface reflection coefficient \(\paren{-1}\).
This allows the modelling scheme to be written in a more compact way, which will be used in following calculations,
\begin{subequations}\label{eq:def_P}
\begin{empheq}[left=\empheqlbrace]{align}
	\Wop P_0\sxom &=S\sxom\text{,}\\
	\Wop P_1\sxom &=\Kxi{P_0}{\xi}\sxom\text{,}\\
	\Wop P_2\sxom &=\Ms P_1\sxom\text{,}\\
	\Wop P_3\sxom &=\Kxi{P_2}{\xi}\sxom\text{.}
\end{empheq}
\end{subequations}

We have described how to solve the forward problem of the inner inverse problem. We now introduce the corresponding objective function and the resolution of the inverse problem, consisting of determining \(\xi\) from observed data for a given background velocity model \(c_0\vecx\).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%