\section{Synthetic examples}\label{sec:ex_chap2}
%We present four synthetic examples of iterative migration in the extended domain. In the first example, we consider only primary reflections and we investigate the ability of iterative migration to deal with migration artefacts presented in \cref{sec:mva_limitations}. Then we run the same example with first-order surface multiples to illustrate the attenuation of cross-talk artefacts. The case of a velocity increasing with depth where cross-talk artefacts have a different focusing behaviour than events corresponding to the true reflector is studied. Finally we compare non-linear optimisation strategies in the case of an acquisition hole where multiples can bring extra information compared to primaries.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\subsection{Implementation details}\label{sec:implementation details}
%\begin{itemize}
%	\item tapers;
%	\item \(P\) and \(\lambda\) are temporary variables;
%\end{itemize}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Primaries only in a homogeneous medium}\label{sec:prim_ex1}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In the first example, we consider a \SI{450}{\meter} depth and \SI{1620}{\meter} large model discretised along a \(\SI{6}{\meter}\times\SI{6}{\meter}\)~grid. The reflector is \SI{300}{\meter} deep and the exact background velocity model is \SI{3000}{\meter\per\second}. A shot is fired every \SI{24}{\meter} and the maximum surface offset is \SI{\pm 540}{\meter}. Then a single reflected event is visible on observed data (\cref{fig:prim_ex1_Pobs}). For the optimisation process, we use the \(\ell_2\)-norm as regularisation function and the linear conjugate-gradient algorithm. \num{10} iterations are performed in a too slow background velocity model (\SI{2500}{\meter\per\second}) (\cref{fig:prim_ex1_xi}).
%-----------------------------------
\begin{figure}
\includegraphics{Figures/prim_ex1_P/prim_ex1_P}
{\phantomsubcaption\label{fig:prim_ex1_Pobs}}
{\phantomsubcaption\label{fig:prim_ex1_P01}}
{\phantomsubcaption\label{fig:prim_ex1_R01}}
{\phantomsubcaption\label{fig:prim_ex1_P02}}
{\phantomsubcaption\label{fig:prim_ex1_R02}}
{\phantomsubcaption\label{fig:prim_ex1_P05}}
{\phantomsubcaption\label{fig:prim_ex1_R05}}
{\phantomsubcaption\label{fig:prim_ex1_P10}}
{\phantomsubcaption\label{fig:prim_ex1_R10}}
\caption{Observed data obtained for a source at the middle point of the model (left). Corresponding calculated data (middle) and residuals (right) obtained after \numlist{1;2;5;10} iterations (from top to bottom), corresponding to the reflectivity models presented in \cref{fig:prim_ex1_xi}.}
\label{fig:prim_ex1_P}
\end{figure}
%-----------------------------------
%-----------------------------------
\begin{figure}
\includegraphics{Figures/prim_ex1_xi/prim_ex1_xi}
\caption{Results of migration after \numlist{1;2;5;10} iterations (from top to bottom) when using a too slow velocity model (\SI{2500}{\meter\per\second}). We show a  section at \(h=0\) (left), a \gls{cig} at \(x=\SI{1350}{\meter}\) (middle), and the same \gls{cig} multiplied by \(\abs{h}\) (right). Blue, grey and red represent negative, null and positive values respectively. Each image is represented with its own colour scale. The two main events visible in the \gls{cig} at the first iteration are indicated by dashed lines: the downward curved event corresponds to the reflector, while the upward curved one is caused by the limited acquisition aperture.}
\label{fig:prim_ex1_xi}
\end{figure}
%----------------------------------

Two sections of the extended reflectivity volume are shown: one at \(h=\SI{0}{\meter}\) corresponding to the physical reflectivity (\cref{fig:prim_ex1_xi}, left), and a \gls{cig} extracted at \(x=\SI{810}{\meter}\) (\cref{fig:prim_ex1_xi}, middle). The most right panel displays the same \gls{cig} multiplied by the absolute value of the subsurface offset \(\abs{h}\), the actual input of the MVA objective function.
After one iteration, two main events are visible with opposite curvatures, indicated by the dashed lines for the first iteration (\cref{fig:prim_ex1_xi}, top). Because of the too low velocity, the reflector is shifted toward the surface in the zero-offset section and spreads over non-zero offsets with a downward curve, as predicted by the theory \parencite{mulder_subsurface_2014}. The event curved upward above the reflector is a migration artefact due to the limited extension of sources and receivers in the acquisition. It is especially visible in the panels where \(\abs{h}\xi\itn{n}\) is displayed (\cref{fig:prim_ex1_xi}, right). The data reconstructed with this reflectivity model does not match observed data (\cref{fig:prim_ex1_P01,fig:prim_ex1_R01}), as only one iteration is performed.

The residuals are greatly reduced with iterations (\cref{fig:prim_ex1_J}) and the final reflectivity model obtained after \num{10} iterations perfectly explains observed data (\cref{fig:prim_ex1_P10,fig:prim_ex1_R10}). Two improvements are visible on the final reflectivity image \cref{fig:prim_ex1_xi}. Deconvolution of the wavelet source results in a more localised reflector, but the main effect of iterations is the attenuation of migration artefacts and the strengthening of the event corresponding to the true reflector.
%-----------------------------------
\begin{figure}
\includegraphics{Figures/prim_ex1_J/prim_ex1_J}
\caption{Value of the objective function \(J_0\) and norm of its gradient corresponding to the iterative migration results presented in \cref{fig:prim_ex1_xi}.}
\label{fig:prim_ex1_J}
\end{figure}
%-----------------------------------

We run the same experiment for the correct velocity model (\SI{3000}{\meter\per\second}) and for a too high velocity model (\SI{3500}{\meter\per\second}). This test is similar to those presented in \textcite{lameloise_improving_2014,hou_inversion_2016}. The \glspl{cig} obtained after a single iteration are all affected by migration artefacts with an upward curvature (\cref{fig:prim_ex1_3_CIGs}, top), although in the case of a too high migration velocity, these artefacts look similar to the event corresponding to the true reflector and are not distinguishable. After 10 iterations all \glspl{cig} look cleaner, and defocusing of energy is only due to errors in the velocity model.

As a result, attenuation of migration artefacts in the \glspl{cig} improves the properties of the \gls{mva} objective function \(J_1\). To illustrate this, we plot in \cref{fig:prim_ex1_Jv} the value of \(J_1\) calculated after several iterations of migration performed in homogeneous velocity models ranging from \SIrange{2500}{3500}{\meter\per\second} every \SI{100}{\meter\per\second}. To ease the comparison, we have plotted a normalised version of \(J_1\):
\begin{equation}
	\widetilde{J}_1\croch{c_0}=\frac{\norm[\big]{\,\abs{h}\,\xi\itn{N+1}\croch{c_0}}_{\Esp}^2}{\norm[\big]{\xi\itn{N+10}\croch{c_0}}_{\Esp}^2}\text{.}
\end{equation}
As already analysed by \textcite{lameloise_improving_2014}, after one iteration the minimum of \(\widetilde{J_1}\) is not at the true velocity (\SI{3000}{\meter\per\second}) because of migration artefacts not focusing to zero-subsurface offset for the correct velocity. The minimum is reached for a too low velocity model as migration artefacts always have an upward curvature. The position of the minimum is a compromise between the defocusing of migration artefacts (they move away from the \(h=0\) axis as far as the velocity increases) and the defocusing of the event corresponding to the true reflector (decreasing when the velocity approaches the true velocity). As migration artefacts are attenuated when iterating, the minimum is progressively shifted to the true velocity.
%-----------------------------------
\begin{figure}
\begin{minipage}[t]{0.59\linewidth}
	\centering
	\includegraphics{Figures/prim_ex1_3_CIGs/prim_ex1_3_CIGs}
	\caption{\glspl{cig} at \(x=\SI{810}{\meter}\) obtained for a too slow (\SI{2500}{\meter\per\second}, left), a correct (\SI{3000}{\meter\per\second}, middle) and a too high (\SI{3500}{\meter\per\second}, right) background velocity model after \num{1} (top) and \num{10} (bottom) iterations.}
	\label{fig:prim_ex1_3_CIGs}
\end{minipage}\hfill%
\begin{minipage}[t]{0.39\linewidth}
	\centering
	\includegraphics{Figures/prim_ex1_Jv/prim_ex1_Jv}
	\caption{Values of the objective function \(J_1\) for different homogeneous velocity models. The correct velocity is \SI{3000}{\meter\per\second}.}
	\label{fig:prim_ex1_Jv}
\end{minipage}
\end{figure}
%-----------------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Primaries and multiples in a homogeneous medium}\label{sec:mult_ex1}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
We now run the same example with first-order surface-related multiples. There are now two events in observed data (\cref{fig:mult_ex1_Pobs}). At the first iteration, both are interpreted as primary reflections, resulting in two events (\crefrange{fig:mult_ex1_xi01}{fig:mult_ex1_xih01}). The shallower one is the correct interpretation of the primary reflection and is similar to the primary-only case of the previous example. The misinterpretation of the surface multiple as a primary results in a cross-talk artefacts at twice the depth of the true event. As a consequence, reconstructed data do not match observed data. Iterations efficiently reduce the misfit (\cref{fig:mult_ex1_P20,fig:mult_ex1_R20,fig:mult_ex1_J}) and the final reflectivity section is free of migration artefacts and looks similar to the one obtained with primaries only (\cref{fig:mult_ex1_xi}).
%-------------------------------
\begin{figure}
\includegraphics{Figures/mult_ex1_P/mult_ex1_P}
%\caption{(a) Observed data; and calculated data (left) and residuals (right) obtained after (c,d) one; (e,f) five; (g,h) twenty iterations; corresponding to the migration results presented in \cref{fig:mult_ex1_xi}.}
{\phantomsubcaption\label{fig:mult_ex1_Pobs}}
{\phantomsubcaption\label{fig:mult_ex1_P01}}
{\phantomsubcaption\label{fig:mult_ex1_R01}}
{\phantomsubcaption\label{fig:mult_ex1_P05}}
{\phantomsubcaption\label{fig:mult_ex1_R05}}
{\phantomsubcaption\label{fig:mult_ex1_P20}}
{\phantomsubcaption\label{fig:mult_ex1_R20}}
\caption{Observed data obtained for a source at the middle point of the model (left). Corresponding calculated data (middle) and residuals (right) obtained after \numlist{1;5;20} iterations (from top to bottom), corresponding to the reflectivity models presented in \cref{fig:mult_ex1_xi}.}
\label{fig:mult_ex1_P}
\end{figure}
%-------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/mult_ex1_xi/mult_ex1_xi}
{\phantomsubcaption\label{fig:mult_ex1_xi01}}
{\phantomsubcaption\label{fig:mult_ex1_xif01}}
{\phantomsubcaption\label{fig:mult_ex1_xih01}}
{\phantomsubcaption\label{fig:mult_ex1_xi05}}
{\phantomsubcaption\label{fig:mult_ex1_xif05}}
{\phantomsubcaption\label{fig:mult_ex1_xih05}}
{\phantomsubcaption\label{fig:mult_ex1_xi20}}
{\phantomsubcaption\label{fig:mult_ex1_xif20}}
{\phantomsubcaption\label{fig:mult_ex1_xih20}}
\caption{Reflectivity model obtained after \numlist{1;5;20} iterations (from top to bottom) in a too slow background velocity model \(c\ped{initial}=\SI{2500}{\meter\per\second}\).}
\label{fig:mult_ex1_xi}
\end{figure}
%-------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/mult_ex1_J/mult_ex1_J}
\caption{Value of the objective function \(J_0\) and norm of its gradient corresponding to the iterative migration results presented in \cref{fig:mult_ex1_xi}.}
\label{fig:mult_ex1_J}
\end{figure}
%-------------------------------

We also repeat the test of computing \glspl{cig} in three homogeneous velocity models (too low, correct and too high) (\cref{fig:mult_ex1_3_CIGs}). After one iteration, all \glspl{cig} are altered with cross-talk artefacts (\cref{fig:mult_ex1_3_CIGs}, top). Iterating allows to attenuate these artefacts and to obtain \glspl{cig} similar to those obtained in the previous example (\cref{fig:prim_ex1_3_CIGs,fig:mult_ex1_3_CIGs}, bottom). Note that in the special case of homogeneous background velocity models, cross-talk artefacts exhibit the same focusing behaviour as the true events, hence we do not present the equivalent of \cref{fig:prim_ex1_Jv} in the multiple case. In \cref{chap:WIMVA}, we run a similar test in the case where velocity increases with depth and we show that cross-talk artefacts due to multiples favour lower velocities (see \cref{sec:examples_chap4_mult_migr,fig:chap4_mult_Jvinv,fig:chap4_mult_Jviter} for more details).
%-------------------------------
\begin{figure}
\includegraphics{Figures/mult_ex1_3_CIGs/mult_ex1_3_CIGs}
\caption{\glspl{cig} at \(x=\SI{810}{\meter}\) obtained for a too slow (\SI{2500}{\meter\per\second}, left), a correct (\SI{3000}{\meter\per\second}, middle) and a too high (\SI{3500}{\meter\per\second}, right) background velocity model after \num{1} (top) and \num{10} (bottom) iterations.}
\label{fig:mult_ex1_3_CIGs}
\end{figure}
%-----------------------------------

%The focusing properties of the cross-talk artefacts is actually similar to the one of the event corresponding to the true reflector. Therefore we consider a new case where this may not be the case.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\subsection{Non-homogeneous medium}\label{sec:mult_ex2}
%\begin{itemize}
%	\item same thing but with velocity increasing with depth.
%\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\subsection{Acquisition hole: importance of the optimisation strategy}\label{sec:mult_ex3_hole}
%\begin{itemize}
%	\item compare the result after \num{30} iterations of steepest descent, conjugate gradient (FR and PR), and l-BFGS (truncated Newton ?)
%\end{itemize}
%%-------------------------------
%\begin{figure}
%\includegraphics{Figures/mult_ex3_J/mult_ex3_J}
%\caption{Decrease of the objective function}
%\label{fig:mult_ex3_J}
%\end{figure}
%%-------------------------------