%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Optimisation strategy}\label{sec:optim}
Iterative migration is an inverse problem posed in the extended domain \(\Esp\). This is the inverse problem we consider in this chapter. It is solved as an unconstrained optimisation problem. In this section we present the objective function to be minimised and the optimisation strategy which will be used in numerical examples. We eventually explain how the gradient of the objective function is computed.
\subsection{Definition of an objective function for iterative migration}
The objective function \(J_0\) of iterative migration reads
\begin{equation}\label{eq:def_J0}
	J_0\croch{c_0,\xi}=\Jres\croch{c_0,\xi}+\lreg\phi\croch{\xi}\text{.}
\end{equation}
Iterative migration aims at determining an extended reflectivity model \(\xi\) minimising \(J_0\). The estimated background velocity \(c_0\), which might be inaccurate, is kept fixed during the minimisation of \(J_0\). The objective function \(J_0\) is made of two contributions,
\begin{itemize}
	\item the first term evaluates the misfit between data computed with the reflectivity model \(\xi\) and observed data.
	It is expressed as
	\begin{equation}
		\Jres\croch{c_0,\xi}=\frac{1}{2}\norm[\big]{M\paren[\big]{P_1\croch{c_0,\xi}+P_3\croch{c_0,\xi}}-\Pobs}^2_{\Dobs}
	\end{equation}
	%We define it with a very general expression
%\begin{equation}
%	\Jres\paren{\xi}=\fj\paren{P_1\paren{\xi},P_3\paren{\xi}}\text{.}
%\end{equation}
%We will consider two different cases for the function \(\fj:\Dsp\times\Dsp\mapsto\mathbb{R}\). Primaries and multiples can be considered together:
%\begin{subequations}
%\begin{align}
%	\fj\paren{P_1,P_3}
%	&=\frac{1}{2}\norm[\big]{M\paren{P_1+P_3}-\Pobs}^2_{\Dobs}\text{,}
%	\label{eq:fj_together}\\
%	\shortintertext{or separately with obeserved data decomposed into \(\Pobs=\PPobs+\PMobs\)
%		}
%	\fj\paren{P_1,P_3}
%	&=\frac{1}{2}\norm[\big]{MP_1-\PPobs}^2_{\Dobs}+\am\frac{1}{2}\norm[\big]{MP_3-\PMobs}_\Dobs\label{eq:fj_separately}
%\end{align}
%\end{subequations}
The projection operator \(M:\Dsp\mapsto\Dobs\) selects the value of the wavefields \(P_1\) and \(P_3\) at the position of receivers before comparison with observed data;
	\item the second term is a regularisation function whose weight is controlled by the scalar \(a_\phi\). In this chapter, we will consider simple regularisation functions such as the \(\ell_2\) norm \(\phi\croch{\xi}=\frac{1}{2}\norm{\xi}^2_\Esp\), but other terms may easily be introduced.
\end{itemize}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Linear case of primaries only}\label{sec:optim_lin}
In the numerical example section, we begin by applying iterative migration in a simple case without multiples. The first-order Born modelling can be represented by a linear operator \(F:\Esp\mapsto\Dobs\). To simplify the following equations, we assume that the projection operator \(M\) is included in the definition of \(F\). With the \(\ell_2\) norm as regularisation, the objective function~\eqref{eq:def_J0} now reads
\begin{equation}\label{eq:defJ0_linear}
	J_0\croch{c_0,\xi}=\frac{1}{2}\norm[\big]{F\croch{c_0}\xi-\Pobs}^2_\Dobs+\lreg\phi\croch{\xi}\text{.}
\end{equation}
Its minimisation is equivalent to the resolution of the linear system
\begin{equation}\label{eq:lin_case_equation}
	\paren[\big]{\Fadj F+\lreg I}\xi=F^T\Pobs\text{,}
\end{equation}
which is obtained by zeroing the gradient of the objective function~\eqref{eq:defJ0_linear}. An efficient method to solve this system is the linear conjugate gradient algorithm \parencite[112]{nocedal_numerical_2006}. Note that in practice an \(F\) and \(\Fadj\) will not be expliclty computed and an equivalent formulation will be used (\cref{sec:Grad_J0}).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Non-linear optimisation in the non-linear case of multiple reflections}\label{sec:optim_nonlin}
With multiples, the modelling operator is not linear any more and non-linear optimisation techniques have to be used. With gradient-based methods, the reflectivity model at iteration \(\paren{n+1}\) is updated from the reflectivity model at iteration \(\paren{n}\) following
\begin{equation}\label{eq:update_formula}
	\xi\itn{n+1}=\xi\itn{n}+\alpha\itn{n} d\itn{n}\text{,}
\end{equation}
where \(d\in\Esp\) is called \emph{descent direction} and the positive scalar \(\alpha\) is called \emph{step size} or \emph{step length} determined by a procedure called \emph{linesearch}.
The process is initialised with \(\xi\itn{1}=0\) and \(\xi\) is updated until a convergence criterion is satisfied, for example when the value of the objective function or the norm of its gradient goes below a given threshold. In the numeric examples shown at the end of this chapter, we set a maximum number of iterations \(N\), so that the final result is \(\xi\itn{N+1}\). \(d\) and \(\alpha\) are determined such that the value of \(J_0\) decreases at each iteration, that is \({J_0\paren{\xi\itn{n+1}}<J_0\paren{\xi\itn{n}}}\).

The most simple choice for the descent direction is given by the opposite of the gradient of the objective function, that is \(d\itn{n}=-g\itn{n}\), with \(g\itn{n}\) the gradient of \(J_0\) computed at \(\xi\itn{n}\),
\begin{equation}
	g\itn{n}=\pderiv{J_0}{\xi}\croch{c_0,\xi\itn{n}}\text{,}
\end{equation}
but alternative strategies providing faster convergence exist. The optimal strategy is a trade-off between the number of iteration needed to reach a satisfactory minimisation of the objective function and the numerical cost for determining \(d\) and \(\alpha\). We briefly review standard non-linear optimisation strategies in \cref{app:optim}. The reader is referred to \textcite{nocedal_numerical_2006} for an extensive review.

In all the following numerical examples, we use the linesearch procedure of \textcite{more_line_1994}, which ensures that the strong Wolfe conditions (see \cref{sec:linesearch} and \cref{fig:wolfe_conditions}) are satisfied. The procedure requires the evaluation of the value and the gradient of the objective function at successive trial points \(\xi\itn{n}+\alpha_id\itn{n}\), until a satisfactory value for \(\alpha_i\) is found. This may be expensive if numerous step size have to be tested. However we observed in practice that this optimal value \(\alpha\ped{opt}\itn{n}\) is usually found in two or three trial steps. Besides as we set \(\xi\itn{n+1}=\xi\itn{n}+\alpha\ped{opt}\itn{n}d\itn{n}\), the linesearch procedure already provides the value of \(J_0\paren{\xi\itn{n+1}}\) and \(g\itn{n+1}\) at the next iteration. So the additional cost of a linesearch procedure satisfying the strong Wolfe conditions remains affordable.

We use the non-linear conjugate-gradient technique to determine the descent direction,
\begin{align}
	d\itn{n}&=-g\itn{n}+\beta\itn{n}d\itn{n-1}\text{,}\\
	\shortintertext{where the scalar \(\beta\) is given by the formula Polak-Ribière formula}
	\beta\itn{n}&
	=\frac{\pscal[\big]{g\itn{n}}{g\itn{n}-g\itn{n-1}}_\Esp}{\pscal[\big]{g\itn{n-1}}{g\itn{n-1}}_\Esp}
\end{align}
A step length satisfying the strong Wolfe conditions ensures that \(d\) is effectively a descent direction \parencite[122]{nocedal_numerical_2006}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\subsection{Preconditioning}
%To accelerate the convergence of the minimisation algorithm, a change of variables can be performed can be performed using a preconditioner \(C\):
%\begin{equation}
%	\Pc{\xi}=C\xi\text{,}
%\end{equation}
%the new objective function then reads
%\begin{equation}
%	\Pc{J_0}\paren{\Pc{\xi}}=J_0\paren{P\xi}
%\end{equation}
%and the gradient of the new objective function is computed after the gradient of the original objective function and is scaled by the preconditioner.
%\begin{equation}
%	\pderiv{\Pc{J}}{\Pc{\xi}}\paren{\Pc{\xi}}=C^{-1}\pderiv{J}{\xi}\text{.}
%\end{equation}
%In the linear case described in \cref{sec:optim_lin}, the linear system~\eqref{eq:lin_case_equation} is transformed into
%\begin{equation}
%	C^{-T}F^TM^TMFC^{-1}\xi=C^{-T}F^T\Pobs\text{,}
%\end{equation}
%with \(C^{-T}=\paren{C^{-1}}^T\).
%There is no general rule to choose a preconditioner. An efficient preconditioner should ensure that the operator \(C^{-T}F^TM^TMFC^{-1}\) has a better condition number than \(C^{-T}F^TM^TMFC^{-1}\). Its computation should also not require much effort. Preconditioning of iterative migration will be discussed in \cref{chap:WIMVA}.