\section{Derivatives of \texorpdfstring{\(J_0\)}{J0}}
One of the key ingredients of local optimisation methods is the gradient of the objective function. Its computation is the most computationally expensive step of the optimisation procedure and we present here an efficient way of deriving the gradient of \(J_0\) based on the adjoint state method. We illustrate on an example the different contributions of the gradient to the reflectivity update. Eventually, the shape of the Hessian matrix is studied on a simple 1D-case.
%-----------------------------------------------------
\subsection{Computation of the gradient of \texorpdfstring{\(J_0\)}{J0}}\label{sec:Grad_J0}
In the objective function defined in \cref{eq:def_J0}, the contribution to the gradient due to the regularisation is straightforward. When the regularisation term is defined as the \(\ell_2\)-norm of \(\xi\), we have
\begin{align}
	\phi\croch{\xi}&=\frac{1}{2}\norm{\xi}^2_\Esp\text{;}\\
	\pderiv{\phi}{\xi}\croch{\xi}&=\xi\text{.}
\end{align}
In this section, we focus on the gradient of \(\Jres\). It can be expressed as
\begin{equation}\label{eq:def_gradJ0_frechet}
\pderiv{\Jres}{\xi}=
	\croch[\Bigg]{\pderiv{P_1}{\xi}\croch{c_0,\xi\itn{n}}+\pderiv{P_3}{\xi}\paren{c_0,\xi\itn{n}}}^TM^T
	\paren[\Big]{M\paren{P_1\croch{c_0,\xi\itn{n}}+P_3\croch{c_0,\xi\itn{n}}}-\Pobs}\text{.}
\end{equation}
%\begin{equation}
%\begin{split}
%	\pderiv{\Jres}{\xi}=
%	&\hphantom{+}\croch[\Bigg]{\pderiv{P_1}{\xi}\paren{\xi\itn{n}}}^T\pderiv{\fj}{{P_1}}\paren[\Big]{P_1\paren{\xi\itn{n}},P_3\paren{\xi\itn{n}}}\\
%	&+\croch[\Bigg]{\pderiv{P_3}{\xi}\paren{\xi\itn{n}}}^T\pderiv{\fj}{{P_3}}\paren[\Big]{P_1\paren{\xi\itn{n}},P_3\paren{\xi\itn{n}}}\text{.}
%\end{split}
%\end{equation}
%In the case where primaries and multiples are not separated (\cref{eq:fj_together}), this gives
%\begin{subequations}
%\begin{align}
%	\pderiv{\Jres}{\xi}\paren{\xi\itn{n}}&
%	=\croch[\Bigg]{\pderiv{P_1}{\xi}\paren{\xi\itn{n}}+\pderiv{P_3}{\xi}
%	\paren{\xi\itn{n}}}^T\paren[\big]{P_1\paren{\xi\itn{n}}+P_3\paren{\xi\itn{n}}-\Pobs}
%	\\ \shortintertext{and in the case they are considered separately (\cref{eq:fj_separately})}
%	\pderiv{\Jres}{\xi}\paren{\xi\itn{n}}&
%	=\croch[\Bigg]{\pderiv{P_1}{\xi}\paren{\xi\itn{n}}}^T\paren[\Big]{P_1\paren{\xi\itn{n}}-\PPobs}
%	+\croch[\Bigg]{\pderiv{P_3}{\xi}\paren{\xi\itn{n}}}^T\paren[\Big]{P_3\paren{\xi\itn{n}}-\PMobs}
%\end{align}
%\end{subequations}
The quantities \(\partial P_1/\partial\xi\) and \(\partial P_3/\partial\xi\) are called Fréchet derivatives and correspond to the partial derivatives of specific data with respect to model parameters. Their computation is expensive, and we prefer to use a more efficient gradient computation method, the adjoint state method \parencite{plessix_review_2006}. This method will directly compute the product of the Fréchet derivatives with the residuals rather than explicitly deriving the two terms.

An augmented functional called Lagrangian is defined with constraints added to the objective function. Each \emph{state variable} \(P_0\), \(P_1\), \(P_2\) and \(P_3\) is associated to an \emph{adjoint variable} \(\lambda_0\), \(\lambda_1\), \(\lambda_2\) and  \(\lambda_3\). The \(\lambda_i\) are vectors of \(\Dsp\) and are used as Lagrange multipliers of the state equations~\eqref{eq:def_P}
\begin{equation}\label{eq:Lagrangian_J0}
\begin{split}
	\Lagr{\Jres}\paren{\xi\itn{n},P_0,P_1,P_2,P_3,\lambda_0,\lambda_1,\lambda_2,\lambda_3}=\Jres\paren{\xi}
		&-\pscal[\big]{\lambda_0}{\Wop P_0-S}_\Dsp\\
		&-\pscal[\big]{\lambda_1}{\Wop P_1-\Kxi{P_0}{\xi}}_\Dsp\\
		&-\pscal[\big]{\lambda_2}{\Wop P_2-M_s P_1}_\Dsp\\
		&-\pscal[\big]{\lambda_3}{\Wop P_3-\Kxi{P_2}{\xi}}_\Dsp\text{,}
\end{split}
\end{equation}
where we have dropped the dependencies to \(\sxom\) for simplicity.
The derivative of the Lagrangian with respect to \(\xi\) can be written
\begin{equation}
	\deriv{\Lagr{\Jres}}{\xi}
	=\pderiv{\Lagr{\Jres}}{\xi}
	+\sum_{i=0}^3\pderiv{P_i}{\xi}\pderiv{\Lagr{\Jres}}{{P_i}}
	+\sum_{i=0}^3\pderiv{\lambda_i}{\xi}\pderiv{\Lagr{\Jres}}{{\lambda_i}}\text{,}
\end{equation}
where the overlined variable \(\Lagr{\Jres}\) refers to the extended Lagrangian~\eqref{eq:Lagrangian_J0}. We decide to set the derivatives \(\partial\Lagr{\Jres}/\partial\lambda_i\) to zero, which ensures that the \(P_i\) are solutions of the state equations~\eqref{eq:def_P}. Similarly we set the derivatives \(\partial\Lagr{\Jres}/\partial P_i\) to zero. This way, the computation of the Fréchet derivatives is not required and we obtain the \emph{adjoint equations} defining the adjoint variables \(\lambda_i\)
\begin{subequations}\label{eq:def_lambda}\label[pluralequation]{eqs:def_lambda}
	\begin{empheq}[left=\empheqlbrace]{align}
	\WopA \lambda_3&=M^T\paren[\big]{M\paren{P_1+P_3}-\Pobs} \label{eq:lambda3}\\
	\WopA \lambda_2&=\KKxi{\lambda_3}{\xi}\\
	\WopA \lambda_1&=M^T\paren[\big]{M\paren{P_1+P_3}-\Pobs}+\MsA\lambda_2 \label{eq:lambda1}\\
	\WopA \lambda_0&=\KKxi{\lambda_1}{\xi}\text{,}
	\end{empheq}
\end{subequations}
where operator \(\KKxi{u}{\chi}:\Dsp\times\Esp\mapsto\Dsp\) is defined for \(u\in\Dsp\) and \(\chi\in\Esp\) by
\begin{equation}\label{eq:def_KKxi}
	\KKxi{u}{\chi}\sxom =\int_h \iomsqr u(s,\vec{x}+2h,\omega)\chi\paren{\vec{x}+h,h}\diff h\text{,}
\end{equation}
and can is an adjoint operator of \(K^{-}\) \pcref{eq:def_Kxi}:
\begin{equation}
	\pscal[\Big]{u}{\Kxi{v}{\chi}}_\Dsp=\pscal[\Big]{\KKxi{u}{\chi}}{v}_\Dsp
\end{equation}
for \(\paren{u,v}\in\Dsp\) and \(\chi\in\Esp\).

Eventually the gradient is given by the partial derivative of the Lagrangian with respect to \(\xi\):
\begin{equation}\label{eq:def_gradJ0_adjstate}
	\pderiv{\Jres}{\xi}=\deriv{\Lagr{\Jres}}{\xi}=\pderiv{\Lagr{\Jres}}{\xi}=\Qxi{P_0}{\lambda_1}+\Qxi{P_2}{\lambda_3}
\end{equation}
where operator \(\Qxi{u}{v}:\Dsp\times\Dsp\mapsto\Esp\) is an extended cross-correlation defined for \(\paren{u,v}\in\Dsp\) by
\begin{equation}\label{eq:def_Qxi}
	\Qxi{u}{v}\xh =\int_s\int_\omega \iomsqr u^*\paren{s,\vec{x}-h,\omega}v\paren{s,\vec{x}+h,\omega}\diff\omega\diff s\text{.}
\end{equation}
It is related to \(K^{-}\) and \(K^{+}\) by
\begin{equation}
	 \pscal[\Big]{\Qxi{u}{v}}{\chi}_\Esp
	 =\pscal[\Big]{\Kxi{u}{\chi}}{v}_\Dsp
	 =\pscal[\Big]{u}{\KKxi{v}{\chi}}_\Dsp\text{.}
\end{equation}
%In the case where primaries and multiples are not separated (\cref{eq:fj_together}), \cref{eq:lambda3,eq:lambda1} read
%\begin{subequations}
%\begin{empheq}[left=\empheqlbrace]{align}
%	\WopA \lambda_3&=M^T\paren[\big]{M\paren{P_1+P_3}-\Pobs}\text{,}\\
%	\WopA \lambda_1&=M^T\paren[\big]{M\paren{P_1+P_3}-\Pobs}+\Ms\lambda_2\text{.}
%\end{empheq}
%\end{subequations}
%If they are consider separately
%\begin{subequations}
%\begin{empheq}[left=\empheqlbrace]{align}
%	\WopA \lambda_3&=M^T\paren[\big]{MP_3-\PMobs}\text{,}\\
%	\WopA \lambda_1&=M^T\paren[\big]{MP_1-\PPobs}+\Ms\lambda_2\text{.}
%\end{empheq}
%\end{subequations}
%\begin{subequations}
%\begin{align}
%	\pderiv{\Jres}{\xi}\paren{\xi\itn{n}}&
%	=\croch[\Bigg]{\pderiv{P_1}{\xi}\paren{\xi\itn{n}}+\pderiv{P_3}{\xi}
%	\paren{\xi\itn{n}}}^T\paren[\big]{P_1\paren{\xi\itn{n}}+P_3\paren{\xi\itn{n}}-\Pobs}
%	\\ \shortintertext{and in the case they are considered separately (\cref{eq:fj_separately})}
%	\pderiv{\Jres}{\xi}\paren{\xi\itn{n}}&
%	=\croch[\Bigg]{\pderiv{P_1}{\xi}\paren{\xi\itn{n}}}^T\paren[\Big]{P_1\paren{\xi\itn{n}}-\PPobs}
%	+\croch[\Bigg]{\pderiv{P_3}{\xi}\paren{\xi\itn{n}}}^T\paren[\Big]{P_3\paren{\xi\itn{n}}-\PMobs}
%\end{align}
%\end{subequations}


The adjoint variables are solutions of equations similar to the state variables. They involve the adjoint of the wave operator, corresponding to a back-propagation in the time domain. Note that the computation of \(\lambda_0\) is not necessary and the computation of the gradient is less then twice as costly as the computation of the value of the objective function (\cref{tab:cost_grad}).
%---------------------------
\begin{table}[!htb]
\begin{tabular}{cclcl}
 \toprule
 & \multicolumn{2}{l}{primaries only} & \multicolumn{2}{l}{primaries and multiples} \\ \midrule
 evaluate \(J_0\) & 2 & \(\paren{P_0,P_1}\) & 4 & \(\paren{P_0,P_1,P_2,P_3}\) \\ \midrule
 evaluate \(J_0\) and \(\pderiv{J_0}{\xi}\)  & 3 & \(\paren{P_0,P_1,\lambda_1}\) & 7 & \(\paren{P_0,P_1,P_2,P_3,\lambda_3,\lambda_2,\lambda_1}\) \\ \bottomrule
\end{tabular}
\caption{Number of wave-equations to be solved to evaluate the value and the gradient of the objective function at a point \(\xi\in\Esp\). The number indicated here should be multiplied by the number of sources position considered in the acquisition.}
\label{tab:cost_grad}
\end{table}
%---------------------------

We now explain the physical meaning of the different parts of the gradient.

\subsection{Interpretation of the gradient}
Noting that the source terms of the equations defining \(\lambda_1\) and \(\lambda_3\) \pcref{eq:lambda1,eq:lambda3} have a common term, we can split the adjoint variable \(\lambda_1\) into two contributions
\begin{equation}
	\lambda_1=\lambda_3+\lambda_4\text{,}
\end{equation}
with \(\lambda_4\) solution of
\begin{equation}
	\WopA \lambda_4 = \MsA \lambda_2\text{.}
\end{equation}
so that the gradient of \(\Jres\) has three contributions
\begin{equation}\label{eq:grad_J0_decomp}
	\pderiv{\Jres}{\xi}=
	\underbrace{\Qxi{P_0}{\lambda_3}}_{g_1}
	+\underbrace{\Qxi{P_0}{\lambda_4}}_{g_2}
	+\underbrace{\Qxi{P_2}{\lambda_3}}_{g_3}\text{.}
\end{equation}

To illustrate the contributions of each term to the gradient, we consider a simple example. A single horizontal reflector is located at \SI{600}{\meter} in a homogeneous velocity model (\SI{3000}{\meter\per\second}) (\cref{fig:grad_decomp_exact_a}). In the trial reflectivity \(\xi^t\), a reflector is located at depth \SI{400}{\meter} (\cref{fig:grad_decomp_exact_b}). The modelling of both observed data (\cref{fig:grad_decomp_exact_c}) and calculated data is performed with the correct velocity model. Four events are visible in the residuals (\cref{fig:grad_decomp_exact_d}) defined as the difference between data calculated with the trial reflectivity and observed data. Each part of the gradient convert each event to the model space.
\begin{itemize}
	\item the first contribution (\cref{fig:grad_decomp_a}) noted \(g_1\) in \cref{eq:grad_J0_decomp} interprets the four events in the residuals as primaries, resulting in four events in the gradient. Among them, only one corresponds to the true reflector located at \SI{600}{\meter} depth. The three other events are cross-talk artefacts.
	\item the second and third contributions (\cref{fig:grad_decomp_b,fig:grad_decomp_c}) look very similar. They both interpret the four events in the residuals as multiples. The second term \(g_2\) images a multiple on the source side, while the third term \(g_3\) images a multiple on the receiver side. They can be understood as follows: among the two reflections defining the multiples, one is produced by the trial reflectivity \(\xi^t\) (\cref{fig:grad_decomp_exact_b}) and the gradient tries to position the second reflector which allows to reconstruct the events in the data.  In our example, the interpretation of the two primaries as multiples results in two other cross-talk artefacts. Finally the interpretation of multiples as multiples leads to two other events, but none of them is positioned at the depth of the reflector, because they are deduced from assumption that a first reflection occurred in \(\xi^t\) which is an incorrect model.
\end{itemize}
%---------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/grad_decomp_exact_model/grad_decomp_exact_model}
{\phantomsubcaption\label{fig:grad_decomp_exact_a}}
{\phantomsubcaption\label{fig:grad_decomp_exact_b}}
{\phantomsubcaption\label{fig:grad_decomp_exact_c}}
{\phantomsubcaption\label{fig:grad_decomp_exact_d}}
\caption{Model constructed to interpret the three parts of the gradients of \(J_0\) as defined in \cref{eq:grad_J0_decomp}. \subref{fig:grad_decomp_exact_a} Exact reflectivity model \(\xi^e\); \subref{fig:grad_decomp_exact_c} Corresponding observed data \(\Pobs\); \subref{fig:grad_decomp_exact_b} Trial reflectivity model \(\xi^t\); \subref{fig:grad_decomp_exact_d} Residuals \(\paren{P\croch{\xi^t}-\Pobs}\).  \(P^e\) and \(M^e\) are the primary and multiple corresponding to the true reflector, \(P^t\) and \(M^t\) the primary and multiple corresponding to the trial reflectivity.}
\label{fig:grad_decomp_exact_model}
\end{figure}
%---------------------------------
\begin{figure}[!htbp]
\includegraphics{Figures/grad_decomp_three_contribs/grad_decomp_three_contribs}
{\phantomsubcaption\label{fig:grad_decomp_a}}
{\phantomsubcaption\label{fig:grad_decomp_b}}
{\phantomsubcaption\label{fig:grad_decomp_c}}
\caption{Section at \(h=\SI{0}{\meter}\) of the three contributions of the gradient \pcref{eq:grad_J0_decomp} computed at the point \(\xi^t\) as defined in \cref{fig:grad_decomp_exact_model}. \subref{fig:grad_decomp_a} \(g_1=\Qxi{P_0}{\lambda_3}\) is the interpretation of the events in the residuals as primary reflections; \subref{fig:grad_decomp_b} \(g_2=\Qxi{P_0}{\lambda_4}\) and \subref{fig:grad_decomp_c} \(g_3=\Qxi{P_2}{\lambda_3}\) are the interpretation of the same events as multiple reflections.  The labels indicate the corresponding events in the data residual (\cref{fig:grad_decomp_exact_d}).}
\label{fig:grad_decomp_three_contribs}
\end{figure}
%---------------------------------

This example actually considers an extreme case, where the second and third part of the gradient do not bring any useful information and only mislead the reflectivity update. In practice, the inversion starts at \(\xi=0\), where only the first part of the gradient is non-zero. It results in two kinds of events: cross-talk artefacts and reflectors whose positions are kinematically consistent with the primaries in the data. The latter allow a correct interpretation of multiples at following iterations by the second and third term in the gradient.

%\subsection{Hessian matrix of \texorpdfstring{\(J_0\)}{J0}}\label{sec:Hess_J0}
%We perform a similar analysis on the Hessian. Again we consider only the contribution of the data misfit \(\Jres\) to the cost function, as the regularisation part is easy to compute. The expression of the Hessian can be expressed by deriving \cref{eq:def_gradJ0_frechet} with respect to \(\xi\).  It is made of four contributions,
%\begin{multline}\label{eq:Hessian_decomp_Frechet}
%	\pderiv[2]{\Jres}{\xi}=
%	\underbrace{\croch[\Bigg]{\pderiv[2]{P_1}{\xi}+\pderiv[2]{P_3}{\xi}}^T\paren{P_1+P_3-\Pobs}}_{H_A}\\
%	+\underbrace{\croch[\Bigg]{\pderiv{P_1}{\xi}}^T\pderiv{P_1}{\xi}}_{H_B}
%	+\underbrace{2\croch[\Bigg]{\pderiv{P_1}{\xi}}^T\pderiv{P_3}{\xi}}_{H_C}
%	+\underbrace{\croch[\Bigg]{\pderiv{P_3}{\xi}}^T\pderiv{P_3}{\xi}}_{H_D}\text{,}
%\end{multline}
%where the dependence to \(\xi\itn{n}\) and to the operator \(M\) have been omitted to simplify the expressions.
%
%Several second-order adjoint-states techniques exists to compute the Hessian matrix  \parencite{papadimitriou_direct_2008}. To solve the Newton equation~\eqref{eq:linear_system_newton}, the most efficient strategy is the truncated Newton method presented in \cref{sec:descent_direction}. It requires only a procedure to compute the product of the Hessian matrix with a vector, instead of computing the whole Hessian matrix. We will elaborate on this calculation in \cref{sec:Hess_vec_chap3}.
%
%We compute the Hessian matrix for a full 1D-example where only one source, one receiver and considered. There is also a single subsurface-offset, so that the Hessian matrix is of size \(\paren{n_z,n_z}\). This full 1D-case will be studied in detail in \cref{sec:article_Herve}. We compute the Hessian matrix first in \(\xi=0\) (\cref{fig:hessian_4_contrib}, top) and then in \(\xi\approx\xi\ap{exact}\) (\cref{fig:hessian_4_contrib}, bottom),
%\begin{itemize}
%	\item \(H_A\) is anti-diagonal and proportional to the data residuals. Note that \(\pderiv[2]{P_1}{\xi}\) is null and \(\pderiv[2]{P_1}{\xi}\) constant. This first contribution vanishes as far as the minimiser is approached.
%	\item \(H_B\) is constant and corresponds to the Hessian of the case of primaries only. It is mainly diagonal.
%	\item \(H_C\) represents the cross-talk between primaries and multiples. It is made of off-diagonal terms.
%	\item \(H_D\) is the equivalent of \(H_B\) for the multiples and is also diagonally dominant.
%\end{itemize}
%%-------------------------------------------------
%\begin{figure}[!htbp]
%\includegraphics{Figures/hessian_4_contrib/hessian_4_contrib}
%\caption{The complete Hessian matrix (left) and its four components as expressed in \cref{eq:Hessian_decomp_Frechet} computed at \(\xi=0\) (top) and \(\xi\approx\xi\ap{exact}\) (bottom).}
%\label{fig:hessian_4_contrib}
%\end{figure}
%%-------------------------------------------------
%
%Because of the first contribution \(H_A\), the Hessian matrix of \(J_0\) is not positive semi-definite. In the Gauss-Newton approximation, the Hessian matrix is approximated by \(H_B+H_C+H_D\). However, residuals decrease across iterations, and \(H_A\) becomes small with respect to the other contributions. %The quantity \(H_B+H_C+H_D\) is the Gauss-Newton approximation of the Hessian and is positive definite, then the resolution of the Newton equation~\eqref{eq:linear_system_newton} with the Gauss-Newton approximation of the Hessian is easier.

We have presented optimisation strategies for iterative migration and an efficient way to compute the gradient of the associated objective function. We now present applications on a simple synthetic example.
