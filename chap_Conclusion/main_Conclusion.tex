\chapter{Conclusions and Perspectives}\label{chap:concl}
\glsresetall
{\hypersetup{linkcolor=black}
\minitoc
}
%------------------------------------
\subimport{}{sec_Resume_francais_chap7}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusions}
In this thesis, I have investigated a \gls{mva} technique for the resolution of the seismic inverse problem under the constant-density acoustic wave-equation approximation. This technique is defined in the image domain and relies on the Born approximation and a separation of scales of the velocity model into a rapidly varying reflectivity model and a smooth macro-model. It aims at assessing the quality of an estimated background velocity model using the redundancy of seismic data. In the depth-oriented formulation, physical reflectivity images parametrised by spatial coordinates are extended with an additional variable \(h\) called subsurface-offset. After migration of seismic data to this extended domain, inaccuracies in the initial macro-model result in defocused energy at non-zero values of \(h\). Using the \gls{dso} strategy, the macro-model is iteratively corrected by minimising an objective function penalising defocused energy in \glspl{cig} until the extended reflectivity model becomes physical.

In this thesis, I have addressed the issue of spurious events appearing in \glspl{cig} which prevent the adoption of \gls{mva} techniques as standard seismic data processing tools. These artefacts are due to limited extension of acquisition geometries on one hand \parencite{lameloise_improving_2014,mulder_subsurface_2014}, and to multiple reflections misinterpreted as primary events on the other hand \parencite{mulder_automatic_2002,li_interval_2007}, and do not properly focus for the correct velocity model. As a consequence, the \gls{mva} objective function is not minimum for the correct velocity model and its gradient does not provide a consistent background velocity update.
Here I have investigated the use of inversion instead of migration to deal with both issues and obtain \glspl{cig} free of artefacts as well as consistent \gls{mva} gradients.


%-------------------------------------
\subsection{Inversion Velocity Analysis}
The original formulation of \gls{mva} assumes primary reflection data only, and defines a reflectivity image by application of the adjoint of the extended Born modelling operator to observed data.
In the context of high-frequency approximation of the wave-equation, \textcite{lameloise_improving_2014} introduced an extended quantitative migration compensating for uneven illumination and geometrical spreading. Combined with the horizontal contraction technique \parencite{fei_gradient_2010,shen_horizontal_2015}, this approach yields \glspl{cig} free of migration artefacts and smooth gradients. However ray theory is limited in the presence of complex geology, for which wave-equation based approaches are more appropriate.
In this study, an approximate inverse using only wave-equation operators has been presented (\cref{sec:article_Herve}), with a formulation similar to other proposals \parencite{hou_approximate_2015,hou_alternative_2017}. All these formulas are actually inverse operators in an asymptotic sense. Their derivation rely on ray theory and high-frequency approximations, but their final expressions are free of ray quantities. 
This strategy results in improved background velocity updates compared to standard migration. Moreover the introduction of a power of the background velocity in the annihilator has been shown to remove oscillations located around the reflector position from the \gls{mva} gradient (\cref{sec:article_Herve}) yielding results similar to the horizontal contraction approach \parencite{fei_gradient_2010,shen_horizontal_2015}. However the macro-model update derived with inversion is the gradient of an objective function, contrary to the case of horizontal contraction.
Eventually, note that the advantage of this strategy is the recovery of true-amplitude images and \glspl{cig} free of migration artefacts.
%the aim of inversion here is not to recover a true-amplitude image but to obtain \glspl{cig} free of migration artefacts.

As inversion formulas proposed so far are designed for primary reflections only, I have studied in this thesis iterative migration as a replacement for direct inversion.
The final formulation has been constructed progressively in \crefrange{chap:iter_migr}{chap:RMVA}. The analysis has been performed in the case of primaries only, using the result of direct inversion as a reference, and then applied to the case of multiples.
I also studied an equivalent 1D \gls{mva} problem in \cref{sec:article_Herve_1D,sec:1D_chap5} which may not be representative for all the features of 2D subsurface-oriented \gls{dso}, but is a useful analysis tool as it exhibits a behaviour very similar to the 2D case with a much lower computational cost. The main conclusions of each chapter are summarised in \cref{tab:concl}. I underline here the key aspects of the method.
%-------------------------------------
\begin{table}[!htbp]
\begin{tabular}{ccc}%{5.8cm}p{5.8cm}}
	\toprule
	 & main realisations and conclusions & limitations \\\midrule
	%%%%%%%%%%%%%%%%%%%
	%%%% Chapter 2 %%%%
	%%%%%%%%%%%%%%%%%%%
%	\multirow{1}{*}{
	\rotatebox[origin=c]{90}{\cref{chap:iter_migr}}&%}&
	\begin{minipage}[c]{0.48\textwidth}\begin{itemize}[noitemsep,leftmargin=*]
		\item an extended reflectivity model is defined by iterative migration to minimise the misfit between observed data and calculated data
		\item migration and cross-talk artefacts are greatly attenuated with iterations, improving the shape of the \gls{mva} cost function;
	\end{itemize}\end{minipage}&
	\begin{minipage}[c]{0.40\textwidth}\begin{itemize}[noitemsep,leftmargin=*]
		\item a strategy should be defined for the coupling with velocity analysis;
	\end{itemize}\end{minipage}\\\midrule
	%%%%%%%%%%%%%%%%%%%
	%%%% Chapter 3 %%%%
	%%%%%%%%%%%%%%%%%%%
	\rotatebox[origin=c]{90}{\cref{chap:IMVA}} &
	\begin{minipage}[c]{0.48\textwidth}\begin{itemize}[noitemsep,leftmargin=*]
		\item two methods for the computation of the \gls{mva} gradient after iterative migration are compared;
		\item the selected method assumes that iterative migration reaches convergence. An adjoint variable is determined as the solution of a linear problem and the gradient is computed from the last iterates of the direct and adjoint problem;
	\end{itemize}\end{minipage}&
	\begin{minipage}[c]{0.40\textwidth}\begin{itemize}[noitemsep,leftmargin=*]
		\item iterative MVA is computationally expensive as two iterative systems have to be resolved for the gradient computation;
		\item sufficiently strong regularisation is needed to obtain gradients free of spurious oscillations and stable across inner-iterations;
	\end{itemize}\end{minipage}\\\midrule
	%%%%%%%%%%%%%%%%%%%
	%%%% Chapter 4 %%%%
	%%%%%%%%%%%%%%%%%%%
	\rotatebox[origin=c]{90}{\cref{chap:WIMVA}} &
	\begin{minipage}[c]{0.48\textwidth}\begin{itemize}[noitemsep,leftmargin=*]
		\item an approximate inverse of the extended Born modelling operator is defined for primaries only, leading to a new IVA strategy;
		\item introducing this approximate inverse as a preconditioner greatly accelerates the convergence speed of the direct problem;
	\end{itemize}\end{minipage}&
	\begin{minipage}[c]{0.40\textwidth}\begin{itemize}[noitemsep,leftmargin=*]
		\item the adjoint problem converges much slower than the direct problem and the associated sequence of \gls{mva} gradients are not stable;
		\item the \gls{mva} gradients obtained after direct and iterative migration are quite different despite the similarity of the associated reflectivity images;
	\end{itemize}\end{minipage}\\\midrule
	%%%%%%%%%%%%%%%%%%%
	%%%% Chapter 5 %%%%
	%%%%%%%%%%%%%%%%%%%
	\rotatebox[origin=c]{90}{\cref{chap:RMVA}} &
	\begin{minipage}[c]{0.48\textwidth}\begin{itemize}[noitemsep,leftmargin=*]
		\item iterative \gls{mva} is analysed on a pure 1D case. It exhibits a behaviour similar to the 2D case;
		\item a filter \(\Fdag F\) is introduced in the definition of the \gls{mva} objective function to attenuate unwanted energy at large values of \(h\);
		\item with this modified \gls{mva} objective function, the adjoint problem converges much faster and the associated sequence of gradients is more stable with a shape similar to the gradient of direct inversion;
	\end{itemize}\end{minipage} &
	\begin{minipage}[c]{0.40\textwidth}\begin{itemize}[noitemsep,leftmargin=*]
		\item the robustness of the approach regarding non-Born data should be investigated (beyond the inverse crime);
	\end{itemize}\end{minipage}\\\midrule
	 %%%%%%%%%%%%%%%%%%%
	 %%%% Chapter 6 %%%%
	 %%%%%%%%%%%%%%%%%%%
	\rotatebox[origin=c]{90}{\cref{chap:applications}} &
	\begin{minipage}[c]{0.48\textwidth}\begin{itemize}[noitemsep,leftmargin=*]
		\item the direct and iterative inversion strategy are applied to a series of synthetic data computed with a modelling engine different from the one used during inversion;
		\item direct inversion in the primaries only case is robust to inaccuracies of the source wavelet and to amplitude errors;
		\item direct and iterative inversion strategies are robust with respect to density perturbations;
	\end{itemize}\end{minipage}&
	\begin{minipage}[c]{0.40\textwidth}\begin{itemize}[noitemsep,leftmargin=*]
		\item in the multiple case, iterative migration fails to explain both primaries and multiples correctly if the shape of the source wavelet is inaccurately estimated;
		\item application to real data.
	\end{itemize}\end{minipage}\\\bottomrule
	%%%%%%%%%%%%%%%%%%%
	%%%%%%%%%%%%%%%%%%%
\end{tabular}
\caption{Summary of the main conclusions of each chapter.}
\label{tab:concl}
\end{table}
%-------------------------------------

In iterative \gls{mva}, the extended reflectivity image is not obtained via an inverse formula but as the solution of an inverse problem. For a given background velocity model, the model perturbation best explaining observed data in a least-squares sense is determined through an optimisation procedure. If calculated data are computed under the first-order Born approximation (primaries only), the associated inverse problem is linear and can be solved with a standard conjugate gradient algorithm. I have shown that this technique efficiently attenuates migration artefacts in \glspl{cig} and yields results similar to direct inversion in the primary-only case. Compared to the result of direct inversion, we obtain after several iterations of migration a reflectivity image with a better deconvolution of the source wavelet, but this has little impact on the final data misfit.

The derivation of the gradient of the outer objective function has been analysed in \cref{chap:IMVA}. An approximate value of this gradient is computed using the final result of two iterative problems: iterative migration on one hand, and a linear adjoint problem, similar to the direct problem but with a different source term on the other hand. One would expect the gradient to converge to a stable value at the same pace as iterative migration. Besides, as direct and iterative inversion provide similar reflectivity images, we may presume that the associated background velocity updates would be similar.
However, numerical examples in \cref{chap:IMVA,chap:WIMVA,chap:RMVA} have shown that this is not the case in practice. The value of the reflectivity for large \(h\) has been shown to have little impact on data residuals but a major influence on the velocity analysis, after being amplified by the annihilator. Similar observations have been made by \textcite{huang_born_2016}.
This issue has been identified by comparing the value of both objective function \(J_0\) and \(J_1\) across inner iterations. The first one reaches convergence after a few iterations, while the second does not stabilise because of small modification at large values of \(h\) (\cref{fig:ObjFctWithoutReg}).
Another practical difficulty is the slow convergence speed of the adjoint problem. As a consequence, the value of the gradient obtained with successive values of the adjoint variable is not stable. This is an undesirable behaviour as we would like the gradient to converge to a stable value after a given number of iterations performed for the resolution of both problems.
I have shown that sufficient regularisation on the reflectivity model helps mitigating these issues, but this solution is not fully satisfactory. In particular, the determination of regularisation parameters remains a tedious task and we still observe residual oscillations around reflector positions not present in the gradient obtained by direct inversion. For a more stable procedure, I have proposed to modify the usual \gls{mva} objective function by applying a \enquote{filter} depending on the background velocity model to \glspl{cig} before measuring defocused energy. This filter does not change the shape of defocused events in \glspl{cig} but attenuates spurious oscillations responsible for the instabilities described above. I have shown that this filter changes the source term of the adjoint problem which is now easier to solve. The value of the gradient is stable with iterations and similar to the one obtained by direct inversion. The additional cost represented by the application of the filter is largely compensated by the improved convergence of the adjoint problem, allowing to reduce the number of iterations.

%-------------------------------------

\subsection{Multiple reflections}
I have studied the extension of this iterative procedure to the case of first-order surface-related multiples. It consists of using a second-order Born approximation for forward modelling in the inner inverse problem. This changes the gradient formula in both the inner and outer minimisation problem. Iterative migration is not a linear inverse problem any more. Regarding the computation of \gls{mva} gradient, the adjoint problem is still linear but may not be positive-definite if final data residuals are not small enough.

I have shown that cross-talk artefacts due to multiples are attenuated across iterations, leading to \gls{mva} gradients consistent with the primary-only case. However the computation of the migration objective function and of its derivatives is much more expensive compared to the linear case (\cref{tab:cost_hess}). Moreover iterative migration becomes a non-linear optimisation procedure. To reduce the computational expense of the method, we have proposed to use the approximate inverse as a preconditioner, which greatly improves the convergence of the direct and adjoint problem, even in the case of multiples. Finally we have proposed an alternative strategy where iterative migration is used to retrieve an extended reflectivity image free of cross-talk artefacts, allowing to re-compute primary reflections and to apply the direct inversion procedure which has been shown to be robust and computationally efficient. Although the background velocity update defined by this strategy is not the gradient of an objective function, the procedure is lees computationally expensive as the iterative resolution of the adjoint problem is not necessary. Furthermore, we can reconstruct data for source positions missing in the original acquisition. Hence eventual additional information in the reflectivity image due to extra-information contained in multiples may be incorporated in the re-computation of primaries.

In this study, multiples have been introduced with the objective of retrieving gradients similar to those obtained with multiple-free data. The models and acquisition settings used in the examples were not specifically designed to use the additional information contained in multiples. We have shown that iterative migration succeeds in removing the imprint of multiples on the reflectivity, leading to gradients similar to those obtained in the case of primaries only. However, the example of \cref{sec:hole} shows that extracting additional information from multiples is a much more complex task, especially when considering only first-order surface-related multiples. The areas investigated by this kind of multiples is actually small and located on the edges of the acquisition where edge effects alter \gls{mva} gradient obtained in the primaries only case. Extending \gls{mva} techniques to all order of multiples and internal multiples may provide more favourable examples with wider illumination of the subsurface (see next section).

 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Perspectives}
%-------------------------------------
\subsection{Using all orders of multiples}
Extending \gls{mva} to all orders of multiples could be performed with the approach investigated by \textcite{diaz_extended_2016-1}, based on the resolution of the \emph{Marchenko} equation. Using observed data and Green's function \(G_0\paren{s,\vec{x},\omega}\) computed in an estimated macro-model \(c_0\vecx\), the iterative resolution of the Marchenko equations allows to retrieve the complete Green's functions \(G\paren{s,\vec{x},\omega}\) including all multiple reflections. This step is discussed in \textcite{wapenaar_marchenko_2014} for the case of internal multiples and extended to surface multiples by \textcite{singh_marchenko_2015}. The accuracy of the macro-model can then be evaluated in two different ways. First, one possibility is to decompose the complete Green's function into a downgoing \(G^-\) and an upgoing wavefield \(G^+\) to construct extended \glspl{cig} with a deconvolution imaging condition and measure defocused energy in a standard way \parencite{diaz_extended_2016}. An alternative consists of directly estimating the velocity model as \(G\) is reconstructed everywhere within the subsurface and is solution of the wave-equation
\begin{equation}
	\frac{\iomsqr}{c^2\vecx}G\sxom-\Lapl G\sxom=\delta\paren{x-s}\text{,}
\end{equation}
the value of the velocity model \(c\vecx\) can be recovered from the knowledge of the complete Green's function following
\begin{equation}
	c^2\vecx=\frac{\pscal[\big]{\iomsqr G}{G}}{\pscal[\big]{\Lapl G}{G}}.
\end{equation}
This model can be decomposed into \(c\vecx=c_0\vecx+\delta c\vecx\). For the determination of \(G\sxom\), one needs to provide a smooth background model. If the kinematics is not correct, a standard \gls{mva} procedure can be defined using the values of \(\Pobs\) and \(c_0\vecx\).
Both the strategy of \textcite{diaz_extended_2016-1} and our approach rely on an iterative procedure depending on the value of the macro-model \(c_0\vecx\). In \textcite{diaz_extended_2016-1} the purpose is to retrieve the complete Green's function \(G\), whereas here the unknown of the iterative process is an extended reflectivity model best explaining observed data. In both cases, the difficulty is to compute the velocity update due to the dependence of the iterative process to the macro-model \(c_0\vecx\). Here we derive a strategy using the adjoint-state method and the assumption that the output of the iterative migration is defined as the minimiser of a cost function. Further investigations are required to define a similar strategy in the case of \textcite{diaz_extended_2016-1}. It is possible that a regularisation term (equivalent of the filter \(K\)) has to be introduced with Marchenko-based \gls{mva} process. Note that the major advantage of the Marchenko approach is that both internal and surface multiples are consistently integrated in the imaging procedure, yielding \glspl{cig} free of cross-talk artefacts \parencite{diaz_extended_2016-1}.
%-------------------------------------
\subsection{Introducing more physics in MVA techniques}
In \cref{sec:article_Herve}, an inversion formula allowing to retrieve an extended velocity perturbation explaining observed data has been presented. This formula was tested in \cref{sec:app_drho} on reflection data originating from a density perturbation and modelled under the Born approximation.
Alternatively, one could also derive an inversion formula for density perturbation data, using a strategy similar to the one used in \cref{sec:article_Herve}. The difference is that here, gradients of Green's function are involved instead of a their product with a second-order time derivative, which requires the derivation of new weighting operators different from the original inverse formula.
This may enable an other inversion strategy for multi-component observed data. Using the surface recording of both horizontal and vertical displacements, one could try to invert for a velocity or a density perturbation, or for a velocity and impedance perturbation, as these two parameters exhibit less coupling \parencite{zhou_full_2015}.

In this study we have used an explicit scale separation between a smooth background velocity model and a model perturbation. Following a strategy proposed by \parencite{zhou_velocity_2016} in the framework of \gls{fwi}, one could also investigate a more natural scale separation, using velocity and variable density to parametrise the model, the velocity model controlling the kinematics of wave propagation and density accounting for the reflective property of the subsurface instead of the velocity model perturbation. 

In the constant-density acoustic approximation, \textcite{lameloise_extension_2016} show how transmitted waves can be included in \gls{mva} techniques by constructing extended images in a very similar manner to the usual procedure for reflections. They propose a strategy in which these two kind of events are used successively. Transmitted waves are used in a first step to update the shallow part of the velocity model; then primary reflections are inverted to reach the deeper part of the model. In our approach, primaries and multiples are naturally inverted together. Inverting reflection and transmission data simultaneously could potentially better constrain the inversion.  As pointed out by \textcite{lameloise_analyse_2015}, an issue is that transmitted events are more energetic than reflections, requiring to introduce weights to balance the amplitudes of both kinds of events. Moreover, \glspl{cig} constructed with transmitted events do not have the same physical interpretation as \glspl{cig} built with reflection data; in particular they are not linked to a specific interface. Hence, the inclusion of reflection and transmitted data in a unified framework needs further investigation.
%-------------------------------------
%\subsection{Data-domain DSO}
%\parencite{gao_new_2014}
%\begin{align}
%	J_{1}\ap{model-domain}&=\frac{1}{2}\norm{A\Fdag\Pobs}_\Esp^2\\
%	J_{1}\ap{data-domain}&=\frac{1}{2}\norm{F A\Fdag\Pobs}_\Esp^2
%\end{align}
%-------------------------------------
\subsection{Importance of the Hessian for iterations over the velocity model}
A more thorough study should be led on the choice of optimisation strategies for the external loop aiming at determining the background velocity model.
In particular the effect of the Hessian of the objective function should be better taken into account. The gradients obtained by direct inversion in the case of primaries (\cref{sec:article_Herve}) are remarkably smooth, but their shape, and in particular the importance of side effect vary greatly with the number of surface and subsurface-offsets, and the reflectors' depth. We may expect a proper introduction of the Hessian to attenuate these variations. Most applications on synthetic and real data \enquote{only} use the l-BFGS strategy to take second-order effects in to account.
An interesting strategy is proposed by \textcite{liu_inversion_2014,shen_horizontal_2015} who compute an estimate of the diagonal of the Hessian matrix as the result of its application to a unit vector. An extension is the Truncated Newton strategy, already studied in the framework of FWI \parencite{metivier_full_2013,metivier_full_2014}. Contrary to the l-BFGS algorithm, these techniques require an efficient way of computing the product of the Hessian of the objective function with a vector of the \(\Msp\) space. One could use a second-order adjoint-state technique similar to the one presented in \textcite{metivier_full_2013,metivier_full_2014} and in \cref{sec:Hess_vec_chap3}. An example of derivation of the matrix-vector product is presented in \cref{app:HessVec_Vel} in the case of direct inversion. Note however that this approach is quite expensive. Moreover the positive-definitiveness of the Hessian matrix should be investigated.
%-------------------------------------
\subsection{Inversion strategy}
We have considered here a nested optimisation procedure with two separate objective functions, the inner inverse problem solving for the reflectivity for a given macro-model, and the outer objective function solving for the macro-model. Another possibility is to use a single objective function \parencite{fleury_bi-objective_2012,huang_born_2015},
\begin{equation}\label{eq:VPM}
	J_2\croch{c_0,\xi}=\frac{1}{2}\norm[\big]{F\croch{c_0}\xi-\Pobs}^2_\Dobs+\frac{\lambda}{2}\norm[\big]{A\xi}_\Esp^2\text{,}
\end{equation}
where the first term is related to data misfit and the second measures defocused energy in \glspl{cig}.
Note that the \gls{dso} strategy was originally introduced as a regularisation for the \gls{fwi} objective function \parencite{symes_inversion_1994}.

An efficient procedure for the minimisation of this objective function is the \gls{vpm} \parencite{van_leeuwen_variable_2009,rickett_variable_2013,huang_born_2015}. It consists of inverting first for the reflectivity \(\xi\) (following \(\xi=\Fdag\croch{c_0}\Pobs\) in the direct inversion case) and to replace in the definition of the objective function. The first step is similar to what have been proposed in this study, the second is different because we considered in this thesis only the second term in \cref{eq:VPM} for the derivation of a velocity model. %\textcite{rickett_variable_2013,huang_born_2015} suggest the VPM strategy is more efficient.

A difficulty remains the choice of the weight \(\lambda\). Furthermore, in the case of iterative migration with multiples, the expression replacing \(\xi\) in \cref{eq:VPM} should properly take cross-talk effects into account, otherwise the second term of the objective function may mislead the background velocity update. 
%-------------------------------------
\subsection{Extension to 3D}
In 3D, the observed data space has five dimensions \(\paren{s_x,s_y,r_x,r_y,t}\), requiring two extension parameters for the model space, for example horizontal subsurface offsets \(h_x\) and \(h_y\) (\cref{tab:dimension}). A new inversion formula taking into account these new dimensions should be defined with a strategy similar to the 2D case (\cref{sec:article_Herve}). The major issue of the extension is its computational cost, which disqualifies the iterative determination of an extended reflectivity image with a large number of iterations. Preconditioning as proposed in this thesis or in \textcite{hou_accelerating_2016} is an essential element for the extension to 3D.
In addition to 3D wavefield propagation, a cross-correlation should be performed for each couple \(\paren{h_x,h_y}\).  New strategies such as those mentioned in \cref{sec:mva_limitations} should be further investigated for a more affordable extension of \gls{mva} to 3D. Another difficulty is that 3D acquisition do not provide dense source and receiver coverage in all directions, which may have undesirable effects on reflectivity image and associated gradients.
%-------------------------------------
\subsection{Application to real data}
As a prelude to real data applications, we have applied in \cref{chap:applications} the proposed \gls{mva} strategy to observed data computed with a forward modelling code different from the one used during inversion. We showed that an incorrect estimation of the source wavelet was an issue for the iterative case in the presence of multiples, but that direct inversion was insensitive to this kind of imperfection. More importantly, ghosts effects due to the free-surface were shown to be an issue for iterative migration as they modify the amplitude and phase of reflection data, especially at wide angles. Ghost should be removed from observed data or properly included in the forward modelling to cope with this issue.

More generally, propagation effects that are not accounted for in the forward propagation step may lead to inconsistent \glspl{cig} and velocity updates.
For example the impact of attenuation and elastic effects on the amplitude may be an issue for regular \gls{mva} strategies. Converted waves are also not predicted under the Born approximation.

Finally the irregularity of the acquisition geometry should be taken care of. Direct inversion formulas are indeed derived with the assumption of dense source and receiver coverage. Iterative migration may be an interesting alternative in this case as it deals more effectively with irregular acquisition \parencite{nemeth_leastsquares_1999}.

